﻿# TODO: Translation updated at 2022-06-01 12:28

translate tokipona strings:

    # game/screens.rpy:253
    old "Back"
    new ""

    # game/screens.rpy:254
    old "History"
    new ""

    # game/screens.rpy:255
    old "Skip"
    new ""

    # game/screens.rpy:256
    old "Auto"
    new ""

    # game/screens.rpy:257
    old "Save"
    new ""

    # game/screens.rpy:258
    old "Q.save"
    new ""

    # game/screens.rpy:259
    old "Q.load"
    new ""

    # game/screens.rpy:260
    old "Settings"
    new ""

    # game/screens.rpy:302
    old "New game"
    new ""

    # game/screens.rpy:311
    old "Update Available!"
    new ""

    # game/screens.rpy:322
    old "Load"
    new ""

    # game/screens.rpy:325
    old "Bonus"
    new ""

    # game/screens.rpy:331
    old "End Replay"
    new ""

    # game/screens.rpy:335
    old "Main menu"
    new ""

    # game/screens.rpy:337
    old "About"
    new ""

    # game/screens.rpy:344
    old "Please donate!"
    new ""

    # game/screens.rpy:347
    old "Help"
    new ""

    # game/screens.rpy:353
    old "Quit"
    new ""

    # game/screens.rpy:592
    old "Version [config.version!t]\n"
    new ""

    # game/screens.rpy:598
    old "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"
    new ""

    # game/screens.rpy:638
    old "Page {}"
    new ""

    # game/screens.rpy:638
    old "Automatic saves"
    new ""

    # game/screens.rpy:638
    old "Quick saves"
    new ""

    # game/screens.rpy:680
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new ""

    # game/screens.rpy:680
    old "empty slot"
    new ""

    # game/screens.rpy:697
    old "<"
    new ""

    # game/screens.rpy:700
    old "{#auto_page}A"
    new ""

    # game/screens.rpy:703
    old "{#quick_page}Q"
    new ""

    # game/screens.rpy:709
    old ">"
    new ""

    # game/screens.rpy:771
    old "Display"
    new ""

    # game/screens.rpy:772
    old "Windowed"
    new ""

    # game/screens.rpy:773
    old "Fullscreen"
    new ""

    # game/screens.rpy:777
    old "Rollback Side"
    new ""

    # game/screens.rpy:778
    old "Disable"
    new ""

    # game/screens.rpy:779
    old "Left"
    new ""

    # game/screens.rpy:780
    old "Right"
    new ""

    # game/screens.rpy:785
    old "Unseen Text"
    new ""

    # game/screens.rpy:786
    old "After choices"
    new ""

    # game/screens.rpy:787
    old "Transitions"
    new ""

    # game/screens.rpy:797
    old "Language"
    new ""

    # game/screens.rpy:829
    old "Text speed"
    new ""

    # game/screens.rpy:833
    old "Auto forward"
    new ""

    # game/screens.rpy:840
    old "Music volume"
    new ""

    # game/screens.rpy:847
    old "Sound volume"
    new ""

    # game/screens.rpy:853
    old "Test"
    new ""

    # game/screens.rpy:857
    old "Voice volume"
    new ""

    # game/screens.rpy:868
    old "Mute All"
    new ""

    # game/screens.rpy:987
    old "The dialogue history is empty."
    new ""

    # game/screens.rpy:1052
    old "Keyboard"
    new ""

    # game/screens.rpy:1053
    old "Mouse"
    new ""

    # game/screens.rpy:1056
    old "Gamepad"
    new ""

    # game/screens.rpy:1069
    old "Enter"
    new ""

    # game/screens.rpy:1070
    old "Advances dialogue and activates the interface."
    new ""

    # game/screens.rpy:1073
    old "Space"
    new ""

    # game/screens.rpy:1074
    old "Advances dialogue without selecting choices."
    new ""

    # game/screens.rpy:1077
    old "Arrow Keys"
    new ""

    # game/screens.rpy:1078
    old "Navigate the interface."
    new ""

    # game/screens.rpy:1081
    old "Escape"
    new ""

    # game/screens.rpy:1082
    old "Accesses the game menu."
    new ""

    # game/screens.rpy:1085
    old "Ctrl"
    new ""

    # game/screens.rpy:1086
    old "Skips dialogue while held down."
    new ""

    # game/screens.rpy:1089
    old "Tab"
    new ""

    # game/screens.rpy:1090
    old "Toggles dialogue skipping."
    new ""

    # game/screens.rpy:1093
    old "Page Up"
    new ""

    # game/screens.rpy:1094
    old "Rolls back to earlier dialogue."
    new ""

    # game/screens.rpy:1097
    old "Page Down"
    new ""

    # game/screens.rpy:1098
    old "Rolls forward to later dialogue."
    new ""

    # game/screens.rpy:1102
    old "Hides the user interface."
    new ""

    # game/screens.rpy:1106
    old "Takes a screenshot."
    new ""

    # game/screens.rpy:1110
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new ""

    # game/screens.rpy:1116
    old "Left Click"
    new ""

    # game/screens.rpy:1120
    old "Middle Click"
    new ""

    # game/screens.rpy:1124
    old "Right Click"
    new ""

    # game/screens.rpy:1128
    old "Mouse Wheel Up\nClick Rollback Side"
    new ""

    # game/screens.rpy:1132
    old "Mouse Wheel Down"
    new ""

    # game/screens.rpy:1139
    old "Right Trigger\nA/Bottom Button"
    new ""

    # game/screens.rpy:1143
    old "Left Trigger\nLeft Shoulder"
    new ""

    # game/screens.rpy:1147
    old "Right Shoulder"
    new ""

    # game/screens.rpy:1151
    old "D-Pad, Sticks"
    new ""

    # game/screens.rpy:1155
    old "Start, Guide"
    new ""

    # game/screens.rpy:1159
    old "Y/Top Button"
    new ""

    # game/screens.rpy:1162
    old "Calibrate"
    new ""

    # game/screens.rpy:1227
    old "Yes"
    new ""

    # game/screens.rpy:1228
    old "No"
    new ""

    # game/screens.rpy:1274
    old "Skipping"
    new ""

    # game/screens.rpy:1495
    old "Menu"
    new ""

    # game/screens.rpy:1549
    old "Musics and pictures"
    new ""

    # game/screens.rpy:1552
    old "Picture gallery"
    new ""

    # game/screens.rpy:1556
    old "Music player"
    new ""

    # game/screens.rpy:1558
    old "Opening song lyrics"
    new ""

    # game/screens.rpy:1561
    old "Achievements"
    new ""

    # game/screens.rpy:1567
    old "Characters profiles"
    new ""

    # game/screens.rpy:1588
    old "Bonus chapters"
    new ""

    # game/screens.rpy:1591
    old "1 - A casual day at the club"
    new ""

    # game/screens.rpy:1596
    old "2 - Questioning sexuality (Sakura's route)"
    new ""

    # game/screens.rpy:1601
    old "3 - Headline news"
    new ""

    # game/screens.rpy:1606
    old "4a - A picnic at the summer (Sakura's route)"
    new ""

    # game/screens.rpy:1611
    old "4b - A picnic at the summer (Rika's route)"
    new ""

    # game/screens.rpy:1616
    old "4c - A picnic at the summer (Nanami's route)"
    new ""

    # game/screens.rpy:1633
    old "Max le Fou - Taichi's Theme"
    new ""

    # game/screens.rpy:1635
    old "Max le Fou - Sakura's Waltz"
    new ""

    # game/screens.rpy:1637
    old "Max le Fou - Rika's theme"
    new ""

    # game/screens.rpy:1639
    old "Max le Fou - Of Bytes and Sanshin"
    new ""

    # game/screens.rpy:1641
    old "Max le Fou - Time for School"
    new ""

    # game/screens.rpy:1643
    old "Max le Fou - Sakura's secret"
    new ""

    # game/screens.rpy:1645
    old "Max le Fou - I think I'm in love"
    new ""

    # game/screens.rpy:1647
    old "Max le Fou - Darkness of the Village"
    new ""

    # game/screens.rpy:1649
    old "Max le Fou - Love always win"
    new ""

    # game/screens.rpy:1651
    old "Max le Fou - Ondo"
    new ""

    # game/screens.rpy:1653
    old "Max le Fou - Happily Ever After"
    new ""

    # game/screens.rpy:1655
    old "J.S. Bach - Air Orchestral suite #3"
    new ""

    # game/screens.rpy:1657
    old "Mew Nekohime - TAKE MY HEART (TV Size)"
    new ""

