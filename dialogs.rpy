﻿# TODO: Translation updated at 2022-06-01 12:28

# game/dialogs.rpy:3
translate tokipona scene1_acf2102c:

    # centered "{size=+12}Summer of the 9th year of Heisei era\n(1997){/size}"
    centered ""

# game/dialogs.rpy:6
translate tokipona scene1_dd8c4dd5:

    # "Life has a way of changing. Often drastically without any warnings whatsoever."
    ""

# game/dialogs.rpy:7
translate tokipona scene1_0497b746:

    # "At first, a day may seem like any other."
    ""

# game/dialogs.rpy:8
translate tokipona scene1_1ab3b689:

    # "But by tomorrow, it can all change...{p}from where you live, to who you are as a person."
    ""

# game/dialogs.rpy:9
translate tokipona scene1_522a76b6:

    # "Recently, I've experienced such a large change."
    ""

# game/dialogs.rpy:10
translate tokipona scene1_011ea937:

    # "My dad wanted to start his own grocery shop. So my parents and I moved to a small village near Osaka."
    ""

# game/dialogs.rpy:11
translate tokipona scene1_2b371705:

    # "I thought I would have a hard time readjusting."
    ""

# game/dialogs.rpy:12
translate tokipona scene1_a26a5f2b:

    # "The move from Tokyo to a small lost village, far from a big city with shops and fast-food restaurants..."
    ""

# game/dialogs.rpy:13
translate tokipona scene1_fa8a1f18:

    # "Being a 18-year-old student, it would be difficult. I was used to the excitement of the big city."
    ""

# game/dialogs.rpy:14
translate tokipona scene1_8d6f1c40:

    # "However, even before my first day of school, my life would change."
    ""

# game/dialogs.rpy:19
translate tokipona scene1_d7a7c4a2:

    # centered "{size=+35}CHAPTER 1\nWhere everything started{fast}{/size}"
    centered ""

# game/dialogs.rpy:20
translate tokipona scene1_3e4e196f:

    # alt "CHAPTER 1\nWhere everything started"
    alt ""

# game/dialogs.rpy:27
translate tokipona scene1_5bb0286e:

    # "It's sunday evening, after dinner."
    ""

# game/dialogs.rpy:28
translate tokipona scene1_a651e12e:

    # "My stuff is all unpacked and placed in my new room."
    ""

# game/dialogs.rpy:29
translate tokipona scene1_54a8b155:

    # "I love this new look!"
    ""

# game/dialogs.rpy:30
translate tokipona scene1_8bb96761:

    # "It sure is smaller than my previous bedroom, but it's not like I needed that much space."
    ""

# game/dialogs.rpy:31
translate tokipona scene1_acf53d85:

    # "As long as I have room to sleep, play games, watch anime, and geek out, it's all good."
    ""

# game/dialogs.rpy:32
translate tokipona scene1_7d722416:

    # "I'm not sure what I should do before going to bed."
    ""

# game/dialogs.rpy:33
translate tokipona scene1_b2b74adf:

    # "I could study a bit, so I don't look too dumb at school tomorrow..."
    ""

# game/dialogs.rpy:34
translate tokipona scene1_ad0d9f75:

    # "But I also want to rest and have a bit of fun..."
    ""

# game/dialogs.rpy:58
translate tokipona scene1_e05b5690:

    # "I should play a short game, or I'll stay up too long. So I decide to play some {i}Tetranet online{/i}."
    ""

# game/dialogs.rpy:59
translate tokipona scene1_7170a1ff:

    # "It's a puzzle game with blocks. The online version lets you attack your opponents and flood them with their own blocks."
    ""

# game/dialogs.rpy:60
translate tokipona scene1_8dd01706:

    # "After a few minutes, I found myself in a game room. One of the players named 'NAMINAMI' had two stars in front of their name."
    ""

# game/dialogs.rpy:61
translate tokipona scene1_962ec706:

    # "Two stars meant that they had won hundreds of games online. Since three stars are for the game developers, I guess that means I'm up against a pro of this game."
    ""

# game/dialogs.rpy:62
translate tokipona scene1_a71313b4:

    # "Judging by the low ping, I guess they live around the same prefecture I live in. Small world..."
    ""

# game/dialogs.rpy:63
translate tokipona scene1_e0301d9f:

    # "In the dozens of games we played, NAMINAMI continued to beat everyone in the game room. But I was always the last one to fall."
    ""

# game/dialogs.rpy:65
translate tokipona scene1_a40571de:

    # write "NAMINAMI> GG dude, u pretty much rock at this game!{fast}"
    write ""

# game/dialogs.rpy:67
translate tokipona scene1_7c6f0607:

    # write "%(stringhero)s>{fast} Nah i'm not that good"
    write ""

# game/dialogs.rpy:69
translate tokipona scene1_0d6dddc9:

    # write "NAMINAMI> yes u r. i had trouble winning against u.{fast}"
    write ""

# game/dialogs.rpy:71
translate tokipona scene1_87ab3c60:

    # write "NAMINAMI> care 4 a rematch? just u and me?{fast}"
    write ""

# game/dialogs.rpy:73
translate tokipona scene1_828e7297:

    # write "%(stringhero)s>{fast} Maybe next time. It's getting late, i have school tomorrow."
    write ""

# game/dialogs.rpy:75
translate tokipona scene1_0d8de120:

    # write "NAMINAMI> ur rite, me 2 anyway. hope 2 c u again soon!{fast}"
    write ""

# game/dialogs.rpy:76
translate tokipona scene1_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:83
translate tokipona scene1_ee02f0bc:

    # "I decide to watch some old episodes of the anime based on my favorite manga, {i}High School Samurai{/i}."
    ""

# game/dialogs.rpy:84
translate tokipona scene1_d91b4fcd:

    # "It's the story about a guy who lived in the mountains, learning the ways of Bushido. Then, all of a sudden, he must go to a new town to learn about life in the city."
    ""

# game/dialogs.rpy:85
translate tokipona scene1_e72d0922:

    # "He resolves a lot of his problems with his samurai skills and his heroic determination."
    ""

# game/dialogs.rpy:90
translate tokipona scene1_a19cfb71:

    # hero "I decide to study. Keep up the great work, %(stringhero)s!"
    hero ""

# game/dialogs.rpy:92
translate tokipona scene1_ad5fbe82:

    # "I studied until very late at night."
    ""

# game/dialogs.rpy:94
translate tokipona scene1_2f455d53:

    # "I don't know how long I stayed up, but I was so tired, I ended up falling asleep..."
    ""

# game/dialogs.rpy:99
translate tokipona scene1_cb77d3d7:

    # "*{i}Riiiiiiiiiiiing{/i}*"
    ""

# game/dialogs.rpy:101
translate tokipona scene1_4037429a:

    # "I switched off the alarm clock and went back to sleep again."
    ""

# game/dialogs.rpy:102
translate tokipona scene1_548721a3:

    # hero "I'm so tired..."
    hero ""

# game/dialogs.rpy:103
translate tokipona scene1_2a13d6dc:

    # "Mom" "%(stringhero)s!! Wake up!!! You'll be late for school!!!"
    "Mom" ""

# game/dialogs.rpy:104
translate tokipona scene1_c83937b9:

    # hero "Huh... What time is it..."
    hero ""

# game/dialogs.rpy:105
translate tokipona scene1_fc1e319f:

    # "Wait... 8:02AM?!{p} School starts at 8:30AM!!!"
    ""

# game/dialogs.rpy:107
translate tokipona scene1_4217b105:

    # hero "Dangit!"
    hero ""

# game/dialogs.rpy:109
translate tokipona scene1_62c6f195:

    # hero "I stayed up and played too much! I'm late!"
    hero ""

# game/dialogs.rpy:111
translate tokipona scene1_b8605e00:

    # hero "I watched too much anime! I'm late!"
    hero ""

# game/dialogs.rpy:113
translate tokipona scene1_a476aee9:

    # hero "I studied too much! I'm late!"
    hero ""

# game/dialogs.rpy:116
translate tokipona scene1_0ff90bcf:

    # "I jumped out of bed, put on my brand new school uniform, and ran down the stairs."
    ""

# game/dialogs.rpy:118
translate tokipona scene1_5c1508de:

    # "I ate my breakfast faster than usual."
    ""

# game/dialogs.rpy:119
translate tokipona scene1_6a44f1a3:

    # "I took a map with a route to school and left after saying goodbye to my parents."
    ""

# game/dialogs.rpy:122
translate tokipona scene1_ad63def5:

    # "I followed the map and dashed through the streets."
    ""

# game/dialogs.rpy:123
translate tokipona scene1_00e59ce7:

    # "This town looks pretty unique."
    ""

# game/dialogs.rpy:124
translate tokipona scene1_689e5094:

    # "It's a strange town, like a mix between a suburb and a rural village."
    ""

# game/dialogs.rpy:125
translate tokipona scene1_a64e7e8b:

    # "Summer had just arrived. The cicadas were crying. And it sure was hot out here..."
    ""

# game/dialogs.rpy:126
translate tokipona scene1_71061539:

    # "I noticed only a few people on the streets."
    ""

# game/dialogs.rpy:127
translate tokipona scene1_1d275b47:

    # "Some of them were kind enough to help me find my way to the school..."
    ""

# game/dialogs.rpy:128
translate tokipona scene1_e123d412:

    # "Then, as I was nearing an intersection..."
    ""

# game/dialogs.rpy:129
translate tokipona scene1_da3e8686:

    # "I see a girl in a school uniform."
    ""

# game/dialogs.rpy:131
translate tokipona scene1_72d48eb1:

    # hero "Well, since village only has one school...that must be where she's heading!"
    hero ""

# game/dialogs.rpy:132
translate tokipona scene1_018ee7a3:

    # hero "And if that is where she's going, maybe she could help me out!"
    hero ""

# game/dialogs.rpy:133
translate tokipona scene1_a95f40e0:

    # "I approached her carefully, as I didn't want to scare her."
    ""

# game/dialogs.rpy:134
translate tokipona scene1_b0e8ce5c:

    # "She looked beautiful; long black hair, thin body, and beautiful legs..."
    ""

# game/dialogs.rpy:135
translate tokipona scene1_d501d47f:

    # "I hesitated for a moment before saying anything..."
    ""

# game/dialogs.rpy:136
translate tokipona scene1_9195ee62:

    # hero "Ahem... Excuse me, miss?"
    hero ""

# game/dialogs.rpy:141
translate tokipona scene1_951b2409:

    # "She turned around to face me."
    ""

# game/dialogs.rpy:142
translate tokipona scene1_b7723325:

    # "Oh wow! She was insanely pretty!"
    ""

# game/dialogs.rpy:143
translate tokipona scene1_64e26830:

    # "Her eyes were blue.{p}A wonderful, deep, ocean blue. They were like the clear waters you'd find in Hawaii!"
    ""

# game/dialogs.rpy:144
translate tokipona scene1_a28c7063:

    # "She looked at me, confused, and a bit timid."
    ""

# game/dialogs.rpy:146
translate tokipona scene1_40d86de1:

    # s "Yes?"
    s ""

# game/dialogs.rpy:147
translate tokipona scene1_013ed851:

    # "Wow! Her voice sounds so adorable!"
    ""

# game/dialogs.rpy:148
translate tokipona scene1_ed63afd9:

    # "She is so cute, that I was at a loss for words."
    ""

# game/dialogs.rpy:149
translate tokipona scene1_d015b36e:

    # hero "Uhhh... I'm new in the village, and I'm trying to find my school."
    hero ""

# game/dialogs.rpy:150
translate tokipona scene1_759f29b6:

    # hero "And since you're wearing a uniform... I thought you could..."
    hero ""

# game/dialogs.rpy:151
translate tokipona scene1_9c182eb6:

    # "The girl stared at me with her big, majestic, blue eyes..."
    ""

# game/dialogs.rpy:152
translate tokipona scene1_e85ec319:

    # "Finally, she smiled."
    ""

# game/dialogs.rpy:154
translate tokipona scene1_208138e6:

    # s "Of course I can!"
    s ""

# game/dialogs.rpy:155
translate tokipona scene1_d94b34fa:

    # s "I'm going there right now. You can follow me!"
    s ""

# game/dialogs.rpy:156
translate tokipona scene1_453ef329:

    # hero "That would be great! Thank you!"
    hero ""

# game/dialogs.rpy:160
translate tokipona scene1_ef8271d0:

    # "We ended up walking to school together."
    ""

# game/dialogs.rpy:161
translate tokipona scene1_e8a9e895:

    # "Darn, looks like it's my lucky day. {p}I'm walking to school with a beautiful girl on the first day!!!"
    ""

# game/dialogs.rpy:162
translate tokipona scene1_cb9f5aad:

    # s "You said you're new in town, right?"
    s ""

# game/dialogs.rpy:163
translate tokipona scene1_36317276:

    # hero "Hmm?"
    hero ""

# game/dialogs.rpy:165
translate tokipona scene1_ecbbbe32:

    # "I had been daydreaming, so I didn't notice that she was talking to me."
    ""

# game/dialogs.rpy:166
translate tokipona scene1_d86baf24:

    # "She started to blush."
    ""

# game/dialogs.rpy:168
translate tokipona scene1_0408e4ff:

    # s "Oh... I'm sorry! I didn't mean to surprise you!"
    s ""

# game/dialogs.rpy:169
translate tokipona scene1_004a1338:

    # "She's just so adorable! I smiled to myself."
    ""

# game/dialogs.rpy:170
translate tokipona scene1_02ac9e9f:

    # hero "You're not!{p}Actually, I just arrived last Friday. My family just moved here from Tokyo."
    hero ""

# game/dialogs.rpy:172
translate tokipona scene1_b0c354bc:

    # s "You're from Tokyo?"
    s ""

# game/dialogs.rpy:173
translate tokipona scene1_7273bcfc:

    # s "It seems like a wonderful city! I wish I could visit it someday!"
    s ""

# game/dialogs.rpy:174
translate tokipona scene1_e87c5166:

    # hero "I bet... I noticed there are fewer distractions here than in Tokyo."
    hero ""

# game/dialogs.rpy:176
translate tokipona scene1_6ab8e3af:

    # s "Mmhmm, it's true. The village is kinda quiet; so there's usually nothing to do..."
    s ""

# game/dialogs.rpy:177
translate tokipona scene1_f5cb34b6:

    # s "When we want to enjoy ourselves, we usually go to the nearest city."
    s ""

# game/dialogs.rpy:179
translate tokipona scene1_f471c6c0:

    # s "It's only a short train ride away, so it's okay!"
    s ""

# game/dialogs.rpy:180
translate tokipona scene1_6136ebba:

    # hero "There's a train that takes you to the city nearby!?"
    hero ""

# game/dialogs.rpy:181
translate tokipona scene1_bbdac77d:

    # hero "I hope there's a lot of stuff to do there."
    hero ""

# game/dialogs.rpy:182
translate tokipona scene1_aa2bb7ad:

    # s "There is, don't worry! There are fast food restaurants, shops, arcades..."
    s ""

# game/dialogs.rpy:183
translate tokipona scene1_cd44299c:

    # hero "You like arcade games?"
    hero ""

# game/dialogs.rpy:185
translate tokipona scene1_69578672:

    # s "Very much so!"
    s ""

# game/dialogs.rpy:186
translate tokipona scene1_13bf5fa0:

    # s "I'm especially a fan of music and rhythm games, but I enjoy shooting games too!"
    s ""

# game/dialogs.rpy:187
translate tokipona scene1_0fef1f71:

    # hero "Shooting games?"
    hero ""

# game/dialogs.rpy:188
translate tokipona scene1_ad5c53dd:

    # "Wow... I couldn't imagine such a cute and timid girl shooting zombies or soldiers in an arcade game..."
    ""

# game/dialogs.rpy:189
translate tokipona scene1_45a2a82d:

    # hero "By the way, what grade are you in?"
    hero ""

# game/dialogs.rpy:191
translate tokipona scene1_68018eef:

    # s "I'm in the second section."
    s ""

# game/dialogs.rpy:192
translate tokipona scene1_4c52535b:

    # hero "Uh...Second section?"
    hero ""

# game/dialogs.rpy:193
translate tokipona scene1_3b1fd997:

    # s "You see, our school is the only school in the village."
    s ""

# game/dialogs.rpy:194
translate tokipona scene1_7e47a0e7:

    # s "There are students of many different ages and grades at our school, but there's an unbalanced amount of students for the usual grade system."
    s ""

# game/dialogs.rpy:195
translate tokipona scene1_a82424ad:

    # s "So they made different classes, called sections, consisting of 25 students each, which are made up of students of different ages and grades."
    s ""

# game/dialogs.rpy:196
translate tokipona scene1_b83c9923:

    # hero "That sounds fun... I wonder where I'll be assigned to..."
    hero ""

# game/dialogs.rpy:197
translate tokipona scene1_696b7e4d:

    # s "Well, there are only five sections."
    s ""

# game/dialogs.rpy:198
translate tokipona scene1_029cc297:

    # hero "So I'll have a 1-in-5 chance of being in the same section as you!"
    hero ""

# game/dialogs.rpy:199
translate tokipona scene1_c959a464:

    # "I chuckled a bit and she giggled, a little embarrassed."
    ""

# game/dialogs.rpy:201
translate tokipona scene1_c152d24c:

    # s "My name is Sakura... And yours?"
    s ""

# game/dialogs.rpy:202
translate tokipona scene1_180ea3e3:

    # hero "Name's %(stringhero)s."
    hero ""

# game/dialogs.rpy:204
translate tokipona scene1_abeffde2:

    # s "%(stringhero)s..."
    s ""

# game/dialogs.rpy:206
translate tokipona scene1_77296229:

    # s "Nice to meet you, %(stringhero)s-san!"
    s ""

# game/dialogs.rpy:207
translate tokipona scene1_6ec72dd8:

    # hero "Pleased to meet you as well, Sakura-san!"
    hero ""

# game/dialogs.rpy:208
translate tokipona scene1_548f585d:

    # "She gave me a sweet smile."
    ""

# game/dialogs.rpy:209
translate tokipona scene1_4466f621:

    # "Gosh, if a smile could kill, I think I'd be dead right now..."
    ""

# game/dialogs.rpy:211
translate tokipona scene1_1a8c3634:

    # "Deep down, I really hoped to be in the same class as her."
    ""

# game/dialogs.rpy:212
translate tokipona scene1_2fcc0bdf:

    # "I wanted to see her again and learn more about her... {image=heart.png}"
    ""

# game/dialogs.rpy:220
translate tokipona scene1_df51f81c:

    # "I can't believe it!"
    ""

# game/dialogs.rpy:221
translate tokipona scene1_06cbe8a5:

    # "I've been assigned to the second section!{p}I'm so happy!"
    ""

# game/dialogs.rpy:222
translate tokipona scene1_a8b3f3cd:

    # "However, I wonder if it's a good section."
    ""

# game/dialogs.rpy:223
translate tokipona scene1_3c0ab1de:

    # "I wouldn't like to be in a classroom full of jerks or something..."
    ""

# game/dialogs.rpy:224
translate tokipona scene1_9a96bdeb:

    # "If that's the case, I just hope I'll be near Sakura-san..."
    ""

# game/dialogs.rpy:226
translate tokipona scene1_1111d955:

    # "*{i}Bump{/i}*"
    ""

# game/dialogs.rpy:227
translate tokipona scene1_89c4aa2c:

    # "While I was daydreaming again, I bumped into someone."
    ""

# game/dialogs.rpy:228
translate tokipona scene1_65939e34:

    # "It was a girl with pigtails."
    ""

# game/dialogs.rpy:232
translate tokipona scene1_7c921ed5:

    # r "Hey, you! Watch where you're going!"
    r ""

# game/dialogs.rpy:233
translate tokipona scene1_5359da65:

    # hero "Sorry... It was my fault..."
    hero ""

# game/dialogs.rpy:235
translate tokipona scene1_b17d24d0:

    # r "Wait... You're new here, aren't ya?"
    r ""

# game/dialogs.rpy:237
translate tokipona scene1_39364aa2:

    # r "Hmph, well whatever, I'll see you later."
    r ""

# game/dialogs.rpy:239
translate tokipona scene1_b67db50c:

    # "She ran down the hall, probably heading to her classroom."
    ""

# game/dialogs.rpy:240
translate tokipona scene1_e2ba9dde:

    # "That girl was kinda rude... But rather pretty-looking..."
    ""

# game/dialogs.rpy:241
translate tokipona scene1_cd4a6e19:

    # "She reminds me of the main female character of the anime Domoco-chan because of her hair... And from what I could tell, her personality matched as well..."
    ""

# game/dialogs.rpy:244
translate tokipona scene1_41041ae3:

    # "Here it is... Second section's classroom."
    ""

# game/dialogs.rpy:245
translate tokipona scene1_201315b4:

    # "Some of students were already here."
    ""

# game/dialogs.rpy:246
translate tokipona scene1_8928782e:

    # "Sakura-san was right, there are students of all ages here."
    ""

# game/dialogs.rpy:247
translate tokipona scene1_563881d0:

    # "The youngest looked around 8 years old and the oldest, about 18."
    ""

# game/dialogs.rpy:249
translate tokipona scene1_75dbcf3d:

    # "I saw Sakura-san sitting by the window."
    ""

# game/dialogs.rpy:250
translate tokipona scene1_3f9d91b2:

    # "She smiled as soon as saw me."
    ""

# game/dialogs.rpy:252
translate tokipona scene1_05ec4d50:

    # s "Ah, %(stringhero)s-san! Over here! There's a desk available here!"
    s ""

# game/dialogs.rpy:253
translate tokipona scene1_a4ac0809:

    # "She pointed the empty desk behind her."
    ""

# game/dialogs.rpy:254
translate tokipona scene1_c00ae698:

    # "How lucky! A desk near her!"
    ""

# game/dialogs.rpy:255
translate tokipona scene1_95960ba8:

    # "This really is my lucky day!"
    ""

# game/dialogs.rpy:257
translate tokipona scene1_c25aa128:

    # hero "I'm so excited we're in the same section!"
    hero ""

# game/dialogs.rpy:258
translate tokipona scene1_7882f4c9:

    # s "So am I!"
    s ""

# game/dialogs.rpy:259
translate tokipona scene1_ef153560:

    # hero "Our classmates really are made up of different ages!"
    hero ""

# game/dialogs.rpy:260
translate tokipona scene1_198a38e2:

    # s "Just like I told you!"
    s ""

# game/dialogs.rpy:261
translate tokipona scene1_44c33509:

    # hero "Actually,{w} this whole town seems pretty special..."
    hero ""

# game/dialogs.rpy:262
translate tokipona scene1_7a669bce:

    # hero "I mean, the school system, the way the streets are, the kind-hearted townfolk..."
    hero ""

# game/dialogs.rpy:263
translate tokipona scene1_3b8a00b5:

    # s "Do you like it?"
    s ""

# game/dialogs.rpy:264
translate tokipona scene1_874f89c2:

    # hero "So far...Yeah, I think I do like it."
    hero ""

# game/dialogs.rpy:265
translate tokipona scene1_d703df5c:

    # s "I hope you'll enjoy your time here, %(stringhero)s-san!"
    s ""

# game/dialogs.rpy:266
translate tokipona scene1_0afeb16b:

    # hero "Thank you, Sakura-senpai."
    hero ""

# game/dialogs.rpy:267
translate tokipona scene1_034d31d6:

    # hero "So, what are we learning in class today?"
    hero ""

# game/dialogs.rpy:268
translate tokipona scene1_21c1af0a:

    # s "Math."
    s ""

# game/dialogs.rpy:269
translate tokipona scene1_a9d114f6:

    # hero "Oh geez..."
    hero ""

# game/dialogs.rpy:270
translate tokipona scene1_0940f6fc:

    # "Looks like my luck couldn't last all day..."
    ""

# game/dialogs.rpy:271
translate tokipona scene1_07506d68:

    # s "You don't like math?"
    s ""

# game/dialogs.rpy:272
translate tokipona scene1_280571a1:

    # hero "Well, I'm not very good at it."
    hero ""

# game/dialogs.rpy:273
translate tokipona scene1_21fba3a7:

    # s "I'm pretty good with math. I can help you, if you want!"
    s ""

# game/dialogs.rpy:274
translate tokipona scene1_0181a8b6:

    # hero "That sounds perfect!"
    hero ""

# game/dialogs.rpy:275
translate tokipona scene1_8b4aa3e9:

    # hero "Thanks a lot, Sakura-senpai!"
    hero ""

# game/dialogs.rpy:276
translate tokipona scene1_1e4821cf:

    # s "Teehee! You're welcome!"
    s ""

# game/dialogs.rpy:278
translate tokipona scene1_a8275802:

    # hero "Well, since I stayed up all night studying, it shouldn't be that hard."
    hero ""

# game/dialogs.rpy:279
translate tokipona scene1_0df50adc:

    # s "I doubt we'll start with something too difficult anyway. But I'll be here if you need me!"
    s ""

# game/dialogs.rpy:283
translate tokipona scene1_6220dd69:

    # "Class started without anything noteworthy happening."
    ""

# game/dialogs.rpy:284
translate tokipona scene1_5181e117:

    # "Math ended up being pretty easy. It was planned for every level, probably because of all the different ages in class."
    ""

# game/dialogs.rpy:286
translate tokipona scene1_a1cb44f3:

    # "Or it could be that all that studying really paid off..."
    ""

# game/dialogs.rpy:287
translate tokipona scene1_3523ed26:

    # "The lessons felt like they were going on for awhile though..."
    ""

# game/dialogs.rpy:297
translate tokipona scene3_2db6f6fb:

    # "The school bell rang to announce the end of classes."
    ""

# game/dialogs.rpy:298
translate tokipona scene3_9a7755fc:

    # "I was surprised by the noise. It looked more like a bell for firemen. This is really not a urban school!"
    ""

# game/dialogs.rpy:299
translate tokipona scene3_99288ad6:

    # "Sakura and I decided to take and eat our lunches together in the classroom, as well as some other classmates."
    ""

# game/dialogs.rpy:300
translate tokipona scene3_d58c404f:

    # "During lunch, I asked Sakura-san if there were clubs here like in normal schools."
    ""

# game/dialogs.rpy:302
translate tokipona scene3_0e1303f7:

    # s "Yes, for example, I'm in the manga club."
    s ""

# game/dialogs.rpy:303
translate tokipona scene3_5d47cb20:

    # s "I like it a lot, plus the leader of the club is a cosplayer."
    s ""

# game/dialogs.rpy:304
translate tokipona scene3_aae30260:

    # hero "A manga club!?"
    hero ""

# game/dialogs.rpy:305
translate tokipona scene3_04e97040:

    # hero "Awesome! I love manga!"
    hero ""

# game/dialogs.rpy:307
translate tokipona scene3_6ec6ef6f:

    # s "You do?{p}That's great!"
    s ""

# game/dialogs.rpy:308
translate tokipona scene3_f9c203d9:

    # s "You're more than welcome if you want to join us, %(stringhero)s-san!"
    s ""

# game/dialogs.rpy:309
translate tokipona scene3_c7e8dac1:

    # hero "That would be awesome!"
    hero ""

# game/dialogs.rpy:310
translate tokipona scene3_c0f4f625:

    # hero "I just hope the club leader will let me join..."
    hero ""

# game/dialogs.rpy:312
translate tokipona scene3_fc29c8d9:

    # s "I'm sure she will! We're always looking for new members, especially right now!"
    s ""

# game/dialogs.rpy:313
translate tokipona scene3_3d9d0b13:

    # hero "'She'?"
    hero ""

# game/dialogs.rpy:314
translate tokipona scene3_ea9b5b89:

    # s "Yup!"
    s ""

# game/dialogs.rpy:315
translate tokipona scene3_cca5acb7:

    # s "You know, a lot of girls enjoy anime and manga."
    s ""

# game/dialogs.rpy:316
translate tokipona scene3_10b77e4e:

    # hero "Yeah, I know..."
    hero ""

# game/dialogs.rpy:317
translate tokipona scene3_65b45dcd:

    # "Actually, I have an older sister who loved shoujo anime and manga when we were younger."
    ""

# game/dialogs.rpy:318
translate tokipona scene3_b56e0491:

    # "But that was a long time ago. She's married now and is living in Tokyo with her husband."
    ""

# game/dialogs.rpy:319
translate tokipona scene3_b784ebc5:

    # "I wonder how she is doing... I should e-mail her when I get back home."
    ""

# game/dialogs.rpy:320
translate tokipona scene3_2494a3a5:

    # hero "How many members are in the club?"
    hero ""

# game/dialogs.rpy:322
translate tokipona scene3_cb2e3f44:

    # s "Actually, we only have three members right now..."
    s ""

# game/dialogs.rpy:323
translate tokipona scene3_76a2a0d6:

    # hero "Three? That's all?"
    hero ""

# game/dialogs.rpy:324
translate tokipona scene3_88a7dbf3:

    # s "Yeah... Three girls."
    s ""

# game/dialogs.rpy:325
translate tokipona scene3_1f17e8f8:

    # s "It's rather hard to find new members, considering the only manga shop around is in the city..."
    s ""

# game/dialogs.rpy:326
translate tokipona scene3_9f89b658:

    # s "Usually, the people that join aren't too invested to get into new manga to read so they end up leaving the club."
    s ""

# game/dialogs.rpy:327
translate tokipona scene3_84b8bd32:

    # hero "I see."
    hero ""

# game/dialogs.rpy:328
translate tokipona scene3_dd98eff8:

    # "Holy crap! Joining a manga club with only three girls!?"
    ""

# game/dialogs.rpy:329
translate tokipona scene3_d931c57f:

    # "Seems like my luck is back! I feel like the hero of a harem anime or a visual novel!"
    ""

# game/dialogs.rpy:331
translate tokipona scene3_454939b4:

    # s "So, shall we go?"
    s ""

# game/dialogs.rpy:332
translate tokipona scene3_0dab1b57:

    # hero "Sure! You lead the way, Sakura-senpai!"
    hero ""

# game/dialogs.rpy:334
translate tokipona scene3_452bb69b:

    # s "Let's go!"
    s ""

# game/dialogs.rpy:339
translate tokipona scene3_afe022e1:

    # "I followed her through the halls. We climbed up the stairs and then entered a small classroom."
    ""

# game/dialogs.rpy:341
translate tokipona scene3_31abf4ae:

    # "The room only had a few desks and they were all placed together to make a big table."
    ""

# game/dialogs.rpy:342
translate tokipona scene3_1b03234c:

    # "There were some manga posters on the walls and a bookshelf full of manga and artbooks."
    ""

# game/dialogs.rpy:343
translate tokipona scene3_bfcd2b6d:

    # "There was also a small TV with a NATO GEO gaming console and a VCR plugged in it."
    ""

# game/dialogs.rpy:345
translate tokipona scene3_f22a2c8a:

    # "Two girls were already sitting at the table. One of them was completely focused on her handheld console."
    ""

# game/dialogs.rpy:346
translate tokipona scene3_85e454f4:

    # "I instantly recognized the other one. She was the same girl that I had bumped into this morning!"
    ""

# game/dialogs.rpy:350
translate tokipona scene3_074e44c0:

    # r "Sakura-chan! How are you?"
    r ""

# game/dialogs.rpy:352
translate tokipona scene3_bedfc3ef:

    # s "Rika-chan! Nanami-chan!"
    s ""

# game/dialogs.rpy:353
translate tokipona scene3_8ac194aa:

    # s "I found a new member for the club!"
    s ""

# game/dialogs.rpy:354
translate tokipona scene3_a4e3e823:

    # hero "Huh... Hello there?"
    hero ""

# game/dialogs.rpy:356
translate tokipona scene3_7fdb9008:

    # r "You!"
    r ""

# game/dialogs.rpy:358
translate tokipona scene3_0ca8552f:

    # s "You know him already?"
    s ""

# game/dialogs.rpy:360
translate tokipona scene3_771d8927:

    # r "Yeah, he bumped into me in the hallway this morning!"
    r ""

# game/dialogs.rpy:361
translate tokipona scene3_56462fb4:

    # hero "Hey, I already told you that I'm sorry! I was daydreaming and I didn't see you!"
    hero ""

# game/dialogs.rpy:362
translate tokipona scene3_4b30e319:

    # r "Heh... I bet you were daydreaming about girls, weren't you!?"
    r ""

# game/dialogs.rpy:363
translate tokipona scene3_d003c469:

    # hero "Yikes!"
    hero ""

# game/dialogs.rpy:364
translate tokipona scene3_67cc7e93:

    # "Well... she was kind of right..."
    ""

# game/dialogs.rpy:365
translate tokipona scene3_7ee0bf30:

    # "I felt a little embarrassed."
    ""

# game/dialogs.rpy:366
translate tokipona scene3_b9df3e31:

    # "I noticed Nanami stopped playing and sneaked over silently to join us."
    ""

# game/dialogs.rpy:369
translate tokipona scene3_2385f465:

    # s "Don't pay attention to her sourness. All boys are perverts to Rika-chan, but it's not in a mean way!"
    s ""

# game/dialogs.rpy:370
translate tokipona scene3_bdadc851:

    # hero "I see. I just hope she doesn't kill me in my sleep."
    hero ""

# game/dialogs.rpy:371
translate tokipona scene3_85dc831b:

    # r "Hmph!"
    r ""

# game/dialogs.rpy:373
translate tokipona scene3_0160ed38:

    # s "He likes anime and manga, plus he seems quite motivated. Don't you want to give him a chance?"
    s ""

# game/dialogs.rpy:374
translate tokipona scene3_bfbf7878:

    # s "We need more boys in this club too. Some people already think we're just a girls only club."
    s ""

# game/dialogs.rpy:378
translate tokipona scene3_49d8fa00:

    # r "Alright, fine, I accept. Welcome to the club, Mr...?"
    r ""

# game/dialogs.rpy:379
translate tokipona scene3_d2163b95:

    # hero "%(stringhero)s. Nice to meet you."
    hero ""

# game/dialogs.rpy:380
translate tokipona scene3_da243e9b:

    # r "Yeah, likewise."
    r ""

# game/dialogs.rpy:381
translate tokipona scene3_48579522:

    # "Rika spoke with a Kansai dialect. Just like most of the village's townfolk."
    ""

# game/dialogs.rpy:386
translate tokipona scene3_d37e267a:

    # "I saw Nanami just looking at me curiously, so Sakura spoke."
    ""

# game/dialogs.rpy:387
translate tokipona scene3_1901ff6f:

    # s "So you already met Rika-chan. Here we have our little Nanami-chan! She entered the club just two weeks ago!"
    s ""

# game/dialogs.rpy:389
translate tokipona scene3_35deb10d:

    # n "Hey, I'm not little! I'm 16!"
    n ""

# game/dialogs.rpy:390
translate tokipona scene3_a2b33e03:

    # "It's true, she looks a lot younger than she is. But she was still pretty cute looking."
    ""

# game/dialogs.rpy:391
translate tokipona scene3_15aa8440:

    # r "She's small but strong! She doesn't brag much about it but she's good at video games. And I mean... {b}very{/b} good!"
    r ""

# game/dialogs.rpy:393
translate tokipona scene3_28d5cd6d:

    # s "She's the prefecture's champion in a lot of games! {i}Lead of Fighter '96, Pika Rocket Online,{/i}... Even {i}Tetranet Online{/i}!"
    s ""

# game/dialogs.rpy:394
translate tokipona scene3_a50ea391:

    # hero "Cool!"
    hero ""

# game/dialogs.rpy:396
translate tokipona scene3_a580dff0:

    # hero "Wait wait wait...{p}Isn't your username... NAMINAMI?"
    hero ""

# game/dialogs.rpy:398
translate tokipona scene3_582ff77a:

    # n "Huh?! How did you know?"
    n ""

# game/dialogs.rpy:399
translate tokipona scene3_8aa6fe25:

    # hero "I think we played {i}Tetranet Online{/i} last night!"
    hero ""

# game/dialogs.rpy:401
translate tokipona scene3_88e0bb32:

    # n "Wait, you're that %(stringhero)s-dude I had a hard time beating?"
    n ""

# game/dialogs.rpy:402
translate tokipona scene3_3052c7b9:

    # "{i}Dude{/i}?"
    ""

# game/dialogs.rpy:403
translate tokipona scene3_0fd6ee48:

    # hero "Yeah that was me!"
    hero ""

# game/dialogs.rpy:404
translate tokipona scene3_e6f7ec96:

    # hero "What a small world..."
    hero ""

# game/dialogs.rpy:405
translate tokipona scene3_a43e5a9b:

    # n "Let's meet up here after club activities. I challenge you to {i}Lead of Fighters '96{/i}!"
    n ""

# game/dialogs.rpy:406
translate tokipona scene3_050d4a5f:

    # "Her eyes were sparkling, full of energy, like she was ready for a real challenge. It was pretty adorable."
    ""

# game/dialogs.rpy:407
translate tokipona scene3_6376ce6f:

    # "I'm sure I'll get my ass kicked again..."
    ""

# game/dialogs.rpy:408
translate tokipona scene3_958d04dd:

    # "But whatever...She was really cute, so..."
    ""

# game/dialogs.rpy:409
translate tokipona scene3_65819df0:

    # hero "Challenge accepted!"
    hero ""

# game/dialogs.rpy:413
translate tokipona scene3_5de027f9:

    # n "So you're the dude Rika-nee was talking about... Nice to meet you!"
    n ""

# game/dialogs.rpy:414
translate tokipona scene3_3052c7b9_1:

    # "{i}Dude{/i}?"
    ""

# game/dialogs.rpy:415
translate tokipona scene3_396612f8:

    # hero "Nice to meet you too."
    hero ""

# game/dialogs.rpy:416
translate tokipona scene3_dd9f61de:

    # hero "Hehe, sorry to say this, but you really don't look like a video game champion."
    hero ""

# game/dialogs.rpy:417
translate tokipona scene3_9458441a:

    # n "One of these days, I'll show you how skilled I am!"
    n ""

# game/dialogs.rpy:418
translate tokipona scene3_54ab12a6:

    # "Her eyes were sparkling, full of energy. It was pretty adorable."
    ""

# game/dialogs.rpy:419
translate tokipona scene3_40cb84ff:

    # hero "Sure, you're on anytime!"
    hero ""

# game/dialogs.rpy:421
translate tokipona scene3_bddf279b:

    # "Then I focused my attention on Rika."
    ""

# game/dialogs.rpy:422
translate tokipona scene3_fff0f085:

    # "Now that I've seen her up close, I noticed that her eyes were unusual."
    ""

# game/dialogs.rpy:423
translate tokipona scene3_3429571b:

    # "One eye was blue and another was green."
    ""

# game/dialogs.rpy:424
translate tokipona scene3_14f143f3:

    # "The guy whose allowed to dive into those eyes will be one lucky guy."
    ""

# game/dialogs.rpy:425
translate tokipona scene3_540f6b1e:

    # "But given her personality, is there any guy who will even get that chance?"
    ""

# game/dialogs.rpy:426
translate tokipona scene3_1eaf8eb5:

    # hero "If you don't mind me saying, Rika-san,...you look like Domoco-chan."
    hero ""

# game/dialogs.rpy:428
translate tokipona scene3_24649718:

    # "Rika-san smiled. She looked really pretty with one."
    ""

# game/dialogs.rpy:429
translate tokipona scene3_63ae1e58:

    # r "Yeah! I love cosplaying as her, she's one of my favorite anime characters!"
    r ""

# game/dialogs.rpy:430
translate tokipona scene3_e1b3e1b9:

    # r "You've seen the show?"
    r ""

# game/dialogs.rpy:431
translate tokipona scene3_dc1e9f8c:

    # hero "I love it. It's a really funny anime!"
    hero ""

# game/dialogs.rpy:432
translate tokipona scene3_7f21dac0:

    # s "Rika-chan can do a really good imitation of Domoco-chan!"
    s ""

# game/dialogs.rpy:433
translate tokipona scene3_7020cc49:

    # r "Watch this!{p}{i}Pipirupiru pépérato!{/i}{p}How is it?"
    r ""

# game/dialogs.rpy:434
translate tokipona scene3_30d87b21:

    # hero "That was pretty great! It looked just like the real thing!"
    hero ""

# game/dialogs.rpy:435
translate tokipona scene3_87588078:

    # n "Very true!"
    n ""

# game/dialogs.rpy:436
translate tokipona scene3_e0982078:

    # r "I'm glad you liked it!"
    r ""

# game/dialogs.rpy:439
translate tokipona scene3_ed04f436:

    # r "Now it's time for the question every member has to answer before they join the club!"
    r ""

# game/dialogs.rpy:440
translate tokipona scene3_322a4cd3:

    # r "Go for it, Sakura!"
    r ""

# game/dialogs.rpy:441
translate tokipona scene3_5a20a3eb:

    # hero "What's with that face? It's creeping me out..."
    hero ""

# game/dialogs.rpy:442
translate tokipona scene3_041b4444:

    # s "Don't worry, %(stringhero)s-san. It's not a hard question."
    s ""

# game/dialogs.rpy:443
translate tokipona scene3_9ae95b50:

    # s "Here it is:{p}What is your all-time favorite manga?"
    s ""

# game/dialogs.rpy:444
translate tokipona scene3_29f0a12c:

    # hero "My all-time favorite one? Hm..."
    hero ""

# game/dialogs.rpy:445
translate tokipona scene3_2df7fab7:

    # "I started to think."
    ""

# game/dialogs.rpy:446
translate tokipona scene3_27a6f658:

    # "There are a few that I really like. I enjoy a lot of ecchi manga, but if I say that, the girls would look at me funny... Especially Rika-san."
    ""

# game/dialogs.rpy:447
translate tokipona scene3_6275c604:

    # "But it's always good to be honest with girls as well..."
    ""

# game/dialogs.rpy:451
translate tokipona scene3_152e1c76:

    # "Let's tell the truth..."
    ""

# game/dialogs.rpy:452
translate tokipona scene3_f2942a1c:

    # "After all, maybe they don't know about this one..."
    ""

# game/dialogs.rpy:453
translate tokipona scene3_923bf6bc:

    # hero "I love {i}High School Samurai{/i}! That's my favorite one!"
    hero ""

# game/dialogs.rpy:458
translate tokipona scene3_459d8d4d:

    # "They started to look funny, as I expected."
    ""

# game/dialogs.rpy:459
translate tokipona scene3_ea8e9dca:

    # "Rika-san looked pissed off again and Sakura-chan was blushing red."
    ""

# game/dialogs.rpy:460
translate tokipona scene3_10363d18:

    # "Nanami was about to burst into tears of laughter."
    ""

# game/dialogs.rpy:461
translate tokipona scene3_cebb2b3d:

    # "Aw crap, maybe I made the wrong choice..."
    ""

# game/dialogs.rpy:462
translate tokipona scene3_d83f0622:

    # r "You damn pervert! You're just like every other guy!"
    r ""

# game/dialogs.rpy:463
translate tokipona scene3_244ff0c7:

    # n "Hahaha! You sure have guts, dude! Confessing something like that in front of Rika-nee!"
    n ""

# game/dialogs.rpy:464
translate tokipona scene3_e1554201:

    # s "..."
    s ""

# game/dialogs.rpy:466
translate tokipona scene3_e08d3442:

    # "I was hanging my head in shame, but suddenly Sakura-san spoke."
    ""

# game/dialogs.rpy:467
translate tokipona scene3_76909f2d:

    # s "Actually, Rika-chan..."
    s ""

# game/dialogs.rpy:468
translate tokipona scene3_4570740d:

    # s "{i}High School Samurai{/i} isn't that bad at all."
    s ""

# game/dialogs.rpy:469
translate tokipona scene3_6bb1ac2f:

    # s "You see, the hero isn't a typical boy like most harem manga. He's actually very heroic!"
    s ""

# game/dialogs.rpy:470
translate tokipona scene3_b2661f33:

    # s "He has a great sense of honor and all the monologues he gives are very righteous and noble."
    s ""

# game/dialogs.rpy:471
translate tokipona scene3_8effa9e6:

    # s "I admire heroes like that."
    s ""

# game/dialogs.rpy:472
translate tokipona scene3_f902c5fb:

    # s "And by the way, the hero himself isn't really a pervert, since every ecchi scene where he's involved happens accidentally."
    s ""

# game/dialogs.rpy:474
translate tokipona scene3_0ddbdce1:

    # r "Really?"
    r ""

# game/dialogs.rpy:475
translate tokipona scene3_68f8b283:

    # n "I've seen some episodes. That guy reminds me of {i}Nanda no Ryu{/i}, who's always resolving conflicts with great punchlines and fighting when necessary."
    n ""

# game/dialogs.rpy:476
translate tokipona scene3_ced41922:

    # n "Sure, it's ecchi sometimes, but it's actually a nice show to watch."
    n ""

# game/dialogs.rpy:477
translate tokipona scene3_9816aa54:

    # r "Well... I guess it's okay, then..."
    r ""

# game/dialogs.rpy:482
translate tokipona scene3_eca282df:

    # "Let's not take any risks..."
    ""

# game/dialogs.rpy:483
translate tokipona scene3_6ba925d2:

    # hero "I love {i}Rosario Maiden{/i}. It's a great manga!"
    hero ""

# game/dialogs.rpy:488
translate tokipona scene3_21a5901b:

    # "The three girls stared at me."
    ""

# game/dialogs.rpy:489
translate tokipona scene3_7f0ae732:

    # "Finally, they all smiled. Rika-san spoke."
    ""

# game/dialogs.rpy:494
translate tokipona scene3_79487764:

    # r "I love that one too!"
    r ""

# game/dialogs.rpy:495
translate tokipona scene3_ca38b9e6:

    # r "The art style is superb and the story is great. It's a pretty great series."
    r ""

# game/dialogs.rpy:496
translate tokipona scene3_c24a4394:

    # s "I like it too."
    s ""

# game/dialogs.rpy:497
translate tokipona scene3_3d29180a:

    # s "It's not my favorite but I like it."
    s ""

# game/dialogs.rpy:498
translate tokipona scene3_1324b4b2:

    # s "I especially love the dolls. I wish I could have one like that to take home!"
    s ""

# game/dialogs.rpy:499
translate tokipona scene3_29de2f7c:

    # hero "Yeah! They're really cute! Especially the green one!"
    hero ""

# game/dialogs.rpy:500
translate tokipona scene3_42f3f6f5:

    # s "The green one is kinda mean but it's still my favorite one!"
    s ""

# game/dialogs.rpy:501
translate tokipona scene3_bf59c037:

    # n "I'm not too into the dolls. I prefer the protagonist. He's so handsome!"
    n ""

# game/dialogs.rpy:502
translate tokipona scene3_f7e6d2b6:

    # r "I prefer the red doll. She's just so... dominant... hehe! {image=heart.png}"
    r ""

# game/dialogs.rpy:503
translate tokipona scene3_40c59aa9:

    # "Rika-san gave me an evil look. What the heck could she be thinking? I'm confused and a bit scared."
    ""

# game/dialogs.rpy:504
translate tokipona scene3_745f17e5:

    # "But it looks like she's starting to like me."
    ""

# game/dialogs.rpy:505
translate tokipona scene3_d79ab191:

    # "Well, at least I hope so..."
    ""

# game/dialogs.rpy:513
translate tokipona scene3_66851b4f:

    # "It was almost time to head home."
    ""

# game/dialogs.rpy:515
translate tokipona scene3_a7a81522:

    # "Rika-chan and Sakura-chan left to head home. I ended up staying with Nanami-chan, ready to challenge her."
    ""

# game/dialogs.rpy:520
translate tokipona scene3_e5863c5e:

    # n "So, are you ready, dude?"
    n ""

# game/dialogs.rpy:521
translate tokipona scene3_30e27e13:

    # hero "Of course I am. I was born ready!"
    hero ""

# game/dialogs.rpy:522
translate tokipona scene3_fa127982:

    # n "I meant, ready to get your butt kicked?"
    n ""

# game/dialogs.rpy:523
translate tokipona scene3_b87b3a25:

    # hero "Only if you're ready to get yours kicked first!"
    hero ""

# game/dialogs.rpy:524
translate tokipona scene3_7cf5de36:

    # "I pretty much know she's going to win the game. But I'm not gonna let win without a challenge. Although, she'll still call me a {i}dude{/i} afterwards."
    ""

# game/dialogs.rpy:525
translate tokipona scene3_c3b34a29:

    # "I gotta defend my pride, eh?"
    ""

# game/dialogs.rpy:529
translate tokipona scene3_d0713ddb:

    # "She turned on the TV and the NATO GEO after inserting Lead of Fighters '96 cartridge."
    ""

# game/dialogs.rpy:530
translate tokipona scene3_3716d683:

    # hero "It's pretty amazing the club has this gaming console here! That's one of the most expensive systems available!"
    hero ""

# game/dialogs.rpy:531
translate tokipona scene3_8ed428db:

    # n "Actually, it's mine. I lent to the club to use."
    n ""

# game/dialogs.rpy:532
translate tokipona scene3_8a31c669:

    # n "Are you familiar with the {i}Lead of Fighters{/i} series?"
    n ""

# game/dialogs.rpy:533
translate tokipona scene3_165263c8:

    # hero "Yeah, actually this game is my favorite one so far."
    hero ""

# game/dialogs.rpy:534
translate tokipona scene3_5ff36a65:

    # hero "It's my favorite game too, but I could only play it in the arcades in Tokyo."
    hero ""

# game/dialogs.rpy:535
translate tokipona scene3_facc5c5d:

    # n "I see."
    n ""

# game/dialogs.rpy:536
translate tokipona scene3_9be30a14:

    # n "So, who's your favorite character?"
    n ""

# game/dialogs.rpy:537
translate tokipona scene3_913605a7:

    # n "I usually like to pick the cute girls."
    n ""

# game/dialogs.rpy:538
translate tokipona scene3_3cba88d7:

    # n "In most fighting games they rarely look threating, but when you know how to play with them, they can be deadly!"
    n ""

# game/dialogs.rpy:539
translate tokipona scene3_978b1237:

    # hero "I'm not sure what my favorite is... I usually pick randomly."
    hero ""

# game/dialogs.rpy:540
translate tokipona scene3_7e18c9db:

    # n "If you're a beginner, you should take this guy, here. {w}He's good for be-{nw}"
    n ""

# game/dialogs.rpy:541
translate tokipona scene3_0d60b236:

    # hero "Hey I'm not a beginner! I was one of the best at my previous school!"
    hero ""

# game/dialogs.rpy:542
translate tokipona scene3_72095650:

    # n "Oh yeah? Let's see about that!"
    n ""

# game/dialogs.rpy:543
translate tokipona scene3_704fb29f:

    # "So we selected our fighters and started the virtual brawl."
    ""

# game/dialogs.rpy:544
translate tokipona scene3_c8bd5427:

    # n "So you're from Tokyo?"
    n ""

# game/dialogs.rpy:545
translate tokipona scene3_6e3125fc:

    # hero "Yeah. I moved here because my parents wanted to open a store in a quiet, peaceful village."
    hero ""

# game/dialogs.rpy:546
translate tokipona scene3_352bd956:

    # n "A store? You mean...the grocery store at the G. street?"
    n ""

# game/dialogs.rpy:547
translate tokipona scene3_cf6c4656:

    # hero "How do you know?"
    hero ""

# game/dialogs.rpy:548
translate tokipona scene3_3b26e73f:

    # n "I... heard it was looking for new owners."
    n ""

# game/dialogs.rpy:549
translate tokipona scene3_9bf86260:

    # n "My brother worked there for a while, back then."
    n ""

# game/dialogs.rpy:551
translate tokipona scene3_28e4c374:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    ""

# game/dialogs.rpy:552
translate tokipona scene3_cd3c8571:

    # "Of course, she beat the crap out of me."
    ""

# game/dialogs.rpy:553
translate tokipona scene3_7944ba15:

    # "I'm not sure if I lost because of her skills or because we're talking at the same time.{p}It might be both..."
    ""

# game/dialogs.rpy:554
translate tokipona scene3_66644a5e:

    # "We chose different fighters and started a new fight."
    ""

# game/dialogs.rpy:555
translate tokipona scene3_b7629213:

    # hero "So you have a brother?"
    hero ""

# game/dialogs.rpy:556
translate tokipona scene3_c1b9564a:

    # n "Yep, an older brother.{p}He's 27."
    n ""

# game/dialogs.rpy:557
translate tokipona scene3_6b9e255a:

    # hero "Is he any good at games?"
    hero ""

# game/dialogs.rpy:558
translate tokipona scene3_25066c43:

    # n "Not really."
    n ""

# game/dialogs.rpy:559
translate tokipona scene3_456c3557:

    # n "I don't think it's from his generation."
    n ""

# game/dialogs.rpy:560
translate tokipona scene3_5f66a4a4:

    # hero "That's possible. My sister is almost as old as him and it's not her thing either."
    hero ""

# game/dialogs.rpy:561
translate tokipona scene3_a2e58feb:

    # n "Oh yeah, your sister... What's she doing?"
    n ""

# game/dialogs.rpy:562
translate tokipona scene3_e1b6d146:

    # hero "She works as a banker."
    hero ""

# game/dialogs.rpy:563
translate tokipona scene3_30b55ef7:

    # n "That sounds boring."
    n ""

# game/dialogs.rpy:564
translate tokipona scene3_a5630de6:

    # hero "I know right! I don't understand how she can do something like that without getting bored."
    hero ""

# game/dialogs.rpy:566
translate tokipona scene3_28e4c374_1:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    ""

# game/dialogs.rpy:567
translate tokipona scene3_821cdbe8:

    # "And yet again, I lost."
    ""

# game/dialogs.rpy:568
translate tokipona scene3_b0f56878:

    # hero "Alright. No more Mister Nice Guy, I'm gonna fight for real!"
    hero ""

# game/dialogs.rpy:569
translate tokipona scene3_7a9ca5b7:

    # n "It's about time..."
    n ""

# game/dialogs.rpy:570
translate tokipona scene3_9f960747:

    # "Nanami gave me a mischievous grin.{p}That looked cute but kinda creepy too."
    ""

# game/dialogs.rpy:571
translate tokipona scene3_e6598e64:

    # "She probably knew I was already giving it my all..."
    ""

# game/dialogs.rpy:572
translate tokipona scene3_ac835df6:

    # "We played through the next round without a single word."
    ""

# game/dialogs.rpy:573
translate tokipona scene3_a3279e7e:

    # "I was finally was gaining an advantage! She stayed less and less calm as I was slowly winning."
    ""

# game/dialogs.rpy:574
translate tokipona scene3_289633fa:

    # "And then finally, using a secret combo I knew well..."
    ""

# game/dialogs.rpy:576
translate tokipona scene3_28e4c374_2:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    ""

# game/dialogs.rpy:577
translate tokipona scene3_ed61300c:

    # "Nanami stared at me with a sad face and began to cry like a small child."
    ""

# game/dialogs.rpy:578
translate tokipona scene3_15a85014:

    # "It didn't look real at all though. There were no tears or anything, she was just moaning like a baby."
    ""

# game/dialogs.rpy:579
translate tokipona scene3_decd73c2:

    # hero "Hey hey, calm down! I'm sorry... I'll let you win next time!"
    hero ""

# game/dialogs.rpy:580
translate tokipona scene3_ab688811:

    # "She instantly stopped crying and made her cute/creepy grin."
    ""

# game/dialogs.rpy:581
translate tokipona scene3_c6891b22:

    # n "Works everytime! {image=heart.png}"
    n ""

# game/dialogs.rpy:582
translate tokipona scene3_56b514db:

    # hero "Oh man..."
    hero ""

# game/dialogs.rpy:583
translate tokipona scene3_2d2950d5:

    # "I started laughing."
    ""

# game/dialogs.rpy:584
translate tokipona scene3_f2d7328e:

    # "Nanami made a goofy pouting face which made me laugh even more."
    ""

# game/dialogs.rpy:585
translate tokipona scene3_05ab44b2:

    # n "Let's fight again!"
    n ""

# game/dialogs.rpy:586
translate tokipona scene3_241aaaf7:

    # hero "Alright, alright, little one!"
    hero ""

# game/dialogs.rpy:587
translate tokipona scene3_52c6a554:

    # n "I'm not little!"
    n ""

# game/dialogs.rpy:592
translate tokipona scene3_176ddd63:

    # "We played for a while, but eventually we both needed to get home."
    ""

# game/dialogs.rpy:593
translate tokipona scene3_ce50b0ab:

    # n "That was fun! I hope we can do this again soon!"
    n ""

# game/dialogs.rpy:594
translate tokipona scene3_24edf3d9:

    # hero "Anytime, Nanami-chan."
    hero ""

# game/dialogs.rpy:596
translate tokipona scene3_7074277a:

    # n "Oh by the way, here's my e-mail."
    n ""

# game/dialogs.rpy:597
translate tokipona scene3_cb953bdb:

    # "She gave me a tiny piece of paper with an e-mail written on it. There was a small heart where the @ sign was supposed to be."
    ""

# game/dialogs.rpy:598
translate tokipona scene3_a99fbe92:

    # "I blushed a bit."
    ""

# game/dialogs.rpy:599
translate tokipona scene3_9f566895:

    # n "It's my personal e-mail... Just in case..."
    n ""

# game/dialogs.rpy:600
translate tokipona scene3_712996f5:

    # hero "Thanks, Nanami-chan."
    hero ""

# game/dialogs.rpy:603
translate tokipona scene3_05df8eb4:

    # n "See you tomorrow, %(stringhero)s-senpai! {image=heart.png}"
    n ""

# game/dialogs.rpy:606
translate tokipona scene3_759e6781:

    # "She left the school, hopping happily like a kid on the street."
    ""

# game/dialogs.rpy:607
translate tokipona scene3_37a9b710:

    # "Dang, for a gaming champion...she's really something."
    ""

# game/dialogs.rpy:610
translate tokipona scene3_68c44298:

    # "The sun was starting to set."
    ""

# game/dialogs.rpy:611
translate tokipona scene3_6f9822aa:

    # "Rika-chan and Nanami-chan take different routes to get home, so only Sakura-chan and I ended up walking home together."
    ""

# game/dialogs.rpy:617
translate tokipona scene3_563d938f:

    # s "So you like seinen manga?"
    s ""

# game/dialogs.rpy:618
translate tokipona scene3_04f08300:

    # hero "Well yes..."
    hero ""

# game/dialogs.rpy:619
translate tokipona scene3_14e7e4b7:

    # hero "On the same subject, you said {i}Rosario Maiden{/i} wasn't your favorite. Which one is your favorite, then?"
    hero ""

# game/dialogs.rpy:620
translate tokipona scene3_41af6c01:

    # s "In the seinen type? It's {i}Desu Motto{/i}."
    s ""

# game/dialogs.rpy:621
translate tokipona scene3_a0282b9a:

    # hero "You like {i}Desu Motto{/i}?"
    hero ""

# game/dialogs.rpy:622
translate tokipona scene3_157e9fd5:

    # "I was kinda surprised."
    ""

# game/dialogs.rpy:623
translate tokipona scene3_3593807d:

    # "{i}Desu Motto{/i} is a story about a living book that eats human guts. It's full of all kinds of blood and gore, more so than in {i}Nanda no Ryu{/i}."
    ""

# game/dialogs.rpy:624
translate tokipona scene3_976cd68f:

    # "And there's a magical girl who pursues this book with a chainsaw in order to destroy it."
    ""

# game/dialogs.rpy:625
translate tokipona scene3_e7ee8e3e:

    # "Anyway, it's not the kind of thing girls her age would watch and like."
    ""

# game/dialogs.rpy:626
translate tokipona scene3_2ae24650:

    # "Appearances can be surprising, sometimes..."
    ""

# game/dialogs.rpy:627
translate tokipona scene3_28bb09a7:

    # hero "I like it too, but there's too much gore for me."
    hero ""

# game/dialogs.rpy:628
translate tokipona scene3_ed0c1175:

    # hero "I can't stand too much blood in an anime. I remember being traumatized by {i}Elfic Leaves{/i}..."
    hero ""

# game/dialogs.rpy:629
translate tokipona scene3_58b557f4:

    # "{i}Elfic Leaves{/i} is known to be the most violent and brutal anime in all of japanimation history. It's so gory, it was never broadcast on TV and it only exists on VHS tapes."
    ""

# game/dialogs.rpy:630
translate tokipona scene3_64b818b7:

    # "I remember watching it by accident, about four years ago. It was on a VHS tape in my sister's room..."
    ""

# game/dialogs.rpy:632
translate tokipona scene3_74bc980e:

    # s "I can understand that. That one is really hardcore."
    s ""

# game/dialogs.rpy:633
translate tokipona scene3_fe827394:

    # s "Though I think it's okay, since they're only drawings. I don't think I could stand it either if it was real."
    s ""

# game/dialogs.rpy:634
translate tokipona scene3_189e996b:

    # hero "Same for me."
    hero ""

# game/dialogs.rpy:636
translate tokipona scene3_1f00d77e:

    # "Sakura stared at me for a moment."
    ""

# game/dialogs.rpy:637
translate tokipona scene3_24848926:

    # "It's like she was wondering about something... Or that she came a conclusion about something..."
    ""

# game/dialogs.rpy:638
translate tokipona scene3_47159e79:

    # s "...{w}{i}Rosario Maiden{/i} isn't actually favorite manga ever, right?"
    s ""

# game/dialogs.rpy:639
translate tokipona scene3_ef3bce8f:

    # "Gah!!"
    ""

# game/dialogs.rpy:640
translate tokipona scene3_bb685cb0:

    # "How'd she make an incredible guess like that?!"
    ""

# game/dialogs.rpy:641
translate tokipona scene3_e633313f:

    # "I guess I couldn't hide it from her anymore..."
    ""

# game/dialogs.rpy:642
translate tokipona scene3_ae8949a7:

    # hero "You got me..."
    hero ""

# game/dialogs.rpy:643
translate tokipona scene3_8137fe91:

    # hero "My favorite is {i}High School Samurai{/i}..."
    hero ""

# game/dialogs.rpy:645
translate tokipona scene3_5c82e516:

    # s "Really?"
    s ""

# game/dialogs.rpy:648
translate tokipona scene3_d2de1953:

    # s "So you like {i}High School Samurai{/i}?"
    s ""

# game/dialogs.rpy:649
translate tokipona scene3_3d802b0c:

    # "I turned my head away from her a bit."
    ""

# game/dialogs.rpy:650
translate tokipona scene3_6449a5e4:

    # hero "Thanks again for saving me back there... Looks like Rika-san doesn't like that kind of manga and anime."
    hero ""

# game/dialogs.rpy:652
translate tokipona scene3_bd58aae1:

    # s "It's okay. Rika-chan probably already forgot about it."
    s ""

# game/dialogs.rpy:653
translate tokipona scene3_5798ec89:

    # s "And to be honest...{p}I really enjoy {i}High School Samurai{/i}!"
    s ""

# game/dialogs.rpy:654
translate tokipona scene3_b3ba8ed1:

    # hero "You do?!"
    hero ""

# game/dialogs.rpy:655
translate tokipona scene3_d207e06f:

    # s "Yes!"
    s ""

# game/dialogs.rpy:658
translate tokipona scene3_2098e30f:

    # s "It's a great manga... The hero is really handsome."
    s ""

# game/dialogs.rpy:659
translate tokipona scene3_2f77db13:

    # s "And... the girls are very cute, too..."
    s ""

# game/dialogs.rpy:660
translate tokipona scene3_1ae45319:

    # hero "Ah?"
    hero ""

# game/dialogs.rpy:661
translate tokipona scene3_22eeaf4a:

    # s "Yes, especially the main one. I wish I could look like her someday..."
    s ""

# game/dialogs.rpy:662
translate tokipona scene3_1171cfe9:

    # hero "You're already pretty cute though, Sakura-san."
    hero ""

# game/dialogs.rpy:663
translate tokipona scene3_aa49989e:

    # "I said that without thinking."
    ""

# game/dialogs.rpy:664
translate tokipona scene3_f463b1f2:

    # "I was on the verge of taking my words back until she replied."
    ""

# game/dialogs.rpy:666
translate tokipona scene3_1c22c637:

    # s "...Do you really think so?"
    s ""

# game/dialogs.rpy:667
translate tokipona scene3_b836a32c:

    # "Her face turned red out of embarrassment."
    ""

# game/dialogs.rpy:668
translate tokipona scene3_87546ecc:

    # "So cute!"
    ""

# game/dialogs.rpy:669
translate tokipona scene3_4baf7e22:

    # "It was so cute that I felt a little embarrassed too."
    ""

# game/dialogs.rpy:670
translate tokipona scene3_1c0b1c3e:

    # hero "Well y-yeah... You're a pretty girl, Sakura-san..."
    hero ""

# game/dialogs.rpy:671
translate tokipona scene3_557b9c7c:

    # s ". . ."
    s ""

# game/dialogs.rpy:672
translate tokipona scene3_5d8f36c3:

    # s "Thank you... %(stringhero)s-kun..."
    s ""

# game/dialogs.rpy:673
translate tokipona scene3_2758fdb5:

    # "She hesitated for awhile before thanking me."
    ""

# game/dialogs.rpy:674
translate tokipona scene3_d3083c53:

    # "Was that her being timid? Probably."
    ""

# game/dialogs.rpy:679
translate tokipona scene3_88d97835:

    # "We finally reached the crossroads of our respective houses."
    ""

# game/dialogs.rpy:680
translate tokipona scene3_95f36d4e:

    # hero "Sakura-chan, since I don't know the path to school very well, do you mind if we go to school together every day?"
    hero ""

# game/dialogs.rpy:681
translate tokipona scene3_c1d7d0f9:

    # s "Sure! I'll wait for you here tomorrow."
    s ""

# game/dialogs.rpy:682
translate tokipona scene3_139aa776:

    # hero "Alright, thank you, Sakura-chan!"
    hero ""

# game/dialogs.rpy:683
translate tokipona scene3_3257668c:

    # s "You're welcome..."
    s ""

# game/dialogs.rpy:685
translate tokipona scene3_00bab8bf:

    # s "See you tomorrow, %(stringhero)s-kun!"
    s ""

# game/dialogs.rpy:686
translate tokipona scene3_6215ba95:

    # hero "See you!"
    hero ""

# game/dialogs.rpy:689
translate tokipona scene3_a46fbd6c:

    # "I watched her walk down the street."
    ""

# game/dialogs.rpy:690
translate tokipona scene3_5e5c6211:

    # "Her hips were swaying a little with each step, her hair moving with the summer wind."
    ""

# game/dialogs.rpy:692
translate tokipona scene3_862f5cfa:

    # "The cicadas were crying, giving an atmosphere of something dreamy and peaceful with the hot summer weather."
    ""

# game/dialogs.rpy:695
translate tokipona scene3_82417432:

    # "Before going to bed, I decided to get a breath of fresh air in front of the house."
    ""

# game/dialogs.rpy:696
translate tokipona scene3_be4997dc:

    # "I must admit, this place is beautiful."
    ""

# game/dialogs.rpy:697
translate tokipona scene3_ba75a6ad:

    # "For sure, finding everything you want isn't as convenient as in Tokyo..."
    ""

# game/dialogs.rpy:698
translate tokipona scene3_d9ac4ff7:

    # "But gee,... It's so good to breath real air instead of smog..."
    ""

# game/dialogs.rpy:699
translate tokipona scene3_8534ca42:

    # ". . ."
    ""

# game/dialogs.rpy:702
translate tokipona scene3_f6793b5b:

    # "What's that?..."
    ""

# game/dialogs.rpy:703
translate tokipona scene3_41539cb7:

    # "Sounds like someone is playing the violin."
    ""

# game/dialogs.rpy:704
translate tokipona scene3_f3b91659:

    # "It's kinda appropriate for a night like this..."
    ""

# game/dialogs.rpy:705
translate tokipona scene3_8534ca42_1:

    # ". . ."
    ""

# game/dialogs.rpy:706
translate tokipona scene3_f07946fa:

    # "I think I'm starting enjoying my new life here."
    ""

# game/dialogs.rpy:740
translate tokipona scene4_840498c7:

    # centered "{size=+35}CHAPTER 2\nA fine date{fast}{/size}"
    centered ""

# game/dialogs.rpy:741
translate tokipona scene4_b2947d53:

    # alt "CHAPTER 2\nA fine date"
    alt ""

# game/dialogs.rpy:752
translate tokipona scene4_46a27380:

    # write "Dear sister."
    write ""

# game/dialogs.rpy:754
translate tokipona scene4_d033b41a:

    # write "How are things?{w} Mom, dad, and I have arrived at our new home.{w} Things here are way different than in Tokyo. It's peaceful, but there's nothing to do in the village.{w} You need to take the train to find something to do in the nearest town."
    write ""

# game/dialogs.rpy:756
translate tokipona scene4_dcc0ac1c:

    # write "I just came back from my first day at my new school. I've already made three friends!"
    write ""

# game/dialogs.rpy:758
translate tokipona scene4_9b62289a:

    # write "Things have changed a lot, but I think I'll get used to it soon enough."
    write ""

# game/dialogs.rpy:759
translate tokipona scene4_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:761
translate tokipona scene4_66476017:

    # write "We're all part of a manga club at school.{w} Since they're all girls, can you recommend any shoujo manga that I can read and talk with them about?"
    write ""

# game/dialogs.rpy:763
translate tokipona scene4_c54efb3e:

    # write "I hope you'll visit us someday soon."
    write ""

# game/dialogs.rpy:765
translate tokipona scene4_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write ""

# game/dialogs.rpy:766
translate tokipona scene4_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:773
translate tokipona scene4_0b847f9b:

    # "The week passed by quickly and it's already Friday."
    ""

# game/dialogs.rpy:774
translate tokipona scene4_a3a675c2:

    # "Spending time with the manga club is great."
    ""

# game/dialogs.rpy:775
translate tokipona scene4_58d076ca:

    # "Sakura is a little shy, but very sweet.{p}Rika still thinks I'm a pervert..."
    ""

# game/dialogs.rpy:777
translate tokipona scene4_8367dc19:

    # "Nanami is playful and competitive. Playing video games with her is always fun."
    ""

# game/dialogs.rpy:779
translate tokipona scene4_0cbaa7bb:

    # "Nanami and I even started to play games online a little bit!"
    ""

# game/dialogs.rpy:780
translate tokipona scene4_4995ec74:

    # "I know it sounds weird that the only friends I've made are three girls. But I already feel like I know I can count on them."
    ""

# game/dialogs.rpy:781
translate tokipona scene4_99d1ceb5:

    # "I think it's better to have a few close friends you can count on than a large group of friends that don't know each other very well."
    ""

# game/dialogs.rpy:782
translate tokipona scene4_6dc4476d:

    # "I've learned a lot about manga, anime and the village itself thanks to them."
    ""

# game/dialogs.rpy:786
translate tokipona scene4_bd1aa541:

    # "On my way to school, I was walking towards the crossroads where I usually join Sakura."
    ""

# game/dialogs.rpy:788
translate tokipona scene4_e8c9ca4f:

    # "As I got closer to her, I noticed that she wasn't alone."
    ""

# game/dialogs.rpy:789
translate tokipona scene4_5108d11f:

    # "When I got closer, I could see three rough looking guys around her."
    ""

# game/dialogs.rpy:790
translate tokipona scene4_6898b2d9:

    # "It was like they were the typical bad boys you see in some manga or anime, with the same exact goofy hairstyles and everything."
    ""

# game/dialogs.rpy:791
translate tokipona scene4_f2f59080:

    # "I vaguely knew who they were. They were from our school, but I remember Rika-chan telling me these guys never show up to school."
    ""

# game/dialogs.rpy:793
translate tokipona scene4_b5bd2c54:

    # "Bad guy 1" "Go on! Say it!"
    "Bad guy 1" ""

# game/dialogs.rpy:794
translate tokipona scene4_2c3b1b19:

    # "One of the guys shouted at Sakura. I instinctively hid myself behind a wall."
    ""

# game/dialogs.rpy:795
translate tokipona scene4_b57e6ca3:

    # "Another guy who looked like their leader forcefully grabbed Sakura by her collar."
    ""

# game/dialogs.rpy:796
translate tokipona scene4_bf711db5:

    # "Bad guy 1" "You're guy, we all know it! So say it! Tell us what you really are!"
    "Bad guy 1" ""

# game/dialogs.rpy:797
translate tokipona scene4_b765fd13:

    # "Bad guy 2" "Yeah, stop playing the cute little girl!"
    "Bad guy 2" ""

# game/dialogs.rpy:798
translate tokipona scene4_dc755d36:

    # "Bad guy 3" "I'm sure you're just a sick pervert that disguises himself into a girl to peek at other girls in the toilets, right!?"
    "Bad guy 3" ""

# game/dialogs.rpy:799
translate tokipona scene4_7849a2e2:

    # "Bad guy 2" "Yeah, you're disgusting!"
    "Bad guy 2" ""

# game/dialogs.rpy:800
translate tokipona scene4_8006ab79:

    # "What the hell are they talking about?!"
    ""

# game/dialogs.rpy:801
translate tokipona scene4_e45090ba:

    # "Anyway, Sakura is trouble! I have to do something!"
    ""

# game/dialogs.rpy:802
translate tokipona scene4_af06121a:

    # "I'm not very good at fighting... I don't have the strength of a super hero..."
    ""

# game/dialogs.rpy:803
translate tokipona scene4_77b9ebe0:

    # "But I don't care! Sakura-chan must be saved!"
    ""

# game/dialogs.rpy:804
translate tokipona scene4_d3847689:

    # "Be strong like the {i}High School Samurai{/i}, %(stringhero)s!"
    ""

# game/dialogs.rpy:805
translate tokipona scene4_38f6654f:

    # "{size=+10}Chaaaaarge!!!!!{/size}"
    ""

# game/dialogs.rpy:806
translate tokipona scene4_e9b169e1:

    # "I ran between the group and started to shout at the guys."
    ""

# game/dialogs.rpy:807
translate tokipona scene4_03cc4ea3:

    # hero "Hey! Leave her alone!"
    hero ""

# game/dialogs.rpy:808
translate tokipona scene4_21ec0b43:

    # "Bad guy 2" "Huh? Who's that little brat?"
    "Bad guy 2" ""

# game/dialogs.rpy:809
translate tokipona scene4_5a5a90ca:

    # "Bad guy 3" "Who are you? His boyfriend? You a faggot!?"
    "Bad guy 3" ""

# game/dialogs.rpy:811
translate tokipona scene4_62845cb0:

    # "Bad guy 1" "Pfft! Whatever, let's go, guys. I had enough fun anyway."
    "Bad guy 1" ""

# game/dialogs.rpy:812
translate tokipona scene4_02769f02:

    # "B.g. 2 & 3" "What?"
    "B.g. 2 & 3" ""

# game/dialogs.rpy:813
translate tokipona scene4_9d93e425:

    # "Bad guy 1" "I said let's go!!!"
    "Bad guy 1" ""

# game/dialogs.rpy:814
translate tokipona scene4_e19b48ff:

    # "Bad guy 1" "There's way more fun shit to do than kicking the asses of these faggots!"
    "Bad guy 1" ""

# game/dialogs.rpy:815
translate tokipona scene4_aadc99c4:

    # "I was ready for a fight but fortunately, they ended up walking away, arguing amongst themselves."
    ""

# game/dialogs.rpy:816
translate tokipona scene4_65210d1a:

    # "I doubt I scared them. Maybe they really thought that with me in the way, harassing Sakura wasn't fun anymore."
    ""

# game/dialogs.rpy:817
translate tokipona scene4_5b2dcd53:

    # "Sakura collapsed on her knees and sat on the ground. She stayed motionless, looking down."
    ""

# game/dialogs.rpy:819
translate tokipona scene4_14ee51d1:

    # "I watched them walk away. The cicadas were crying."
    ""

# game/dialogs.rpy:821
translate tokipona scene4_5714df67:

    # "I got on my knees to reach out to Sakura. She looked dead inside."
    ""

# game/dialogs.rpy:822
translate tokipona scene4_dcd2ce5e:

    # hero "Sakura-chan, are you okay? Are you hurt?"
    hero ""

# game/dialogs.rpy:823
translate tokipona scene4_82675361:

    # "She didn't say a word. I took my bottle of water and gave it to her."
    ""

# game/dialogs.rpy:824
translate tokipona scene4_44a2c36a:

    # hero "Here, drink. Try to calm down."
    hero ""

# game/dialogs.rpy:825
translate tokipona scene4_ecea0509:

    # s "T... thank you..."
    s ""

# game/dialogs.rpy:826
translate tokipona scene4_0196f5ab:

    # "She drank from the bottle slowly. Tears were running down her face. She was definitely traumatized by what just happened."
    ""

# game/dialogs.rpy:827
translate tokipona scene4_e638db20:

    # "Or maybe it was just the refreshing feeling of drinking water, bringing her back to reality. I couldn't tell..."
    ""

# game/dialogs.rpy:828
translate tokipona scene4_76b6a439:

    # hero "It's okay, they're gone... They won't bother you anymore..."
    hero ""

# game/dialogs.rpy:829
translate tokipona scene4_46f4b04d:

    # "I stayed with Sakura in the area for a while, waiting for her to recover."
    ""

# game/dialogs.rpy:830
translate tokipona scene4_2bd9d9ea:

    # "Why did those guys bullied her like that?"
    ""

# game/dialogs.rpy:831
translate tokipona scene4_50b0bbf2:

    # "There's no way an innocent girl like her could be a guy in disguise! No way!"
    ""

# game/dialogs.rpy:832
translate tokipona scene4_4c3197ed:

    # "Maybe they picked on her because she has some tomboyish tastes or something...?"
    ""

# game/dialogs.rpy:833
translate tokipona scene4_a20e210b:

    # "Whatever, that was disgusting of them!"
    ""

# game/dialogs.rpy:834
translate tokipona scene4_3664d290:

    # "In fact, nothing they said made any sense... I still don't get it..."
    ""

# game/dialogs.rpy:835
translate tokipona scene4_846b18ad:

    # "Finally, after Sakura calmed down a bit, we got up and went to school."
    ""

# game/dialogs.rpy:839
translate tokipona scene4_c8300016:

    # "We silently made our way to school together."
    ""

# game/dialogs.rpy:840
translate tokipona scene4_291de2b0:

    # "I was worried. I think I'll need the help of Rika-chan."
    ""

# game/dialogs.rpy:847
translate tokipona scene4_2eae0bd6:

    # hero "...and that's what happened this morning."
    hero ""

# game/dialogs.rpy:848
translate tokipona scene4_4ddab44b:

    # "I didn't bother telling Rika what the bullies actually said to Sakura. Just all the rest."
    ""

# game/dialogs.rpy:849
translate tokipona scene4_6d41b106:

    # "Anyway, I'm pretty sure what they were saying doesn't even matter. They just made up a reason to bully her."
    ""

# game/dialogs.rpy:850
translate tokipona scene4_60f0cbde:

    # n "Those bastards!"
    n ""

# game/dialogs.rpy:851
translate tokipona scene4_cf610c40:

    # r "They were lucky that I wasn't there! I'm pretty good at trashing boys!"
    r ""

# game/dialogs.rpy:852
translate tokipona scene4_ab36d9dc:

    # s "It's okay, Rika-chan. They ran away when %(stringhero)s-kun came to rescue me."
    s ""

# game/dialogs.rpy:854
translate tokipona scene4_9f03aed3:

    # s "In fact, without his help, I don't know what would have happened..."
    s ""

# game/dialogs.rpy:855
translate tokipona scene4_188ed7b1:

    # s "Thank you... Thank you, %(stringhero)s-kun!"
    s ""

# game/dialogs.rpy:856
translate tokipona scene4_c1b029c3:

    # "Sakura bowed to me. I wasn't sure what to say."
    ""

# game/dialogs.rpy:860
translate tokipona scene4_03382661:

    # r "Well, I guess I have to thank you for helping Sakura-chan %(stringhero)s."
    r ""

# game/dialogs.rpy:861
translate tokipona scene4_77f35da3:

    # hero "It's okay, it's okay. After all, it's my duty to save damsels in distress!"
    hero ""

# game/dialogs.rpy:862
translate tokipona scene4_aadcce64:

    # "Sakura laughed and finally smiled."
    ""

# game/dialogs.rpy:863
translate tokipona scene4_f25fd7bb:

    # hero "Sakura-chan..."
    hero ""

# game/dialogs.rpy:864
translate tokipona scene4_75a10854:

    # hero "I promise you right now."
    hero ""

# game/dialogs.rpy:865
translate tokipona scene4_e7c0608a:

    # hero "I'll dedicate myself to escorting you to and from school everyday!"
    hero ""

# game/dialogs.rpy:867
translate tokipona scene4_6d4d7801:

    # s "Really? You promise?"
    s ""

# game/dialogs.rpy:869
translate tokipona scene4_c89aabf6:

    # s "Oh, thank you, %(stringhero)s-kun! {image=heart.png}"
    s ""

# game/dialogs.rpy:871
translate tokipona scene4_6bd67288:

    # r "Hmph! Fine, I'll accept this for now...only because I live the exact opposite direction from her house!"
    r ""

# game/dialogs.rpy:872
translate tokipona scene4_12240494:

    # r "If I could, I would already be escorting her everyday!"
    r ""

# game/dialogs.rpy:874
translate tokipona scene4_87cde803:

    # n "Same for me..."
    n ""

# game/dialogs.rpy:875
translate tokipona scene4_641491a3:

    # r "I just know I can count on you, %(stringhero)s."
    r ""

# game/dialogs.rpy:876
translate tokipona scene4_b799ab6b:

    # "I smiled."
    ""

# game/dialogs.rpy:877
translate tokipona scene4_ea6ddde8:

    # "Rika really does seem like a tsundere but I found it pretty lovable."
    ""

# game/dialogs.rpy:878
translate tokipona scene4_4401cc13:

    # "I felt even better when I saw Sakura's bright smile."
    ""

# game/dialogs.rpy:883
translate tokipona scene4_77f4ec72:

    # r "Anyways..."
    r ""

# game/dialogs.rpy:884
translate tokipona scene4_72233eeb:

    # r "I have an important announcement for the members of the club!"
    r ""

# game/dialogs.rpy:885
translate tokipona scene4_40d86de1:

    # s "Yes?"
    s ""

# game/dialogs.rpy:886
translate tokipona scene4_5ebd74f9:

    # hero "Yes, ma'am!"
    hero ""

# game/dialogs.rpy:887
translate tokipona scene4_2dd18f3e:

    # n "What is it?"
    n ""

# game/dialogs.rpy:888
translate tokipona scene4_8afe26e4:

    # r "Two weeks from now, the summer festival will be happening in the village. And the club must meet together on this day and enjoy the festival!"
    r ""

# game/dialogs.rpy:889
translate tokipona scene4_b181bb98:

    # hero "A summer festival? Really?"
    hero ""

# game/dialogs.rpy:890
translate tokipona scene4_7ea87b78:

    # "I've heard about japanese festivals like the Matsuri."
    ""

# game/dialogs.rpy:891
translate tokipona scene4_0a708135:

    # "They're very popular in older villages. People do things like put on yukatas and visit food stands. There's also a lot games, traditional dances and even a fireworks display at the very end."
    ""

# game/dialogs.rpy:892
translate tokipona scene4_2339b27a:

    # "I've only see them in anime. I've always wanted to go to a real one!"
    ""

# game/dialogs.rpy:893
translate tokipona scene4_97a399b0:

    # r "You never went to any summer festival? You city rat!"
    r ""

# game/dialogs.rpy:894
translate tokipona scene4_e1074e1b:

    # hero "Hey, shut up!"
    hero ""

# game/dialogs.rpy:895
translate tokipona scene4_b091f4a6:

    # s "You'll come to realize, the summer festival of this village is absolutely amazing."
    s ""

# game/dialogs.rpy:896
translate tokipona scene4_15e427d2:

    # s "Sounds like it'll be a lot of fun!"
    s ""

# game/dialogs.rpy:897
translate tokipona scene4_0fcfce33:

    # hero "And no need to take the train this time, right?"
    hero ""

# game/dialogs.rpy:898
translate tokipona scene4_970c7b2d:

    # s "True! *{i}giggles{/i}*"
    s ""

# game/dialogs.rpy:899
translate tokipona scene4_6e7d7bc8:

    # r "I hope you have your yukata, city rat!"
    r ""

# game/dialogs.rpy:900
translate tokipona scene4_7c767f63:

    # hero "Hey! I do!"
    hero ""

# game/dialogs.rpy:901
translate tokipona scene4_2322fa79:

    # "Actually, I didn't. I just wanted her to quit nagging me."
    ""

# game/dialogs.rpy:908
translate tokipona scene4_5805cafa:

    # "As I promised with Sakura-chan, I escorted her on the way home, after school."
    ""

# game/dialogs.rpy:909
translate tokipona scene4_6713403c:

    # "Ah geez...It'll be a problem if I don't have a yukata before the summer festival. Rika would never let it go."
    ""

# game/dialogs.rpy:910
translate tokipona scene4_df7e7b2d:

    # "I have to find one as quickly as possible."
    ""

# game/dialogs.rpy:911
translate tokipona scene4_20088daa:

    # "I know wearing a yukata isn't mandatory for summer festivals, but I just want to show that tsundere that I'm not the city rat she thinks I am."
    ""

# game/dialogs.rpy:912
translate tokipona scene4_2100c01f:

    # hero "Sakura-chan?"
    hero ""

# game/dialogs.rpy:913
translate tokipona scene4_40d86de1_1:

    # s "Yes?"
    s ""

# game/dialogs.rpy:914
translate tokipona scene4_bf7e25cb:

    # hero "Are you free tomorrow?"
    hero ""

# game/dialogs.rpy:915
translate tokipona scene4_9b65d1b6:

    # s "Yes, why?"
    s ""

# game/dialogs.rpy:916
translate tokipona scene4_b543397f:

    # hero "Well..."
    hero ""

# game/dialogs.rpy:917
translate tokipona scene4_d5ca9051:

    # hero "I'm not familiar with the big city yet..."
    hero ""

# game/dialogs.rpy:918
translate tokipona scene4_fb7dbaed:

    # hero "And I'd like to go there tomorrow..."
    hero ""

# game/dialogs.rpy:919
translate tokipona scene4_55bf8a2d:

    # hero "So I was wondering if you would..."
    hero ""

# game/dialogs.rpy:920
translate tokipona scene4_4cf796da:

    # hero "... come... with me...?"
    hero ""

# game/dialogs.rpy:921
translate tokipona scene4_0dc65489:

    # "Those last few words were hard to say. It's like I'm asking her out on a date!"
    ""

# game/dialogs.rpy:922
translate tokipona scene4_02bf7f98:

    # "No... Wait... I'm totally asking her on a date right now!"
    ""

# game/dialogs.rpy:924
translate tokipona scene4_01b0fe5a:

    # s "Of course! I'd love to come with you to the city! {image=heart.png}"
    s ""

# game/dialogs.rpy:925
translate tokipona scene4_c2837dd9:

    # hero "Really? Thanks!"
    hero ""

# game/dialogs.rpy:926
translate tokipona scene4_12d152e0:

    # hero "I couldn't think of a better guide than you!"
    hero ""

# game/dialogs.rpy:927
translate tokipona scene4_4b122407:

    # "We giggled. She blushed."
    ""

# game/dialogs.rpy:928
translate tokipona scene4_2e510646:

    # hero "I'll see you at the train station at 9am."
    hero ""

# game/dialogs.rpy:929
translate tokipona scene4_4131ec23:

    # "Perfect! Just according to keikaku... I mean, the plan."
    ""

# game/dialogs.rpy:930
translate tokipona scene4_2f4016d4:

    # "Not only will this tour in the city inform me of where all the important shops are, but I also can find a yukata!"
    ""

# game/dialogs.rpy:932
translate tokipona scene4_de73d541:

    # "I think she just saved me again."
    ""

# game/dialogs.rpy:934
translate tokipona scene4_586d150a:

    # "I think she just saved me."
    ""

# game/dialogs.rpy:935
translate tokipona scene4_8713b332:

    # "I can't wait to go to the city with Sakura tomorrow!"
    ""

# game/dialogs.rpy:944
translate tokipona scene5_83f03a3f:

    # "The next morning."
    ""

# game/dialogs.rpy:945
translate tokipona scene5_432ea515:

    # "I was at the train station in the village. I'm not even sure that it was a station. It's so small and old."
    ""

# game/dialogs.rpy:946
translate tokipona scene5_55ba7152:

    # "And so small... I didn't know there were such small train stations in the country..."
    ""

# game/dialogs.rpy:947
translate tokipona scene5_ee63449f:

    # s "%(stringhero)s-kuuuuuuuuuuuuun!!!"
    s ""

# game/dialogs.rpy:948
translate tokipona scene5_09ca9e90:

    # "Sakura arrived."
    ""

# game/dialogs.rpy:949
translate tokipona scene5_f274dee3:

    # "She was wearing a small blue summer dress, a striped shirt, and cute laced shoes that didn't hide her tiny feet."
    ""

# game/dialogs.rpy:950
translate tokipona scene5_535385a4:

    # "I usually find school uniforms attractive, but I have to admit that the casual outfit she was wearing suits her so much."
    ""

# game/dialogs.rpy:953
translate tokipona scene5_7eeab041:

    # s "Good morning, %(stringhero)s-kun!{p}Sorry for the wait!"
    s ""

# game/dialogs.rpy:954
translate tokipona scene5_a5707cad:

    # hero "Good morning, Sakura-chan!"
    hero ""

# game/dialogs.rpy:956
translate tokipona scene5_8736489d:

    # s "Ready to visit the city?"
    s ""

# game/dialogs.rpy:957
translate tokipona scene5_eef83bf6:

    # s "It's not as big as Tokyo for sure, but there's a lot more there than the village!"
    s ""

# game/dialogs.rpy:958
translate tokipona scene5_fc3b562c:

    # hero "I guess you'll be my tour guide, senpai!"
    hero ""

# game/dialogs.rpy:959
translate tokipona scene5_c2497b21:

    # "I laughed a bit but I kinda realized something..."
    ""

# game/dialogs.rpy:960
translate tokipona scene5_2326053d:

    # "If I go to the city to buy a yukata, she'll obviously notice it."
    ""

# game/dialogs.rpy:961
translate tokipona scene5_1b0c84a6:

    # "But if I don't, Rika will torment me at the festival."
    ""

# game/dialogs.rpy:965
translate tokipona scene5_2100c01f:

    # hero "Sakura-chan?"
    hero ""

# game/dialogs.rpy:966
translate tokipona scene5_40d86de1:

    # s "Yes?"
    s ""

# game/dialogs.rpy:984
translate tokipona scene5_20b30901:

    # hero "Yesterday...I lied. I don't have any yukata."
    hero ""

# game/dialogs.rpy:985
translate tokipona scene5_adb520a7:

    # hero "In fact, I was planning to buy one in the city while visiting it with you..."
    hero ""

# game/dialogs.rpy:987
translate tokipona scene5_584030bd:

    # s "Is that so?"
    s ""

# game/dialogs.rpy:989
translate tokipona scene5_ef055bc2:

    # s "It's okay! I'll help you to find one!"
    s ""

# game/dialogs.rpy:990
translate tokipona scene5_404c741c:

    # hero "Really? That's a relief! Thanks!"
    hero ""

# game/dialogs.rpy:991
translate tokipona scene5_12d1e584:

    # hero "I said I had one to avoid Rika-chan nagging me."
    hero ""

# game/dialogs.rpy:992
translate tokipona scene5_a7f1d53d:

    # s "Rika-chan likes to give you a hard time, but I'm sure she means well!"
    s ""

# game/dialogs.rpy:993
translate tokipona scene5_fb14ae9d:

    # s "Our Rika-chan is very unique!"
    s ""

# game/dialogs.rpy:994
translate tokipona scene5_fe794b00:

    # "I sighed with relief and laughed with Sakura."
    ""

# game/dialogs.rpy:995
translate tokipona scene5_a45c2161:

    # "I'm glad she's so understanding!"
    ""

# game/dialogs.rpy:999
translate tokipona scene5_e1b13f3b:

    # "Hmm... no... It's bad if I tell her..."
    ""

# game/dialogs.rpy:1000
translate tokipona scene5_7857c1c2:

    # "I'll just go out to have fun with her today. Once I see a yukata shop, I'll take note and I'll will come back tomorrow alone to buy it."
    ""

# game/dialogs.rpy:1001
translate tokipona scene5_8ba6ee94:

    # "Yeah, that should be a good idea..."
    ""

# game/dialogs.rpy:1003
translate tokipona scene5_1e69c51f:

    # s "%(stringhero)s-kun, are you okay?"
    s ""

# game/dialogs.rpy:1004
translate tokipona scene5_fcde3cfe:

    # hero "Oh, yes, nevermind... I was daydreaming again..."
    hero ""

# game/dialogs.rpy:1011
translate tokipona scene5_c85106ec:

    # "The train arrived and we boarded it towards the city."
    ""

# game/dialogs.rpy:1012
translate tokipona scene5_97c5ca7d:

    # "The city was three stations ahead. About 30 minutes of travel time. Sakura and I made small talk on the train."
    ""

# game/dialogs.rpy:1014
translate tokipona scene5_66ca24f3:

    # "We finally arrived at the city."
    ""

# game/dialogs.rpy:1015
translate tokipona scene5_0a3e36c2:

    # "It really is pretty big. Just as Sakura said."
    ""

# game/dialogs.rpy:1017
translate tokipona scene5_0c293bd8:

    # s "Here we are!"
    s ""

# game/dialogs.rpy:1018
translate tokipona scene5_5e4b4e2f:

    # s "The shopping area isn't too far. Only two streets away."
    s ""

# game/dialogs.rpy:1019
translate tokipona scene5_75ad6856:

    # hero "Great! Let's get going, senpai!"
    hero ""

# game/dialogs.rpy:1020
translate tokipona scene5_9e4c1aaf:

    # "Sakura smiled and we started walking through the busy streets."
    ""

# game/dialogs.rpy:1024
translate tokipona scene5_1b70d467:

    # "After some time, we finally reached a street full of a variety of different shops."
    ""

# game/dialogs.rpy:1025
translate tokipona scene5_454785ef:

    # "Clothing, multimedia, restaurants, arcades,..."
    ""

# game/dialogs.rpy:1026
translate tokipona scene5_a2720e0f:

    # "They had everything here!"
    ""

# game/dialogs.rpy:1027
translate tokipona scene5_4f036fa9:

    # "I hasn't been shopping since I moved from Tokyo. I was pretty excited."
    ""

# game/dialogs.rpy:1029
translate tokipona scene5_0b0fbfd9:

    # "But first things first, I have to find a yukata shop."
    ""

# game/dialogs.rpy:1030
translate tokipona scene5_69e0143d:

    # "So we started with the clothes shops."
    ""

# game/dialogs.rpy:1033
translate tokipona scene5_8534ca42:

    # ". . ."
    ""

# game/dialogs.rpy:1034
translate tokipona scene5_aceff1a2:

    # "Argh!!"
    ""

# game/dialogs.rpy:1035
translate tokipona scene5_249d4d18:

    # "There's absolutely no yukata under the price of ¥200,000!"
    ""

# game/dialogs.rpy:1036
translate tokipona scene5_6a82deb5:

    # "It's definitely too expensive for the budget I had."
    ""

# game/dialogs.rpy:1038
translate tokipona scene5_3101ced6:

    # "As I was checking the prices, I noticed Sakura seemed to be thinking about something."
    ""

# game/dialogs.rpy:1039
translate tokipona scene5_01c5b3f7:

    # "She looked kinda sorry that I couldn't afford a yukata for myself."
    ""

# game/dialogs.rpy:1040
translate tokipona scene5_0ddddc7d:

    # hero "Nah, forget it.{p}After all, it's not like my life depends on getting one."
    hero ""

# game/dialogs.rpy:1041
translate tokipona scene5_a727d569:

    # hero "Although...I just hope Rika forgot about it already. I should brace myself for anything..."
    hero ""

# game/dialogs.rpy:1042
translate tokipona scene5_41a74897:

    # hero "Let's go somewhere else for now..."
    hero ""

# game/dialogs.rpy:1043
translate tokipona scene5_5c76b703:

    # s "Hmm... Okay..."
    s ""

# game/dialogs.rpy:1048
translate tokipona scene5_041a4807:

    # s "So, where do you want to go? Is there anything you want to see?"
    s ""

# game/dialogs.rpy:1049
translate tokipona scene5_72bc6d39:

    # hero "How about checking out manga in the bookstores?"
    hero ""

# game/dialogs.rpy:1050
translate tokipona scene5_b6d74bf5:

    # hero "That could be useful for the club!"
    hero ""

# game/dialogs.rpy:1052
translate tokipona scene5_fa9a9f16:

    # s "Nice idea! Let's go!"
    s ""

# game/dialogs.rpy:1056
translate tokipona scene5_2cd8d6de:

    # "The manga bookstore was gigantic!"
    ""

# game/dialogs.rpy:1057
translate tokipona scene5_3f7f2473:

    # "It was three floors full of manga as far as the eye could see!"
    ""

# game/dialogs.rpy:1058
translate tokipona scene5_17b06ea6:

    # "I start by browsing through the shonen harem manga, since it's my favorite genre."
    ""

# game/dialogs.rpy:1059
translate tokipona scene5_54cf672e:

    # "I found all the volumes of {i}High School Samurai{/i}, but I had them already."
    ""

# game/dialogs.rpy:1060
translate tokipona scene5_e98d6302:

    # "Then I saw a series I didn't anything know, called {i}Uchuu Tenshi Moechan{/i}. I found the manga cover to be quite attractive. As I was reaching out to grab a volume..."
    ""

# game/dialogs.rpy:1062
translate tokipona scene5_2c810467:

    # "My hand touched another which went towards the same book. We both retracted our hands back quickly."
    ""

# game/dialogs.rpy:1063
translate tokipona scene5_931b518d:

    # hero "Oh, I'm so sorry..."
    hero ""

# game/dialogs.rpy:1064
translate tokipona scene5_56a93df9:

    # s "Sorry, my bad..."
    s ""

# game/dialogs.rpy:1065
translate tokipona scene5_8534ca42_1:

    # ". . ."
    ""

# game/dialogs.rpy:1066
translate tokipona scene5_cc9a5909:

    # "!!!"
    ""

# game/dialogs.rpy:1068
translate tokipona scene5_85b2e7cc:

    # "Sakura!?"
    ""

# game/dialogs.rpy:1069
translate tokipona scene5_a3335b11:

    # "I was a little caught off guard."
    ""

# game/dialogs.rpy:1071
translate tokipona scene5_960abe9a:

    # "Well only a little, since I knew that she liked {i}High School Samurai{/i} .{w} But I never imagined that she would like shonen harem manga that were aimed towards guys!"
    ""

# game/dialogs.rpy:1072
translate tokipona scene5_adf46165:

    # hero "Heh so...you must really like that genre huh, Sakura-chan?"
    hero ""

# game/dialogs.rpy:1073
translate tokipona scene5_cd229797:

    # s "Well... y-yes..."
    s ""

# game/dialogs.rpy:1074
translate tokipona scene5_0735783a:

    # "She was blushing... Looks like it was embarrassing for her to talk about it."
    ""

# game/dialogs.rpy:1075
translate tokipona scene5_40d8c136:

    # s "I... I like these kinds of manga... The girls in them are very attractive..."
    s ""

# game/dialogs.rpy:1076
translate tokipona scene5_cde483c5:

    # s "I wish I was as beautiful as the women in these manga..."
    s ""

# game/dialogs.rpy:1077
translate tokipona scene5_6ce4f6f3:

    # "That's kinda adorable."
    ""

# game/dialogs.rpy:1078
translate tokipona scene5_b7cf4dac:

    # "Honestly though, she's far prettier than any girl I've seen in any shonen manga!"
    ""

# game/dialogs.rpy:1079
translate tokipona scene5_7658f518:

    # "If I wasn't such a nervous wreck half the time, I'd say it again to her face..."
    ""

# game/dialogs.rpy:1080
translate tokipona scene5_f50ba84f:

    # "But I just couldn't... I lacked the courage to say it out loud."
    ""

# game/dialogs.rpy:1081
translate tokipona scene5_e3f223bb:

    # "My mind started to wander off about touching her hand by accident."
    ""

# game/dialogs.rpy:1082
translate tokipona scene5_25f48c38:

    # "It was so soft... It seemed almost surreal..."
    ""

# game/dialogs.rpy:1083
translate tokipona scene5_875908a3:

    # "I realized that I was daydreaming, and quickly regained my composure."
    ""

# game/dialogs.rpy:1084
translate tokipona scene5_aa64fa57:

    # hero "Oh...Uhh...So do you know what the story is about?"
    hero ""

# game/dialogs.rpy:1086
translate tokipona scene5_dc5fd421:

    # s "Oh, it's a nice one."
    s ""

# game/dialogs.rpy:1087
translate tokipona scene5_9b8c5139:

    # s "It's the story about a boy from Earth who is kidnapped by female aliens because they think he is a god that will save their planet."
    s ""

# game/dialogs.rpy:1088
translate tokipona scene5_85fb97ae:

    # s "They all want to go out with him but he's very shy; plus he has to fight against an horrible and powerful entity..."
    s ""

# game/dialogs.rpy:1089
translate tokipona scene5_f86e719b:

    # s "But fortunately, the girls have super powers and he fights alongside the girls..."
    s ""

# game/dialogs.rpy:1090
translate tokipona scene5_ea859548:

    # s "It's kinda funny and heroic at same time!"
    s ""

# game/dialogs.rpy:1091
translate tokipona scene5_b293cdd7:

    # hero "That sounds pretty entertaining!"
    hero ""

# game/dialogs.rpy:1092
translate tokipona scene5_fa4d34a0:

    # hero "Maybe I should start reading it..."
    hero ""

# game/dialogs.rpy:1094
translate tokipona scene5_40a4c6ba:

    # s "I can lend you the first few volumes at school if you want!"
    s ""

# game/dialogs.rpy:1095
translate tokipona scene5_7fe42341:

    # hero "That would be great! Thanks, Sakura-chan!"
    hero ""

# game/dialogs.rpy:1099
translate tokipona scene5_86f22ee4:

    # "After looking all around the store, we finally ended up leaving the shop."
    ""

# game/dialogs.rpy:1100
translate tokipona scene5_b7801019:

    # "She bought the new volume of {i}Moechan{/i} while I got the first volume of {i}OGT{/i}, a seinen that I heard of a long time ago."
    ""

# game/dialogs.rpy:1103
translate tokipona scene5_5ebb4f12:

    # s "So, what else do you want to do?"
    s ""

# game/dialogs.rpy:1104
translate tokipona scene5_f7ff6212:

    # hero "How about the arcade?"
    hero ""

# game/dialogs.rpy:1105
translate tokipona scene5_d8b99f39:

    # hero "I'm curious to see how good you are at shooting games."
    hero ""

# game/dialogs.rpy:1107
translate tokipona scene5_5b8b0c8c:

    # s "Teehee! Alright, let's go! But I won't hold back!"
    s ""

# game/dialogs.rpy:1111
translate tokipona scene5_fb2b36bb:

    # "We played a game of {i}Silent Crisis{/i} together."
    ""

# game/dialogs.rpy:1112
translate tokipona scene5_e30e02a0:

    # "She was definitely a pro at it!"
    ""

# game/dialogs.rpy:1113
translate tokipona scene5_6f318a21:

    # "She knew exactly where the zombies spawned and shot them before they could even attack!"
    ""

# game/dialogs.rpy:1114
translate tokipona scene5_1c41f171:

    # "I ended up using all my credits since I died over and over, while she didn't lose a single life at all!"
    ""

# game/dialogs.rpy:1115
translate tokipona scene5_ac2581f9:

    # hero "Wow... Sakura-chan, you're definitely better than I am..."
    hero ""

# game/dialogs.rpy:1116
translate tokipona scene5_509bc659:

    # hero "You know the game by heart or something?"
    hero ""

# game/dialogs.rpy:1117
translate tokipona scene5_7ab00054:

    # "She was concentrated on the game."
    ""

# game/dialogs.rpy:1118
translate tokipona scene5_924b98a0:

    # s "Not really... In fact, it's only the third time I played this one."
    s ""

# game/dialogs.rpy:1119
translate tokipona scene5_8c2adcf7:

    # s "I think it's my reflexes that make me good at these types of games."
    s ""

# game/dialogs.rpy:1121
translate tokipona scene5_7846d5b4:

    # "???" "Hey guys! You're here too?"
    "???" ""

# game/dialogs.rpy:1127
translate tokipona scene5_43c67a2f:

    # hero "Oh! Nanami-chan!"
    hero ""

# game/dialogs.rpy:1128
translate tokipona scene5_640b15c3:

    # s "Hey Nana-chan!"
    s ""

# game/dialogs.rpy:1129
translate tokipona scene5_bccc3e1d:

    # hero "What are you doing here?"
    hero ""

# game/dialogs.rpy:1131
translate tokipona scene5_dbdeb2ab:

    # n "Duh, playing arcade games, you dork!"
    n ""

# game/dialogs.rpy:1132
translate tokipona scene5_86073654:

    # hero "Eh? I'm a dork now?"
    hero ""

# game/dialogs.rpy:1133
translate tokipona scene5_a555e2b6:

    # n "Yes, you're a dork dude! {image=heart.png}"
    n ""

# game/dialogs.rpy:1134
translate tokipona scene5_040d24b0:

    # "Sakura laughed so much at the lame joke, she almost took a hit in the game."
    ""

# game/dialogs.rpy:1135
translate tokipona scene5_013529c0:

    # "Still, it's incredible how she can do multiple things at the same time. I heard it's an ability that girls naturally have.{p}I kinda envy them."
    ""

# game/dialogs.rpy:1137
translate tokipona scene5_38444d54:

    # "Nanami put a token in the arcade machine and pressed the button labeled 'Player 2'."
    ""

# game/dialogs.rpy:1139
translate tokipona scene5_a015009c:

    # n "Looks like you could use some back up!"
    n ""

# game/dialogs.rpy:1140
translate tokipona scene5_4bf0d20d:

    # "Then Nanami started to shoot at the virtual mobs in the game with Sakura."
    ""

# game/dialogs.rpy:1142
translate tokipona scene5_2bae2874:

    # n "Hey, %(stringhero)s-senpai, do you know that some video game experts are saying that the golden age of arcade games is coming to an end?"
    n ""

# game/dialogs.rpy:1143
translate tokipona scene5_e7066814:

    # hero "Really?"
    hero ""

# game/dialogs.rpy:1144
translate tokipona scene5_b3e58373:

    # hero "But arcade games seem pretty recent, though..."
    hero ""

# game/dialogs.rpy:1145
translate tokipona scene5_2f385a6e:

    # n "It's because of the Internet and online gaming. It's taking more and more room in the gaming domain."
    n ""

# game/dialogs.rpy:1146
translate tokipona scene5_c0e7820a:

    # n "Soon there won't be as many arcades as there are now. Some are even afraid that arcade games will just end up being boring and flat compared to what you can find online."
    n ""

# game/dialogs.rpy:1147
translate tokipona scene5_34b00fac:

    # n "At least rhythm games that need specific peripherals like {i}Para Para Revolution{/i} will be alright!"
    n ""

# game/dialogs.rpy:1148
translate tokipona scene5_5b2c12b3:

    # s "Oooh I love that game!"
    s ""

# game/dialogs.rpy:1149
translate tokipona scene5_b5fe983b:

    # hero "Ah..."
    hero ""

# game/dialogs.rpy:1150
translate tokipona scene5_7c35731c:

    # hero "Well, what about making arcade games that can be played online?"
    hero ""

# game/dialogs.rpy:1151
translate tokipona scene5_b075ad7f:

    # n "Hmm..."
    n ""

# game/dialogs.rpy:1152
translate tokipona scene5_0817fef1:

    # n "Hey, maybe you're getting at something, senpai!"
    n ""

# game/dialogs.rpy:1153
translate tokipona scene5_88898146:

    # s "I'm not sure that would work for rhythm games..."
    s ""

# game/dialogs.rpy:1154
translate tokipona scene5_25648fa4:

    # "While Nanami was talking about the history of arcade games, she was still doing an amazing job shooting at monsters on the screen."
    ""

# game/dialogs.rpy:1159
translate tokipona scene5_70915e9e:

    # "After a while, Sakura finally lost her last life. Even Nanami was struggling to stay alive."
    ""

# game/dialogs.rpy:1160
translate tokipona scene5_1c665d0e:

    # "Nanami was pretty good, but not as good as when she's playing fighting or puzzle games..."
    ""

# game/dialogs.rpy:1161
translate tokipona scene5_c09efcd8:

    # hero "Looks like we found a game she's not a master of!"
    hero ""

# game/dialogs.rpy:1162
translate tokipona scene5_b4b28eb9:

    # s "Well, nobody's perfect, right?"
    s ""

# game/dialogs.rpy:1165
translate tokipona scene5_ca00aade:

    # s "You know,..."
    s ""

# game/dialogs.rpy:1166
translate tokipona scene5_29a8910b:

    # s "Nana-chan...{w}she... She is more fragile than she looks..."
    s ""

# game/dialogs.rpy:1167
translate tokipona scene5_6d59e0e0:

    # hero "What do you mean?"
    hero ""

# game/dialogs.rpy:1168
translate tokipona scene5_af6cfb46:

    # s "Well, something happened to her that makes her pretty introverted and nervous around people she doesn't know..."
    s ""

# game/dialogs.rpy:1169
translate tokipona scene5_eb73848e:

    # hero "What happened?"
    hero ""

# game/dialogs.rpy:1171
translate tokipona scene5_557b9c7c:

    # s ". . ."
    s ""

# game/dialogs.rpy:1172
translate tokipona scene5_b475ba93:

    # s "I shouldn't tell you more, I'm sorry for bringing it up. I prefer she tells you herself..."
    s ""

# game/dialogs.rpy:1175
translate tokipona scene5_8811f347:

    # s "But on a more positive note,. {p}it's the first time I've seen her speak so much with someone she's only known for a few days."
    s ""

# game/dialogs.rpy:1176
translate tokipona scene5_f97de379:

    # s "That's a pretty good sign for you!"
    s ""

# game/dialogs.rpy:1177
translate tokipona scene5_291c83e9:

    # s "Maybe you'll be one of her best friends!"
    s ""

# game/dialogs.rpy:1178
translate tokipona scene5_723e1b93:

    # hero "Does she has alot of best friends?"
    hero ""

# game/dialogs.rpy:1180
translate tokipona scene5_94da3bfe:

    # s "So far there's only Rika and me."
    s ""

# game/dialogs.rpy:1182
translate tokipona scene5_9a43a0d7:

    # s "I'm sure she'll tell you herself someday, if she trusts you enough."
    s ""

# game/dialogs.rpy:1183
translate tokipona scene5_6a223ad6:

    # s "Anyways, don't mind her if she doesn't speak too much. Befriending her takes time."
    s ""

# game/dialogs.rpy:1184
translate tokipona scene5_b6cbfe55:

    # hero "I understand...{p}Does she trust a lot of people?"
    hero ""

# game/dialogs.rpy:1185
translate tokipona scene5_84d423c2:

    # s "So far, she only trusts her big brother, Rika and me."
    s ""

# game/dialogs.rpy:1186
translate tokipona scene5_ed700bc4:

    # s "I'm sure you'll be next on the list sooner or later!"
    s ""

# game/dialogs.rpy:1188
translate tokipona scene5_e668a3b6:

    # hero "I see..."
    hero ""

# game/dialogs.rpy:1189
translate tokipona scene5_0f2a7110:

    # hero "I'll do my best to be a good friend to her."
    hero ""

# game/dialogs.rpy:1191
translate tokipona scene5_e866394b:

    # s "That's great to hear, %(stringhero)s-kun!"
    s ""

# game/dialogs.rpy:1192
translate tokipona scene5_d01d2141:

    # s "I'm sure someday you and her will be best friends!"
    s ""

# game/dialogs.rpy:1193
translate tokipona scene5_e741d135:

    # "For sure, but I wonder what Nanami thinks of me right now..."
    ""

# game/dialogs.rpy:1194
translate tokipona scene5_4464bb7e:

    # "And I'm wondering what Sakura thinks of me as well..."
    ""

# game/dialogs.rpy:1199
translate tokipona scene5_2f43b611:

    # "After Nanami finished playing, we ate some yakitori at a fast food restaurant together."
    ""

# game/dialogs.rpy:1200
translate tokipona scene5_b84a1878:

    # "Then we boarded the train to return to the village."
    ""

# game/dialogs.rpy:1201
translate tokipona scene5_b6fe7fc0:

    # "The three of us had a pretty fun day together."
    ""

# game/dialogs.rpy:1203
translate tokipona scene5_841a64f2:

    # "Nanami waved goodbye to us as she left to get back home."
    ""

# game/dialogs.rpy:1205
translate tokipona scene5_be1b33c3:

    # "Sakura and I were alone once again."
    ""

# game/dialogs.rpy:1207
translate tokipona scene5_4fcb2681:

    # "Sakura seemed to be deep in thought about something again."
    ""

# game/dialogs.rpy:1209
translate tokipona scene5_5dcdb866:

    # "As we arrived at Sakura's house, Sakura spoke."
    ""

# game/dialogs.rpy:1210
translate tokipona scene5_007ceac2:

    # s "%(stringhero)s-kun..."
    s ""

# game/dialogs.rpy:1211
translate tokipona scene5_a4db12cc:

    # hero "What's up?"
    hero ""

# game/dialogs.rpy:1213
translate tokipona scene5_0f62d32d:

    # s "I think I have an idea! Just wait here a minute!"
    s ""

# game/dialogs.rpy:1215
translate tokipona scene5_91bb60f0:

    # "She ran into her house and came back with something. It looked like a set neatly wrapped of clothes."
    ""

# game/dialogs.rpy:1217
translate tokipona scene5_8941c000:

    # s "Here... Please, take it..."
    s ""

# game/dialogs.rpy:1218
translate tokipona scene5_e8a32052:

    # s "It's a bit small but you can make it bigger with some touch-ups."
    s ""

# game/dialogs.rpy:1219
translate tokipona scene5_a7e82bdc:

    # "I unwrapped the set of clothes. {p}I couldn't believe it!"
    ""

# game/dialogs.rpy:1220
translate tokipona scene5_b12f3dbd:

    # "It was a blue yukata!"
    ""

# game/dialogs.rpy:1221
translate tokipona scene5_10b50536:

    # "I chuckled for a second. For a second, I thought it was one of her own yukata. I was laughing imagining how I would look like in a yukata made for girls."
    ""

# game/dialogs.rpy:1222
translate tokipona scene5_d616579f:

    # "But after a closer look, I notice that it was really a yukata for men, with the right cuttings and colors!"
    ""

# game/dialogs.rpy:1223
translate tokipona scene5_ac50db4a:

    # hero "Whoa... For real? I can borrow this?"
    hero ""

# game/dialogs.rpy:1225
translate tokipona scene5_3e45378b:

    # s "You can keep it if you want, %(stringhero)s-kun, it's okay!"
    s ""

# game/dialogs.rpy:1226
translate tokipona scene5_68f22fc9:

    # hero "Gee...thank you... Thank you so much!"
    hero ""

# game/dialogs.rpy:1227
translate tokipona scene5_882e22b6:

    # hero "Well... I gotta go... See you at school, Sakura-chan!"
    hero ""

# game/dialogs.rpy:1228
translate tokipona scene5_1a4bf588:

    # s "Have a nice weekend, %(stringhero)s-kun!"
    s ""

# game/dialogs.rpy:1231
translate tokipona scene5_bb555e9c:

    # "Sakura waved goodbye and went back into her house."
    ""

# game/dialogs.rpy:1232
translate tokipona scene5_020b0a9c:

    # "She really has a habit of helping me out in a jam!"
    ""

# game/dialogs.rpy:1233
translate tokipona scene5_66a17c47:

    # "But it felt kinda strange."
    ""

# game/dialogs.rpy:1234
translate tokipona scene5_51ab6c7b:

    # "Whose yukata is this?"
    ""

# game/dialogs.rpy:1235
translate tokipona scene5_4735f789:

    # "It looked like it's sized for her..."
    ""

# game/dialogs.rpy:1236
translate tokipona scene5_960d182a:

    # "Maybe it was her father's from when he was young..."
    ""

# game/dialogs.rpy:1237
translate tokipona scene5_cb071560:

    # "But...It looks brand new..."
    ""

# game/dialogs.rpy:1238
translate tokipona scene5_0074c533:

    # "They couldn't have bought one by mistake..."
    ""

# game/dialogs.rpy:1244
translate tokipona scene5_8c3a38cf:

    # hero "So... I'll see you later at school!"
    hero ""

# game/dialogs.rpy:1245
translate tokipona scene5_c2d0f8e4:

    # s "See you around %(stringhero)s-kun!"
    s ""

# game/dialogs.rpy:1246
translate tokipona scene5_c0d35692:

    # s "I had a lot of fun with you today!"
    s ""

# game/dialogs.rpy:1247
translate tokipona scene5_da65aa5d:

    # hero "Likewise!"
    hero ""

# game/dialogs.rpy:1250
translate tokipona scene5_bb555e9c_1:

    # "Sakura waved goodbye and went back into her house."
    ""

# game/dialogs.rpy:1251
translate tokipona scene5_c8d8f45f:

    # "Thanks to her, I knew how to get to the city and where the shops were."
    ""

# game/dialogs.rpy:1252
translate tokipona scene5_67c032e7:

    # "During my visit, I spotted some yukata shops."
    ""

# game/dialogs.rpy:1253
translate tokipona scene5_5854a13b:

    # "Tomorrow, I'll go there alone and I'll buy myself a yukata."
    ""

# game/dialogs.rpy:1254
translate tokipona scene5_bb429b41:

    # "I'll catch that loudmouth Rika by surprise at the festival!"
    ""

# game/dialogs.rpy:1256
translate tokipona scene5_a20cefa7:

    # "..."
    ""

# game/dialogs.rpy:1257
translate tokipona scene5_667ec4fd:

    # "Oh well... Let's go home, now..."
    ""

# game/dialogs.rpy:1289
translate tokipona scene7_237e24d1:

    # centered "{size=+35}CHAPTER 3\nSakura's secret{fast}{/size}"
    centered ""

# game/dialogs.rpy:1290
translate tokipona scene7_a210d65d:

    # alt "CHAPTER 3\nSakura's secret"
    alt ""

# game/dialogs.rpy:1295
translate tokipona scene7_58905c5b:

    # "Another week has passed."
    ""

# game/dialogs.rpy:1296
translate tokipona scene7_19ce328a:

    # "School was great, with Rika, Nanami and Sakura around."
    ""

# game/dialogs.rpy:1297
translate tokipona scene7_2fc12cae:

    # "During these last two weeks, I really started to feel that I was a part of the village now."
    ""

# game/dialogs.rpy:1298
translate tokipona scene7_2c9e68a3:

    # "I'm happy, but I'm unsure if I have feelings for Sakura..."
    ""

# game/dialogs.rpy:1299
translate tokipona scene7_909fa517:

    # "Or maybe even for Nanami..."
    ""

# game/dialogs.rpy:1301
translate tokipona scene7_4dd65759:

    # "One thing's for sure, Nanami and I talk and play games more and more.{p}I think she really likes me."
    ""

# game/dialogs.rpy:1302
translate tokipona scene7_ef8f393e:

    # "But I still don't know what that thing Sakura told me about her was about..."
    ""

# game/dialogs.rpy:1303
translate tokipona scene7_28d00c59:

    # "Well... At least I don't have any feelings for Rika...No way!"
    ""

# game/dialogs.rpy:1305
translate tokipona scene7_10129566:

    # r "Hey, %(stringhero)s!"
    r ""

# game/dialogs.rpy:1310
translate tokipona scene7_57e4be97:

    # "Yikes!!! Speak of the devil herself..."
    ""

# game/dialogs.rpy:1311
translate tokipona scene7_391047cd:

    # hero "Oh, hey, Rika-chan..."
    hero ""

# game/dialogs.rpy:1312
translate tokipona scene7_9c2ccd47:

    # r "Do you have some free time next weekend?"
    r ""

# game/dialogs.rpy:1313
translate tokipona scene7_158b0faf:

    # hero "This weekend?"
    hero ""

# game/dialogs.rpy:1314
translate tokipona scene7_3a013f55:

    # r "No, you idiot. The weekend after this one!"
    r ""

# game/dialogs.rpy:1315
translate tokipona scene7_9d4e7e0f:

    # hero "Oh... Sure I guess. What do you need?"
    hero ""

# game/dialogs.rpy:1316
translate tokipona scene7_acfd623e:

    # r "Well, since you're a manly man that has muscles..."
    r ""

# game/dialogs.rpy:1317
translate tokipona scene7_67864244:

    # r "I thought, maybe you could make yourself useful and help me prepare for festival!"
    r ""

# game/dialogs.rpy:1318
translate tokipona scene7_e98e7579:

    # "Argh! Why did I agree before knowing why?!"
    ""

# game/dialogs.rpy:1319
translate tokipona scene7_df0ced9f:

    # "Looks like I don't have a choice now..."
    ""

# game/dialogs.rpy:1337
translate tokipona scene7_8b156e71:

    # hero "Alright, fine, I'll help."
    hero ""

# game/dialogs.rpy:1339
translate tokipona scene7_10eea5f6:

    # r "Great!"
    r ""

# game/dialogs.rpy:1341
translate tokipona scene7_6f5d2035:

    # r "I'll tell you where to meet later!"
    r ""

# game/dialogs.rpy:1342
translate tokipona scene7_9766f460:

    # hero "Rika-chan, I didn't know you were from a temple."
    hero ""

# game/dialogs.rpy:1343
translate tokipona scene7_82bf0a2d:

    # r "My parents are shinto priests. Sometimes I help them for festivals and other temple related stuff."
    r ""

# game/dialogs.rpy:1344
translate tokipona scene7_c90a7240:

    # hero "Do you live in the temple with your parents?"
    hero ""

# game/dialogs.rpy:1346
translate tokipona scene7_a6c17ff8:

    # r "My..."
    r ""

# game/dialogs.rpy:1347
translate tokipona scene7_95b777d9:

    # "Her face fell dark."
    ""

# game/dialogs.rpy:1348
translate tokipona scene7_51eeeb24:

    # r "My parents divorced."
    r ""

# game/dialogs.rpy:1349
translate tokipona scene7_95db867f:

    # r "I live with my mother and my father guards the temple."
    r ""

# game/dialogs.rpy:1351
translate tokipona scene7_5fbbef96:

    # r "But they're still on good terms and they still have their faith in the shinto order, so it's okay."
    r ""

# game/dialogs.rpy:1352
translate tokipona scene7_868b412a:

    # hero "Ah, I'm sorry, I didn't know. It's good they're on positive terms though."
    hero ""

# game/dialogs.rpy:1353
translate tokipona scene7_0d2419ec:

    # r "Yeah...I guess so. Anyway, I'll meet you at the club, see ya!"
    r ""

# game/dialogs.rpy:1355
translate tokipona scene7_36df1f8b:

    # "I guess I learned something new about Rika. Still...I hope she won't be a total jerk towards me this weekend..."
    ""

# game/dialogs.rpy:1362
translate tokipona scene7_219621f1:

    # s "%(stringhero)s-kun!!"
    s ""

# game/dialogs.rpy:1363
translate tokipona scene7_51a60250:

    # hero "Sakura-chan! How goes?"
    hero ""

# game/dialogs.rpy:1364
translate tokipona scene7_f4d7051e:

    # "Sakura bowed at me. As she rose back up, I noticed her face was flushed."
    ""

# game/dialogs.rpy:1366
translate tokipona scene7_85835f50:

    # s "%(stringhero)s-kun,...are you doing anything this weekend?"
    s ""

# game/dialogs.rpy:1368
translate tokipona scene7_31d438a9:

    # hero "The next weekend?"
    hero ""

# game/dialogs.rpy:1369
translate tokipona scene7_9d85daa8:

    # hero "I'm sorry, I'm helping Rika-chan set up stuff for the festival."
    hero ""

# game/dialogs.rpy:1370
translate tokipona scene7_77ee7366:

    # s "Huh?"
    s ""

# game/dialogs.rpy:1371
translate tokipona scene7_8480da2c:

    # s "Oh... No, I mean, the weekend of this week"
    s ""

# game/dialogs.rpy:1372
translate tokipona scene7_cb5a59ac:

    # hero "Oh whoops. Guess my mind was wandering to far into the future... Hm..."
    hero ""

# game/dialogs.rpy:1373
translate tokipona scene7_d0aa7f64:

    # hero "I'm not doing anything at all."
    hero ""

# game/dialogs.rpy:1374
translate tokipona scene7_2217ef70:

    # hero "Do you want to go out to the city again?"
    hero ""

# game/dialogs.rpy:1376
translate tokipona scene7_1d124fd8:

    # "Sakura's face lit up and she started to giggled."
    ""

# game/dialogs.rpy:1377
translate tokipona scene7_0f5eeead:

    # s "I...I was about to ask you the same question! {image=heart.png}"
    s ""

# game/dialogs.rpy:1378
translate tokipona scene7_9e2e5ae5:

    # hero "Of course, I'd love to go out with you to the city!"
    hero ""

# game/dialogs.rpy:1379
translate tokipona scene7_22e33626:

    # hero "Yeah, I'd like to have even more fun than last time!"
    hero ""

# game/dialogs.rpy:1381
translate tokipona scene7_c31b4dec:

    # s "Same here! I'll wait for you at the train station at 9AM!"
    s ""

# game/dialogs.rpy:1382
translate tokipona scene7_5409a3f5:

    # hero "Alright! I'll see you then!"
    hero ""

# game/dialogs.rpy:1384
translate tokipona scene7_a7db0154:

    # "Ah, it's almost time for class..."
    ""

# game/dialogs.rpy:1388
translate tokipona scene7_13719c4f:

    # "The school day dragged on and on... Maybe I just couldn't wait to hang out with Sakura."
    ""

# game/dialogs.rpy:1389
translate tokipona scene7_85ae4e7c:

    # "The next day came, and I arrived at the train station in the morning to go to the city with Sakura."
    ""

# game/dialogs.rpy:1395
translate tokipona SH1_e0b76a41:

    # "We took the train to the city. We started our day out eating at an American fast food place because we were already hungry."
    ""

# game/dialogs.rpy:1396
translate tokipona SH1_d68b3fb1:

    # "After that, we decided to try karaoke."
    ""

# game/dialogs.rpy:1398
translate tokipona SH1_0c6feee4:

    # "Sakura's voice was absolutely mesmerizing."
    ""

# game/dialogs.rpy:1399
translate tokipona SH1_a925770d:

    # hero "You could be a famous pop idol, Sakura-chan."
    hero ""

# game/dialogs.rpy:1400
translate tokipona SH1_ee5a9ea8:

    # s "Thank you... But I don't want to be one, though..."
    s ""

# game/dialogs.rpy:1401
translate tokipona SH1_b809126d:

    # hero "Eh? Why is that?"
    hero ""

# game/dialogs.rpy:1402
translate tokipona SH1_93f8ec4b:

    # s "Well..."
    s ""

# game/dialogs.rpy:1403
translate tokipona SH1_3098f4a6:

    # "She hesitated a bit before replying."
    ""

# game/dialogs.rpy:1404
translate tokipona SH1_25b5d3d5:

    # s "I don't think I'd like the lifestyle of a pop idol..."
    s ""

# game/dialogs.rpy:1405
translate tokipona SH1_9c5f10fe:

    # s "It's so much work and you don't have a lot of time for yourself."
    s ""

# game/dialogs.rpy:1406
translate tokipona SH1_67650f2f:

    # hero "Yeah, it must be rough..."
    hero ""

# game/dialogs.rpy:1407
translate tokipona SH1_759de575:

    # hero "By the way, what would you would like to do in the future?"
    hero ""

# game/dialogs.rpy:1408
translate tokipona SH1_9028f52b:

    # s "I don't really know yet..."
    s ""

# game/dialogs.rpy:1409
translate tokipona SH1_36411a29:

    # s "Maybe I'll be a part of a symphony orchestra if I can."
    s ""

# game/dialogs.rpy:1410
translate tokipona SH1_ba98acc9:

    # hero "Do you play any instruments?"
    hero ""

# game/dialogs.rpy:1411
translate tokipona SH1_cc1fe723:

    # s "Yes, the violin."
    s ""

# game/dialogs.rpy:1412
translate tokipona SH1_5632a1a6:

    # "I suddenly remembered..."
    ""

# game/dialogs.rpy:1417
translate tokipona SH1_1075e8d3:

    # "So the violin I hear sometimes at night... That must be her..."
    ""

# game/dialogs.rpy:1422
translate tokipona SH1_6ae339be:

    # hero "I think I hear you playing the violin during the evening from time to time."
    hero ""

# game/dialogs.rpy:1423
translate tokipona SH1_ccbcdf1d:

    # "She looked down, kinda embarrassed."
    ""

# game/dialogs.rpy:1424
translate tokipona SH1_457a5f49:

    # s "Oh... You heard me playing?"
    s ""

# game/dialogs.rpy:1425
translate tokipona SH1_0027dd08:

    # hero "Yeah... You play so perfectly... It's amazing!"
    hero ""

# game/dialogs.rpy:1426
translate tokipona SH1_55a77664:

    # s "W-why T-t-thank you..."
    s ""

# game/dialogs.rpy:1427
translate tokipona SH1_7b7e5c26:

    # hero "I hope I can watch you play someday."
    hero ""

# game/dialogs.rpy:1428
translate tokipona SH1_32eac59f:

    # s "I'd be happy to play for you, %(stringhero)s-kun!"
    s ""

# game/dialogs.rpy:1432
translate tokipona SH1_3e846a41:

    # "I tried to sing the next song, but I was horribly awful at singing. At least it made Sakura laugh."
    ""

# game/dialogs.rpy:1433
translate tokipona SH1_7f6f8e16:

    # "When we were done, we headed back to to the city's train station."
    ""

# game/dialogs.rpy:1437
translate tokipona SH1_884ab08c:

    # "As we were about to cross the road..."
    ""

# game/dialogs.rpy:1438
translate tokipona SH1_5b2451aa:

    # "A car, ignoring the red light, kept driving and almost hit Sakura!"
    ""

# game/dialogs.rpy:1439
translate tokipona SH1_d2261bcd:

    # "Sakura quickly stepped back but lost her balance and was about to fall on her back."
    ""

# game/dialogs.rpy:1441
translate tokipona SH1_eb7a286f:

    # "I managed to catch her just in time and she fell into arms."
    ""

# game/dialogs.rpy:1442
translate tokipona SH1_e5780a08:

    # "She looked into my eyes and I got lost in hers."
    ""

# game/dialogs.rpy:1443
translate tokipona SH1_ac769ce2:

    # "Time stopped."
    ""

# game/dialogs.rpy:1444
translate tokipona SH1_e75587c1:

    # "As we stared at each other, we forgot all about the car incident."
    ""

# game/dialogs.rpy:1445
translate tokipona SH1_0657ef81:

    # s "%(stringhero)s....-kun...."
    s ""

# game/dialogs.rpy:1446
translate tokipona SH1_c6dc2ed4:

    # hero "....Sakura-chan..."
    hero ""

# game/dialogs.rpy:1448
translate tokipona SH1_ae0ed22a:

    # "Her eyes slowly shut, her face closed in on mine, her small mouth beckoned for something... It was her lips that seemed to be waiting for me."
    ""

# game/dialogs.rpy:1449
translate tokipona SH1_07a9d021:

    # "Oh my gosh... I think she wants me to kiss her! I can't believe it! What do I do!?"
    ""

# game/dialogs.rpy:1451
translate tokipona SH1_4b8ffc21:

    # "I could hear my heart beating loudly. It felt so loud, I was almost sure that the people around us could hear it too."
    ""

# game/dialogs.rpy:1452
translate tokipona SH1_fe20f7a5:

    # "This was it. It's now or never. My face slowly approached hers..."
    ""

# game/dialogs.rpy:1453
translate tokipona SH1_8538723d:

    # "Very slowly..."
    ""

# game/dialogs.rpy:1454
translate tokipona SH1_f1ab825c:

    # "I could feel her breath on my face."
    ""

# game/dialogs.rpy:1459
translate tokipona SH1_16f58986:

    # "And the second my nose touched hers...{p}She turned her face away, completely embarrassed, and stood up almost immediately."
    ""

# game/dialogs.rpy:1460
translate tokipona SH1_56dcd521:

    # s "I'm... I'm okay..."
    s ""

# game/dialogs.rpy:1464
translate tokipona SH1_557b9c7c:

    # s ". . ."
    s ""

# game/dialogs.rpy:1465
translate tokipona SH1_b64fd408:

    # s "Let's go home..."
    s ""

# game/dialogs.rpy:1470
translate tokipona SH1_32abdb6f:

    # "On the train ride home, I was in a complete daze..."
    ""

# game/dialogs.rpy:1471
translate tokipona SH1_06b56af7:

    # "I thought she wanted me to kiss her... But in the end, she refused..."
    ""

# game/dialogs.rpy:1472
translate tokipona SH1_a20cefa7:

    # "..."
    ""

# game/dialogs.rpy:1473
translate tokipona SH1_23dda12a:

    # "Dammit, stop being paranoid, %(stringhero)s!...{p}Maybe it's just because it was her first time. She might not have felt ready..."
    ""

# game/dialogs.rpy:1474
translate tokipona SH1_e1f82eca:

    # "I felt kinda happy inside though. At least it means that she's attracted to me... I think..."
    ""

# game/dialogs.rpy:1475
translate tokipona SH1_a3dbd16d:

    # "But I also felt just as confused..."
    ""

# game/dialogs.rpy:1476
translate tokipona SH1_246f930a:

    # "As we sat together in silence, I occasionally looked over to Sakura."
    ""

# game/dialogs.rpy:1477
translate tokipona SH1_acd10f88:

    # "She didn't look angry. But she seemed really embarrassed still."
    ""

# game/dialogs.rpy:1478
translate tokipona SH1_045668b1:

    # "But, most of all, she looked upset about something. She just sat there lost in her thoughts like she was profoundly thinking about something."
    ""

# game/dialogs.rpy:1481
translate tokipona SH1_3c386e89:

    # "Sakura was completely quiet along the way."
    ""

# game/dialogs.rpy:1482
translate tokipona SH1_849b7889:

    # "I didn't know what to say...or what to do."
    ""

# game/dialogs.rpy:1483
translate tokipona SH1_c312d8ea:

    # "The atmosphere was uncomfortable."
    ""

# game/dialogs.rpy:1484
translate tokipona SH1_59826635:

    # "Suddenly, she broke the silence."
    ""

# game/dialogs.rpy:1485
translate tokipona SH1_1eeb4b66:

    # s "...%(stringhero)s-kun?"
    s ""

# game/dialogs.rpy:1486
translate tokipona SH1_4edce7c8:

    # hero "Yes?"
    hero ""

# game/dialogs.rpy:1488
translate tokipona SH1_782acf27:

    # s "I...{p}........................{p}I have something I want to tell you..."
    s ""

# game/dialogs.rpy:1489
translate tokipona SH1_d2dcc110:

    # hero "What is it?"
    hero ""

# game/dialogs.rpy:1490
translate tokipona SH1_719ce3f8:

    # "I followed her as she entered a rice field."
    ""

# game/dialogs.rpy:1491
translate tokipona SH1_d7b94be6:

    # "The path inside was small, surrounded by big rice plants."
    ""

# game/dialogs.rpy:1492
translate tokipona SH1_481e4154:

    # "It was completely desolate. There was nobody around us. Only the chants of the cicadas could be heard."
    ""

# game/dialogs.rpy:1494
translate tokipona SH1_8329e10f:

    # s "This is something very difficult to tell you...{p}Painfully difficult..."
    s ""

# game/dialogs.rpy:1495
translate tokipona SH1_c02f0d8f:

    # s "This isn't something you might not even comprehend, but..."
    s ""

# game/dialogs.rpy:1496
translate tokipona SH1_7cf20b6f:

    # hero "This sounds pretty serious."
    hero ""

# game/dialogs.rpy:1497
translate tokipona SH1_a4c7d906:

    # "I could feel a sort of anxiety building up from inside me."
    ""

# game/dialogs.rpy:1498
translate tokipona SH1_ec713d94:

    # "I've never seen Sakura behave this seriously before."
    ""

# game/dialogs.rpy:1499
translate tokipona SH1_49f22c99:

    # s "I'm about to tell you this because I trust you."
    s ""

# game/dialogs.rpy:1500
translate tokipona SH1_afd8f97b:

    # "I smiled, but I was nervous."
    ""

# game/dialogs.rpy:1501
translate tokipona SH1_5066f9b8:

    # hero "Y-you can trust me, Sakura-chan. You're my friend. You, Rika and Nanami are my best friends and I'll never betray any of you. Never."
    hero ""

# game/dialogs.rpy:1502
translate tokipona SH1_9b73dceb:

    # "I mean that, from the bottom of my heart."
    ""

# game/dialogs.rpy:1503
translate tokipona SH1_64b40e00:

    # s "Do you promise? Never?"
    s ""

# game/dialogs.rpy:1504
translate tokipona SH1_b7fb0fa9:

    # hero "Forever and ever."
    hero ""

# game/dialogs.rpy:1505
translate tokipona SH1_ac397e42:

    # "This time I smiled with a little more confidence."
    ""

# game/dialogs.rpy:1506
translate tokipona SH1_43dc30fb:

    # "She gave a faint smile but reverted back to her serious face after."
    ""

# game/dialogs.rpy:1507
translate tokipona SH1_ee01b059:

    # s "It's... So hard for me to tell you...{p}Besides my parents, only Rika-chan knows about it.{p}Not even Nanami-chan knows..."
    s ""

# game/dialogs.rpy:1508
translate tokipona SH1_a56d7a94:

    # s "Everyone else seems to have just heard rumors about it... or aren't sure at all..."
    s ""

# game/dialogs.rpy:1509
translate tokipona SH1_d0d64bc6:

    # hero "Oh..."
    hero ""

# game/dialogs.rpy:1510
translate tokipona SH1_2fcf5cc9:

    # "I was confused... What was she trying to say?"
    ""

# game/dialogs.rpy:1511
translate tokipona SH1_0ebf712b:

    # "Obviously, she and Rika-chan knew a secret.{p}And Sakura-chan was about to tell me this secret."
    ""

# game/dialogs.rpy:1512
translate tokipona SH1_4df61edb:

    # "I feel happy. That means she must feel I'm a close enough friend and that she really trusts me."
    ""

# game/dialogs.rpy:1514
translate tokipona SH1_32e60ae0:

    # "She stopped after walking a small distance from me, facing away from me."
    ""

# game/dialogs.rpy:1515
translate tokipona SH1_a0dd6cdb:

    # "Her body covered the sun and I saw only her shadow in the distance."
    ""

# game/dialogs.rpy:1517
translate tokipona SH1_e1554201:

    # s "..."
    s ""

# game/dialogs.rpy:1518
translate tokipona SH1_007ceac2:

    # s "%(stringhero)s-kun..."
    s ""

# game/dialogs.rpy:1519
translate tokipona SH1_d5eced1a:

    # s "Do you remember the day when those thugs bullied me?"
    s ""

# game/dialogs.rpy:1520
translate tokipona SH1_d19c5e51:

    # hero "Yes, I remember."
    hero ""

# game/dialogs.rpy:1521
translate tokipona SH1_3b33aa23:

    # s "Do you remember what they said to me?"
    s ""

# game/dialogs.rpy:1522
translate tokipona SH1_ec6b7626:

    # hero "Yeah, I remember."
    hero ""

# game/dialogs.rpy:1523
translate tokipona SH1_b28fcf52:

    # hero "They were trying to make you admit that you were a boy, or something like that."
    hero ""

# game/dialogs.rpy:1524
translate tokipona SH1_bf9bdd8b:

    # s "Yes."
    s ""

# game/dialogs.rpy:1525
translate tokipona SH1_b5a2fb72:

    # s "It's because they..."
    s ""

# game/dialogs.rpy:1526
translate tokipona SH1_9fce5758:

    # s "They guessed it... For some reason they knew it..."
    s ""

# game/dialogs.rpy:1527
translate tokipona SH1_bbc36fab:

    # hero "I... What are you trying to say?..."
    hero ""

# game/dialogs.rpy:1528
translate tokipona SH1_007ceac2_1:

    # s "%(stringhero)s-kun..."
    s ""

# game/dialogs.rpy:1547
translate tokipona SH1_e4a4445c:

    # s "I {b}really{/b} am a boy."
    s ""

# game/dialogs.rpy:1548
translate tokipona SH1_16bb48d0:

    # hero "...W-what?"
    hero ""

# game/dialogs.rpy:1580
translate tokipona SH1_47f1e757:

    # centered "{size=+35}CHAPTER 4\nBig decisions{fast}{/size}"
    centered ""

# game/dialogs.rpy:1581
translate tokipona SH1_0357ee04:

    # alt "CHAPTER 4\nBig decisions"
    alt ""

# game/dialogs.rpy:1591
translate tokipona SH1_bb09a9d7:

    # hero "You... You're... a boy!?"
    hero ""

# game/dialogs.rpy:1592
translate tokipona SH1_a82ece96:

    # "My mind went completely numb for a second."
    ""

# game/dialogs.rpy:1593
translate tokipona SH1_7bdf1789:

    # "I couldn't believe it."
    ""

# game/dialogs.rpy:1594
translate tokipona SH1_cd989119:

    # "No, she couldn't be a boy!{p} That's impossible!!!"
    ""

# game/dialogs.rpy:1595
translate tokipona SH1_9f02c3f2:

    # "I started to nervously laugh to myself. There's no way. It was just a joke..."
    ""

# game/dialogs.rpy:1596
translate tokipona SH1_1faf5341:

    # hero "Haha it's a joke...No way...No, you can't be a boy, Sakura-chan!"
    hero ""

# game/dialogs.rpy:1597
translate tokipona SH1_b293358c:

    # "Sakura-chan didn't say a thing."
    ""

# game/dialogs.rpy:1598
translate tokipona SH1_b2a3712a:

    # "She suddenly turned around, and approached me."
    ""

# game/dialogs.rpy:1599
translate tokipona SH1_fcff274b:

    # s "...If you don't believe me, take a look for yourself!"
    s ""

# game/dialogs.rpy:1604
translate tokipona SH1_93439998:

    # "She suddenly lifted her skirt up, showing me her panties!"
    ""

# game/dialogs.rpy:1605
translate tokipona SH1_6a7c30f8:

    # "I gasped out of embarrassment and shock. I took a look around, but fortunately we were still alone in the rice fields."
    ""

# game/dialogs.rpy:1606
translate tokipona SH1_6db43b58:

    # "I tried my hardest to not look at Sakura and covered my eyes."
    ""

# game/dialogs.rpy:1607
translate tokipona SH1_7987ae0e:

    # hero "No, Sakura-chan, I can't look, this is just inappropriate!"
    hero ""

# game/dialogs.rpy:1608
translate tokipona SH1_ea014391:

    # s "Please, just look at me!"
    s ""

# game/dialogs.rpy:1609
translate tokipona SH1_14a15247:

    # "I peeked a little and could see her eyes intensely glaring at me. She was dead serious."
    ""

# game/dialogs.rpy:1610
translate tokipona SH1_453421f4:

    # "My eyes slowly scrolled down to look below her waistline."
    ""

# game/dialogs.rpy:1611
translate tokipona SH1_952d1725:

    # "She was wearing blue-colored panties..."
    ""

# game/dialogs.rpy:1612
translate tokipona SH1_ede13101:

    # "But then, I noticed that...in her panties...there was...something.{p}Something that shouldn't have been there."
    ""

# game/dialogs.rpy:1613
translate tokipona SH1_dbbe62c0:

    # "...But the evidence was right there... "
    ""

# game/dialogs.rpy:1614
translate tokipona SH1_a97eb7ed:

    # "I couldn't believe my eyes but the truth was there..."
    ""

# game/dialogs.rpy:1616
translate tokipona SH1_6ae69507:

    # "She was really a HE!!!"
    ""

# game/dialogs.rpy:1617
translate tokipona SH1_20403ffb:

    # "I started to shake."
    ""

# game/dialogs.rpy:1618
translate tokipona SH1_99484c94:

    # "So the cute girl I was following for school.{p}The wonderful girl who was talking about manga and anime with Rika and Nanami..."
    ""

# game/dialogs.rpy:1619
translate tokipona SH1_0e81ba72:

    # "The girl I was infatuated with for almost two weeks...{p}The girl I was about to kiss today..."
    ""

# game/dialogs.rpy:1620
translate tokipona SH1_a2b0302d:

    # "That girl... Was a boy! A guy! A dude! A male!!!!!"
    ""

# game/dialogs.rpy:1621
translate tokipona SH1_51a16ccf:

    # "I stood there, motionless."
    ""

# game/dialogs.rpy:1622
translate tokipona SH1_0cbdd6b2:

    # hero "You... yo-yo-yo-yo-you're a..."
    hero ""

# game/dialogs.rpy:1623
translate tokipona SH1_29fb07f0:

    # hero "{size=+10}You're a boy?!{/size}"
    hero ""

# game/dialogs.rpy:1624
translate tokipona SH1_37a5e581:

    # "I ended up shouted at her. I couldn't believe it."
    ""

# game/dialogs.rpy:1625
translate tokipona SH1_024d5fe2:

    # "My outburst made her...I mean him a bit scared. He stepped back a little."
    ""

# game/dialogs.rpy:1626
translate tokipona SH1_12900b8c:

    # hero "Sorry... Sorry, Sakura-chan, I didn't mean to scare you..."
    hero ""

# game/dialogs.rpy:1627
translate tokipona SH1_a613c480:

    # hero "Huh...no, I... I mean, Sakura-kun...? Huh... Aaaaargh!!"
    hero ""

# game/dialogs.rpy:1628
translate tokipona SH1_32d25ac1:

    # "My mind felt like it split into a million pieces."
    ""

# game/dialogs.rpy:1629
translate tokipona SH1_e0277eeb:

    # hero "It's... incredible... You..."
    hero ""

# game/dialogs.rpy:1630
translate tokipona SH1_00287bf2:

    # hero "But... You know, I..."
    hero ""

# game/dialogs.rpy:1635
translate tokipona SH1_a783e872:

    # "As if reading my mind, Sakura turned around, facing the sun again, and he... or she... started to calmly speak."
    ""

# game/dialogs.rpy:1637
translate tokipona SH1_b0e013d4:

    # s "I was born with a very rare... {w}Well, I don't know if it's a genital disorder or something..."
    s ""

# game/dialogs.rpy:1638
translate tokipona SH1_41913518:

    # s "Technically, I inherited all the chromosomes of my mother, but I got a Y chromosome anyway."
    s ""

# game/dialogs.rpy:1639
translate tokipona SH1_f7622dac:

    # s "It made me a very girlish boy. So girlish that even my brain is one of a girl."
    s ""

# game/dialogs.rpy:1640
translate tokipona SH1_20638c61:

    # s "I was a normal boy when I was young, but my mind and body transformed me as I grew older."
    s ""

# game/dialogs.rpy:1641
translate tokipona SH1_1cb3b079:

    # s "I was more attracted by dolls than little cars, and I preferred talking with girls than playing soccer with the boys..."
    s ""

# game/dialogs.rpy:1642
translate tokipona SH1_425c4b72:

    # s "When I was 11-years-old, my dad started to get exhausted of this... \"joke\", that change inside of me... So he started getting me back into \"male stuff\"..."
    s ""

# game/dialogs.rpy:1643
translate tokipona SH1_c69a2dcb:

    # s "He bought me outfits and books usually made for boys..."
    s ""

# game/dialogs.rpy:1644
translate tokipona SH1_3df3d361:

    # s "But as time passed by...he started to drink more... and his alcohol addiction grew. He started to become more violent."
    s ""

# game/dialogs.rpy:1645
translate tokipona SH1_c8d0b471:

    # s "He even tried to cut my hair once."
    s ""

# game/dialogs.rpy:1646
translate tokipona SH1_f480d83d:

    # s "But my mother protected me. She has always accepted me for who I am, no matter what."
    s ""

# game/dialogs.rpy:1647
translate tokipona SH1_64529bc1:

    # s "I can't help it. {p}I feel like a girl inside, that's all."
    s ""

# game/dialogs.rpy:1648
translate tokipona SH1_f674ff4c:

    # s "Actually, my father is really pissed off at me. My mother tries to protect me but my father becomes crazy when I'm around..."
    s ""

# game/dialogs.rpy:1649
translate tokipona SH1_b935cc96:

    # "I nodded, taking in her...his monologue slowly, digesting his story..."
    ""

# game/dialogs.rpy:1651
translate tokipona SH1_0317f7d7:

    # "I didn't know what to do..."
    ""

# game/dialogs.rpy:1652
translate tokipona SH1_03c57d51:

    # "His...her life must have been pretty tough growing up...but..."
    ""

# game/dialogs.rpy:1653
translate tokipona SH1_20a5c853:

    # hero "So you want to be a girl. No matter what everybody thinks, and whatever your own body thinks, you want to be a girl and that's all."
    hero ""

# game/dialogs.rpy:1654
translate tokipona SH1_10c7cae4:

    # s "Yes...more than anything."
    s ""

# game/dialogs.rpy:1655
translate tokipona SH1_71387f7a:

    # hero "Hmm..."
    hero ""

# game/dialogs.rpy:1656
translate tokipona SH1_60e224a2:

    # "I thought for a moment...and I finally smiled and accepted it."
    ""

# game/dialogs.rpy:1657
translate tokipona SH1_c35613b6:

    # "After all... Maybe she's a boy, but Sakura is still Sakura."
    ""

# game/dialogs.rpy:1658
translate tokipona SH1_de287c57:

    # hero "Hmm okay... It's okay for me..."
    hero ""

# game/dialogs.rpy:1659
translate tokipona SH1_5c82e516:

    # s "Really?"
    s ""

# game/dialogs.rpy:1660
translate tokipona SH1_456228e3:

    # hero "Yeah! If you're happy like this, then I'm happy for you as well!"
    hero ""

# game/dialogs.rpy:1661
translate tokipona SH1_923bc3f8:

    # "Sakura was still facing away from me, she still seemed a little unsatisfied."
    ""

# game/dialogs.rpy:1662
translate tokipona SH1_8a085455:

    # s "You must have some questions at least, don't you?"
    s ""

# game/dialogs.rpy:1663
translate tokipona SH1_bc81f33c:

    # "Sheesh...she's right, I do have a lot of questions. It's scary how he...uh...she can read my mind."
    ""

# game/dialogs.rpy:1664
translate tokipona SH1_9614bee5:

    # "She spoke softly."
    ""

# game/dialogs.rpy:1665
translate tokipona SH1_3caf349f:

    # s "Go ahead, please. You're my friend, it's normal to have questions about this. And you have the right to know more."
    s ""

# game/dialogs.rpy:1666
translate tokipona SH1_64036f15:

    # "I calmed myself down a bit and gave Sakura a smile."
    ""

# game/dialogs.rpy:1667
translate tokipona SH1_9877ea0f:

    # hero "Okay...{p}So you prefer that everyone say 'she' to you instead of 'he'?"
    hero ""

# game/dialogs.rpy:1668
translate tokipona SH1_bf9bdd8b_1:

    # s "Yes."
    s ""

# game/dialogs.rpy:1669
translate tokipona SH1_caa3249e:

    # hero "I see... Alright..."
    hero ""

# game/dialogs.rpy:1670
translate tokipona SH1_20b19b0b:

    # "The problem of she and he is all solved now. It feels as confusion is already going away."
    ""

# game/dialogs.rpy:1671
translate tokipona SH1_9ef84186:

    # hero "And, well... What about your... uhh... b... breasts?"
    hero ""

# game/dialogs.rpy:1672
translate tokipona SH1_28e89323:

    # hero "I see that y-your chest is... Well..."
    hero ""

# game/dialogs.rpy:1673
translate tokipona SH1_7622ed66:

    # s "Mmm? Oh... {p}Just a bra with some cotton inside..."
    s ""

# game/dialogs.rpy:1674
translate tokipona SH1_38942254:

    # hero "Oh...why do that?"
    hero ""

# game/dialogs.rpy:1675
translate tokipona SH1_18c07f8f:

    # s "I've always wanted real breasts and would like to wear a bra..."
    s ""

# game/dialogs.rpy:1676
translate tokipona SH1_f56274ef:

    # s "They don't look big as Rika-chan's but it's enough to make me feel like I have breasts."
    s ""

# game/dialogs.rpy:1677
translate tokipona SH1_972441ce:

    # hero "Oh, on the same subject...Have you ever thought about doing... surgery?"
    hero ""

# game/dialogs.rpy:1678
translate tokipona SH1_a711c810:

    # s "Yes, I've thought about it..."
    s ""

# game/dialogs.rpy:1679
translate tokipona SH1_a18ea8c8:

    # s "But I don't want to... I'm too afraid of surgery!"
    s ""

# game/dialogs.rpy:1680
translate tokipona SH1_27bb92e4:

    # "The way she said that sounded pretty cute but she seemed pretty frustrated."
    ""

# game/dialogs.rpy:1681
translate tokipona SH1_b5f35068:

    # "...huh? What the heck am I saying? It's a guy!"
    ""

# game/dialogs.rpy:1682
translate tokipona SH1_d25154cc:

    # "I can't fall in love with a dude! Even if he looks like a girl!"
    ""

# game/dialogs.rpy:1683
translate tokipona SH1_86c14a59:

    # "Even if he looks like...{w}such a wonderful girl...{w}who seems so fragile in the shadow..."
    ""

# game/dialogs.rpy:1684
translate tokipona SH1_31273a30:

    # "Ugh dammit! What should I do?! I'm still confused!!!"
    ""

# game/dialogs.rpy:1688
translate tokipona SH1_7072f3b4:

    # "No..."
    ""

# game/dialogs.rpy:1689
translate tokipona SH1_7eb4948e:

    # "No way... I can't! I am a pure and straight guy..."
    ""

# game/dialogs.rpy:1690
translate tokipona SH1_5b52ab19:

    # "But whatever...{p}What's the big deal?"
    ""

# game/dialogs.rpy:1691
translate tokipona SH1_81380db8:

    # "It's a boy? Okay. So what? I'm just fixed on her gender."
    ""

# game/dialogs.rpy:1692
translate tokipona SH1_27c4c879:

    # "But that doesn't change much. I can't love her, but we're still friends!"
    ""

# game/dialogs.rpy:1693
translate tokipona SH1_d4af9d2c:

    # "With such a big secret between us, I feel like we're even closer friends now!"
    ""

# game/dialogs.rpy:1694
translate tokipona SH1_2b941147:

    # hero "Oh...Now I understand why you refused the kiss earlier..."
    hero ""

# game/dialogs.rpy:1695
translate tokipona SH1_508719ae:

    # hero "It's because you didn't want to break my heart, huh?"
    hero ""

# game/dialogs.rpy:1696
translate tokipona SH1_e17592e6:

    # s "Yes, exactly..."
    s ""

# game/dialogs.rpy:1697
translate tokipona SH1_1f18b742:

    # hero "You know, I feel happy, now."
    hero ""

# game/dialogs.rpy:1699
translate tokipona SH1_5ab99daa:

    # "Sakura turns around to face me."
    ""

# game/dialogs.rpy:1700
translate tokipona SH1_51437fd0:

    # s "Eh? Why is that?"
    s ""

# game/dialogs.rpy:1701
translate tokipona SH1_d89b0eb5:

    # hero "Because you shared your secret with me. And that you didn't want to break my heart."
    hero ""

# game/dialogs.rpy:1702
translate tokipona SH1_1a3b0477:

    # hero "I feel like you must trust me a lot. And that means that you consider me a close friend..."
    hero ""

# game/dialogs.rpy:1703
translate tokipona SH1_d9d841a7:

    # hero "I'm happy to be your friend, Sakura-chan."
    hero ""

# game/dialogs.rpy:1705
translate tokipona SH1_6caf3c5b:

    # "Sakura didn't say anything for a moment..."
    ""

# game/dialogs.rpy:1706
translate tokipona SH1_bdc6ac8a:

    # "Finally she gave me a big smile. She looked like she was going to cry from happiness."
    ""

# game/dialogs.rpy:1709
translate tokipona SH1_3f0e1dcd:

    # s "Thank you, %(stringhero)s-kun!"
    s ""

# game/dialogs.rpy:1710
translate tokipona SH1_9db5fc3e:

    # s "I'm so glad that you took the time to understand me! {p}And I'm happy to be your friend, too!"
    s ""

# game/dialogs.rpy:1711
translate tokipona SH1_cc14d6a4:

    # hero "I promise, I'll keep your secret safe."
    hero ""

# game/dialogs.rpy:1712
translate tokipona SH1_639e5f3c:

    # "We held our pinky fingers together to seal the promise. We both gave each other a big smile."
    ""

# game/dialogs.rpy:1713
translate tokipona SH1_3c805b45:

    # "Maybe she's a he... but she's a friend first and foremost."
    ""

# game/dialogs.rpy:1719
translate tokipona SH1_c8994519:

    # "We reached her house talking happily along the way about a variety of things."
    ""

# game/dialogs.rpy:1720
translate tokipona SH1_fa80129a:

    # "We were enjoying our time, as the best of friends."
    ""

# game/dialogs.rpy:1721
translate tokipona SH1_d62f35ea:

    # hero "I wonder how Rika-chan will react when she realizes I know your secret too."
    hero ""

# game/dialogs.rpy:1723
translate tokipona SH1_589baf2e:

    # s "Teehee! She will probably try to hit on you, then! {image=heart.png}"
    s ""

# game/dialogs.rpy:1724
translate tokipona SH1_d5bffef3:

    # hero "Gah! That's a bit cruel, Sakura-chan!"
    hero ""

# game/dialogs.rpy:1726
translate tokipona SH1_b112bb86:

    # hero "...Although you're probably right..."
    hero ""

# game/dialogs.rpy:1727
translate tokipona SH1_2a493962:

    # "We both laughed."
    ""

# game/dialogs.rpy:1729
translate tokipona SH1_7a9a62fd:

    # "Something crossed my mind though..."
    ""

# game/dialogs.rpy:1731
translate tokipona SH1_2100c01f:

    # hero "Sakura-chan?"
    hero ""

# game/dialogs.rpy:1732
translate tokipona SH1_282f01ee:

    # hero "You know..."
    hero ""

# game/dialogs.rpy:1733
translate tokipona SH1_e9e23b04:

    # hero "Nanami-chan...{p}She really seems to care about you. More than you think."
    hero ""

# game/dialogs.rpy:1734
translate tokipona SH1_82f05805:

    # hero "So... Maybe it's time for her to know the truth as well."
    hero ""

# game/dialogs.rpy:1736
translate tokipona SH1_37bc0b4d:

    # "Sakura started to think."
    ""

# game/dialogs.rpy:1737
translate tokipona SH1_eef310db:

    # s "I know...{p}I really do want to tell her..."
    s ""

# game/dialogs.rpy:1738
translate tokipona SH1_08cb90bc:

    # s "The fact is... I'm so scared of her reaction..."
    s ""

# game/dialogs.rpy:1739
translate tokipona SH1_337166d2:

    # hero "Hmmm...I can understand."
    hero ""

# game/dialogs.rpy:1740
translate tokipona SH1_93f8ec4b_1:

    # s "Well..."
    s ""

# game/dialogs.rpy:1741
translate tokipona SH1_d9c81ff9:

    # s "I don't know..."
    s ""

# game/dialogs.rpy:1742
translate tokipona SH1_aac62100:

    # hero "There's no reason to be scared, Sakura-chan."
    hero ""

# game/dialogs.rpy:1743
translate tokipona SH1_45627ed5:

    # hero "Listen..."
    hero ""

# game/dialogs.rpy:1745
translate tokipona SH1_ad02dfe3:

    # hero "I'll talk to Rika-chan. Next Monday, let's tell Nanami-chan when we're all together at the club."
    hero ""

# game/dialogs.rpy:1746
translate tokipona SH1_82ea3c85:

    # hero "Rika and I will be there to support you."
    hero ""

# game/dialogs.rpy:1747
translate tokipona SH1_f63d0a16:

    # hero "Nanami-chan is your close friend, right? She has the right to know too. I'm sure she'll understand."
    hero ""

# game/dialogs.rpy:1748
translate tokipona SH1_c91ce444:

    # hero "You must take your life in your own hands, Sakura-chan!!!"
    hero ""

# game/dialogs.rpy:1749
translate tokipona SH1_938668c5:

    # "Sakura laughed at the lame line I said and finally smiled."
    ""

# game/dialogs.rpy:1751
translate tokipona SH1_757d342d:

    # s "Alright. Let's do this!"
    s ""

# game/dialogs.rpy:1752
translate tokipona SH1_6a86d9b7:

    # hero "On Monday then."
    hero ""

# game/dialogs.rpy:1753
translate tokipona SH1_47e672b9:

    # hero "Don't forget: Rika and I will be there for you. You won't be alone!"
    hero ""

# game/dialogs.rpy:1754
translate tokipona SH1_69496358:

    # s "Thank you, %(stringhero)s-kun!{p}I'll be as brave as I can!"
    s ""

# game/dialogs.rpy:1757
translate tokipona SH1_e029963b:

    # "She giggled and I waved goodbye to her as she went back to her house."
    ""

# game/dialogs.rpy:1758
translate tokipona SH1_5feddf4e:

    # "She is really unique. I'm glad to have a friend like her."
    ""

# game/dialogs.rpy:1759
translate tokipona SH1_8534ca42:

    # ". . ."
    ""

# game/dialogs.rpy:1760
translate tokipona SH1_aed2ad78:

    # "Although, I was a kinda depressed... I was so in love with her..."
    ""

# game/dialogs.rpy:1761
translate tokipona SH1_051a7c18:

    # "My heart was a bit broken..."
    ""

# game/dialogs.rpy:1762
translate tokipona SH1_7af2582c:

    # "But strangely, I wasn't completely devastated."
    ""

# game/dialogs.rpy:1763
translate tokipona SH1_12bd407a:

    # "Despite the fact we can't be lovers, I feel a lot closer to her."
    ""

# game/dialogs.rpy:1764
translate tokipona SH1_46bb790b:

    # "Plus, it looked like she felt the same... she didn't look that sad either."
    ""

# game/dialogs.rpy:1765
translate tokipona SH1_a20cefa7_1:

    # "..."
    ""

# game/dialogs.rpy:1766
translate tokipona SH1_8d8f367d:

    # "Nah... Everything is just fine.{p}Fine and even better."
    ""

# game/dialogs.rpy:1772
translate tokipona SH1_e3ea38e8:

    # "Yeah, okay... It's a guy..."
    ""

# game/dialogs.rpy:1773
translate tokipona SH1_40db10a1:

    # "So what???"
    ""

# game/dialogs.rpy:1774
translate tokipona SH1_d6afd40a:

    # "Who said that love can only work with the opposite gender!?"
    ""

# game/dialogs.rpy:1775
translate tokipona SH1_4158b541:

    # "I loved Sakura since the start. I desired her since the beginning."
    ""

# game/dialogs.rpy:1776
translate tokipona SH1_12080c5e:

    # "She told me that she's a boy, but I still love and want her! So be it!"
    ""

# game/dialogs.rpy:1777
translate tokipona SH1_a337f545:

    # "I love Sakura-chan, gender doesn't matter!"
    ""

# game/dialogs.rpy:1778
translate tokipona SH1_b5134f34:

    # hero "I understand, now, why you rejected me when I tried to kiss you..."
    hero ""

# game/dialogs.rpy:1779
translate tokipona SH1_c7f3c846:

    # s "Yes..."
    s ""

# game/dialogs.rpy:1780
translate tokipona SH1_690e9f0a:

    # "I smiled and started to come closer to her."
    ""

# game/dialogs.rpy:1781
translate tokipona SH1_a66f34d1:

    # s "As a girl in soul, I prefer boys to girls,"
    s ""

# game/dialogs.rpy:1782
translate tokipona SH1_a5e73d0f:

    # s "But I was afraid to break our hearts if I do this..."
    s ""

# game/dialogs.rpy:1783
translate tokipona SH1_d4993a91:

    # "She felt that I was behind her so she turned around."
    ""

# game/dialogs.rpy:1784
translate tokipona SH1_ef87d060:

    # s "I didn't want to lose my friend, and-"
    s ""

# game/dialogs.rpy:1787
translate tokipona SH1_5cc40c7f:

    # "I don't even let her finish her sentence."
    ""

# game/dialogs.rpy:1788
translate tokipona SH1_747a95a0:

    # "I took her in my arms and kissed her."
    ""

# game/dialogs.rpy:1789
translate tokipona SH1_de5e4ab3:

    # "She tried to step back for a second, {p}but finally she gave in."
    ""

# game/dialogs.rpy:1790
translate tokipona SH1_ddcc906a:

    # "Our lips were lovingly united."
    ""

# game/dialogs.rpy:1791
translate tokipona SH1_96042711:

    # "Yes... I love her."
    ""

# game/dialogs.rpy:1792
translate tokipona SH1_5b84a0a9:

    # "I don't care about what people say.{p}She's a girl and I love her. It's as simple as that."
    ""

# game/dialogs.rpy:1794
translate tokipona SH1_21f57c0f:

    # "We stopped kissing and Sakura-chan looked me in the eyes."
    ""

# game/dialogs.rpy:1795
translate tokipona SH1_7673b5e7:

    # "I found myself swimming into the endless ocean of beauty her gaze offered me..."
    ""

# game/dialogs.rpy:1796
translate tokipona SH1_04263bd9:

    # s "... %(stringhero)s... -kun..."
    s ""

# game/dialogs.rpy:1797
translate tokipona SH1_752a2eec:

    # hero "... Sa... Sakura-san..."
    hero ""

# game/dialogs.rpy:1817
translate tokipona SH1_aab57b73:

    # "We kissed again, but this time, even more intensely."
    ""

# game/dialogs.rpy:1818
translate tokipona SH1_d01f1362:

    # "I found my hands were running down her body. Her body was so womanly, so perfect."
    ""

# game/dialogs.rpy:1819
translate tokipona SH1_b11b14d6:

    # "Her fingers were ruffling my hair as she was held me tighter."
    ""

# game/dialogs.rpy:1820
translate tokipona SH1_fd13e895:

    # "We stayed in the rice fields for a long while. We just couldn't stop..."
    ""

# game/dialogs.rpy:1821
translate tokipona SH1_4237d00f:

    # "Even though my head was spinning a bit from the situation, I still couldn't stop giving her my love..."
    ""

# game/dialogs.rpy:1822
translate tokipona SH1_a7d8f7db:

    # "The sun began to set and filled the sky with a beautiful shade of orange as the evening arrived."
    ""

# game/dialogs.rpy:1826
translate tokipona SH1_5b186bfb:

    # "It was getting late. I had to let Sakura return home. We held hands as we walked to her house."
    ""

# game/dialogs.rpy:1827
translate tokipona SH1_0163bcac:

    # "We were silent. There was no need for words. We were just enjoying every moment together."
    ""

# game/dialogs.rpy:1828
translate tokipona SH1_94a25b9b:

    # "My heart was still racing. I was overwhelmed with joy."
    ""

# game/dialogs.rpy:1829
translate tokipona SH1_32219c59:

    # "Suddenly, Sakura held my arm tighter."
    ""

# game/dialogs.rpy:1830
translate tokipona SH1_15b1a829:

    # s "I wonder what Rika-chan will say when she finds out about us!"
    s ""

# game/dialogs.rpy:1832
translate tokipona SH1_164ae856:

    # s "Knowing her, she might try to kill you! Haha!"
    s ""

# game/dialogs.rpy:1833
translate tokipona SH1_d0bb62e3:

    # hero "...That's kinda scary to think about..."
    hero ""

# game/dialogs.rpy:1834
translate tokipona SH1_d7c1b42b:

    # s "In fact, I've always wondered if she was in love with me or something..."
    s ""

# game/dialogs.rpy:1835
translate tokipona SH1_555aded3:

    # s "She is the Rika-chan you know since she knows my secret."
    s ""

# game/dialogs.rpy:1836
translate tokipona SH1_e7066814:

    # hero "Really?"
    hero ""

# game/dialogs.rpy:1837
translate tokipona SH1_2e63f4b7:

    # hero "Heh... To be honest... She kinda scares me more, now!"
    hero ""

# game/dialogs.rpy:1838
translate tokipona SH1_edeec3ff:

    # "Sakura-chan giggles."
    ""

# game/dialogs.rpy:1839
translate tokipona SH1_c9ae305b:

    # s "Don't worry, I'll protect you! {image=heart.png}"
    s ""

# game/dialogs.rpy:1840
translate tokipona SH1_9554a6c1:

    # "As we approached the front of her house, she rubbed her head against my shoulder."
    ""

# game/dialogs.rpy:1842
translate tokipona SH1_ee198ceb:

    # "I couldn't resist her touch. I took her in my arms and kissed her again."
    ""

# game/dialogs.rpy:1843
translate tokipona SH1_ab476564:

    # "She softly kissed me back."
    ""

# game/dialogs.rpy:1845
translate tokipona SH1_8944b322:

    # "When she stopped, she smiled, squeezed my butt and whispered in my ear."
    ""

# game/dialogs.rpy:1846
translate tokipona SH1_cc8b4089:

    # s "See you tomorrow... sweetheart! {image=heart.png}"
    s ""

# game/dialogs.rpy:1847
translate tokipona SH1_30f6ec48:

    # hero "Heh, you're acting more like a guy when you do stuff like that!"
    hero ""

# game/dialogs.rpy:1848
translate tokipona SH1_f2fe27d5:

    # s "Meanie! {image=heart.png}"
    s ""

# game/dialogs.rpy:1850
translate tokipona SH1_31fe93a4:

    # "She sticked her tongue out at me before disappearing into her house..."
    ""

# game/dialogs.rpy:1852
translate tokipona SH1_f5ae9218:

    # "I... don't know what happened within me..."
    ""

# game/dialogs.rpy:1853
translate tokipona SH1_506f97de:

    # "I was flooding with energy and happiness!"
    ""

# game/dialogs.rpy:1854
translate tokipona SH1_9e8998ef:

    # "I ran along the streets."
    ""

# game/dialogs.rpy:1855
translate tokipona SH1_3f79e021:

    # "I ran, I ran, I ran. As fast as I could. Overwhelmed with joy."
    ""

# game/dialogs.rpy:1856
translate tokipona SH1_91e3f3ea:

    # "I wanted to sing. I wanted to laugh. I wanted to dance."
    ""

# game/dialogs.rpy:1857
translate tokipona SH1_eb6be300:

    # "I felt completely crazy!!!"
    ""

# game/dialogs.rpy:1858
translate tokipona SH1_375cf97b:

    # "I had never felt that kind of euphoria before..."
    ""

# game/dialogs.rpy:1860
translate tokipona SH1_e1e2560b:

    # "As I was running home, I passed by Nanami who was coming back from groceries."
    ""

# game/dialogs.rpy:1861
translate tokipona SH1_6bbff298:

    # n "Hey, %(stringhero)s-senpai! What's cookin'?"
    n ""

# game/dialogs.rpy:1862
translate tokipona SH1_bfe48993:

    # "I lifted her off the ground like a small kitten and spun with her around happily!"
    ""

# game/dialogs.rpy:1863
translate tokipona SH1_0da09179:

    # hero "I'm so happy, Nanami-chan! So so so so so so {b}SO{/b} happy!"
    hero ""

# game/dialogs.rpy:1865
translate tokipona SH1_7e7bd642:

    # n "Eeeeek! W-what the heck!? W-w-why you're so happy? You're acting crazy!"
    n ""

# game/dialogs.rpy:1866
translate tokipona SH1_c2832101:

    # hero "It's because I..."
    hero ""

# game/dialogs.rpy:1867
translate tokipona SH1_192f06a3:

    # "I put back Nanami on the ground."
    ""

# game/dialogs.rpy:1868
translate tokipona SH1_00ab9ecc:

    # hero "No, wait- I don't want to spoil the surprise."
    hero ""

# game/dialogs.rpy:1869
translate tokipona SH1_99c8156c:

    # hero "I'll tell you at school on Monday alongside Rika-chan!"
    hero ""

# game/dialogs.rpy:1871
translate tokipona SH1_a79e612d:

    # n "Monday?! But that's too looooong!"
    n ""

# game/dialogs.rpy:1872
translate tokipona SH1_380da8df:

    # n "Now I really want to know!"
    n ""

# game/dialogs.rpy:1873
translate tokipona SH1_5f2414ee:

    # hero "Patience, little one, patience!"
    hero ""

# game/dialogs.rpy:1875
translate tokipona SH1_f5bde8dc:

    # "I continued running back home, screaming \"WOOOOOOOOOOOOO!\" out loud. I heard a scream from Nanami-"
    ""

# game/dialogs.rpy:1877
translate tokipona SH1_84fa99ca:

    # n "{size=+15}I'M NOT LITTLE!{/size}"
    n ""

# game/dialogs.rpy:1879
translate tokipona SH1_d69bfabe:

    # "My gosh, what a day...!"
    ""

# game/dialogs.rpy:1880
translate tokipona SH1_0d4f6adf:

    # "Sakura called earlier. I'm going on another date with her tomorrow!"
    ""

# game/dialogs.rpy:1881
translate tokipona SH1_dece2bab:

    # "I was about to ask her out on a date myself...looks like she beat me to it."
    ""

# game/dialogs.rpy:1882
translate tokipona SH1_e91f1c64:

    # "Maybe we can read each other's minds now that we're lovers! Anyway...I better get some rest."
    ""

# game/dialogs.rpy:1892
translate tokipona sceneS1_b5e51d3c:

    # "I was waiting for Sakura at the train station of the village."
    ""

# game/dialogs.rpy:1893
translate tokipona sceneS1_6f70efee:

    # "I was getting used to these trips, now. But I'm a bit tired today."
    ""

# game/dialogs.rpy:1894
translate tokipona sceneS1_6a4ff20d:

    # "I couldn't sleep at all last night...I was full of excitement thinking about our lovely date."
    ""

# game/dialogs.rpy:1895
translate tokipona sceneS1_32c4a28b:

    # "Then a joyful Sakura appeared! She was listening to music on a CD player."
    ""

# game/dialogs.rpy:1896
translate tokipona sceneS1_cfc9f30e:

    # "When she saw me, a big smile filled her face. She turned off the CD player and walked toward me."
    ""

# game/dialogs.rpy:1898
translate tokipona sceneS1_641c11ba:

    # s "%(stringhero)s-kun!! Sorry for the wait! {image=heart.png}"
    s ""

# game/dialogs.rpy:1899
translate tokipona sceneS1_a5707cad:

    # hero "Good morning, Sakura-chan!"
    hero ""

# game/dialogs.rpy:1901
translate tokipona sceneS1_bd4c0f66:

    # "We embraced each other with a kiss."
    ""

# game/dialogs.rpy:1902
translate tokipona sceneS1_a85140f3:

    # "Darn, I missed these sweet lips so much."
    ""

# game/dialogs.rpy:1903
translate tokipona sceneS1_0e294c8c:

    # "I had a feeling she had put on a little more makeup on her face for the occasion."
    ""

# game/dialogs.rpy:1905
translate tokipona sceneS1_2c25f5b4:

    # hero "You look wonderful today Sakura-chan."
    hero ""

# game/dialogs.rpy:1906
translate tokipona sceneS1_280a32f7:

    # "She was overflowing with happiness."
    ""

# game/dialogs.rpy:1907
translate tokipona sceneS1_f3a61c54:

    # "I have never seen her so happy before."
    ""

# game/dialogs.rpy:1909
translate tokipona sceneS1_1bc30b12:

    # s "Oh, does that mean I'm not pretty everyday...?"
    s ""

# game/dialogs.rpy:1910
translate tokipona sceneS1_103429d2:

    # hero "Ah! N-no, I mean, you're as pretty as usual... I mean you are always..."
    hero ""

# game/dialogs.rpy:1912
translate tokipona sceneS1_ee5f4b9e:

    # "Sakura gave me a mischievous grin and gave me a quick kiss on the lips."
    ""

# game/dialogs.rpy:1913
translate tokipona sceneS1_7641d5e7:

    # s "Silly... I'm just messing with you! {image=heart.png}"
    s ""

# game/dialogs.rpy:1914
translate tokipona sceneS1_fe43f145:

    # hero "Gah! That's too cruel! {image=heart.png}"
    hero ""

# game/dialogs.rpy:1915
translate tokipona sceneS1_73ac0c99:

    # "We laughed together as the train arrived in the station."
    ""

# game/dialogs.rpy:1920
translate tokipona sceneS1_c93ea523:

    # "We sat on the train, going towards the grand city."
    ""

# game/dialogs.rpy:1921
translate tokipona sceneS1_2af5fbd9:

    # "I was wondering about her CD player, so I decided to ask her."
    ""

# game/dialogs.rpy:1922
translate tokipona sceneS1_95d5b90a:

    # hero "What were you listening to on your CD player?"
    hero ""

# game/dialogs.rpy:1923
translate tokipona sceneS1_d701c031:

    # s "Oh, some classical music."
    s ""

# game/dialogs.rpy:1924
translate tokipona sceneS1_9071067a:

    # hero "You like classical music?"
    hero ""

# game/dialogs.rpy:1925
translate tokipona sceneS1_5ad3424b:

    # s "I love it!"
    s ""

# game/dialogs.rpy:1926
translate tokipona sceneS1_302806ba:

    # "I'm not especially a fan of that kind of music but I will admit that it's good to listen to sometimes. It can be very relaxing."
    ""

# game/dialogs.rpy:1927
translate tokipona sceneS1_865dd143:

    # hero "What's your favorite song?"
    hero ""

# game/dialogs.rpy:1929
translate tokipona sceneS1_ae336353:

    # "Sakura gave me one side of her headphones and started the CD player."
    ""

# game/dialogs.rpy:1932
translate tokipona sceneS1_8534ca42:

    # ". . ."
    ""

# game/dialogs.rpy:1933
translate tokipona sceneS1_083198ae:

    # hero "Oh, I know this song... but I don't remember its name..."
    hero ""

# game/dialogs.rpy:1934
translate tokipona sceneS1_ef73b932:

    # s "It's the {i}Air Orchestral suite #3{/i} of Bach."
    s ""

# game/dialogs.rpy:1935
translate tokipona sceneS1_00935374:

    # s "I don't know why... But I love to listen to this when I'm in a good mood..."
    s ""

# game/dialogs.rpy:1936
translate tokipona sceneS1_6bbb4eeb:

    # hero "You must be in a pretty good mood then, huh?"
    hero ""

# game/dialogs.rpy:1937
translate tokipona sceneS1_1833fef5:

    # "She doesn't reply. She just leans her head on my shoulder and closes her eyes."
    ""

# game/dialogs.rpy:1938
translate tokipona sceneS1_bb3922e5:

    # "I gently lean my head against her."
    ""

# game/dialogs.rpy:1939
translate tokipona sceneS1_54d8e14e:

    # "Her presence, her warmth, her strangely feminine smell, along with the music..."
    ""

# game/dialogs.rpy:1940
translate tokipona sceneS1_b2bf1fcc:

    # "I wish this train would never stop and that we could stay like this forever."
    ""

# game/dialogs.rpy:1945
translate tokipona sceneS1_4bdd421c:

    # hero "...Some things make more sense now that you've told me your secret."
    hero ""

# game/dialogs.rpy:1947
translate tokipona sceneS1_49c95ed0:

    # s "What do you mean?"
    s ""

# game/dialogs.rpy:1948
translate tokipona sceneS1_35d9c8b5:

    # hero "Well, I finally understand why you like shooting games and harem ecchi manga."
    hero ""

# game/dialogs.rpy:1949
translate tokipona sceneS1_1dcf5a89:

    # hero "You have some more boyish tastes in you, despite that the rest of you is pretty feminine."
    hero ""

# game/dialogs.rpy:1950
translate tokipona sceneS1_1ae549d3:

    # s "Yes, it's true."
    s ""

# game/dialogs.rpy:1951
translate tokipona sceneS1_64d14f00:

    # s "I think I got that from my father."
    s ""

# game/dialogs.rpy:1952
translate tokipona sceneS1_75219ee1:

    # s "For my last few birthdays, my father bought me shooting games and ecchi manga."
    s ""

# game/dialogs.rpy:1953
translate tokipona sceneS1_0d2a822f:

    # s "I guess it just stuck with me..."
    s ""

# game/dialogs.rpy:1954
translate tokipona sceneS1_e668a3b6:

    # hero "I see..."
    hero ""

# game/dialogs.rpy:1955
translate tokipona sceneS1_0676a71e:

    # hero "How it was when you were younger? Was it difficult?"
    hero ""

# game/dialogs.rpy:1957
translate tokipona sceneS1_b6297f8f:

    # s "My childhood was kinda chaotic, yes."
    s ""

# game/dialogs.rpy:1958
translate tokipona sceneS1_125b816d:

    # s "I was a boy like the others during my first years at the kindergarten."
    s ""

# game/dialogs.rpy:1959
translate tokipona sceneS1_f7b3d45f:

    # s "But the time passed..."
    s ""

# game/dialogs.rpy:1960
translate tokipona sceneS1_cc5f078f:

    # s "It was clear that I had more girlish tendencies. Very strong ones in fact..."
    s ""

# game/dialogs.rpy:1963
translate tokipona sceneS1_e1554201:

    # s "..."
    s ""

# game/dialogs.rpy:1964
translate tokipona sceneS1_146f1b24:

    # s "When I was going through puberty, I clearly expressed my desire to be considered as a real girl."
    s ""

# game/dialogs.rpy:1965
translate tokipona sceneS1_384c09e4:

    # s "I couldn't help it... My mind was locked in more feminine habits..."
    s ""

# game/dialogs.rpy:1966
translate tokipona sceneS1_eb52cb72:

    # s "It looked like my own body was confused and didn't know how to evolve. For example, my voice didn't deepen like a male."
    s ""

# game/dialogs.rpy:1967
translate tokipona sceneS1_167b4ba2:

    # s "So I let my hair grow, pierced my ears, started to wear skirts, used bras and even put stuff inside my bra to simulate real breasts..."
    s ""

# game/dialogs.rpy:1968
translate tokipona sceneS1_2476fd15:

    # s "When I looked at myself in a mirror dressed like a girl, I knew definitely that I was one. I wanted to be one... And I will be one."
    s ""

# game/dialogs.rpy:1969
translate tokipona sceneS1_e1554201_1:

    # s "..."
    s ""

# game/dialogs.rpy:1970
translate tokipona sceneS1_a678d317:

    # s "My mother accepted it well. She loves me more than anything and will accept anything as long as it makes me happy."
    s ""

# game/dialogs.rpy:1971
translate tokipona sceneS1_6bc7ecb8:

    # s "But other people didn't want to understand..."
    s ""

# game/dialogs.rpy:1972
translate tokipona sceneS1_22ca470a:

    # s "My father, my classmates,..."
    s ""

# game/dialogs.rpy:1973
translate tokipona sceneS1_f7a73d77:

    # s "You get the picture...{p}Well to continue on, my first teenage years were horrible..."
    s ""

# game/dialogs.rpy:1974
translate tokipona sceneS1_e0339899:

    # hero "You mean here? In the village?"
    hero ""

# game/dialogs.rpy:1975
translate tokipona sceneS1_e40cc235:

    # s "No.{p}We were living in Kyoto before, but we moved out here..."
    s ""

# game/dialogs.rpy:1976
translate tokipona sceneS1_b830f777:

    # s "When we started our new life, I decided that I would fight to be considered as a girl in the new town."
    s ""

# game/dialogs.rpy:1977
translate tokipona sceneS1_e1a08602:

    # s "Only my parents and Rika-chan know the secret...{p}And you, of course."
    s ""

# game/dialogs.rpy:1978
translate tokipona sceneS1_fdbe45ca:

    # s "And Nanami-chan, on Monday."
    s ""

# game/dialogs.rpy:1979
translate tokipona sceneS1_e1554201_2:

    # s "..."
    s ""

# game/dialogs.rpy:1980
translate tokipona sceneS1_92a39690:

    # s "You know, I..."
    s ""

# game/dialogs.rpy:1981
translate tokipona sceneS1_8e16ee4d:

    # s "I wish I was a real girl..."
    s ""

# game/dialogs.rpy:1982
translate tokipona sceneS1_c10c0ef9:

    # s "I've always wanted to be a girl. {p}I've always wanted to have long hair.{p}I've always wanted to wear dresses..."
    s ""

# game/dialogs.rpy:1983
translate tokipona sceneS1_d8ba4cdd:

    # s "And I've always wanted... to have a boyfriend..."
    s ""

# game/dialogs.rpy:1984
translate tokipona sceneS1_dbdb5e9d:

    # "I listened to her in complete silence."
    ""

# game/dialogs.rpy:1985
translate tokipona sceneS1_c2573e27:

    # "As she was remembering her childhood memories, I could see some tears were about to come out from her eyes."
    ""

# game/dialogs.rpy:1986
translate tokipona sceneS1_64770d3e:

    # "I put my hand on her shoulder and kiss her head."
    ""

# game/dialogs.rpy:1987
translate tokipona sceneS1_f25fd7bb:

    # hero "Sakura-chan..."
    hero ""

# game/dialogs.rpy:1989
translate tokipona sceneS1_1ad3847b:

    # hero "I know your secret. But I promise you... {p}To me, you'll always be a girl. A real girl..."
    hero ""

# game/dialogs.rpy:1991
translate tokipona sceneS1_4ee0f788:

    # hero "I don't care about what other people think.{p}I don't care what your body thinks."
    hero ""

# game/dialogs.rpy:1992
translate tokipona sceneS1_01d48485:

    # hero "I don't even care if your father hates me someday because of us going out together."
    hero ""

# game/dialogs.rpy:1993
translate tokipona sceneS1_f0c87851:

    # hero "Sakura-chan...You are a girl and I...{p}And I......{p}I........."
    hero ""

# game/dialogs.rpy:1994
translate tokipona sceneS1_2573e7bb:

    # "I never thought it would be so hard to say it, even if you want to express it so much at the same time."
    ""

# game/dialogs.rpy:1995
translate tokipona sceneS1_8dca2ab0:

    # "I forced myself to let it all out."
    ""

# game/dialogs.rpy:1996
translate tokipona sceneS1_5bec7fb2:

    # hero "I... Sakura-chan, I lo... I lo......{p}{size=+15}I love you, Sakura-chan!!!{/size}"
    hero ""

# game/dialogs.rpy:1997
translate tokipona sceneS1_c352284f:

    # "I ended up loudly shouting that out."
    ""

# game/dialogs.rpy:1998
translate tokipona sceneS1_68b19ed1:

    # "Some people in the street were so surprised, they stopped to stare at us."
    ""

# game/dialogs.rpy:1999
translate tokipona sceneS1_1496db50:

    # "I felt terribly embarrassed."
    ""

# game/dialogs.rpy:2000
translate tokipona sceneS1_7bb8b5bf:

    # "Even Sakura turned red with embarrassment."
    ""

# game/dialogs.rpy:2001
translate tokipona sceneS1_c3bbc64c:

    # "Suddenly I realized the deep sense of what I just said."
    ""

# game/dialogs.rpy:2002
translate tokipona sceneS1_0c128ea7:

    # "Yes... Yes, I love her...{p}And I'm not afraid to say it!!!"
    ""

# game/dialogs.rpy:2003
translate tokipona sceneS1_5d447c9e:

    # hero "Yes everyone!! I love her!!! You hear me??? I love HER!!!"
    hero ""

# game/dialogs.rpy:2004
translate tokipona sceneS1_71bc4397:

    # "Strangers just stared at us awkwardly."
    ""

# game/dialogs.rpy:2005
translate tokipona sceneS1_df63e596:

    # "Sakura was still blushing... But suddenly..."
    ""

# game/dialogs.rpy:2006
translate tokipona sceneS1_0f3f7530:

    # "She started to reply, in the same tone as me... Stuttering and trembling a bit but with the same conviction, and as loud as me!"
    ""

# game/dialogs.rpy:2007
translate tokipona sceneS1_c4c3f14c:

    # s "Y... Ye...Yes!!!"
    s ""

# game/dialogs.rpy:2008
translate tokipona sceneS1_d1b481d2:

    # s "{size=+10}Yes, I am a girl!...{p}I am a girl and I love you too, %(stringhero)s-kun!!!!{/size}"
    s ""

# game/dialogs.rpy:2009
translate tokipona sceneS1_6252a7c4:

    # "People continued to stare at us. But soon, they carried on with their lives, ignoring us."
    ""

# game/dialogs.rpy:2010
translate tokipona sceneS1_82c8f385:

    # "I rolled my eyes, appalled at the people."
    ""

# game/dialogs.rpy:2011
translate tokipona sceneS1_7e3455d5:

    # hero "Pfft... People..."
    hero ""

# game/dialogs.rpy:2012
translate tokipona sceneS1_e9431aa5:

    # "Sakura made a strange face... She was holding her mouth while blushing...{p}She was... laughing???"
    ""

# game/dialogs.rpy:2014
translate tokipona sceneS1_17b9939f:

    # "She starts to laugh loudly. It was a nice laugh. The sound filled me with comfort."
    ""

# game/dialogs.rpy:2016
translate tokipona sceneS1_cf3fcfe1:

    # "I was a little surprised, but her laugh made me want to laugh as well."
    ""

# game/dialogs.rpy:2017
translate tokipona sceneS1_5da66cff:

    # hero "Hahaha, what's so funny, Sakura-chan?"
    hero ""

# game/dialogs.rpy:2018
translate tokipona sceneS1_8da8d865:

    # "She tried to reply, but couldn't stop laughing."
    ""

# game/dialogs.rpy:2019
translate tokipona sceneS1_b3fc2700:

    # s "It's... It's just I'm... I'm relieved now that I said it!!"
    s ""

# game/dialogs.rpy:2020
translate tokipona sceneS1_22593ea9:

    # s "And.... the way you rolled your eyes... It was too funny, I couldn't hold it!"
    s ""

# game/dialogs.rpy:2021
translate tokipona sceneS1_917bb3ec:

    # hero "Hey, that's just how I naturally react!"
    hero ""

# game/dialogs.rpy:2022
translate tokipona sceneS1_1809902d:

    # s "Teeheehee! I'm so sorry, %(stringhero)s-kun..."
    s ""

# game/dialogs.rpy:2023
translate tokipona sceneS1_50395e0e:

    # "I rolled my eyes again without realizing and she started to laugh even louder..."
    ""

# game/dialogs.rpy:2024
translate tokipona sceneS1_36e4217d:

    # "I began to laugh as well."
    ""

# game/dialogs.rpy:2025
translate tokipona sceneS1_3b7641fe:

    # "I was so happy."
    ""

# game/dialogs.rpy:2027
translate tokipona sceneS1_8efda62d:

    # "She stopped after a while, wiping away her tears of laughter."
    ""

# game/dialogs.rpy:2028
translate tokipona sceneS1_e15c0017:

    # s "I've never felt happiness like this."
    s ""

# game/dialogs.rpy:2029
translate tokipona sceneS1_1baf5e06:

    # s "Thanks to you, I feel that those sad memories have gone away now."
    s ""

# game/dialogs.rpy:2030
translate tokipona sceneS1_b27b381b:

    # s "Thank you so much...%(stringhero)s-kun! Thank you!"
    s ""

# game/dialogs.rpy:2031
translate tokipona sceneS1_0cb95e5a:

    # "She embraced me tightly like a plushie, like a little girl to her father, with an adorable smile on her lips. I could feel her heart beating against my chest..."
    ""

# game/dialogs.rpy:2037
translate tokipona sceneS1_00f63e60:

    # "We had a lot of fun today in the city."
    ""

# game/dialogs.rpy:2038
translate tokipona sceneS1_936e8fa2:

    # "Holding hands, we were on our way back to the station."
    ""

# game/dialogs.rpy:2039
translate tokipona sceneS1_eb6c4ac4:

    # "But suddenly, Sakura had stopped."
    ""

# game/dialogs.rpy:2040
translate tokipona sceneS1_e8cdcca5:

    # "I turned around to look at her, while still holding her hand."
    ""

# game/dialogs.rpy:2042
translate tokipona sceneS1_e9b16d76:

    # "Her expression was blank, then turned to embarrassment. She was beet-root red."
    ""

# game/dialogs.rpy:2043
translate tokipona sceneS1_27c88c8d:

    # hero "Something wrong?"
    hero ""

# game/dialogs.rpy:2044
translate tokipona sceneS1_0d15642b:

    # "She pointed toward a building near us..."
    ""

# game/dialogs.rpy:2045
translate tokipona sceneS1_1c82a33d:

    # "I looked to see, and it was...{w}{size=+5}love hotel!?!{/size}"
    ""

# game/dialogs.rpy:2046
translate tokipona sceneS1_df826d7c:

    # "Whoa wait what!?"
    ""

# game/dialogs.rpy:2047
translate tokipona sceneS1_58865132:

    # "Does she want to..."
    ""

# game/dialogs.rpy:2048
translate tokipona sceneS1_57d99560:

    # hero "D-do you...you want to...go there?"
    hero ""

# game/dialogs.rpy:2049
translate tokipona sceneS1_be50fa6b:

    # "She nodded and looked down at the ground to hide her shyness. Her hand gripped me tighter."
    ""

# game/dialogs.rpy:2051
translate tokipona sceneS1_c7711bb7:

    # hero "S-sure, let's go."
    hero ""

# game/dialogs.rpy:2055
translate tokipona sceneS1_8534ca42_1:

    # ". . ."
    ""

# game/dialogs.rpy:2056
translate tokipona sceneS1_2c0af0e2:

    # "I woke up in the bed of the hotel."
    ""

# game/dialogs.rpy:2057
translate tokipona sceneS1_1b6c4bc4:

    # "I think I doozed off for only 15 minutes or so. I was feeling pretty sleepy."
    ""

# game/dialogs.rpy:2058
translate tokipona sceneS1_d36164f4:

    # "In my arms, Sakura was sleeping against me."
    ""

# game/dialogs.rpy:2059
translate tokipona sceneS1_9f8a91dc:

    # "She held me tight..."
    ""

# game/dialogs.rpy:2060
translate tokipona sceneS1_f78bfd73:

    # "I guess she must have been exhausted, after so much fun in that room..."
    ""

# game/dialogs.rpy:2061
translate tokipona sceneS1_2418a26e:

    # "She woke up still a little drowsy, and spoke with a weary voice,"
    ""

# game/dialogs.rpy:2062
translate tokipona sceneS1_943f038e:

    # s "%(stringhero)s-kun?.."
    s ""

# game/dialogs.rpy:2063
translate tokipona sceneS1_36317276:

    # hero "Hmm?"
    hero ""

# game/dialogs.rpy:2064
translate tokipona sceneS1_e490aa43:

    # s "Was it... your first time too...?"
    s ""

# game/dialogs.rpy:2065
translate tokipona sceneS1_36fd2a62:

    # hero "Y-yeah..."
    hero ""

# game/dialogs.rpy:2066
translate tokipona sceneS1_f2210395:

    # s "You... Do you regret it?"
    s ""

# game/dialogs.rpy:2067
translate tokipona sceneS1_13b2e8fe:

    # hero "Me?... No... Why should I?"
    hero ""

# game/dialogs.rpy:2068
translate tokipona sceneS1_d723f1ce:

    # s "Nevermind..."
    s ""

# game/dialogs.rpy:2069
translate tokipona sceneS1_3191b879:

    # s "I'm just... I still can't believe I finally found a boy who accepts me the way I am..."
    s ""

# game/dialogs.rpy:2070
translate tokipona sceneS1_67fabef6:

    # s "You even gave me my first time..."
    s ""

# game/dialogs.rpy:2071
translate tokipona sceneS1_f8751689:

    # hero "I'm happy to be your first...{p}And I'm happy that you were my first too."
    hero ""

# game/dialogs.rpy:2072
translate tokipona sceneS1_61308ad3:

    # s "I'm so happy %(stringhero)s-kun... This is almost like a dream..."
    s ""

# game/dialogs.rpy:2073
translate tokipona sceneS1_66805949:

    # "We lay there in peaceful silence together..."
    ""

# game/dialogs.rpy:2074
translate tokipona sceneS1_0af1e620:

    # "I was completely crazy about her... "
    ""

# game/dialogs.rpy:2075
translate tokipona sceneS1_a71c4c3b:

    # "We got lost in each other's eyes."
    ""

# game/dialogs.rpy:2076
translate tokipona sceneS1_4a521333:

    # "Her eyes were filled with love and happiness. She smiled at me..."
    ""

# game/dialogs.rpy:2077
translate tokipona sceneS1_dd054cb9:

    # "We ended up making love one more time... then some time later..."
    ""

# game/dialogs.rpy:2078
translate tokipona sceneS1_0dcaa626:

    # "We took a shower, left the hotel, and took the train home. I walked her home."
    ""

# game/dialogs.rpy:2082
translate tokipona sceneS1_2724f4f7:

    # s "See you at school, honey!"
    s ""

# game/dialogs.rpy:2083
translate tokipona sceneS1_90456e76:

    # hero "See you tomorrow, love..."
    hero ""

# game/dialogs.rpy:2085
translate tokipona sceneS1_adc5375c:

    # "She waved goodbye and entered her house."
    ""

# game/dialogs.rpy:2086
translate tokipona sceneS1_8c41d669:

    # "So much happened this weekend..."
    ""

# game/dialogs.rpy:2087
translate tokipona sceneS1_3eb2b85d:

    # "Sakura told me that she was a boy,{p}then we kissed and went on a date,{p}then we did \"it\"..."
    ""

# game/dialogs.rpy:2088
translate tokipona sceneS1_e36e1081:

    # "I'm so tired... So many things to wrap my head around..."
    ""

# game/dialogs.rpy:2089
translate tokipona sceneS1_45a34284:

    # "I'd like to take the day off tomorrow, but I'll get to see Sakura tomorrow too!"
    ""

# game/dialogs.rpy:2097
translate tokipona sceneS1_443e2e2a:

    # r "{size=+15}WHAAAAAAAAAT??????{/size}"
    r ""

# game/dialogs.rpy:2103
translate tokipona sceneS1_d1f76e80:

    # r "You... Y-Y-Y-Y-You are lo-lo-lo-lovers?!!!"
    r ""

# game/dialogs.rpy:2104
translate tokipona sceneS1_1182fed0:

    # s "Teehee!..."
    s ""

# game/dialogs.rpy:2105
translate tokipona sceneS1_70b77763:

    # hero "Well, yeah..."
    hero ""

# game/dialogs.rpy:2106
translate tokipona sceneS1_bf2888c1:

    # n "Seriously, guys!!!!"
    n ""

# game/dialogs.rpy:2107
translate tokipona sceneS1_87964ca3:

    # r "B... B-B-B-Bu-Bu-Bu...."
    r ""

# game/dialogs.rpy:2108
translate tokipona sceneS1_0e57396a:

    # r "I'm... I-I-I....I'm...."
    r ""

# game/dialogs.rpy:2110
translate tokipona sceneS1_322f4f6b:

    # r "I'm so happy for both of you!!!"
    r ""

# game/dialogs.rpy:2112
translate tokipona sceneS1_d35b9154:

    # r "No, wait. I'm not. Not at all."
    r ""

# game/dialogs.rpy:2113
translate tokipona sceneS1_b5c49fda:

    # r "%(stringhero)s is a damn pervert, he... He will..."
    r ""

# game/dialogs.rpy:2114
translate tokipona sceneS1_5d72dbb5:

    # hero "Hey!!!"
    hero ""

# game/dialogs.rpy:2115
translate tokipona sceneS1_e5583358:

    # n "{size=+10}Seriously, guys!!!!{/size}"
    n ""

# game/dialogs.rpy:2116
translate tokipona sceneS1_35b8ccde:

    # "Sakura looked at me and smiles brightly as she turned red."
    ""

# game/dialogs.rpy:2117
translate tokipona sceneS1_124abe51:

    # "I looked right back at her and I smirked. She starts to laugh a little."
    ""

# game/dialogs.rpy:2119
translate tokipona sceneS1_df3a5fe2:

    # "Rika sees this exchange and starts to stutter."
    ""

# game/dialogs.rpy:2120
translate tokipona sceneS1_6151b676:

    # r "No... N-N-No, d-d-d-don't tell me you... Don't tell me you already...."
    r ""

# game/dialogs.rpy:2121
translate tokipona sceneS1_1d81a8c5:

    # hero "Hmm, alright, we won't tell you, then..."
    hero ""

# game/dialogs.rpy:2127
translate tokipona sceneS1_cc4c3814:

    # r "{size=+15}EEEEEEEEHHHHHHHH!!!{/size}"
    r ""

# game/dialogs.rpy:2129
translate tokipona sceneS1_e4cfb08c:

    # n "{size=+15}SERIOUSLY, GUYS!!!!{/size}"
    n ""

# game/dialogs.rpy:2130
translate tokipona sceneS1_0e6125af:

    # "Rika and Nanami were absolutely shocked."
    ""

# game/dialogs.rpy:2131
translate tokipona sceneS1_1067a846:

    # "Even after a few days passed, Rika still wasn't over it!"
    ""

# game/dialogs.rpy:2132
translate tokipona sceneS1_5c7feca9:

    # "And Nanami randomly shouted \"Seriously, guys!!\" at us for the same amount of time!"
    ""

# game/dialogs.rpy:2133
translate tokipona sceneS1_9ed40c66:

    # "It was kinda fun to watch."
    ""

# game/dialogs.rpy:2165
translate tokipona secretnanami_d274ba9e:

    # "The next day, Nanami invited me to play videogames at her house"
    ""

# game/dialogs.rpy:2167
translate tokipona secretnanami_d7735442:

    # "Nanami's room was a bit messy."
    ""

# game/dialogs.rpy:2168
translate tokipona secretnanami_2bf0b11f:

    # "Her bed and computer setup took up most of the room. Her computer looked more powerful than the eMac I had in my bedroom."
    ""

# game/dialogs.rpy:2169
translate tokipona secretnanami_992fbe8e:

    # "There was also an old TV with several gaming consoles plugged in it."
    ""

# game/dialogs.rpy:2170
translate tokipona secretnanami_2bd3e307:

    # "Random bags of snacks and clothes were scattered about. It was a mess, but kind of an impressive mess."
    ""

# game/dialogs.rpy:2171
translate tokipona secretnanami_2f4f2a6e:

    # "Nanami seemed a bit embarrassed as I was looking around the room."
    ""

# game/dialogs.rpy:2173
translate tokipona secretnanami_4a017f1e:

    # n "D-don't pay any attention to the mess, %(stringhero)s-senpai..."
    n ""

# game/dialogs.rpy:2174
translate tokipona secretnanami_7a1bf678:

    # hero "It's okay, I don't mind."
    hero ""

# game/dialogs.rpy:2175
translate tokipona secretnanami_9b653222:

    # hero "You have so much gaming stuff. It's impressive!"
    hero ""

# game/dialogs.rpy:2176
translate tokipona secretnanami_2b844856:

    # hero "But there's barely any space. Doesn't it feel cramped for you at all?"
    hero ""

# game/dialogs.rpy:2178
translate tokipona secretnanami_d4d4357b:

    # n "Not at all!"
    n ""

# game/dialogs.rpy:2179
translate tokipona secretnanami_8d45096b:

    # n "Everything I could ever need is here!"
    n ""

# game/dialogs.rpy:2180
translate tokipona secretnanami_3e97bcd0:

    # hero "Heh, well after all, you're small. Seems perfect for you."
    hero ""

# game/dialogs.rpy:2182
translate tokipona secretnanami_db57c973:

    # n "I'm not that small!"
    n ""

# game/dialogs.rpy:2183
translate tokipona secretnanami_2fc28d42:

    # "I laughed at her adorable reaction. I continued checking out the room."
    ""

# game/dialogs.rpy:2184
translate tokipona secretnanami_51b37813:

    # hero "Your brother isn't here, today?"
    hero ""

# game/dialogs.rpy:2186
translate tokipona secretnanami_d2016bd8:

    # n "Toshio-nii? No. He usually works on Sunday. He's only home on Mondays."
    n ""

# game/dialogs.rpy:2187
translate tokipona secretnanami_b67b5fcc:

    # n "We have the house to ourselves until 6PM!"
    n ""

# game/dialogs.rpy:2188
translate tokipona secretnanami_84b8bd32:

    # hero "I see."
    hero ""

# game/dialogs.rpy:2190
translate tokipona secretnanami_fec633fd:

    # "I noticed a framed picture near her computer desk."
    ""

# game/dialogs.rpy:2191
translate tokipona secretnanami_03d84c1c:

    # "It looked like it was an old picture of Nanami as a child."
    ""

# game/dialogs.rpy:2192
translate tokipona secretnanami_865a0544:

    # "Beside her, there was an older child. This must be her brother.{p}And behind the both of them, there was an adult couple. Probably her parents..."
    ""

# game/dialogs.rpy:2195
translate tokipona secretnanami_d381eb8b:

    # "Nanami noticed I was looking at it and came closer. Her pleasant expression faded away."
    ""

# game/dialogs.rpy:2196
translate tokipona secretnanami_3d2aa8bc:

    # n "That's...{w}the last picture I have of my parents."
    n ""

# game/dialogs.rpy:2197
translate tokipona secretnanami_2462192a:

    # hero "You mean... They're gone? Are they divorced or something?"
    hero ""

# game/dialogs.rpy:2199
translate tokipona secretnanami_3ac96ee3:

    # "She turned away from me. I faced her back, and she began to speak..."
    ""

# game/dialogs.rpy:2200
translate tokipona secretnanami_03e340ee:

    # n "No..."
    n ""

# game/dialogs.rpy:2201
translate tokipona secretnanami_ad2cdc06:

    # n "They are...{w}dead."
    n ""

# game/dialogs.rpy:2203
translate tokipona secretnanami_cc9f4dc4:

    # "I stayed motionless and was speechless. Shocked by the revelation."
    ""

# game/dialogs.rpy:2204
translate tokipona secretnanami_2c0862cf:

    # hero "I-... I'm so sorry to hear that, Nanami-chan... I didn't mean to bring up a sore subject..."
    hero ""

# game/dialogs.rpy:2206
translate tokipona secretnanami_80110a90:

    # "Nanami sat on the floor, holding her legs."
    ""

# game/dialogs.rpy:2208
translate tokipona secretnanami_eb85d77c:

    # n "You know...They were working at the grocery store your parents are managing today."
    n ""

# game/dialogs.rpy:2209
translate tokipona secretnanami_de2b803f:

    # hero "What...!?"
    hero ""

# game/dialogs.rpy:2210
translate tokipona secretnanami_59529ad1:

    # "Just then, I remembered something. My parents got the shop because the previous owners hadn't been around for a few years."
    ""

# game/dialogs.rpy:2211
translate tokipona secretnanami_9fec9592:

    # "I didn't think they were actually gone..."
    ""

# game/dialogs.rpy:2212
translate tokipona secretnanami_ee152b17:

    # "I sat next to Nanami."
    ""

# game/dialogs.rpy:2213
translate tokipona secretnanami_b4b7b899:

    # n "It happened four years ago."
    n ""

# game/dialogs.rpy:2214
translate tokipona secretnanami_f2bc7b70:

    # n "It... It was on my brother's birthday."
    n ""

# game/dialogs.rpy:2215
translate tokipona secretnanami_90a4c3c8:

    # n "They went to the city to get a surprise present for him..."
    n ""

# game/dialogs.rpy:2216
translate tokipona secretnanami_1a38e7d1:

    # n "But they never came back..."
    n ""

# game/dialogs.rpy:2217
translate tokipona secretnanami_728dd651:

    # n "I heard they were hit by a truck when they were coming back..."
    n ""

# game/dialogs.rpy:2218
translate tokipona secretnanami_28efb3c9:

    # n "Toshio-nii told me. He was the one to get the phone call about my parents."
    n ""

# game/dialogs.rpy:2220
translate tokipona secretnanami_e75dc79b:

    # n "You know... My brother, he...{p}he changed a lot after that."
    n ""

# game/dialogs.rpy:2221
translate tokipona secretnanami_d070d077:

    # n "He blames himself for the death of our parents."
    n ""

# game/dialogs.rpy:2222
translate tokipona secretnanami_d8670523:

    # n "I told him that wasn't true. That it's not his fault."
    n ""

# game/dialogs.rpy:2223
translate tokipona secretnanami_65090fe7:

    # hero "That's right. It's not his fault... It was the fate..."
    hero ""

# game/dialogs.rpy:2224
translate tokipona secretnanami_b6f21cea:

    # n "But he doesn't listen."
    n ""

# game/dialogs.rpy:2225
translate tokipona secretnanami_76cf9acc:

    # n "He's become a little unstable because of that. Sometimes, he scares me a bit."
    n ""

# game/dialogs.rpy:2226
translate tokipona secretnanami_d1c2fbdd:

    # "I instantly thought about Sakura's relationship with her father. But Nanami corrected me immediately like she knew what I was thinking."
    ""

# game/dialogs.rpy:2227
translate tokipona secretnanami_d01d8042:

    # n "He doesn't beat me or argue with me... It's more like the opposite..."
    n ""

# game/dialogs.rpy:2228
translate tokipona secretnanami_d40e7776:

    # n "He's become over-protective of me."
    n ""

# game/dialogs.rpy:2229
translate tokipona secretnanami_43d2f391:

    # n "He says we're each other's only family now and that we have to take good care of each other."
    n ""

# game/dialogs.rpy:2230
translate tokipona secretnanami_f8c997a1:

    # hero "You don't have any grand-parents?"
    hero ""

# game/dialogs.rpy:2231
translate tokipona secretnanami_8fcd3561:

    # n "My father's parents passed away before I was born."
    n ""

# game/dialogs.rpy:2232
translate tokipona secretnanami_eb73e5cd:

    # n "My mother's parents are still alive... But they're all the way in Okinawa."
    n ""

# game/dialogs.rpy:2233
translate tokipona secretnanami_4b6a67a0:

    # n "We write to them, from time to time..."
    n ""

# game/dialogs.rpy:2234
translate tokipona secretnanami_eb3d403f:

    # n "Afterwards, Toshio-nii took a job to afford the house rent and our other needs."
    n ""

# game/dialogs.rpy:2235
translate tokipona secretnanami_e668a3b6:

    # hero "I see..."
    hero ""

# game/dialogs.rpy:2236
translate tokipona secretnanami_a20cefa7:

    # "..."
    ""

# game/dialogs.rpy:2237
translate tokipona secretnanami_0b1702b3:

    # "Darn, I don't know what to say."
    ""

# game/dialogs.rpy:2238
translate tokipona secretnanami_12251861:

    # "Losing your parents at this age must be traumatic. I can barely imagine it."
    ""

# game/dialogs.rpy:2239
translate tokipona secretnanami_1d53eed8:

    # "I felt like Nanami was about to cry."
    ""

# game/dialogs.rpy:2240
translate tokipona secretnanami_966d23a4:

    # "I wrapped an arm around her shoulder and she started to sob silently in my arms."
    ""

# game/dialogs.rpy:2241
translate tokipona secretnanami_ee5f81b5:

    # "She must have greatly loved her parents..."
    ""

# game/dialogs.rpy:2243
translate tokipona secretnanami_8b459908:

    # "After a while, she stopped crying. She wiped her tears and smiled at me."
    ""

# game/dialogs.rpy:2247
translate tokipona secretnanami_42d6bc4c:

    # n "Only a few people know the truth about my parents. Sakura-nee, Rika-nee... And now, you too."
    n ""

# game/dialogs.rpy:2248
translate tokipona secretnanami_0fc2108d:

    # n "People think our parents have gone back to Okinawa, since Toshio-nii is now an adult."
    n ""

# game/dialogs.rpy:2253
translate tokipona secretnanami_511241cb:

    # "So that's what Sakura was talking about at the arcade..."
    ""

# game/dialogs.rpy:2258
translate tokipona secretnanami_c3c4b0be:

    # hero "How are you feeling now? Are you okay?"
    hero ""

# game/dialogs.rpy:2259
translate tokipona secretnanami_8ed55284:

    # n "Yeah... I'm okay, %(stringhero)s-nii."
    n ""

# game/dialogs.rpy:2261
translate tokipona secretnanami_c259ee4e:

    # "I smiled and gently pat her head."
    ""

# game/dialogs.rpy:2262
translate tokipona secretnanami_8f2bc02b:

    # hero "So, are we playing some games? You did invite me for that."
    hero ""

# game/dialogs.rpy:2264
translate tokipona secretnanami_7b9494dd:

    # n "Sure! Let me show you what I have."
    n ""

# game/dialogs.rpy:2266
translate tokipona secretnanami_b5ed2554:

    # "Some time later, we were playing a fighting game called {i}Mortal Battle{/i}."
    ""

# game/dialogs.rpy:2267
translate tokipona secretnanami_b69a371f:

    # "This was an American game known to have complicated and unconventional special attacks. It's hard to get used to it when you mostly play Japanese fighting games."
    ""

# game/dialogs.rpy:2268
translate tokipona secretnanami_1145ba4f:

    # "But I ended up mastering one of the characters in a short amount of time."
    ""

# game/dialogs.rpy:2269
translate tokipona secretnanami_c49f161e:

    # "Every time I selected this character, I ended up beating the crap out of Nanami's character."
    ""

# game/dialogs.rpy:2271
translate tokipona secretnanami_073340cb:

    # n "Eeeeeh! How did you do that special attack?!"
    n ""

# game/dialogs.rpy:2272
translate tokipona secretnanami_0883b3a9:

    # hero "It's not too hard. You just roll the stick like this, then like this and you press HK."
    hero ""

# game/dialogs.rpy:2274
translate tokipona secretnanami_45d61fa6:

    # n "...I still don't get it..."
    n ""

# game/dialogs.rpy:2275
translate tokipona secretnanami_27514623:

    # hero "Hold on, I'll show you."
    hero ""

# game/dialogs.rpy:2280
translate tokipona secretnanami_9d9c7a4c:

    # "I placed myself behind Nanami."
    ""

# game/dialogs.rpy:2281
translate tokipona secretnanami_d11638d3:

    # hero "Don't move, I'll show you exactly how."
    hero ""

# game/dialogs.rpy:2282
translate tokipona secretnanami_98b0a82d:

    # n "O-okay!"
    n ""

# game/dialogs.rpy:2283
translate tokipona secretnanami_9b84d6a3:

    # "I placed my hands on Nanami's, holding the controller with her."
    ""

# game/dialogs.rpy:2284
translate tokipona secretnanami_e5d45e92:

    # "I realized how small her hands were...but they were so soft and warm as well. I started to get a little nervous and blush."
    ""

# game/dialogs.rpy:2285
translate tokipona secretnanami_b142513a:

    # "I felt like she was starting get just as nervous as well."
    ""

# game/dialogs.rpy:2286
translate tokipona secretnanami_72fc66bd:

    # "I went into the training mode in the game."
    ""

# game/dialogs.rpy:2287
translate tokipona secretnanami_068e1468:

    # "Behind the game's sound effects, I could faintly hear Nanami's soft breathing."
    ""

# game/dialogs.rpy:2288
translate tokipona secretnanami_dad13c6d:

    # "I started to feel the warmth of her back on my chest. I could feel myself losing focus..."
    ""

# game/dialogs.rpy:2289
translate tokipona secretnanami_a3e2db4f:

    # hero "O-okay... S-so you... You roll the left stick like that..."
    hero ""

# game/dialogs.rpy:2290
translate tokipona secretnanami_b73c31d9:

    # "I tried the special attack but it failed."
    ""

# game/dialogs.rpy:2291
translate tokipona secretnanami_f8130d09:

    # hero "W-wait, let me try again..."
    hero ""

# game/dialogs.rpy:2292
translate tokipona secretnanami_8fe679da:

    # "I couldn't concentrate with Nanami in my arms like this."
    ""

# game/dialogs.rpy:2294
translate tokipona secretnanami_9f50bc6f:

    # "I could feel my heart beating against her back."
    ""

# game/dialogs.rpy:2295
translate tokipona secretnanami_516207af:

    # "I tried to redo the special attack but I just couldn't do it correctly. My hands were shaking."
    ""

# game/dialogs.rpy:2296
translate tokipona secretnanami_9a928511:

    # "All my thoughts were floating towards Nanami."
    ""

# game/dialogs.rpy:2297
translate tokipona secretnanami_11daa19b:

    # "Her soft smell, her warmth invading my hands and my chest against her back..."
    ""

# game/dialogs.rpy:2298
translate tokipona secretnanami_702709c6:

    # "I... I think I'm falling in love with Nanami..."
    ""

# game/dialogs.rpy:2299
translate tokipona secretnanami_0290a533:

    # "Nanami turned to face to me, her face was glowing red, just like mine."
    ""

# game/dialogs.rpy:2300
translate tokipona secretnanami_1126b44f:

    # "Her eyes were locked to mine. I got lost in her big emerald green eyes."
    ""

# game/dialogs.rpy:2301
translate tokipona secretnanami_1b126339:

    # "It was the coup de grace"
    ""

# game/dialogs.rpy:2302
translate tokipona secretnanami_f5a66b6d:

    # n ".....%(stringhero)s-nii...."
    n ""

# game/dialogs.rpy:2303
translate tokipona secretnanami_c7ff9e11:

    # hero "...Na....Nanami-chan..."
    hero ""

# game/dialogs.rpy:2307
translate tokipona secretnanami_07a482ff:

    # "I felt myself moving toward her lips."
    ""

# game/dialogs.rpy:2326
translate tokipona secretnanami_6546765d:

    # "She came towards me as well, and our lips met."
    ""

# game/dialogs.rpy:2327
translate tokipona secretnanami_ad565af5:

    # "We both dropped the controller and embraced each other tightly, still kissing. Neither of us could stop at all."
    ""

# game/dialogs.rpy:2328
translate tokipona secretnanami_52b5a252:

    # "We fell to the floor in our embrace, trying to avoid some of the mess around."
    ""

# game/dialogs.rpy:2329
translate tokipona secretnanami_ca782044:

    # "Her kiss, her softness, her moaning of desire and her sweet smell were driving me crazy."
    ""

# game/dialogs.rpy:2330
translate tokipona secretnanami_474523d0:

    # "We kept going, sometimes stopping to pronounce each other's name..."
    ""

# game/dialogs.rpy:2331
translate tokipona secretnanami_5ca23c3c:

    # "After a while, she took my hand and gently dragged me to her bed."
    ""

# game/dialogs.rpy:2332
translate tokipona secretnanami_6d6ab9c6:

    # "We continued kissing there, more torridly..."
    ""

# game/dialogs.rpy:2336
translate tokipona secretnanami_8534ca42:

    # ". . ."
    ""

# game/dialogs.rpy:2337
translate tokipona secretnanami_0e5d8dad:

    # "The idle video game was still making some noise in the room."
    ""

# game/dialogs.rpy:2338
translate tokipona secretnanami_95a4b2f0:

    # "I was on the bed, holding Nanami by my side."
    ""

# game/dialogs.rpy:2339
translate tokipona secretnanami_d89bb97f:

    # "She was hugging me like a plushie."
    ""

# game/dialogs.rpy:2340
translate tokipona secretnanami_45685a47:

    # hero "W-was it okay?"
    hero ""

# game/dialogs.rpy:2341
translate tokipona secretnanami_d1a94b7f:

    # n "Y-yeah... It hurt a lil' bit at first,... but it felt good at the end..."
    n ""

# game/dialogs.rpy:2342
translate tokipona secretnanami_5abb8e5f:

    # n "I'm so happy that you were my first, %(stringhero)s-nii..."
    n ""

# game/dialogs.rpy:2343
translate tokipona secretnanami_fa1ef15c:

    # hero "Me too..."
    hero ""

# game/dialogs.rpy:2344
translate tokipona secretnanami_1d6ad191:

    # n "I feel like a woman now thanks to you..."
    n ""

# game/dialogs.rpy:2345
translate tokipona secretnanami_4ce41c79:

    # "I kissed her forehead with a smile."
    ""

# game/dialogs.rpy:2346
translate tokipona secretnanami_e9590123:

    # "Nanami suddenly giggled."
    ""

# game/dialogs.rpy:2347
translate tokipona secretnanami_08ea1428:

    # n "I can't wait to see Sakura-nee and Rika-nee's faces when they find out about us!"
    n ""

# game/dialogs.rpy:2348
translate tokipona secretnanami_d3d236dd:

    # hero "Hehe I'm curious to see that too..."
    hero ""

# game/dialogs.rpy:2349
translate tokipona secretnanami_523e30b8:

    # "Then I had a mischievous thought..."
    ""

# game/dialogs.rpy:2350
translate tokipona secretnanami_ceb62c81:

    # hero "I have a fun idea:{p}Why not just let them figure it out themselves?"
    hero ""

# game/dialogs.rpy:2351
translate tokipona secretnanami_46f913e1:

    # hero "They might end up even more surprised!"
    hero ""

# game/dialogs.rpy:2352
translate tokipona secretnanami_ae388490:

    # "Nanami giggled louder."
    ""

# game/dialogs.rpy:2353
translate tokipona secretnanami_6ab9da22:

    # n "Good idea! Let's do this!"
    n ""

# game/dialogs.rpy:2354
translate tokipona secretnanami_b3625191:

    # hero "Right!"
    hero ""

# game/dialogs.rpy:2355
translate tokipona secretnanami_ec3a4b43:

    # "I giggled and kissed her lips again. She embraced me tighter, kissing back..."
    ""

# game/dialogs.rpy:2356
translate tokipona secretnanami_82623620:

    # "Before I could even notice, we were on for a second round..."
    ""

# game/dialogs.rpy:2357
translate tokipona secretnanami_8116efac:

    # "Hehe... \"Second round\"...{p}I bet she finds this 'new game' better than any other game she's played!"
    ""

# game/dialogs.rpy:2361
translate tokipona secretnanami_3402d7bf:

    # "After that, it was late so it was time for me to go. "
    ""

# game/dialogs.rpy:2362
translate tokipona secretnanami_924e253f:

    # hero "Don't forget: Don't tell anyone in the club!"
    hero ""

# game/dialogs.rpy:2364
translate tokipona secretnanami_91f1d653:

    # n "Yes sir!"
    n ""

# game/dialogs.rpy:2366
translate tokipona secretnanami_743da19c:

    # "We kissed again and then I left for home."
    ""

# game/dialogs.rpy:2370
translate tokipona secretnanami_ffc14834:

    # "I was so happy. I was hopping around and dancing a bit."
    ""

# game/dialogs.rpy:2371
translate tokipona secretnanami_bb28eb57:

    # "People probably thought I was crazy but I didn't care."
    ""

# game/dialogs.rpy:2375
translate tokipona secretnanami_50788d9c:

    # "Monday arrived..."
    ""

# game/dialogs.rpy:2376
translate tokipona secretnanami_e94279e1:

    # "I was thinking about all the new things I learned this weekend..."
    ""

# game/dialogs.rpy:2377
translate tokipona secretnanami_ff29a530:

    # "I still can't believe Sakura was born as a boy."
    ""

# game/dialogs.rpy:2378
translate tokipona secretnanami_d6d86bf5:

    # "But I can't wait to see her again anyway. She's my best friend."
    ""

# game/dialogs.rpy:2380
translate tokipona secretnanami_12a5358b:

    # "I also thought about Nanami... My dear love..."
    ""

# game/dialogs.rpy:2381
translate tokipona secretnanami_55bde2ad:

    # "I can't wait to see her again too... And to make that joke to our friends!"
    ""

# game/dialogs.rpy:2382
translate tokipona secretnanami_e6c4d8b3:

    # "Thinking of Sakura and Nanami made me remember my plan for Sakura."
    ""

# game/dialogs.rpy:2383
translate tokipona secretnanami_da787f5b:

    # "I found Rika and told her I knew about Sakura, and that she needs help to tell the truth to Nanami as well."
    ""

# game/dialogs.rpy:2384
translate tokipona secretnanami_f5d57b70:

    # "She gladly accepted."
    ""

# game/dialogs.rpy:2385
translate tokipona secretnanami_eb899bec:

    # "For the first time, I saw on her a sincere sweet smile."
    ""

# game/dialogs.rpy:2386
translate tokipona secretnanami_258e716e:

    # "She told me that I'm sure to be a special member of the club now."
    ""

# game/dialogs.rpy:2388
translate tokipona secretnanami_4fe7e943:

    # "Of course, I didn't tell her about Nanami and I..."
    ""

# game/dialogs.rpy:2389
translate tokipona secretnanami_e02d74e7:

    # "Yet..."
    ""

# game/dialogs.rpy:2395
translate tokipona secretnanami_3e63050d:

    # "Lunch time came."
    ""

# game/dialogs.rpy:2397
translate tokipona secretnanami_7d31f878:

    # "I was eating lunch at my desk with Sakura, as we always do."
    ""

# game/dialogs.rpy:2398
translate tokipona secretnanami_96641885:

    # "Usually, Rika and Nanami join us a few minutes after we start."
    ""

# game/dialogs.rpy:2399
translate tokipona secretnanami_fcd3f037:

    # hero "Oh, guess what? I know about Nanami, now."
    hero ""

# game/dialogs.rpy:2400
translate tokipona secretnanami_7f54d05d:

    # "Sakura nodded, knowing what I meant."
    ""

# game/dialogs.rpy:2401
translate tokipona secretnanami_26f2656b:

    # s "Looks like she really likes and trusts you, now! That's great!"
    s ""

# game/dialogs.rpy:2402
translate tokipona secretnanami_d5961b9f:

    # "I held a laugh. If only she knew how much she likes me now!"
    ""

# game/dialogs.rpy:2405
translate tokipona secretnanami_ba640f4d:

    # "Nanami came first."
    ""

# game/dialogs.rpy:2406
translate tokipona secretnanami_eb443bf4:

    # n "Hey, Sakura-nee!"
    n ""

# game/dialogs.rpy:2408
translate tokipona secretnanami_0f4878e5:

    # n "Hey,...honey!"
    n ""

# game/dialogs.rpy:2410
translate tokipona secretnanami_349c95fd:

    # "Nanami left a kiss on my cheek, took a free chair and table and joined us like if nothing happened."
    ""

# game/dialogs.rpy:2411
translate tokipona secretnanami_91c5f81c:

    # "Wow, she's good. It's hard to not blush in this situation."
    ""

# game/dialogs.rpy:2412
translate tokipona secretnanami_0083c6b0:

    # "The face Sakura made was priceless."
    ""

# game/dialogs.rpy:2413
translate tokipona secretnanami_91e03536:

    # "Her face was flustered and expressed something along the lines of \"What the heck?\" and \"That's embarassing!\""
    ""

# game/dialogs.rpy:2414
translate tokipona secretnanami_d1b32a3c:

    # "I had a hard time trying to not laugh out loud.{p}I could tell, Nanami was holding it in too."
    ""

# game/dialogs.rpy:2416
translate tokipona secretnanami_5e2e8af1:

    # "Then Rika appeared and joined us."
    ""

# game/dialogs.rpy:2417
translate tokipona secretnanami_2f6f7af4:

    # r "Hey guys! Did you have a good Sunday?"
    r ""

# game/dialogs.rpy:2418
translate tokipona secretnanami_b408e187:

    # "Nanami and I almost choked on our meal at the question."
    ""

# game/dialogs.rpy:2420
translate tokipona secretnanami_e5fcea13:

    # r "Sakura, you're okay?"
    r ""

# game/dialogs.rpy:2421
translate tokipona secretnanami_5ab8d803:

    # s "I'm... I'm not sure..."
    s ""

# game/dialogs.rpy:2422
translate tokipona secretnanami_5ddf9bba:

    # "All during lunch, Nanami and I were exchanging glances, and every time we had to hold a laugh."
    ""

# game/dialogs.rpy:2423
translate tokipona secretnanami_3df5cd5f:

    # "All of this was making Sakura and Rika even more confused and uncomfortable."
    ""

# game/dialogs.rpy:2424
translate tokipona secretnanami_0469525e:

    # "Finally, Rika decided to speak up."
    ""

# game/dialogs.rpy:2426
translate tokipona secretnanami_848c0a9a:

    # r "Alright, guys. It looks like Sakura and I missed the memo. What's going on?"
    r ""

# game/dialogs.rpy:2427
translate tokipona secretnanami_78a3d7bc:

    # r "What happened this weekend?!"
    r ""

# game/dialogs.rpy:2429
translate tokipona secretnanami_a2210385:

    # "Nanami finally cracked. She burst into laughter. Some classmates turned their heads wondering what was going on."
    ""

# game/dialogs.rpy:2430
translate tokipona secretnanami_e7188aa7:

    # "Her laugh instantly unlocked mine and I finished laughing as well!"
    ""

# game/dialogs.rpy:2432
translate tokipona secretnanami_1b6c8610:

    # r "What the!?"
    r ""

# game/dialogs.rpy:2434
translate tokipona secretnanami_5bc75703:

    # r "Sakura, tell me! You must know something!"
    r ""

# game/dialogs.rpy:2435
translate tokipona secretnanami_a3d554dc:

    # s "I... They... I don't know!..."
    s ""

# game/dialogs.rpy:2436
translate tokipona secretnanami_d6d264c2:

    # s "Nana-chan kissed %(stringhero)s-kun on the cheek and they've been acting like this since!"
    s ""

# game/dialogs.rpy:2438
translate tokipona secretnanami_dba98263:

    # r "No way! {w}You mean..."
    r ""

# game/dialogs.rpy:2439
translate tokipona secretnanami_8fd173cb:

    # "I calmed down and spoke with my natural voice:"
    ""

# game/dialogs.rpy:2440
translate tokipona secretnanami_588f721e:

    # hero "Oh yeah, Nanami and I started going out together."
    hero ""

# game/dialogs.rpy:2441
translate tokipona secretnanami_83c319f1:

    # hero "We didn't tell you?"
    hero ""

# game/dialogs.rpy:2464
translate tokipona secretnanami_e9fe3d46:

    # s "{size=+25}EEEEEEEEHHHHHHHHH???!!!!!{/size}"
    s ""

# game/dialogs.rpy:2466
translate tokipona secretnanami_e1326833:

    # r "{size=+25}I KNEW IT!!!!!{/size}"
    r ""

# game/dialogs.rpy:2467
translate tokipona secretnanami_e9aabc9d:

    # "Another burst of laughter occurred between Nanami and I."
    ""

# game/dialogs.rpy:2472
translate tokipona secretnanami_597fe66a:

    # "After lunch, I found Sakura speaking with Rika alone."
    ""

# game/dialogs.rpy:2473
translate tokipona secretnanami_d72a5ad0:

    # "When Rika saw me, she beckoned me to come over."
    ""

# game/dialogs.rpy:2477
translate tokipona secretnanami_eec796bb:

    # hero "What's going on?"
    hero ""

# game/dialogs.rpy:2478
translate tokipona secretnanami_7f82104e:

    # r "It's time. Nanami is waiting for us at the club room."
    r ""

# game/dialogs.rpy:2479
translate tokipona secretnanami_b6e3bb19:

    # r "Are you both ready?"
    r ""

# game/dialogs.rpy:2480
translate tokipona secretnanami_326263c8:

    # hero "I am. As long as Sakura is."
    hero ""

# game/dialogs.rpy:2482
translate tokipona secretnanami_feb04fe2:

    # s "I am... Let's go."
    s ""

# game/dialogs.rpy:2488
translate tokipona secretnanami_9df2c3c5:

    # n "Hey again, guys!"
    n ""

# game/dialogs.rpy:2492
translate tokipona secretnanami_4ceec5a6:

    # r "Hey, Nanami."
    r ""

# game/dialogs.rpy:2493
translate tokipona secretnanami_fe8acb83:

    # "I whispered \"Go on, don't worry!\" into Sakura's ear and she nodded."
    ""

# game/dialogs.rpy:2495
translate tokipona secretnanami_10c08f00:

    # s "Nanami-chan..."
    s ""

# game/dialogs.rpy:2497
translate tokipona secretnanami_ee407c47:

    # s "There's something I want to tell you."
    s ""

# game/dialogs.rpy:2499
translate tokipona secretnanami_54f6b843:

    # n "What is it?...{p}It looks serious... {w}Are you guys okay?"
    n ""

# game/dialogs.rpy:2500
translate tokipona secretnanami_1feba992:

    # hero "Yeah, no worries. It's just...something that you need to know..."
    hero ""

# game/dialogs.rpy:2501
translate tokipona secretnanami_609aaa6d:

    # s "Since Rika-chan and %(stringhero)s-kun are aware of it, I thought it would be unfair for you to not know..."
    s ""

# game/dialogs.rpy:2504
translate tokipona secretnanami_827414d5:

    # n "Hey hey, that's okay! Hiding things can be fun sometimes! Remember the lunch?"
    n ""

# game/dialogs.rpy:2505
translate tokipona secretnanami_1ac65a8e:

    # s "It's not really the same thing, Nana-chan..."
    s ""

# game/dialogs.rpy:2507
translate tokipona secretnanami_73096c17:

    # n "Ah?..."
    n ""

# game/dialogs.rpy:2508
translate tokipona secretnanami_790191ec:

    # s "It's always hard for me to find the right words so it doesn't shock you too much..."
    s ""

# game/dialogs.rpy:2509
translate tokipona secretnanami_5667ce78:

    # n "It's something that big?"
    n ""

# game/dialogs.rpy:2510
translate tokipona secretnanami_22a96dc3:

    # "Sakura nodded."
    ""

# game/dialogs.rpy:2511
translate tokipona secretnanami_f8f4fa69:

    # s "And before you ask..."
    s ""

# game/dialogs.rpy:2512
translate tokipona secretnanami_6d099b03:

    # s "I'm so sorry I didn't tell you earlier. I didn't know how to tell you."
    s ""

# game/dialogs.rpy:2513
translate tokipona secretnanami_2a2228a7:

    # s "Before I tell you, can you forgive me for this?"
    s ""

# game/dialogs.rpy:2515
translate tokipona secretnanami_564d9e25:

    # "Nanami smiled softly."
    ""

# game/dialogs.rpy:2516
translate tokipona secretnanami_67350be4:

    # n "Of course I can. You're my Sakura-nee!"
    n ""

# game/dialogs.rpy:2517
translate tokipona secretnanami_c66e8457:

    # "Sakura nodded again with a faint smile. Then, after a moment, she started to reveal her secret."
    ""

# game/dialogs.rpy:2518
translate tokipona secretnanami_727b9f43:

    # s "Nana-chan..."
    s ""

# game/dialogs.rpy:2519
translate tokipona secretnanami_5b84e198:

    # s "I am...{w}not...{w}technically a girl..."
    s ""

# game/dialogs.rpy:2521
translate tokipona secretnanami_33d36d5f:

    # n "Huh?"
    n ""

# game/dialogs.rpy:2522
translate tokipona secretnanami_e8a331c8:

    # s "I..."
    s ""

# game/dialogs.rpy:2523
translate tokipona secretnanami_ce4432b4:

    # "Rika came closer and held Sakura's hand to give her strength. Rika helped Sakura gather herself."
    ""

# game/dialogs.rpy:2524
translate tokipona secretnanami_d8383a12:

    # r "Sakura...{w}wasn't born as a girl, like you and me."
    r ""

# game/dialogs.rpy:2525
translate tokipona secretnanami_80777581:

    # "I placed my hand on Sakura's shoulder. I helped Rika speak on behalf of Sakura."
    ""

# game/dialogs.rpy:2526
translate tokipona secretnanami_fd25aa6e:

    # hero "Yeah...She's a girl on the inside, but originally, she was born as a boy. Like me."
    hero ""

# game/dialogs.rpy:2528
translate tokipona secretnanami_77609d5b:

    # "Nanami didn't say a word. She was trying to process the information. Her face displayed confusion."
    ""

# game/dialogs.rpy:2529
translate tokipona secretnanami_8961280a:

    # "Sakura gathered herself. She started to speak about her childhood, almost repeating word by word what she told me."
    ""

# game/dialogs.rpy:2531
translate tokipona secretnanami_d7384a8d:

    # "Nanami listened silently, nodding from time to time.{p}After Sakura finished, Nanami had similar questions to mine at time."
    ""

# game/dialogs.rpy:2532
translate tokipona secretnanami_e145542f:

    # n "I understand now..."
    n ""

# game/dialogs.rpy:2534
translate tokipona secretnanami_8d8cc9b7:

    # "Then Nanami grew a cute smile."
    ""

# game/dialogs.rpy:2535
translate tokipona secretnanami_5399f645:

    # n "That's okay for me!"
    n ""

# game/dialogs.rpy:2539
translate tokipona secretnanami_e160708b:

    # n "I'm happy that you told me your secret!"
    n ""

# game/dialogs.rpy:2540
translate tokipona secretnanami_93b2b022:

    # n "I know I'm special to you, Sakura-nee, but now I feel it more than ever!"
    n ""

# game/dialogs.rpy:2541
translate tokipona secretnanami_bfeb43f4:

    # "Sakura smiled, some tears coming from her eyes."
    ""

# game/dialogs.rpy:2544
translate tokipona secretnanami_957c3f61:

    # "Nanami jumped on Sakura and hugged her tightly with a bright smile and some tears."
    ""

# game/dialogs.rpy:2545
translate tokipona secretnanami_7c8449b8:

    # "That was touching."
    ""

# game/dialogs.rpy:2546
translate tokipona secretnanami_106608f8:

    # n "I understand why it was so hard to tell me that.{p}I'm sure it's not easy to tell a secret like that!"
    n ""

# game/dialogs.rpy:2547
translate tokipona secretnanami_df955372:

    # "Sakura chuckles."
    ""

# game/dialogs.rpy:2548
translate tokipona secretnanami_3c734a09:

    # s "It sure isn't!"
    s ""

# game/dialogs.rpy:2549
translate tokipona secretnanami_120f3ff5:

    # s "But now I'm the happiest girl ever..."
    s ""

# game/dialogs.rpy:2550
translate tokipona secretnanami_ba0e9538:

    # s "Because, all the people I love and care about know my secret now."
    s ""

# game/dialogs.rpy:2551
translate tokipona secretnanami_1abc70b9:

    # s "I don't need to hide anything to any of you now."
    s ""

# game/dialogs.rpy:2552
translate tokipona secretnanami_128578da:

    # n "Hiding what?"
    n ""

# game/dialogs.rpy:2553
translate tokipona secretnanami_77ee7366:

    # s "Huh?"
    s ""

# game/dialogs.rpy:2554
translate tokipona secretnanami_ce51de81:

    # n "You're a girl, Sakura-nee."
    n ""

# game/dialogs.rpy:2555
translate tokipona secretnanami_20f09b8b:

    # n "You're born as a boy? That's a small detail."
    n ""

# game/dialogs.rpy:2556
translate tokipona secretnanami_1ca8316e:

    # n "To me, you'll always be my Sakura-nee!"
    n ""

# game/dialogs.rpy:2557
translate tokipona secretnanami_e130ea9c:

    # "Sakura kissed Nanami's head and rubbed her hair like for a child."
    ""

# game/dialogs.rpy:2558
translate tokipona secretnanami_b45c3328:

    # s "Thank you so much... My Nana-chan...{p}My little one..."
    s ""

# game/dialogs.rpy:2576
translate tokipona secretnanami_9e1d86cb:

    # n "I'm not little!..."
    n ""

# game/dialogs.rpy:2612
translate tokipona scene_R_13535866:

    # centered "{size=+35}CHAPTER 5\nThe festival{fast}{/size}"
    centered ""

# game/dialogs.rpy:2613
translate tokipona scene_R_9a9fec2d:

    # alt "CHAPTER 5\nThe festival"
    alt ""

# game/dialogs.rpy:2617
translate tokipona scene_R_31591cda:

    # "The week went by quickly..."
    ""

# game/dialogs.rpy:2620
translate tokipona scene_R_57141cf2:

    # "Friday evening came. The day before the festival..."
    ""

# game/dialogs.rpy:2623
translate tokipona scene_R_1b27c60b:

    # "Preparations for the festival were awfully hard. But I promised Rika I'd help her out."
    ""

# game/dialogs.rpy:2624
translate tokipona scene_R_2d950149:

    # "We had to prepare the stands and the decorations."
    ""

# game/dialogs.rpy:2625
translate tokipona scene_R_9be7bc2a:

    # "Rika came to tell what to do and where to go."
    ""

# game/dialogs.rpy:2626
translate tokipona scene_R_b0c123df:

    # hero "Hey, maybe could you give us a hand!?"
    hero ""

# game/dialogs.rpy:2627
translate tokipona scene_R_8a0b645d:

    # r "I can't, I'm guiding you, idiot!"
    r ""

# game/dialogs.rpy:2628
translate tokipona scene_R_139a795e:

    # "We really don't have the same understanding of the word \"help.\" "
    ""

# game/dialogs.rpy:2630
translate tokipona scene_R_08547004:

    # "I had to move some boxes inside the temple. I was alone."
    ""

# game/dialogs.rpy:2631
translate tokipona scene_R_166ebec1:

    # "The boxes were inside a big dark room with a lot of old junk."
    ""

# game/dialogs.rpy:2632
translate tokipona scene_R_9899d4dc:

    # "It's so dark. I can't see a thing..."
    ""

# game/dialogs.rpy:2633
translate tokipona scene_R_7f278c6a:

    # hero "Rika-chan? Are you in there?"
    hero ""

# game/dialogs.rpy:2634
translate tokipona scene_R_a5dde9ae:

    # r "Yes, come over here!"
    r ""

# game/dialogs.rpy:2635
translate tokipona scene_R_cd339c0e:

    # "I follow the direction of her voice. But I suddenly stumbled onto something and I fell on the floor."
    ""

# game/dialogs.rpy:2636
translate tokipona scene_R_4aa7bb8a:

    # "Well, not exactly the floor. I fell on something warm and comfy."
    ""

# game/dialogs.rpy:2637
translate tokipona scene_R_e0fa74ca:

    # hero "Ah, I fell on something..."
    hero ""

# game/dialogs.rpy:2638
translate tokipona scene_R_4642c748:

    # "My hand squeezed the mellow thing I was on."
    ""

# game/dialogs.rpy:2656
translate tokipona scene_R_02b7fdcb:

    # hero "What is this... I don't even-"
    hero ""

# game/dialogs.rpy:2657
translate tokipona scene_R_d557eb58:

    # r "Y-You {size=+20}IDIOT!!!!!!{/size}"
    r ""

# game/dialogs.rpy:2660
translate tokipona scene_R_73674ca2:

    # "*{i}SMACK!{/i}*"
    ""

# game/dialogs.rpy:2661
translate tokipona scene_R_c81c00b8:

    # "Something hit my cheek and I instantly got up by instinct..."
    ""

# game/dialogs.rpy:2662
translate tokipona scene_R_ece959c9:

    # "I realized what I fell on. Or who I fell on..."
    ""

# game/dialogs.rpy:2663
translate tokipona scene_R_b70add4f:

    # r "You PERVERT!!! Stop touching me!!!"
    r ""

# game/dialogs.rpy:2664
translate tokipona scene_R_5462e3bc:

    # hero "I stopped! I stopped!"
    hero ""

# game/dialogs.rpy:2665
translate tokipona scene_R_5c272729:

    # hero "I'm sorry, it was an accident!"
    hero ""

# game/dialogs.rpy:2666
translate tokipona scene_R_bd0bcebc:

    # r "Turn the freakin light on and help me lift this box up there!"
    r ""

# game/dialogs.rpy:2668
translate tokipona scene_R_268b921a:

    # "Damn! This girl is nuts!!!"
    ""

# game/dialogs.rpy:2669
translate tokipona scene_R_a20cefa7:

    # "..."
    ""

# game/dialogs.rpy:2670
translate tokipona scene_R_4d9f847b:

    # "Although her breast felt pretty nice...{p}It was warm... and mellow... and big..."
    ""

# game/dialogs.rpy:2671
translate tokipona scene_R_45a00c24:

    # "Eh!? What the hell am I thinking about!?"
    ""

# game/dialogs.rpy:2672
translate tokipona scene_R_62d361bd:

    # "Rika is definitely not my type of girl. Not at all!"
    ""

# game/dialogs.rpy:2674
translate tokipona scene_R_9f1c3606:

    # "Plus I'm already with Sakura-chan!"
    ""

# game/dialogs.rpy:2676
translate tokipona scene_R_076bdc09:

    # "Plus I'm already with Nanami-chan!"
    ""

# game/dialogs.rpy:2678
translate tokipona scene_R_a20cefa7_1:

    # "..."
    ""

# game/dialogs.rpy:2684
translate tokipona decision_31826e00:

    # "The day of the matsuri arrived."
    ""

# game/dialogs.rpy:2685
translate tokipona decision_400d0a26:

    # "As the club planned, I will go with Sakura-chan and Rika-chan. And maybe Nanami-chan if she's motivated."
    ""

# game/dialogs.rpy:2686
translate tokipona decision_07ca0e28:

    # "I can't wait to see them!"
    ""

# game/dialogs.rpy:2697
translate tokipona matsuri_S_2ef9ea22:

    # "I was waiting for Sakura in front of her house."
    ""

# game/dialogs.rpy:2699
translate tokipona matsuri_S_1dcaa12e:

    # "I was wearing my brand new yukata that she gave me."
    ""

# game/dialogs.rpy:2701
translate tokipona matsuri_S_201b46ce:

    # "I was wearing the yukata I recently bought... One which was awfully expensive..."
    ""

# game/dialogs.rpy:2702
translate tokipona matsuri_S_9c64454a:

    # "She finally appeared, in a wonderful pink and red kimono."
    ""

# game/dialogs.rpy:2703
translate tokipona matsuri_S_c633905a:

    # "She looked like a miko priestess. She was pretty..."
    ""

# game/dialogs.rpy:2705
translate tokipona matsuri_S_678a9d48:

    # hero "Wow... Sakura-chan... You look superb!"
    hero ""

# game/dialogs.rpy:2706
translate tokipona matsuri_S_4eeac269:

    # s "%(stringhero)s-kun! The yukata suits you very nicely!"
    s ""

# game/dialogs.rpy:2708
translate tokipona matsuri_S_02c86c6c:

    # "She comes closer and whispers in my ear with a grin."
    ""

# game/dialogs.rpy:2709
translate tokipona matsuri_S_9e55e999:

    # s "I wonder what's hidden inside..."
    s ""

# game/dialogs.rpy:2710
translate tokipona matsuri_S_e44df551:

    # hero "Hehe maybe you'll see later..."
    hero ""

# game/dialogs.rpy:2712
translate tokipona matsuri_S_96913011:

    # "I saw a woman come out of the house. That must be Sakura's mother."
    ""

# game/dialogs.rpy:2713
translate tokipona matsuri_S_af9ec1db:

    # "She looks incredibly young for a mother of an 18-year-old. She looks like she's in her early 30s at least!"
    ""

# game/dialogs.rpy:2717
translate tokipona matsuri_S_6d0cbd15:

    # smom "Oh, you must be %(stringhero)s-san, I presume!"
    smom ""

# game/dialogs.rpy:2718
translate tokipona matsuri_S_fde87b49:

    # smom "My daughter never stops talking about you!"
    smom ""

# game/dialogs.rpy:2720
translate tokipona matsuri_S_273b854b:

    # "Sakura looked embarrassed and blushed."
    ""

# game/dialogs.rpy:2721
translate tokipona matsuri_S_2bf9e723:

    # s "Mom!!!"
    s ""

# game/dialogs.rpy:2722
translate tokipona matsuri_S_b8980021:

    # hero "From the yukata you're wearing, I assume that you'll come to the matsuri too, ma'am?"
    hero ""

# game/dialogs.rpy:2724
translate tokipona matsuri_S_ed2ca74b:

    # smom "Yes. Once my husband is ready, we'll go as well."
    smom ""

# game/dialogs.rpy:2725
translate tokipona matsuri_S_5fc7645d:

    # s "We'll be going on ahead, mom. See you later!"
    s ""

# game/dialogs.rpy:2726
translate tokipona matsuri_S_0c6a0f45:

    # smom "Have fun, you two!"
    smom ""

# game/dialogs.rpy:2730
translate tokipona matsuri_S_be3185f9:

    # "We were about to leave but then, Sakura's mother called me."
    ""

# game/dialogs.rpy:2732
translate tokipona matsuri_S_6c045540:

    # "She spoke quietly."
    ""

# game/dialogs.rpy:2733
translate tokipona matsuri_S_8c173d23:

    # smom "I know that you and my daughter are going out."
    smom ""

# game/dialogs.rpy:2734
translate tokipona matsuri_S_c0582308:

    # "Yikes!"
    ""

# game/dialogs.rpy:2735
translate tokipona matsuri_S_a5ebdd09:

    # smom "Sakura told me how much you love each other."
    smom ""

# game/dialogs.rpy:2736
translate tokipona matsuri_S_043f4b2a:

    # smom "I'm so happy that my daughter found someone so kind and so understanding."
    smom ""

# game/dialogs.rpy:2737
translate tokipona matsuri_S_47abc600:

    # hero "I-it's fine, ma'am... I...{p}I do love Sakura-chan. More than anything... No matter how she was born."
    hero ""

# game/dialogs.rpy:2738
translate tokipona matsuri_S_8a914f1f:

    # smom "That's sweet."
    smom ""

# game/dialogs.rpy:2739
translate tokipona matsuri_S_9981c331:

    # smom "Can I count on you to make her the happiest woman she's always wished to be?"
    smom ""

# game/dialogs.rpy:2740
translate tokipona matsuri_S_707d7f10:

    # hero "I promise I will, ma'am!"
    hero ""

# game/dialogs.rpy:2742
translate tokipona matsuri_S_c944f29d:

    # "She smiled and I went back to Sakura. We waved goodbye to Sakura's mom and made our way to the festival."
    ""

# game/dialogs.rpy:2743
translate tokipona matsuri_S_463b86c5:

    # "Loud noises and music were becoming audible as we got closer to the festival."
    ""

# game/dialogs.rpy:2748
translate tokipona matsuri_S_9ab29675:

    # s "What were you talking about with mom?"
    s ""

# game/dialogs.rpy:2749
translate tokipona matsuri_S_b96431ab:

    # hero "Umm... Nothing much..."
    hero ""

# game/dialogs.rpy:2750
translate tokipona matsuri_S_3edbe663:

    # hero "By the way, your mom looks incredibly young."
    hero ""

# game/dialogs.rpy:2751
translate tokipona matsuri_S_258d2b74:

    # s "Yeah, she gave birth to me when she was only 16 years old."
    s ""

# game/dialogs.rpy:2752
translate tokipona matsuri_S_bac1bc61:

    # s "My father was her Japanese teacher."
    s ""

# game/dialogs.rpy:2753
translate tokipona matsuri_S_d3f7f082:

    # hero "Haha, sounds like a manga cliché!"
    hero ""

# game/dialogs.rpy:2754
translate tokipona matsuri_S_8dbb02af:

    # s "Teehee! Yes it is!"
    s ""

# game/dialogs.rpy:2756
translate tokipona matsuri_S_f72540d4:

    # "She smiled for a while, but then it grew thin."
    ""

# game/dialogs.rpy:2757
translate tokipona matsuri_S_e1554201:

    # s "..."
    s ""

# game/dialogs.rpy:2758
translate tokipona matsuri_S_c41191e9:

    # s "I don't like my father very much..."
    s ""

# game/dialogs.rpy:2759
translate tokipona matsuri_S_b2052a3c:

    # hero "Yeah, I remember why, I think. Because he doesn't understand you, right?"
    hero ""

# game/dialogs.rpy:2760
translate tokipona matsuri_S_db0a0f02:

    # s "Sometimes he scares me..."
    s ""

# game/dialogs.rpy:2761
translate tokipona matsuri_S_9465293c:

    # s "Recently he..."
    s ""

# game/dialogs.rpy:2762
translate tokipona matsuri_S_557b9c7c:

    # s ". . ."
    s ""

# game/dialogs.rpy:2763
translate tokipona matsuri_S_2639bac0:

    # s "He said he would never accept me as his daughter..."
    s ""

# game/dialogs.rpy:2764
translate tokipona matsuri_S_337c15c1:

    # "I didn't know what to say."
    ""

# game/dialogs.rpy:2765
translate tokipona matsuri_S_e2c7c8ae:

    # "Her father seems to be confused with the case of his child."
    ""

# game/dialogs.rpy:2766
translate tokipona matsuri_S_0c7d97ed:

    # s "In fact, he even admitted that he has always preferred having a real boy,"
    s ""

# game/dialogs.rpy:2767
translate tokipona matsuri_S_9f13d1a6:

    # s "But I'm a boy with everything girly. And it makes him angry..."
    s ""

# game/dialogs.rpy:2768
translate tokipona matsuri_S_66341891:

    # "I nodded again."
    ""

# game/dialogs.rpy:2769
translate tokipona matsuri_S_cd99f638:

    # hero "And how does your mom feel, about everything?"
    hero ""

# game/dialogs.rpy:2771
translate tokipona matsuri_S_257a9f41:

    # s "She preferred a girl at the time, but she doesn't care at all, now."
    s ""

# game/dialogs.rpy:2772
translate tokipona matsuri_S_53c7acc4:

    # s "No matter if I'm a boy or a girl, I'm her child first of all..."
    s ""

# game/dialogs.rpy:2773
translate tokipona matsuri_S_08457630:

    # s "She understands me and she takes me for who I am."
    s ""

# game/dialogs.rpy:2774
translate tokipona matsuri_S_05bae488:

    # "I guessed that easily after the chat I had with her earlier."
    ""

# game/dialogs.rpy:2775
translate tokipona matsuri_S_636c5687:

    # hero "That's a good thing."
    hero ""

# game/dialogs.rpy:2776
translate tokipona matsuri_S_9600023e:

    # hero "If only your dad saw things like your mom does..."
    hero ""

# game/dialogs.rpy:2777
translate tokipona matsuri_S_88285ade:

    # s "Right."
    s ""

# game/dialogs.rpy:2778
translate tokipona matsuri_S_c43195f9:

    # hero "You can count on your mother if something bad happens..."
    hero ""

# game/dialogs.rpy:2779
translate tokipona matsuri_S_36e0944d:

    # hero "And you know..."
    hero ""

# game/dialogs.rpy:2780
translate tokipona matsuri_S_5303d699:

    # hero "You can count on Rika-chan, Nanami-chan, and me if you need."
    hero ""

# game/dialogs.rpy:2782
translate tokipona matsuri_S_1b982e5f:

    # "Sakura smiled and held my arm tightly."
    ""

# game/dialogs.rpy:2783
translate tokipona matsuri_S_2775e546:

    # s "I know, %(stringhero)s-kun..."
    s ""

# game/dialogs.rpy:2788
translate tokipona matsuri_S_8fa16578:

    # "We arrived at the festival and were joined by Rika-chan."
    ""

# game/dialogs.rpy:2789
translate tokipona matsuri_S_42b57c94:

    # "She was wearing a very pretty purple yukata that showed her shoulders outside."
    ""

# game/dialogs.rpy:2791
translate tokipona matsuri_S_856a9a31:

    # r "Hey hey hey, you lovers!!!"
    r ""

# game/dialogs.rpy:2793
translate tokipona matsuri_S_aa24547d:

    # s "Rika-chan!"
    s ""

# game/dialogs.rpy:2794
translate tokipona matsuri_S_70def7d1:

    # hero "Rika-chan! Your yukata is great!!"
    hero ""

# game/dialogs.rpy:2795
translate tokipona matsuri_S_1c3fbf24:

    # r "Pfft, I know what you really mean, you pervert! {image=heart.png}{p}How are you guys?"
    r ""

# game/dialogs.rpy:2797
translate tokipona matsuri_S_b140b475:

    # r "My, my! Looks like our city rat has himself a nice yukata!"
    r ""

# game/dialogs.rpy:2798
translate tokipona matsuri_S_8051a326:

    # hero "Thanks, Rika-chan- {p}Hey wait! I'm no city rat!!!"
    hero ""

# game/dialogs.rpy:2800
translate tokipona matsuri_S_9cb75162:

    # hero "By the way, where is Nanami-chan?"
    hero ""

# game/dialogs.rpy:2801
translate tokipona matsuri_S_6683fd53:

    # r "Knowing her, she probably went off to playing the shooting games."
    r ""

# game/dialogs.rpy:2802
translate tokipona matsuri_S_0fef1f71:

    # hero "Shooting games?"
    hero ""

# game/dialogs.rpy:2803
translate tokipona matsuri_S_8644460f:

    # hero "Let's go join her and play too!"
    hero ""

# game/dialogs.rpy:2805
translate tokipona matsuri_S_5a861c69:

    # s "Oh I would love to!!!"
    s ""

# game/dialogs.rpy:2806
translate tokipona matsuri_S_e0fe552c:

    # r "Beware, %(stringhero)s, Sakura is the best shooter in the whole village!"
    r ""

# game/dialogs.rpy:2807
translate tokipona matsuri_S_64c71cc7:

    # hero "We'll see about that!"
    hero ""

# game/dialogs.rpy:2808
translate tokipona matsuri_S_acc4c1f3:

    # "So we walk to a shooting stand..."
    ""

# game/dialogs.rpy:2812
translate tokipona matsuri_S_1978a1c0:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    ""

# game/dialogs.rpy:2813
translate tokipona matsuri_S_449ff6ae:

    # "Sakura made three perfect shots in a row on a big plushie of {i}ByeBye-Neko{/i} which fell on its back."
    ""

# game/dialogs.rpy:2814
translate tokipona matsuri_S_5f5c92ba:

    # s "Yay! I love plushies!!!"
    s ""

# game/dialogs.rpy:2815
translate tokipona matsuri_S_d15d8e67:

    # n "You're great at this, Sakura-nee!"
    n ""

# game/dialogs.rpy:2816
translate tokipona matsuri_S_db10b573:

    # n "It doesn't feel like it does in the arcade."
    n ""

# game/dialogs.rpy:2817
translate tokipona matsuri_S_b799ab6b:

    # "I smiled."
    ""

# game/dialogs.rpy:2818
translate tokipona matsuri_S_dd8906ec:

    # "I knew what to do..."
    ""

# game/dialogs.rpy:2819
translate tokipona matsuri_S_0b28ef8e:

    # "I had to get another plushie for Sakura-chan!"
    ""

# game/dialogs.rpy:2820
translate tokipona matsuri_S_0d1e3f41:

    # hero "My turn to try!"
    hero ""

# game/dialogs.rpy:2821
translate tokipona matsuri_S_997b966e:

    # "I gave ¥100 to the guy at the stand, took the gun and targeted a cute crocodile plushie that seemed easy to get."
    ""

# game/dialogs.rpy:2822
translate tokipona matsuri_S_1978a1c0_1:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    ""

# game/dialogs.rpy:2823
translate tokipona matsuri_S_c8f69f16:

    # "Missed!"
    ""

# game/dialogs.rpy:2824
translate tokipona matsuri_S_924fcb79:

    # hero "Hold on, I'll try again!"
    hero ""

# game/dialogs.rpy:2825
translate tokipona matsuri_S_2ffa84ff:

    # "¥100 more on the table."
    ""

# game/dialogs.rpy:2826
translate tokipona matsuri_S_1978a1c0_2:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    ""

# game/dialogs.rpy:2827
translate tokipona matsuri_S_df4219e2:

    # "Darn!!! The plushie moved a bit but it wasn't enough for it to fall!"
    ""

# game/dialogs.rpy:2828
translate tokipona matsuri_S_6cc32b6b:

    # "¥100 more!"
    ""

# game/dialogs.rpy:2829
translate tokipona matsuri_S_1978a1c0_3:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    ""

# game/dialogs.rpy:2830
translate tokipona matsuri_S_bea4f24c:

    # "I got it!!! I got the plushie!!!"
    ""

# game/dialogs.rpy:2831
translate tokipona matsuri_S_67dae2f0:

    # "I was about to give it to Sakura, but I remembered Rika and Nanami."
    ""

# game/dialogs.rpy:2832
translate tokipona matsuri_S_ad68e441:

    # "Maybe I should get one for each of them too..."
    ""

# game/dialogs.rpy:2833
translate tokipona matsuri_S_52d0a5e1:

    # "As I was about to pay ¥100 more to the guy, Rika shoved herself through."
    ""

# game/dialogs.rpy:2834
translate tokipona matsuri_S_602dd045:

    # r "Hold it, city rat!!! It's my turn to play!!!"
    r ""

# game/dialogs.rpy:2835
translate tokipona matsuri_S_8834e601:

    # r "I'll get the same plushie with only three shots, you'll see!!!"
    r ""

# game/dialogs.rpy:2836
translate tokipona matsuri_S_2a606826:

    # "While Rika and Nanami were busy at the stand, I gave the plushie to Sakura."
    ""

# game/dialogs.rpy:2837
translate tokipona matsuri_S_2c879bcb:

    # s "For me?"
    s ""

# game/dialogs.rpy:2838
translate tokipona matsuri_S_c824434e:

    # s "Oh, it's so cute!!!"
    s ""

# game/dialogs.rpy:2839
translate tokipona matsuri_S_20fdfbfd:

    # s "Thank you, %(stringhero)s-kun!!"
    s ""

# game/dialogs.rpy:2840
translate tokipona matsuri_S_f2aa2a51:

    # "She kissed me wildly in front of everybody, in a way that a man is more used to doing than a girl."
    ""

# game/dialogs.rpy:2841
translate tokipona matsuri_S_fcea0f28:

    # "I felt embarrassed and shy, but happy..."
    ""

# game/dialogs.rpy:2842
translate tokipona matsuri_S_15bd7f0a:

    # r "Ah!!! Missed again, darn!"
    r ""

# game/dialogs.rpy:2843
translate tokipona matsuri_S_b3487258:

    # "Apparently, Rika-chan isn't very good at shooting games..."
    ""

# game/dialogs.rpy:2844
translate tokipona matsuri_S_4ca1680a:

    # "But she finally got a plushie too, after a while..."
    ""

# game/dialogs.rpy:2845
translate tokipona matsuri_S_016f8a4d:

    # "I think it's because the guy was seduced by Rika-chan's sexy yukata..."
    ""

# game/dialogs.rpy:2846
translate tokipona matsuri_S_20e94235:

    # "Gah, seriously..."
    ""

# game/dialogs.rpy:2847
translate tokipona matsuri_S_015dea9a:

    # hero "How about we get some food now? I feel hungry."
    hero ""

# game/dialogs.rpy:2848
translate tokipona matsuri_S_80443115:

    # r "Excellent idea!!! Me too!!!"
    r ""

# game/dialogs.rpy:2849
translate tokipona matsuri_S_3e4fce92:

    # s "Me too, a little."
    s ""

# game/dialogs.rpy:2850
translate tokipona matsuri_S_278f0f18:

    # r "Let's go! I'll show you a good stand where they make excellent takoyaki!"
    r ""

# game/dialogs.rpy:2855
translate tokipona takoyakis_b486162f:

    # "Our meal was just excellent, as Rika-chan promised!"
    ""

# game/dialogs.rpy:2856
translate tokipona takoyakis_b36de308:

    # hero "I'm full!"
    hero ""

# game/dialogs.rpy:2857
translate tokipona takoyakis_c5430508:

    # r "Told ya it was good! This is the best place to find them!"
    r ""

# game/dialogs.rpy:2858
translate tokipona takoyakis_f4e941cb:

    # r "I want more! Hey, more of these please!!!"
    r ""

# game/dialogs.rpy:2859
translate tokipona takoyakis_b42e6c2a:

    # n "Can't... eat... more..."
    n ""

# game/dialogs.rpy:2860
translate tokipona takoyakis_032debaa:

    # hero "Rika-chan, how can you eat so much of this stuff??? Do you have a black hole instead of a stomach or what?"
    hero ""

# game/dialogs.rpy:2861
translate tokipona takoyakis_dc489790:

    # s "Actually, Rika-chan can eat a mountain of food without adding on any pounds."
    s ""

# game/dialogs.rpy:2862
translate tokipona takoyakis_11fca699:

    # hero "Really? She's lucky."
    hero ""

# game/dialogs.rpy:2863
translate tokipona takoyakis_70921009:

    # hero "I have to control my own weight to make sure I don't end up too fat..."
    hero ""

# game/dialogs.rpy:2864
translate tokipona takoyakis_e6e2b7e9:

    # s "Teehee! Same for me!"
    s ""

# game/dialogs.rpy:2869
translate tokipona takoyakis_b3e735ce:

    # "We finished the festival by watching the fireworks..."
    ""

# game/dialogs.rpy:2870
translate tokipona takoyakis_d988d3e5:

    # "The show was fabulous."
    ""

# game/dialogs.rpy:2871
translate tokipona takoyakis_7e1eaf4f:

    # "At a moment, Sakura and I kissed discreetly in the dark under the fireworks..."
    ""

# game/dialogs.rpy:2874
translate tokipona takoyakis_f06e1942:

    # "Sakura-chan and I were on the way back home."
    ""

# game/dialogs.rpy:2875
translate tokipona takoyakis_5cc5084b:

    # "I'm completely overwhelmed of happiness because of the festival and that night with Sakura-chan."
    ""

# game/dialogs.rpy:2876
translate tokipona takoyakis_6cf004ac:

    # "Without a second thought, I tried to act out a cliché from movies I've seen before."
    ""

# game/dialogs.rpy:2877
translate tokipona takoyakis_60443369:

    # hero "Want to come back to my place for a drink?"
    hero ""

# game/dialogs.rpy:2878
translate tokipona takoyakis_52a57200:

    # s "Huh...?"
    s ""

# game/dialogs.rpy:2879
translate tokipona takoyakis_676d485e:

    # "I suddenly noticed what I just said. I started to blush and spoke incoherently."
    ""

# game/dialogs.rpy:2880
translate tokipona takoyakis_8d758b57:

    # hero "I-I-I-I mean if... If you... Well I mean y-y--{nw}"
    hero ""

# game/dialogs.rpy:2881
translate tokipona takoyakis_e043d770:

    # s "Okay."
    s ""

# game/dialogs.rpy:2882
translate tokipona takoyakis_b16c33f4:

    # "I was surprised."
    ""

# game/dialogs.rpy:2884
translate tokipona takoyakis_dc6dab16:

    # "Sakura watched me with a tinge of shyness."
    ""

# game/dialogs.rpy:2885
translate tokipona takoyakis_74a6bdef:

    # "She looked like a cute begging kitten."
    ""

# game/dialogs.rpy:2886
translate tokipona takoyakis_da34f8b5:

    # s "Y-Yes, take me to your home...%(stringhero)s-kun..."
    s ""

# game/dialogs.rpy:2888
translate tokipona takoyakis_ee16eceb:

    # "My heart skipped a beat."
    ""

# game/dialogs.rpy:2889
translate tokipona takoyakis_ceb3d2b6:

    # "My own girlfriend... In a kimono...coming to my bedroom...for the night..."
    ""

# game/dialogs.rpy:2890
translate tokipona takoyakis_ddafe283:

    # "I'm pretty sure that we wouldn't just sleep that night..."
    ""

# game/dialogs.rpy:2894
translate tokipona takoyakis_f412f669:

    # "Apparently my parents weren't back from the festival yet."
    ""

# game/dialogs.rpy:2897
translate tokipona takoyakis_4bb8cb02:

    # "We entered my house silently and went upstairs and into my bedroom."
    ""

# game/dialogs.rpy:2899
translate tokipona takoyakis_86309b2e:

    # "When I turned on the lights, Sakura opened her eyes widely."
    ""

# game/dialogs.rpy:2901
translate tokipona takoyakis_abbe7785:

    # s "Whoaaa!!!"
    s ""

# game/dialogs.rpy:2902
translate tokipona takoyakis_215547d3:

    # s "It's incredible!!! All of these posters!"
    s ""

# game/dialogs.rpy:2903
translate tokipona takoyakis_6430e2fa:

    # s "Oh! It's {i}High School Samurai{/i}!"
    s ""

# game/dialogs.rpy:2904
translate tokipona takoyakis_52931ee2:

    # hero "Haha, yeah. I took the habit of collecting posters..."
    hero ""

# game/dialogs.rpy:2905
translate tokipona takoyakis_f00c4d4e:

    # s "You bought them at Tokyo?"
    s ""

# game/dialogs.rpy:2906
translate tokipona takoyakis_0551d793:

    # hero "Yes. I was living near Gaymerz, the biggest manga shop of Tokyo."
    hero ""

# game/dialogs.rpy:2908
translate tokipona takoyakis_b4f4239b:

    # s "Oh, I've heard of it!"
    s ""

# game/dialogs.rpy:2909
translate tokipona takoyakis_4e9a74e5:

    # s "I hope I can visit it someday."
    s ""

# game/dialogs.rpy:2910
translate tokipona takoyakis_05c9701b:

    # "As she was speaking, I hugged her back lovingly."
    ""

# game/dialogs.rpy:2915
translate tokipona takoyakis_ef409314:

    # "We made sweet love to each other."
    ""

# game/dialogs.rpy:2916
translate tokipona takoyakis_c6b9d465:

    # "We were so tired after this evening...We dozed off..."
    ""

# game/dialogs.rpy:2919
translate tokipona takoyakis_0cee0472:

    # "I woke up slowly."
    ""

# game/dialogs.rpy:2920
translate tokipona takoyakis_b2766fe2:

    # "Looking at the alarm clock, it's kinda late."
    ""

# game/dialogs.rpy:2921
translate tokipona takoyakis_f87aab37:

    # "I felt something warm against me..."
    ""

# game/dialogs.rpy:2922
translate tokipona takoyakis_58e3a51c:

    # "It was Sakura."
    ""

# game/dialogs.rpy:2923
translate tokipona takoyakis_ec9d25fd:

    # "She was sleeping naked in my arms..."
    ""

# game/dialogs.rpy:2924
translate tokipona takoyakis_93a9f4ba:

    # "I smile and watched her sleeping..."
    ""

# game/dialogs.rpy:2925
translate tokipona takoyakis_4774de34:

    # "Even after the obvious evidence I've seen, I still can't believe that she's actually a boy..."
    ""

# game/dialogs.rpy:2926
translate tokipona takoyakis_c2358eed:

    # "She looks like a girl... She speaks with a voice of a girl... She smells like a girl... She has soft skin like a girl..."
    ""

# game/dialogs.rpy:2927
translate tokipona takoyakis_6f1e912a:

    # "I felt my heart beating sweetly as her warm sweet little hand was on my chest..."
    ""

# game/dialogs.rpy:2928
translate tokipona takoyakis_eda104c0:

    # "She finally opened her eyes. As she saw me, she smiled and cuddled me tighter with a sweet sigh... and went back to sleep..."
    ""

# game/dialogs.rpy:2929
translate tokipona takoyakis_0deabf9f:

    # "I sighed of happiness..."
    ""

# game/dialogs.rpy:2930
translate tokipona takoyakis_a20cefa7:

    # "..."
    ""

# game/dialogs.rpy:2931
translate tokipona takoyakis_8b8ceb72:

    # "............"
    ""

# game/dialogs.rpy:2932
translate tokipona takoyakis_9ed134ce:

    # "Oh..."
    ""

# game/dialogs.rpy:2933
translate tokipona takoyakis_c1399882:

    # "Oh no!!!{p}My parents!!!"
    ""

# game/dialogs.rpy:2934
translate tokipona takoyakis_fb40c1b5:

    # "They're surely back from the festival!!!"
    ""

# game/dialogs.rpy:2935
translate tokipona takoyakis_3b788596:

    # "What are they gonna say if they see me with a girl in my bed???"
    ""

# game/dialogs.rpy:2936
translate tokipona takoyakis_2cffeee3:

    # "What should I do? Dammit, dammit, dammit!!!"
    ""

# game/dialogs.rpy:2937
translate tokipona takoyakis_c1fa5fd9:

    # hero "Huh... Sakura-chan?..."
    hero ""

# game/dialogs.rpy:2938
translate tokipona takoyakis_dfd2530f:

    # s "{size=-5}%(stringhero)s-kun... I love you...{/size}"
    s ""

# game/dialogs.rpy:2939
translate tokipona takoyakis_817848c9:

    # "She fell asleep again!?"
    ""

# game/dialogs.rpy:2940
translate tokipona takoyakis_0df5e11e:

    # "I stopped panicking to listen for a moment. I was waiting for an indication that would tell me if my parents were back."
    ""

# game/dialogs.rpy:2941
translate tokipona takoyakis_9850f53c:

    # "Nothing."
    ""

# game/dialogs.rpy:2942
translate tokipona takoyakis_d047fac0:

    # "I slowly moved out from my bed, letting Sakura sleep and looked outside the window."
    ""

# game/dialogs.rpy:2943
translate tokipona takoyakis_f6b451ec:

    # "My parent's car wasn't there... Maybe I can do something at last..."
    ""

# game/dialogs.rpy:2944
translate tokipona takoyakis_4d679905:

    # s "%(stringhero)s-kun?...."
    s ""

# game/dialogs.rpy:2945
translate tokipona takoyakis_08748d66:

    # "Sakura just woke up and sat on the bed."
    ""

# game/dialogs.rpy:2946
translate tokipona takoyakis_f25fd7bb:

    # hero "Sakura-chan..."
    hero ""

# game/dialogs.rpy:2947
translate tokipona takoyakis_ddcb931f:

    # hero "My parents aren't here yet. You should go home now before they come back."
    hero ""

# game/dialogs.rpy:2948
translate tokipona takoyakis_7295fac4:

    # hero "My parents are kinda strict when it comes to relationships."
    hero ""

# game/dialogs.rpy:2949
translate tokipona takoyakis_b3eeb634:

    # "Sakura nodded. She looked all sleepy."
    ""

# game/dialogs.rpy:2950
translate tokipona takoyakis_aee46875:

    # "That was cute. I smiled."
    ""

# game/dialogs.rpy:2951
translate tokipona takoyakis_8891ed58:

    # hero "Aww. Had too much fun last night?"
    hero ""

# game/dialogs.rpy:2952
translate tokipona takoyakis_e15b24d1:

    # "Sakura giggled. She stood up and took me in her arms."
    ""

# game/dialogs.rpy:2953
translate tokipona takoyakis_67bc7996:

    # "Her warm naked body was against mine."
    ""

# game/dialogs.rpy:2954
translate tokipona takoyakis_387596f6:

    # "I kissed her again."
    ""

# game/dialogs.rpy:2956
translate tokipona takoyakis_7dbc9795:

    # "After some time, I helped her put on her kimono and took her to her house..."
    ""

# game/dialogs.rpy:2957
translate tokipona takoyakis_5ffe88e0:

    # "We promised to have another date very soon..."
    ""

# game/dialogs.rpy:2958
translate tokipona takoyakis_17899d6b:

    # "I went back home and turned on my computer."
    ""

# game/dialogs.rpy:2962
translate tokipona takoyakis_2b150c3e:

    # write "Dear sister,"
    write ""

# game/dialogs.rpy:2964
translate tokipona takoyakis_a50c844b:

    # write "How are things? It's been awhile since I wrote."
    write ""

# game/dialogs.rpy:2966
translate tokipona takoyakis_54b9d70d:

    # write "You'll never guess what happened!"
    write ""

# game/dialogs.rpy:2968
translate tokipona takoyakis_67d2ac31:

    # write "I'm going out with someone!"
    write ""

# game/dialogs.rpy:2970
translate tokipona takoyakis_df940a1c:

    # write "She's a wonderful person... A unique person...{w} It's kinda hard to tell you more about this...{w}You have to promise that you'll never tell our parents!"
    write ""

# game/dialogs.rpy:2972
translate tokipona takoyakis_bc7a4cdb:

    # write "At least, I'm happy... More happy than I have ever been... It's as if I just got the best Christmas present ever."
    write ""

# game/dialogs.rpy:2973
translate tokipona takoyakis_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:2975
translate tokipona takoyakis_bf522c92:

    # write "I'll send you a photo of us. I don't know how much longer our story will last, but I'm sure I'll never forget it for the rest of my life. What I'm living is really unique!"
    write ""

# game/dialogs.rpy:2977
translate tokipona takoyakis_f52f892c:

    # write "I hope everything is okay for you too and that you are as happy as I am..."
    write ""

# game/dialogs.rpy:2979
translate tokipona takoyakis_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write ""

# game/dialogs.rpy:2981
translate tokipona takoyakis_4387dacf:

    # write "PS: {w}The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write ""

# game/dialogs.rpy:2982
translate tokipona takoyakis_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:3015
translate tokipona matsuri_N_46751f32:

    # "I came to the festival with Sakura."
    ""

# game/dialogs.rpy:3016
translate tokipona matsuri_N_17894765:

    # "She was wearing a cute yukata. It fit her well."
    ""

# game/dialogs.rpy:3018
translate tokipona matsuri_N_ec440c0a:

    # "I was wearing the brand new yukata that she gave me."
    ""

# game/dialogs.rpy:3020
translate tokipona matsuri_N_201b46ce:

    # "I was wearing the yukata I recently bought... One which was awfully expensive..."
    ""

# game/dialogs.rpy:3021
translate tokipona matsuri_N_38154d05:

    # "We quickly found Nanami and Rika despite the big crowd."
    ""

# game/dialogs.rpy:3026
translate tokipona matsuri_N_866e240e:

    # "Rika was wearing a very pretty purple yukata that showed her shoulders outside."
    ""

# game/dialogs.rpy:3027
translate tokipona matsuri_N_e57fd5f6:

    # "Nanami was wearing a more simple green yukata but it was incredibly cute on her."
    ""

# game/dialogs.rpy:3028
translate tokipona matsuri_N_c1358676:

    # r "So, what we do first? I can't wait!"
    r ""

# game/dialogs.rpy:3030
translate tokipona matsuri_N_e60b3319:

    # n "I want to go to the shooting stands!"
    n ""

# game/dialogs.rpy:3031
translate tokipona matsuri_N_a6a52bc4:

    # s "That sounds nice!"
    s ""

# game/dialogs.rpy:3032
translate tokipona matsuri_N_e8f6b7d0:

    # hero "I'd like that too!"
    hero ""

# game/dialogs.rpy:3033
translate tokipona matsuri_N_f8974ce9:

    # r "Let's go!"
    r ""

# game/dialogs.rpy:3035
translate tokipona matsuri_N_228ae33a:

    # "We arrived just in time. The stand was pretty empty."
    ""

# game/dialogs.rpy:3037
translate tokipona matsuri_N_4cdd6550:

    # n "Hey, %(stringhero)s-nii... I challenge you to make a plushie fall before I do!"
    n ""

# game/dialogs.rpy:3038
translate tokipona matsuri_N_2635f363:

    # hero "I thought you weren't good at shooting games."
    hero ""

# game/dialogs.rpy:3039
translate tokipona matsuri_N_88c489c7:

    # n "I trained hard! You'll see!"
    n ""

# game/dialogs.rpy:3044
translate tokipona matsuri_N_cd54b02a:

    # "We both payed ¥100 and started to frantically shoot at the targets with our toy rifles."
    ""

# game/dialogs.rpy:3045
translate tokipona matsuri_N_1978a1c0:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    ""

# game/dialogs.rpy:3046
translate tokipona matsuri_N_d4bb63e2:

    # "We both missed... We tried again."
    ""

# game/dialogs.rpy:3047
translate tokipona matsuri_N_1978a1c0_1:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    ""

# game/dialogs.rpy:3048
translate tokipona matsuri_N_df8da603:

    # "We hit the same plush but it barely moved."
    ""

# game/dialogs.rpy:3049
translate tokipona matsuri_N_8d01865c:

    # n "Eeeeh!"
    n ""

# game/dialogs.rpy:3050
translate tokipona matsuri_N_1feb4bc0:

    # hero "No fair! It was a critical hit!"
    hero ""

# game/dialogs.rpy:3051
translate tokipona matsuri_N_cf97790a:

    # n "I'm not done yet!"
    n ""

# game/dialogs.rpy:3052
translate tokipona matsuri_N_3093850e:

    # "We tried again once more..."
    ""

# game/dialogs.rpy:3053
translate tokipona matsuri_N_1978a1c0_2:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    ""

# game/dialogs.rpy:3054
translate tokipona matsuri_N_ffac77fc:

    # "I got it! I got the plush!"
    ""

# game/dialogs.rpy:3058
translate tokipona matsuri_N_2530a4ee:

    # hero "Haha I won!"
    hero ""

# game/dialogs.rpy:3059
translate tokipona matsuri_N_638da9e6:

    # "It was a cute plush of crocodile."
    ""

# game/dialogs.rpy:3060
translate tokipona matsuri_N_b9dca595:

    # "Nanami started to pout."
    ""

# game/dialogs.rpy:3061
translate tokipona matsuri_N_0d531ead:

    # n "Eeeeh that's unfair, you got it because I hit it a couple times already!"
    n ""

# game/dialogs.rpy:3062
translate tokipona matsuri_N_1bdd906d:

    # n "I wanted that plush!"
    n ""

# game/dialogs.rpy:3063
translate tokipona matsuri_N_adab4cae:

    # "I laughed and rubbed her hair."
    ""

# game/dialogs.rpy:3064
translate tokipona matsuri_N_e0e8e5a5:

    # hero "Don't be silly. {p}I got it for you."
    hero ""

# game/dialogs.rpy:3065
translate tokipona matsuri_N_c55aa101:

    # "I gave Nanami the plushie."
    ""

# game/dialogs.rpy:3066
translate tokipona matsuri_N_3eb6b6ea:

    # "She looked at me with a cute expression, then she gave me a big hug."
    ""

# game/dialogs.rpy:3068
translate tokipona matsuri_N_5693f2ef:

    # n "Yaaay! Thank you, %(stringhero)s-nii!"
    n ""

# game/dialogs.rpy:3069
translate tokipona matsuri_N_162f5687:

    # r "Hey, get a room, you two!"
    r ""

# game/dialogs.rpy:3070
translate tokipona matsuri_N_9a893c8c:

    # "We all laughed."
    ""

# game/dialogs.rpy:3075
translate tokipona matsuri_N_9ced4dba:

    # hero "I'm hungry... How about we get some food now?"
    hero ""

# game/dialogs.rpy:3080
translate tokipona matsuri_N_80443115:

    # r "Excellent idea!!! Me too!!!"
    r ""

# game/dialogs.rpy:3081
translate tokipona matsuri_N_01dbb1d1:

    # s "I'm a little hungry too."
    s ""

# game/dialogs.rpy:3082
translate tokipona matsuri_N_5e163699:

    # r "Let's go! I'll show you an amazing stand where they make the best takoyaki!"
    r ""

# game/dialogs.rpy:3089
translate tokipona matsuri_N2_b3e735ce:

    # "We finished the festival by watching the fireworks..."
    ""

# game/dialogs.rpy:3090
translate tokipona matsuri_N2_d988d3e5:

    # "The show was fabulous."
    ""

# game/dialogs.rpy:3093
translate tokipona matsuri_N2_576baed2:

    # "As we we walked around some more, Nanami took my hand and guided me into the forest behind the temple..."
    ""

# game/dialogs.rpy:3094
translate tokipona matsuri_N2_c50c979d:

    # "We made discreet, passionate love behind the bushes..."
    ""

# game/dialogs.rpy:3095
translate tokipona matsuri_N2_633fea5f:

    # "Now that's what I call a night!"
    ""

# game/dialogs.rpy:3098
translate tokipona matsuri_N2_2ab4bd94:

    # "After escorting Sakura to her house, I went back to my own."
    ""

# game/dialogs.rpy:3100
translate tokipona matsuri_N2_6f974e08:

    # "My parents weren't back yet so I just goofed around and read some manga.{p} I also decided to write to my sister."
    ""

# game/dialogs.rpy:3103
translate tokipona matsuri_N2_2b150c3e:

    # write "Dear sister,"
    write ""

# game/dialogs.rpy:3105
translate tokipona matsuri_N2_92248cbb:

    # write "How are things?"
    write ""

# game/dialogs.rpy:3107
translate tokipona matsuri_N2_bb83fcbc:

    # write "You'll never guess what happened to me actually!"
    write ""

# game/dialogs.rpy:3109
translate tokipona matsuri_N2_4967d059:

    # write "I'm going out with a girl!"
    write ""

# game/dialogs.rpy:3111
translate tokipona matsuri_N2_534e4421:

    # write "Her name is Nanami. She's a really cute and energetic girl from school.{w} We play a lot of video games together. We're really into each other!"
    write ""

# game/dialogs.rpy:3113
translate tokipona matsuri_N2_4f13aea8:

    # write "I'm happy... Really happy... I can't wait to introduce her to you, someday! {w}I'll send you a photo of us soon!"
    write ""

# game/dialogs.rpy:3114
translate tokipona matsuri_N2_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:3116
translate tokipona matsuri_N2_ab4193c3:

    # write "I hope everything going well for you! I can't wait to see you again!"
    write ""

# game/dialogs.rpy:3118
translate tokipona matsuri_N2_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write ""

# game/dialogs.rpy:3120
translate tokipona matsuri_N2_91609d26:

    # write "PS:{w} The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write ""

# game/dialogs.rpy:3122
translate tokipona matsuri_N2_deb10d39:

    # "I heard my cellphone ring."
    ""

# game/dialogs.rpy:3127
translate tokipona matsuri_N2_dbd7e6ae:

    # hero "Hello?"
    hero ""

# game/dialogs.rpy:3128
translate tokipona matsuri_N2_77c066ad:

    # n "{i}It's me, dude!{/i}"
    n ""

# game/dialogs.rpy:3129
translate tokipona matsuri_N2_f295726b:

    # "I chuckled as I recognized Nanami's voice."
    ""

# game/dialogs.rpy:3130
translate tokipona matsuri_N2_404c955e:

    # hero "We're going out and you still call me dude?"
    hero ""

# game/dialogs.rpy:3131
translate tokipona matsuri_N2_284b7d55:

    # n "{i}Well, you're a guy after all, right?{/i}"
    n ""

# game/dialogs.rpy:3132
translate tokipona matsuri_N2_770ce25a:

    # n "{i}I've seen you from enough close to see it with my own eyes!{/i} {image=heart.png}"
    n ""

# game/dialogs.rpy:3133
translate tokipona matsuri_N2_7139659c:

    # "I blushed, remembering what we did earlier in the forest."
    ""

# game/dialogs.rpy:3134
translate tokipona matsuri_N2_e86fa184:

    # hero "Hehe... That was good, yeah..."
    hero ""

# game/dialogs.rpy:3135
translate tokipona matsuri_N2_7bfb8ae4:

    # n "{i}Teehee! To be honest... the more we do it, the more I want to do it again!{/i} {image=heart.png}"
    n ""

# game/dialogs.rpy:3136
translate tokipona matsuri_N2_69a9deb4:

    # n "{i}Speaking which, are you busy tomorrow?{/i}"
    n ""

# game/dialogs.rpy:3137
translate tokipona matsuri_N2_2224fcb7:

    # hero "Except coming to see you? No, nothing at all."
    hero ""

# game/dialogs.rpy:3138
translate tokipona matsuri_N2_8da73ec6:

    # "I laughed and heard Nanami laughing on the other end as well."
    ""

# game/dialogs.rpy:3139
translate tokipona matsuri_N2_26411a89:

    # n "{i}Alright, %(stringhero)s-nii. {w}I'll wait for you at el-{nw}{/i}"
    n ""

# game/dialogs.rpy:3140
translate tokipona matsuri_N2_587273a6:

    # n "{i}.........{/i}"
    n ""

# game/dialogs.rpy:3141
translate tokipona matsuri_N2_fd4b149d:

    # hero "Hello? Nanami-chan?"
    hero ""

# game/dialogs.rpy:3142
translate tokipona matsuri_N2_97743246:

    # n "{i}{size=-12}It's my boyfriend.....{p}It's none of your business!!......{/size}{/i}"
    n ""

# game/dialogs.rpy:3143
translate tokipona matsuri_N2_5027b83e:

    # n "{i}{size=-12}Big brother!!!......{/size}{/i}"
    n ""

# game/dialogs.rpy:3144
translate tokipona matsuri_N2_2f756018:

    # n "{i}I'll call you back.{/i}"
    n ""

# game/dialogs.rpy:3146
translate tokipona matsuri_N2_6c3f05d2:

    # "*{i}click{/i}*"
    ""

# game/dialogs.rpy:3147
translate tokipona matsuri_N2_2e0a800f:

    # "What was that about...? I shrugged and went back to my computer."
    ""

# game/dialogs.rpy:3148
translate tokipona matsuri_N2_73d69e81:

    # "I have a bad feeling about this... {p}But, she said she'll call back."
    ""

# game/dialogs.rpy:3151
translate tokipona matsuri_N2_1569a42e:

    # "Some time passed as I goofed around. All of a sudden, I heard the doorbell."
    ""

# game/dialogs.rpy:3153
translate tokipona matsuri_N2_8ef37003:

    # "Suddenly, I got goosebumps. A sense of dread came over me. It pushed me to run downstairs and to open the door as fast I could."
    ""

# game/dialogs.rpy:3155
translate tokipona matsuri_N2_ee440385:

    # "Nanami was at the door. She was in tears."
    ""

# game/dialogs.rpy:3156
translate tokipona matsuri_N2_e3c36773:

    # "When I opened up, she ran in the living room."
    ""

# game/dialogs.rpy:3157
translate tokipona matsuri_N2_fe828537:

    # hero "Nanami-chan?? {p}Are you okay? What's going on?!"
    hero ""

# game/dialogs.rpy:3158
translate tokipona matsuri_N2_0b51bf1c:

    # n "He's coming! He's scaring me, %(stringhero)s-nii!"
    n ""

# game/dialogs.rpy:3159
translate tokipona matsuri_N2_2952dd4d:

    # hero "Who? Who's coming?"
    hero ""

# game/dialogs.rpy:3160
translate tokipona matsuri_N2_817e602f:

    # "I got my answer right after I asked."
    ""

# game/dialogs.rpy:3161
translate tokipona matsuri_N2_d7a874f3:

    # "A guy entered the house with a demented look on his face."
    ""

# game/dialogs.rpy:3162
translate tokipona matsuri_N2_5b3076dd:

    # "I recognize him. He was on Nanami's picture frame in her bedroom."
    ""

# game/dialogs.rpy:3163
translate tokipona matsuri_N2_203812e1:

    # hero "Are you Toshio-san?"
    hero ""

# game/dialogs.rpy:3164
translate tokipona matsuri_N2_23531e72:

    # "He ignored my question and barged in straight toward Nanami."
    ""

# game/dialogs.rpy:3166
translate tokipona matsuri_N2_f15432c1:

    # "I instinctively interposed myself between them."
    ""

# game/dialogs.rpy:3167
translate tokipona matsuri_N2_5d32f865:

    # hero "Whoa! Wait wait wait... What are you going to do to her?!"
    hero ""

# game/dialogs.rpy:3168
translate tokipona matsuri_N2_91b34485:

    # toshio "Nanami! Come back home right now!"
    toshio ""

# game/dialogs.rpy:3169
translate tokipona matsuri_N2_54fe86a6:

    # n "No! You're scaring me, big brother!"
    n ""

# game/dialogs.rpy:3170
translate tokipona matsuri_N2_9e884cfa:

    # "Then Toshio outrageously pointed at me."
    ""

# game/dialogs.rpy:3171
translate tokipona matsuri_N2_b495b159:

    # toshio "And you...{p}How dare you soil my sister!"
    toshio ""

# game/dialogs.rpy:3172
translate tokipona matsuri_N2_4d869fcc:

    # "What?!{p}Soil her?! He's crazy!"
    ""

# game/dialogs.rpy:3173
translate tokipona matsuri_N2_47b87910:

    # hero "Hey man, calm down!"
    hero ""

# game/dialogs.rpy:3174
translate tokipona matsuri_N2_5f2f5e83:

    # hero "Calm down and listen, please!"
    hero ""

# game/dialogs.rpy:3175
translate tokipona matsuri_N2_c025cc4c:

    # "Toshio ignored my words completely and approached me menacingly."
    ""

# game/dialogs.rpy:3176
translate tokipona matsuri_N2_d17a127d:

    # "Oh shit, he's going to kill me!"
    ""

# game/dialogs.rpy:3177
translate tokipona matsuri_N2_8d99340f:

    # "In pure panic, I spoke what came to mind."
    ""

# game/dialogs.rpy:3178
translate tokipona matsuri_N2_d2e3bc0f:

    # hero "Just calm down! I didn't soil her! I-"
    hero ""

# game/dialogs.rpy:3180
translate tokipona matsuri_N2_499d2845:

    # hero "{size=+15}I love her!!{/size}"
    hero ""

# game/dialogs.rpy:3182
translate tokipona matsuri_N2_e3f58031:

    # "There was an awkward silence in the room."
    ""

# game/dialogs.rpy:3183
translate tokipona matsuri_N2_0ca571d7:

    # "Toshio had stopped walking... He looked more upset than ever."
    ""

# game/dialogs.rpy:3184
translate tokipona matsuri_N2_cef855da:

    # "Strangely, saying that out loud gave me more courage and strength."
    ""

# game/dialogs.rpy:3185
translate tokipona matsuri_N2_e6bb1d42:

    # hero "I love her... I love her with all my heart."
    hero ""

# game/dialogs.rpy:3186
translate tokipona matsuri_N2_d225817d:

    # hero "I'm in love with her!... All my intentions towards her are from the bottom of my heart!"
    hero ""

# game/dialogs.rpy:3187
translate tokipona matsuri_N2_f4b05b05:

    # toshio "How... How dare you..."
    toshio ""

# game/dialogs.rpy:3191
translate tokipona matsuri_N2_bc61413a:

    # "Nanami came between us, protecting me this time. She was holding me tight."
    ""

# game/dialogs.rpy:3192
translate tokipona matsuri_N2_ff4ee66e:

    # "Toshio stopped again."
    ""

# game/dialogs.rpy:3193
translate tokipona matsuri_N2_95c7f55c:

    # toshio "Nanami, get the fuck off him!!!"
    toshio ""

# game/dialogs.rpy:3194
translate tokipona matsuri_N2_5050139a:

    # n "No! Stop it, Big brother!!"
    n ""

# game/dialogs.rpy:3195
translate tokipona matsuri_N2_746a8fc4:

    # n "If you want to hit him, then hit me first!"
    n ""

# game/dialogs.rpy:3196
translate tokipona matsuri_N2_1053aa7b:

    # n "I love him too, big brother!!{p}{size=+15}I love him!!!{/size}"
    n ""

# game/dialogs.rpy:3197
translate tokipona matsuri_N2_bb6eef02:

    # n "He did nothing wrong! I'm the only culprit!"
    n ""

# game/dialogs.rpy:3198
translate tokipona matsuri_N2_7a8a2dfd:

    # "Toshio stood motionless."
    ""

# game/dialogs.rpy:3199
translate tokipona matsuri_N2_9b1a0a97:

    # "Nanami's confession filled me with joy... But I was too scared to savor it fully."
    ""

# game/dialogs.rpy:3200
translate tokipona matsuri_N2_a79664fb:

    # "Tears were coming from Nanami's eyes. I knew she meant it. She was just as terrified."
    ""

# game/dialogs.rpy:3201
translate tokipona matsuri_N2_38b0386e:

    # toshio "Nanami..."
    toshio ""

# game/dialogs.rpy:3202
translate tokipona matsuri_N2_e4239ff7:

    # toshio "You can't...{p}You can't love him. You shouldn't."
    toshio ""

# game/dialogs.rpy:3204
translate tokipona matsuri_N2_89e8a925:

    # toshio "If you love him, someday he'll go away, just like our parents."
    toshio ""

# game/dialogs.rpy:3205
translate tokipona matsuri_N2_626d5481:

    # toshio "We shouldn't love anyone again. We only have each other!"
    toshio ""

# game/dialogs.rpy:3206
translate tokipona matsuri_N2_710fb61d:

    # toshio "In this world, we can only count on ourselves, Nanami."
    toshio ""

# game/dialogs.rpy:3207
translate tokipona matsuri_N2_c60cf6df:

    # n "No...{p}No you're wrong, big brother..."
    n ""

# game/dialogs.rpy:3208
translate tokipona matsuri_N2_6112b197:

    # n "When we lost our parents, I was stuck in the same mindset."
    n ""

# game/dialogs.rpy:3209
translate tokipona matsuri_N2_3646ab26:

    # n "I thought we shouldn't deeply love anyone anymore."
    n ""

# game/dialogs.rpy:3210
translate tokipona matsuri_N2_7531d9d0:

    # n "And as time passed, I thought I was going more and more crazy because of loneliness."
    n ""

# game/dialogs.rpy:3211
translate tokipona matsuri_N2_5366f0e4:

    # n "Until I joined Rika-nee's manga club."
    n ""

# game/dialogs.rpy:3212
translate tokipona matsuri_N2_2759bd16:

    # n "Now I have two wonderful friends..."
    n ""

# game/dialogs.rpy:3213
translate tokipona matsuri_N2_d47cc30c:

    # n "And %(stringhero)s definitely changed my life..."
    n ""

# game/dialogs.rpy:3214
translate tokipona matsuri_N2_17bb8570:

    # n "I've never been so happy in my life, thanks to them..."
    n ""

# game/dialogs.rpy:3215
translate tokipona matsuri_N2_1a096aae:

    # n "They all made me realize that we shouldn't turn our back to love."
    n ""

# game/dialogs.rpy:3216
translate tokipona matsuri_N2_9afef4f0:

    # n "The only way to heal our hearts is to embrace love and friendship, big brother... Not being stuck in loneliness!"
    n ""

# game/dialogs.rpy:3217
translate tokipona matsuri_N2_ff64941d:

    # n "If... If you kill %(stringhero)s, I... I will... I..."
    n ""

# game/dialogs.rpy:3218
translate tokipona matsuri_N2_7f7dd2d3:

    # "Nanami broke into tears again... She probably couldn't bear to live without the manga club."
    ""

# game/dialogs.rpy:3220
translate tokipona matsuri_N2_7953243c:

    # "Toshio was still motionless. But his face was overflowing with tears now as he realized what he's done."
    ""

# game/dialogs.rpy:3221
translate tokipona matsuri_N2_0b7e8bd9:

    # "He fell on his knees, staring down at the floor... Guilt seemed to overpower him."
    ""

# game/dialogs.rpy:3222
translate tokipona matsuri_N2_5e48472d:

    # toshio "You..."
    toshio ""

# game/dialogs.rpy:3223
translate tokipona matsuri_N2_b9326ce8:

    # toshio "You're right, Nanami..."
    toshio ""

# game/dialogs.rpy:3224
translate tokipona matsuri_N2_19220ebd:

    # toshio "Oh dear, what have I done..."
    toshio ""

# game/dialogs.rpy:3225
translate tokipona matsuri_N2_1e2845d6:

    # toshio "I... I thought you... I don't know..."
    toshio ""

# game/dialogs.rpy:3226
translate tokipona matsuri_N2_1e3aa01c:

    # toshio "I thought having a boyfriend would make you forget our parents..."
    toshio ""

# game/dialogs.rpy:3227
translate tokipona matsuri_N2_ad121cc5:

    # n "How could I forget them?... I love them more than anything...!"
    n ""

# game/dialogs.rpy:3228
translate tokipona matsuri_N2_678f6c05:

    # n "And I'm sure that they would be happy to see me with %(stringhero)s and with the others if they were still here..."
    n ""

# game/dialogs.rpy:3229
translate tokipona matsuri_N2_d387126d:

    # toshio "You're right..."
    toshio ""

# game/dialogs.rpy:3230
translate tokipona matsuri_N2_c1e47d68:

    # toshio "I'm an awful brother. A terrible brother..."
    toshio ""

# game/dialogs.rpy:3232
translate tokipona matsuri_N2_721624ab:

    # "Nanami left my arms to hold her brother."
    ""

# game/dialogs.rpy:3233
translate tokipona matsuri_N2_8e3dd312:

    # n "No, you are not, big brother."
    n ""

# game/dialogs.rpy:3234
translate tokipona matsuri_N2_98ee3ce3:

    # n "You're just still grieving over what happened to us..."
    n ""

# game/dialogs.rpy:3235
translate tokipona matsuri_N2_e056bbe7:

    # n "Since they died, I've never seen you interact with others as much. You were always thinking of working to help us survive."
    n ""

# game/dialogs.rpy:3236
translate tokipona matsuri_N2_2f0b2303:

    # n "That's not healthy... {p}Loneliness isn't healthy..."
    n ""

# game/dialogs.rpy:3237
translate tokipona matsuri_N2_0a9e2c99:

    # "I watched the scene without a word."
    ""

# game/dialogs.rpy:3240
translate tokipona matsuri_N2_f5d91d1c:

    # "When Toshio felt better, Nanami released her embrace."
    ""

# game/dialogs.rpy:3241
translate tokipona matsuri_N2_2ef38f10:

    # hero "You both need some fresh water. I'll get you some."
    hero ""

# game/dialogs.rpy:3242
translate tokipona matsuri_N2_01068bf5:

    # toshio "S-sure... Thank you..."
    toshio ""

# game/dialogs.rpy:3243
translate tokipona matsuri_N2_56a7dd0c:

    # toshio "I'm... I'm sorry for..."
    toshio ""

# game/dialogs.rpy:3244
translate tokipona matsuri_N2_f3ee0ddb:

    # hero "It's okay..."
    hero ""

# game/dialogs.rpy:3245
translate tokipona matsuri_N2_a44ad6f3:

    # hero "I don't have a little sister. But I might be a bit worried if I knew she was seeing some random dude I didn't know about."
    hero ""

# game/dialogs.rpy:3246
translate tokipona matsuri_N2_1e293463:

    # n "Hey, I'm the one saying you're a dude, not you!"
    n ""

# game/dialogs.rpy:3247
translate tokipona matsuri_N2_c02bf505:

    # "I giggled softly."
    ""

# game/dialogs.rpy:3248
translate tokipona matsuri_N2_e1ca6ffd:

    # "I went to the kitchen and came back with three glasses of fresh water."
    ""

# game/dialogs.rpy:3249
translate tokipona matsuri_N2_9d66422e:

    # hero "Toshio-san, I promise you that I want Nanami-chan to be happy and that I'll do my best to make her happy."
    hero ""

# game/dialogs.rpy:3250
translate tokipona matsuri_N2_b183c263:

    # toshio "T-thank you..."
    toshio ""

# game/dialogs.rpy:3251
translate tokipona matsuri_N2_407f6320:

    # "Nanami smiled at me..."
    ""

# game/dialogs.rpy:3253
translate tokipona matsuri_N2_5b99a7c8:

    # "I escorted both Nanami and Toshio back to their house."
    ""

# game/dialogs.rpy:3254
translate tokipona matsuri_N2_750a3cb5:

    # "I handshaked Toshio and we smiled to each other."
    ""

# game/dialogs.rpy:3255
translate tokipona matsuri_N2_471cd43e:

    # toshio "You better not make her sad, %(stringhero)s-san!"
    toshio ""

# game/dialogs.rpy:3256
translate tokipona matsuri_N2_3fd749c7:

    # hero "I won't. I promise."
    hero ""

# game/dialogs.rpy:3257
translate tokipona matsuri_N2_611bd54f:

    # "He entered the house."
    ""

# game/dialogs.rpy:3258
translate tokipona matsuri_N2_0f0ea53d:

    # hero "Nanami, wait..."
    hero ""

# game/dialogs.rpy:3259
translate tokipona matsuri_N2_943066fb:

    # n "{size=-5}I'm coming, big brother, hold on.{/size}"
    n ""

# game/dialogs.rpy:3261
translate tokipona matsuri_N2_b0f85b49:

    # n "Yes, %(stringhero)s-nii?"
    n ""

# game/dialogs.rpy:3262
translate tokipona matsuri_N2_d6c71472:

    # hero "What I said in that moment... {w}It wasn't very enjoyable... {p}So I want to try again, here, right now."
    hero ""

# game/dialogs.rpy:3263
translate tokipona matsuri_N2_ce0929b4:

    # "I held her small hands."
    ""

# game/dialogs.rpy:3264
translate tokipona matsuri_N2_6baf7cff:

    # hero "I love you, Nanami-chan."
    hero ""

# game/dialogs.rpy:3266
translate tokipona matsuri_N2_f18aa4de:

    # "Nanami smiled and lovingly kissed my lips."
    ""

# game/dialogs.rpy:3267
translate tokipona matsuri_N2_30fea132:

    # n "I love you too, %(stringhero)s-nii."
    n ""

# game/dialogs.rpy:3269
translate tokipona matsuri_N2_6f7d9b3e:

    # "Then she went into her house."
    ""

# game/dialogs.rpy:3270
translate tokipona matsuri_N2_75c7dfac:

    # "I sighed from relief... What a night..."
    ""

# game/dialogs.rpy:3271
translate tokipona matsuri_N2_abd512ab:

    # "My emotions were put to the test tonight."
    ""

# game/dialogs.rpy:3272
translate tokipona matsuri_N2_a7393e9c:

    # "But I get the strange feeling that I'm not done yet..."
    ""

# game/dialogs.rpy:3273
translate tokipona matsuri_N2_230f323d:

    # "That sense of dread was still in the back of my mind. Something else is about to come soon..."
    ""

# game/dialogs.rpy:3304
translate tokipona matsuri_R_040d4c37:

    # "When I arrived at the festival, I was joined by Rika-chan."
    ""

# game/dialogs.rpy:3305
translate tokipona matsuri_R_42b57c94:

    # "She was wearing a very pretty purple yukata that showed her shoulders outside."
    ""

# game/dialogs.rpy:3306
translate tokipona matsuri_R_a636fbfa:

    # "She was insanely sexy!"
    ""

# game/dialogs.rpy:3308
translate tokipona matsuri_R_6db6001a:

    # hero "Ah, Rika-chan! That yukata looks great on you!"
    hero ""

# game/dialogs.rpy:3309
translate tokipona matsuri_R_4ec8e433:

    # r "Hmph! Don't stare too much, pervert!"
    r ""

# game/dialogs.rpy:3310
translate tokipona matsuri_R_8686ea2b:

    # hero "Where are the others?"
    hero ""

# game/dialogs.rpy:3312
translate tokipona matsuri_R_e7b4be7c:

    # r "Well... Sakura couldn't come."
    r ""

# game/dialogs.rpy:3313
translate tokipona matsuri_R_1457c98c:

    # r "She felt sick so she stayed home."
    r ""

# game/dialogs.rpy:3314
translate tokipona matsuri_R_f0ca7698:

    # r "And Nanami wasn't around either. Festivals aren't really her thing. Too much of a crowd."
    r ""

# game/dialogs.rpy:3315
translate tokipona matsuri_R_4299e4c0:

    # hero "Aw damn, that really sucks... Guess it's just us two..."
    hero ""

# game/dialogs.rpy:3316
translate tokipona matsuri_R_42954815:

    # hero "Do you think we should pay them a visit after the festival?"
    hero ""

# game/dialogs.rpy:3317
translate tokipona matsuri_R_ae397942:

    # r "Hmm..."
    r ""

# game/dialogs.rpy:3319
translate tokipona matsuri_R_67d6aa19:

    # r "That's a good idea."
    r ""

# game/dialogs.rpy:3320
translate tokipona matsuri_R_4a7cd128:

    # r "So, shall we grab a souvenir for them at the festival?"
    r ""

# game/dialogs.rpy:3321
translate tokipona matsuri_R_2bd890ea:

    # "I nodded in agreement."
    ""

# game/dialogs.rpy:3322
translate tokipona matsuri_R_068f330c:

    # "We started to walk around the festival."
    ""

# game/dialogs.rpy:3326
translate tokipona matsuri_R_19be20ef:

    # "We went to almost every stall, from the fishing game stands to every single food stand."
    ""

# game/dialogs.rpy:3327
translate tokipona matsuri_R_ff5174ca:

    # "Rika got pretty pissed off that I was better than her at some of the games."
    ""

# game/dialogs.rpy:3328
translate tokipona matsuri_R_8a3d2cd4:

    # "When she eventually gave up, we continued to roam around. Finally, an outburst."
    ""

# game/dialogs.rpy:3330
translate tokipona matsuri_R_8693e2bc:

    # r "Ugh, you know, you really don't know how to be a gentleman!"
    r ""

# game/dialogs.rpy:3331
translate tokipona matsuri_R_6b9f9eed:

    # r "Didn't your mom ever teach you to respect women, city rat!?"
    r ""

# game/dialogs.rpy:3332
translate tokipona matsuri_R_d78833cc:

    # "What the heck is she talking about?! I got irritated with her."
    ""

# game/dialogs.rpy:3333
translate tokipona matsuri_R_23fae77a:

    # hero "Shut up! If you don't want to be with me then get lost. Nobody asked you to come!"
    hero ""

# game/dialogs.rpy:3334
translate tokipona matsuri_R_848651e9:

    # r "I can leave anytime! I just decided to stay because I felt bad that Sakura-chan and Nanami-chan ditched a loser like you!"
    r ""

# game/dialogs.rpy:3335
translate tokipona matsuri_R_c4e42251:

    # r "You're the worst kind of guy! You're not even my type!"
    r ""

# game/dialogs.rpy:3336
translate tokipona matsuri_R_1527ed79:

    # hero "Fine! Whatever! You're not my type of woman either!!!"
    hero ""

# game/dialogs.rpy:3337
translate tokipona matsuri_R_08fc819f:

    # r "Fine!!!"
    r ""

# game/dialogs.rpy:3338
translate tokipona matsuri_R_bb288886:

    # hero "Fine!!!"
    hero ""

# game/dialogs.rpy:3339
translate tokipona matsuri_R_1a15f35b:

    # "We turned back against each other, sulking..."
    ""

# game/dialogs.rpy:3340
translate tokipona matsuri_R_a20cefa7:

    # "..."
    ""

# game/dialogs.rpy:3341
translate tokipona matsuri_R_b9e5c171:

    # "And after a few seconds..."
    ""

# game/dialogs.rpy:3342
translate tokipona matsuri_R_ed6f129a:

    # "Something came over me. I don't know why...but..."
    ""

# game/dialogs.rpy:3345
translate tokipona matsuri_R_d9b9652e:

    # "We both turned around to face each other and we kissed wildly!"
    ""

# game/dialogs.rpy:3346
translate tokipona matsuri_R_0af6321e:

    # "It was a passionate, torrid kiss in front of a whole crowd of strangers!"
    ""

# game/dialogs.rpy:3347
translate tokipona matsuri_R_5fbe52ef:

    # "When we stopped, she was looking at me with her pretty bi-colored eyes. She was blushing but she still looked pretty pissed off."
    ""

# game/dialogs.rpy:3348
translate tokipona matsuri_R_76e3f3b2:

    # hero "Ri... Rika-chan... I..."
    hero ""

# game/dialogs.rpy:3349
translate tokipona matsuri_R_cd9391ee:

    # r "Shut up! Just shut up and kiss me again!"
    r ""

# game/dialogs.rpy:3368
translate tokipona matsuri_R_83960144:

    # "And we kissed again."
    ""

# game/dialogs.rpy:3369
translate tokipona matsuri_R_0d89a6a1:

    # "...Rika took held my hand and guided me into the forest behind the temple."
    ""

# game/dialogs.rpy:3373
translate tokipona matsuri_R_02658004:

    # "Hidden by the bushes... we made intense love there."
    ""

# game/dialogs.rpy:3374
translate tokipona matsuri_R_862f50c1:

    # "It was crazy..."
    ""

# game/dialogs.rpy:3375
translate tokipona matsuri_R_91cf2fbf:

    # "I didn't think Rika was into me at all."
    ""

# game/dialogs.rpy:3376
translate tokipona matsuri_R_f1d355d1:

    # "And as strange as it looks, I thought I didn't like her at all either."
    ""

# game/dialogs.rpy:3377
translate tokipona matsuri_R_9540939d:

    # "Tsundere love is a really strange thing..."
    ""

# game/dialogs.rpy:3378
translate tokipona matsuri_R_9d8d28e3:

    # "I never thought that Rika, of all the people in the world, would be my first..."
    ""

# game/dialogs.rpy:3379
translate tokipona matsuri_R_0256c5ee:

    # "...Actually, even though it was both our first time, she was pretty good at it..."
    ""

# game/dialogs.rpy:3383
translate tokipona matsuri_R_bff9fa0e:

    # "We were planning to go back to our respective homes."
    ""

# game/dialogs.rpy:3385
translate tokipona matsuri_R_975037cd:

    # r "Hey, I almost forgot!"
    r ""

# game/dialogs.rpy:3386
translate tokipona matsuri_R_09ea128c:

    # r "We should get a present and visit Sakura-chan and Nanami-chan."
    r ""

# game/dialogs.rpy:3387
translate tokipona matsuri_R_23aa15f6:

    # hero "Oh yeah, I almost forgot! Let's go!"
    hero ""

# game/dialogs.rpy:3388
translate tokipona matsuri_R_1f36ff66:

    # hero "I wonder how she'll react when she finds out about us!"
    hero ""

# game/dialogs.rpy:3390
translate tokipona matsuri_R_c90d887e:

    # r "Haha, I bet she'll just think we're pulling a prank on her or-"
    r ""

# game/dialogs.rpy:3394
translate tokipona matsuri_R_22d23a85:

    # hero "Huh? My cellphone? Who could that be?"
    hero ""

# game/dialogs.rpy:3398
translate tokipona matsuri_R_e91d81d4:

    # hero "Hello? Who is it?"
    hero ""

# game/dialogs.rpy:3399
translate tokipona matsuri_R_0c279eeb:

    # s "{i}It's me, Sakura. How are you?{/i}"
    s ""

# game/dialogs.rpy:3400
translate tokipona matsuri_R_baf56ae6:

    # hero "Sakura-chan! Are you okay? I heard you were sick."
    hero ""

# game/dialogs.rpy:3401
translate tokipona matsuri_R_16bb0b38:

    # s "{i}I'm feeling better. Thank you, don't worry.{/i}"
    s ""

# game/dialogs.rpy:3402
translate tokipona matsuri_R_cafbc553:

    # s "{i}I'm actually at Nana-chan's house. We were playing some video games together.{/i}"
    s ""

# game/dialogs.rpy:3403
translate tokipona matsuri_R_e322e7d4:

    # hero "Oh alright! I'm glad you feel better!"
    hero ""

# game/dialogs.rpy:3404
translate tokipona matsuri_R_311bdbc8:

    # hero "Well hey, we were about to come visit you. So I guess we'll meet you at Nanami-chan's?"
    hero ""

# game/dialogs.rpy:3405
translate tokipona matsuri_R_cb76d6a6:

    # s "{i}Sure! We'll wait right here for you!{/i}"
    s ""

# game/dialogs.rpy:3407
translate tokipona matsuri_R_1713379a:

    # r "Sakura's feeling better?"
    r ""

# game/dialogs.rpy:3408
translate tokipona matsuri_R_ea44e64c:

    # hero "Yeah. She's feeling better and she's at Nanami-chan's house."
    hero ""

# game/dialogs.rpy:3409
translate tokipona matsuri_R_17259851:

    # r "Let's go visit them then!"
    r ""

# game/dialogs.rpy:3414
translate tokipona matsuri_R_2f5a201e:

    # "Nanami's room was a bit of a mess."
    ""

# game/dialogs.rpy:3415
translate tokipona matsuri_R_53129eea:

    # "Her computer setup took up most of the space, besides her bed."
    ""

# game/dialogs.rpy:3416
translate tokipona matsuri_R_992fbe8e:

    # "There was also an old TV with several gaming consoles plugged in it."
    ""

# game/dialogs.rpy:3417
translate tokipona matsuri_R_3b146827:

    # "It was cramped but, it seemed homely for Nanami."
    ""

# game/dialogs.rpy:3418
translate tokipona matsuri_R_61c51bff:

    # "I told the both of them about Rika and I. Sakura couldn't believe it when she heard the news!"
    ""

# game/dialogs.rpy:3419
translate tokipona matsuri_R_c281b0de:

    # "Nanami was in disbelief and was looking at us with a funny face."
    ""

# game/dialogs.rpy:3423
translate tokipona matsuri_R_586a170e:

    # s "That's incredible!"
    s ""

# game/dialogs.rpy:3424
translate tokipona matsuri_R_72ae68c4:

    # n "Seriously, guys!!"
    n ""

# game/dialogs.rpy:3425
translate tokipona matsuri_R_5bfe62d0:

    # s "I'm so happy for both of you!"
    s ""

# game/dialogs.rpy:3426
translate tokipona matsuri_R_391891ff:

    # s "I'd never guess you two would end up together! Teehee!"
    s ""

# game/dialogs.rpy:3427
translate tokipona matsuri_R_04b5e9a4:

    # hero "Haha, I have to admit, me neither!"
    hero ""

# game/dialogs.rpy:3428
translate tokipona matsuri_R_2a5f51c2:

    # r "Hmph, it's true. I didn't think I'd fall for him either...He's still an idiot though..."
    r ""

# game/dialogs.rpy:3429
translate tokipona matsuri_R_73e2cbfb:

    # hero "Whatever, you're the one whose blushing..."
    hero ""

# game/dialogs.rpy:3430
translate tokipona matsuri_R_34e95488:

    # "Sakura giggled as she watched us bicker as usual."
    ""

# game/dialogs.rpy:3431
translate tokipona matsuri_R_885f1f06:

    # "Looks like our new relationship didn't change anything about the way we communicate."
    ""

# game/dialogs.rpy:3432
translate tokipona matsuri_R_7a965734:

    # "But that's okay with me..."
    ""

# game/dialogs.rpy:3439
translate tokipona matsuri_R_0cee0472:

    # "I woke up slowly."
    ""

# game/dialogs.rpy:3440
translate tokipona matsuri_R_b2766fe2:

    # "Looking at the alarm clock, it's kinda late."
    ""

# game/dialogs.rpy:3441
translate tokipona matsuri_R_f87aab37:

    # "I felt something warm against me..."
    ""

# game/dialogs.rpy:3442
translate tokipona matsuri_R_2d8ea27f:

    # "It was Rika."
    ""

# game/dialogs.rpy:3443
translate tokipona matsuri_R_8d3f93d7:

    # "She was fast asleep, naked in my arms."
    ""

# game/dialogs.rpy:3444
translate tokipona matsuri_R_b91ebafe:

    # "I let out a smile as I held her tight."
    ""

# game/dialogs.rpy:3445
translate tokipona matsuri_R_880d8986:

    # "After visiting Sakura and Nanami, I offered her \"to have a drink.\""
    ""

# game/dialogs.rpy:3446
translate tokipona matsuri_R_87283b9b:

    # "She responded in an incredibly seductive way, \"It's not that I want to sleep at your place...I'm just thirsty...\""
    ""

# game/dialogs.rpy:3447
translate tokipona matsuri_R_d65a0334:

    # "We made intense and passionate love twice during the night."
    ""

# game/dialogs.rpy:3448
translate tokipona matsuri_R_3e893f32:

    # "It's funny how much of a tsundere cliché she can be sometimes. But I like it a lot."
    ""

# game/dialogs.rpy:3449
translate tokipona matsuri_R_0726d127:

    # "I pet Rika's hair tenderly. Her hair was longer than I thought with her pigtails removed."
    ""

# game/dialogs.rpy:3450
translate tokipona matsuri_R_c6b79cfc:

    # "She finally opened her eyes. As she saw me, she smiled and sat on the bed..."
    ""

# game/dialogs.rpy:3451
translate tokipona matsuri_R_044a4f8a:

    # "She was still half asleep when she spoke."
    ""

# game/dialogs.rpy:3452
translate tokipona matsuri_R_ddaea844:

    # r "Hmmmm... Did you have a good nap, city rat?"
    r ""

# game/dialogs.rpy:3453
translate tokipona matsuri_R_ffcb2dd8:

    # hero "Geez, even if we're in a relationship, even with you naked in my own bed, you'll still keep calling me that for the rest of your life huh?"
    hero ""

# game/dialogs.rpy:3454
translate tokipona matsuri_R_51e9effd:

    # r "Mmmm stop complaining... You should be thankful to have a girl like me as your lover..."
    r ""

# game/dialogs.rpy:3455
translate tokipona matsuri_R_13bdddc3:

    # "I gave a small laugh and kissed her. She began to embrace me..."
    ""

# game/dialogs.rpy:3456
translate tokipona matsuri_R_a20cefa7_1:

    # "..."
    ""

# game/dialogs.rpy:3457
translate tokipona matsuri_R_b072efdd:

    # "I suddenly realized something:{p}My parents!!!"
    ""

# game/dialogs.rpy:3458
translate tokipona matsuri_R_361d12e9:

    # "Oh shit, they're surely back from the festival!!!"
    ""

# game/dialogs.rpy:3459
translate tokipona matsuri_R_37e91653:

    # "They'll kill me if they see that I brought a girl home, let alone in my bed!!!"
    ""

# game/dialogs.rpy:3460
translate tokipona matsuri_R_ead2c864:

    # "What should I do? Dammit, dammit!!!"
    ""

# game/dialogs.rpy:3461
translate tokipona matsuri_R_a7bc91fd:

    # "I listened for a moment, waiting for an indication that would tell me if my parents were here."
    ""

# game/dialogs.rpy:3462
translate tokipona matsuri_R_06130763:

    # "Nothing..."
    ""

# game/dialogs.rpy:3463
translate tokipona matsuri_R_bb28b60c:

    # "Rika looked a bit confused...that or she was still half asleep."
    ""

# game/dialogs.rpy:3464
translate tokipona matsuri_R_c4b1d086:

    # hero "Rika-chan..."
    hero ""

# game/dialogs.rpy:3465
translate tokipona matsuri_R_9e07d6af:

    # hero "My parents aren't there. You should go home now before they come back."
    hero ""

# game/dialogs.rpy:3466
translate tokipona matsuri_R_272a8b83:

    # hero "My parents are kinda strict when it comes to love relationships."
    hero ""

# game/dialogs.rpy:3467
translate tokipona matsuri_R_7bf71025:

    # "Rika nodded."
    ""

# game/dialogs.rpy:3468
translate tokipona matsuri_R_22236077:

    # r "Alright... I understand. My parents would react the same in this kinda situation..."
    r ""

# game/dialogs.rpy:3470
translate tokipona matsuri_R_fb307b0d:

    # "I helped her put on her yukata and took her to her house..."
    ""

# game/dialogs.rpy:3471
translate tokipona matsuri_R_5ffe88e0:

    # "We promised to have another date very soon..."
    ""

# game/dialogs.rpy:3472
translate tokipona matsuri_R_17899d6b:

    # "I went back home and turned on my computer."
    ""

# game/dialogs.rpy:3476
translate tokipona matsuri_R_2b150c3e:

    # write "Dear sister,"
    write ""

# game/dialogs.rpy:3478
translate tokipona matsuri_R_92248cbb:

    # write "How are things?"
    write ""

# game/dialogs.rpy:3480
translate tokipona matsuri_R_54b9d70d:

    # write "You'll never guess what happened!"
    write ""

# game/dialogs.rpy:3482
translate tokipona matsuri_R_4967d059:

    # write "I'm going out with a girl!"
    write ""

# game/dialogs.rpy:3484
translate tokipona matsuri_R_c293cd3c:

    # write "Her name is Rika. She's a strange girl. It's a bit hard to understand she's really thinking.{w} She seems to mock me all the time, but we're really into each other."
    write ""

# game/dialogs.rpy:3486
translate tokipona matsuri_R_1773278b:

    # write "I'm happy... Really happy... I can't wait to introduce her to you someday! {w}I'll send you a photo of us. She's incredibly gorgeous."
    write ""

# game/dialogs.rpy:3487
translate tokipona matsuri_R_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:3489
translate tokipona matsuri_R_c42ad76e:

    # write "I hope everything is going great for you as well!"
    write ""

# game/dialogs.rpy:3491
translate tokipona matsuri_R_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write ""

# game/dialogs.rpy:3493
translate tokipona matsuri_R_91609d26:

    # write "PS:{w} The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write ""

# game/dialogs.rpy:3494
translate tokipona matsuri_R_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:3526
translate tokipona scene8_66fd8474:

    # centered "{size=+35}FINAL CHAPTER\nI wish I was a real girl{fast}{/size}"
    centered ""

# game/dialogs.rpy:3527
translate tokipona scene8_50254a18:

    # alt "FINAL CHAPTER\nI wish I was a real girl"
    alt ""

# game/dialogs.rpy:3533
translate tokipona scene8_26da46e6:

    # centered "Three days after the festival..."
    centered ""

# game/dialogs.rpy:3536
translate tokipona scene8_3193f804:

    # "It was late in the evening. I was in front of my computer, surfing on the web..."
    ""

# game/dialogs.rpy:3538
translate tokipona scene8_216308f6:

    # "My cellphone rang."
    ""

# game/dialogs.rpy:3542
translate tokipona scene8_dbd7e6ae:

    # hero "Hello?"
    hero ""

# game/dialogs.rpy:3543
translate tokipona scene8_5354350a:

    # r "{i}%(stringhero)s?{/i}"
    r ""

# game/dialogs.rpy:3545
translate tokipona scene8_30e755be:

    # hero "What's cooking, love?"
    hero ""

# game/dialogs.rpy:3547
translate tokipona scene8_6d9c1d81:

    # hero "Yeah, Rika-chan?"
    hero ""

# game/dialogs.rpy:3548
translate tokipona scene8_237675bd:

    # r "{i}Come to my house right now! It's very important!{/i}"
    r ""

# game/dialogs.rpy:3550
translate tokipona scene8_0bf1cc8f:

    # r "{i}It's about Sakura!{/i}"
    r ""

# game/dialogs.rpy:3551
translate tokipona scene8_eec796bb:

    # hero "What's going on?"
    hero ""

# game/dialogs.rpy:3552
translate tokipona scene8_87546516:

    # r "{i}Just come!{/i}"
    r ""

# game/dialogs.rpy:3554
translate tokipona scene8_495869e3:

    # "*{i}clic{/i}*"
    ""

# game/dialogs.rpy:3556
translate tokipona scene8_4f4c3c02:

    # "What could have happened to Sakura? Oh man, I hope she's okay."
    ""

# game/dialogs.rpy:3558
translate tokipona scene8_8a4ee6ed:

    # "I switched off my computer, took my bicycle and went to her house, worried to death."
    ""

# game/dialogs.rpy:3559
translate tokipona scene8_a60f3c18:

    # "It was late in the night and the little rural streets of the village were a bit scary in the dark."
    ""

# game/dialogs.rpy:3560
translate tokipona scene8_85e64de3:

    # "Now isn't the time to be scared of the small things."
    ""

# game/dialogs.rpy:3562
translate tokipona scene8_b7c54990:

    # "My love needs help!"
    ""

# game/dialogs.rpy:3564
translate tokipona scene8_ab18a7cd:

    # "My friend needs help!"
    ""

# game/dialogs.rpy:3567
translate tokipona scene8_993a8f06:

    # "*{i}Knock knock{/i}*"
    ""

# game/dialogs.rpy:3569
translate tokipona scene8_6d6e7ebc:

    # "The door of the house opened and Rika-chan appeared."
    ""

# game/dialogs.rpy:3570
translate tokipona scene8_68bac721:

    # "She looked very worried and upset, as I imagined."
    ""

# game/dialogs.rpy:3572
translate tokipona scene8_47dbdd39:

    # r "Ah, there you are. Come i-{nw}"
    r ""

# game/dialogs.rpy:3574
translate tokipona scene8_27651eaf:

    # "I didn't give her any time to show me where Sakura-chan was."
    ""

# game/dialogs.rpy:3575
translate tokipona scene8_0a632bcf:

    # "I instinctively ran into the house and searched for her."
    ""

# game/dialogs.rpy:3577
translate tokipona scene8_c676b3c1:

    # "I found her in the living room."
    ""

# game/dialogs.rpy:3579
translate tokipona scene8_37ffbdd8:

    # r "Ah, there you are. Come in, quickly."
    r ""

# game/dialogs.rpy:3581
translate tokipona scene8_616170d4:

    # "I followed Rika-chan into her house, to the living room."
    ""

# game/dialogs.rpy:3583
translate tokipona scene8_b8ce5033:

    # "Near the kotatsu, there was Sakura sitting on her knees, wearing her pajamas."
    ""

# game/dialogs.rpy:3584
translate tokipona scene8_c44364bc:

    # "Her long hair was hiding her face, but she looked shocked."
    ""

# game/dialogs.rpy:3585
translate tokipona scene8_d47b58c5:

    # "Thank heavens, she's alive."
    ""

# game/dialogs.rpy:3586
translate tokipona scene8_c89376fe:

    # hero "Sakura-chan! Are you okay?"
    hero ""

# game/dialogs.rpy:3587
translate tokipona scene8_1740f374:

    # "She turned her face to me, speechless."
    ""

# game/dialogs.rpy:3588
translate tokipona scene8_15ef4c6c:

    # "She had a large bruise on her cheek and tears were flowing slowly from her sweet blue eyes."
    ""

# game/dialogs.rpy:3589
translate tokipona scene8_d0d64bc6:

    # hero "Oh..."
    hero ""

# game/dialogs.rpy:3590
translate tokipona scene8_74d2762c:

    # hero "Oh no..."
    hero ""

# game/dialogs.rpy:3591
translate tokipona scene8_6d9e86b7:

    # hero "Who the heck did this to you?!"
    hero ""

# game/dialogs.rpy:3593
translate tokipona scene8_67ac6839:

    # "Rika arrived at the living room."
    ""

# game/dialogs.rpy:3594
translate tokipona scene8_4fc2e4fc:

    # hero "Rika-chan, what happened? Was it the gang from school again?"
    hero ""

# game/dialogs.rpy:3595
translate tokipona scene8_391ed42e:

    # "Rika shook her head."
    ""

# game/dialogs.rpy:3596
translate tokipona scene8_93c707d1:

    # r "Her father did this."
    r ""

# game/dialogs.rpy:3597
translate tokipona scene8_03cc985d:

    # hero "What!?{p}Her father!?"
    hero ""

# game/dialogs.rpy:3598
translate tokipona scene8_7bf71025:

    # "Rika nodded."
    ""

# game/dialogs.rpy:3599
translate tokipona scene8_6da77c2c:

    # r "He was drunk, he hit her and even broke her violin..."
    r ""

# game/dialogs.rpy:3600
translate tokipona scene8_75cef802:

    # r "Her mother helped her to flee the house. So she came to me. When she told me what happened, I called you."
    r ""

# game/dialogs.rpy:3601
translate tokipona scene8_5820756e:

    # r "She needs both of us, more than anything..."
    r ""

# game/dialogs.rpy:3602
translate tokipona scene8_f4f4126a:

    # hero "Does Nanami-chan know?"
    hero ""

# game/dialogs.rpy:3603
translate tokipona scene8_65b003bb:

    # r "Not yet. I prefer not to worry her."
    r ""

# game/dialogs.rpy:3604
translate tokipona scene8_8f451a85:

    # r "She cares a lot for Sakura but she's very fragile."
    r ""

# game/dialogs.rpy:3605
translate tokipona scene8_35bf5a71:

    # "I nodded."
    ""

# game/dialogs.rpy:3606
translate tokipona scene8_5bd541e7:

    # "From what I saw, I could see that she really considers Sakura and Rika like her sisters."
    ""

# game/dialogs.rpy:3607
translate tokipona scene8_a20cefa7:

    # "..."
    ""

# game/dialogs.rpy:3608
translate tokipona scene8_380894b8:

    # "Anyway, Sakura's father surely did this because he was still pissed that his boy is a girl inside."
    ""

# game/dialogs.rpy:3610
translate tokipona scene8_63e75ac3:

    # "Or maybe he was pissed because Sakura gave away her yukata for men. I've heard that he bought it for her in the hopes of making her a man."
    ""

# game/dialogs.rpy:3611
translate tokipona scene8_0745cc53:

    # "I felt enraged."
    ""

# game/dialogs.rpy:3612
translate tokipona scene8_1a6c86d8:

    # "But actually, I had no idea of what to do."
    ""

# game/dialogs.rpy:3617
translate tokipona scene8_91aef672:

    # "We stayed at Rika-chan's home for the night."
    ""

# game/dialogs.rpy:3618
translate tokipona scene8_7267642d:

    # "But I wanted to avenge my friend."
    ""

# game/dialogs.rpy:3619
translate tokipona scene8_0057c44c:

    # "I had to go there and talk to her father!"
    ""

# game/dialogs.rpy:3620
translate tokipona scene8_8e59b631:

    # "While they were both asleep, I stood up and went outside with my bicycle, on my way to Sakura's house."
    ""

# game/dialogs.rpy:3623
translate tokipona scene8_b1145ef5:

    # "In front of the house, there was her father."
    ""

# game/dialogs.rpy:3624
translate tokipona scene8_8fb982f8:

    # "He was mumbling some gibberish and was holding a bottle of sake."
    ""

# game/dialogs.rpy:3625
translate tokipona scene8_d9426232:

    # "As I could guess, he was still drunk."
    ""

# game/dialogs.rpy:3626
translate tokipona scene8_185ef70f:

    # hero "Hey you!"
    hero ""

# game/dialogs.rpy:3627
translate tokipona scene8_cb6740c6:

    # sdad "Wh--- Who are you?!"
    sdad ""

# game/dialogs.rpy:3629
translate tokipona scene8_032d6f9c:

    # hero "Name's %(stringhero)s... I'm the boyfriend of your daughter! Her boyfriend!!!"
    hero ""

# game/dialogs.rpy:3631
translate tokipona scene8_e3dcc113:

    # hero "Name's %(stringhero)s... I'm a friend of your daughter!"
    hero ""

# game/dialogs.rpy:3632
translate tokipona scene8_18e3dade:

    # sdad "*{i}hiccups{/i}* I don't have any d-d-daughter... I only have a son!..."
    sdad ""

# game/dialogs.rpy:3633
translate tokipona scene8_9fd9385d:

    # sdad "And my son is a freaking pervert who dresses like a girl, that's all!!!"
    sdad ""

# game/dialogs.rpy:3634
translate tokipona scene8_4576c2b6:

    # hero "SHUT UP!!!"
    hero ""

# game/dialogs.rpy:3635
translate tokipona scene8_f199e2fa:

    # "I shouted so loud, some dogs in the neighborhood started to bark."
    ""

# game/dialogs.rpy:3636
translate tokipona scene8_bb4f1d08:

    # hero "Whether he is a girl or a boy,... he's your child!! Your own flesh and blood!!!"
    hero ""

# game/dialogs.rpy:3637
translate tokipona scene8_90a161cb:

    # hero "You must accept him as he is!"
    hero ""

# game/dialogs.rpy:3638
translate tokipona scene8_042b0d72:

    # hero "Sakura accepts her father like he is, so why don't you do the same!!?"
    hero ""

# game/dialogs.rpy:3639
translate tokipona scene8_16e9224f:

    # sdad "*{i}hiccups{/i}*"
    sdad ""

# game/dialogs.rpy:3640
translate tokipona scene8_a67faa19:

    # sdad "I d-d-d-on't have any lessons to take from a kid!"
    sdad ""

# game/dialogs.rpy:3641
translate tokipona scene8_5f87fa70:

    # "He started to shout too. Some lights turned on in the neighborhood, awakened by the argument."
    ""

# game/dialogs.rpy:3642
translate tokipona scene8_4d26840c:

    # "Sakura's mother came out from the house and stood by the porch. She had some bruises on her body and her eyes were red... she must have been crying a lot."
    ""

# game/dialogs.rpy:3643
translate tokipona scene8_3a555d15:

    # "She seemed scared and was watching the scene quietly."
    ""

# game/dialogs.rpy:3644
translate tokipona scene8_380ead38:

    # "Looks like Sakura wasn't the only one who got hurt..."
    ""

# game/dialogs.rpy:3645
translate tokipona scene8_4e5e9fd5:

    # "Her father broke his empty bottle, held it like a knife and grinned."
    ""

# game/dialogs.rpy:3646
translate tokipona scene8_23076b47:

    # sdad "Now get out or I'm gonna stab you... you gay brat!"
    sdad ""

# game/dialogs.rpy:3647
translate tokipona scene8_415034f0:

    # "Shit, is this guy for real?!"
    ""

# game/dialogs.rpy:3648
translate tokipona scene8_5ac5be9a:

    # "He began to charge at me brandishing the broken glass. I was paralyzed with fear."
    ""

# game/dialogs.rpy:3649
translate tokipona scene8_22ee59f4:

    # sdad "{size=+10}Go to hell!!{/size}"
    sdad ""

# game/dialogs.rpy:3650
translate tokipona scene8_f5b011b4:

    # "Just as he closed in, someone jumped in front of me."
    ""

# game/dialogs.rpy:3653
translate tokipona scene8_1d392ab1:

    # "It was a deafening sound.{p}I heard a painful moan.{p}It resonated in my head even after it was done.."
    ""

# game/dialogs.rpy:3654
translate tokipona scene8_6ead108f:

    # "Time stopped. I just remember being pushed to the floor."
    ""

# game/dialogs.rpy:3657
translate tokipona scene8_1bd19034:

    # "When I came back to my senses, I saw Sakura's father flinched back, shaken and shocked. "
    ""

# game/dialogs.rpy:3658
translate tokipona scene8_770343cb:

    # "The broken bottle he was holding fell on the ground and shattered."
    ""

# game/dialogs.rpy:3659
translate tokipona scene8_2cf1e3ba:

    # "The pieces were stained with blood."
    ""

# game/dialogs.rpy:3661
translate tokipona scene8_43934e89:

    # "On top of me, there was the body of Sakura."
    ""

# game/dialogs.rpy:3662
translate tokipona scene8_9e470804:

    # "Her stomach was bleeding profusely."
    ""

# game/dialogs.rpy:3663
translate tokipona scene8_fc381d0a:

    # hero "Sakura!!!"
    hero ""

# game/dialogs.rpy:3664
translate tokipona scene8_c04fe468:

    # hero "{size=+15}Sakura!!! Hang on!!! Sakura!!!{/size}"
    hero ""

# game/dialogs.rpy:3665
translate tokipona scene8_281954cf:

    # "I started to feel tears in my eyes."
    ""

# game/dialogs.rpy:3666
translate tokipona scene8_b46a96ab:

    # "She saved me by sacrificing herself."
    ""

# game/dialogs.rpy:3667
translate tokipona scene8_2e1ee92a:

    # hero "{size=+15}Please, don't die, Sakura!!!{/size}"
    hero ""

# game/dialogs.rpy:3668
translate tokipona scene8_6f1c6352:

    # "Sakura weakly turned her head to me."
    ""

# game/dialogs.rpy:3670
translate tokipona scene8_22286f8f:

    # "I caressed her cheek tenderly, removing a tear, and kissing her lips softly, my own tears preventing me to see how beautiful she looked, even in that state."
    ""

# game/dialogs.rpy:3671
translate tokipona scene8_1204b721:

    # s "..........{p}......%(stringhero)s....kun....."
    s ""

# game/dialogs.rpy:3673
translate tokipona scene8_001b31dc:

    # s "I'm... so happy to... be... your girlfriend......"
    s ""

# game/dialogs.rpy:3674
translate tokipona scene8_d6ab4c63:

    # s "I... love you... so much,...%(stringhero)s....kun...."
    s ""

# game/dialogs.rpy:3675
translate tokipona scene8_a2f900d8:

    # hero "And I love you, Sakura, more than anything!"
    hero ""

# game/dialogs.rpy:3676
translate tokipona scene8_04745b3b:

    # hero "That's why you must stay alive!! Stay with me!!"
    hero ""

# game/dialogs.rpy:3677
translate tokipona scene8_e8a331c8:

    # s "I..."
    s ""

# game/dialogs.rpy:3679
translate tokipona scene8_379966c6:

    # s "T... Too bad it...{w}didn't worked...between us..."
    s ""

# game/dialogs.rpy:3680
translate tokipona scene8_362ff53c:

    # s "But that's okay...{p}You... and the others...{w} took care of me so nicely..."
    s ""

# game/dialogs.rpy:3681
translate tokipona scene8_0868d776:

    # s "I'm...so happy to... be... {w}your friend......"
    s ""

# game/dialogs.rpy:3682
translate tokipona scene8_f3331226:

    # s "T... Thank...... you..."
    s ""

# game/dialogs.rpy:3684
translate tokipona scene8_a58e1fda:

    # s "I regret I... didn't had the time... to tell Nanami-chan... the truth..."
    s ""

# game/dialogs.rpy:3685
translate tokipona scene8_f8ac1185:

    # s "You know..."
    s ""

# game/dialogs.rpy:3687
translate tokipona scene8_cb894c8f:

    # s "I wish......{p}I was...{p}a real girl..."
    s ""

# game/dialogs.rpy:3689
translate tokipona scene8_ccf33f5d:

    # "She smiled at me, then she fainted in my arms."
    ""

# game/dialogs.rpy:3693
translate tokipona scene8_8e8bdb99:

    # hero "Sakura!!!!!!{p}{size=+15}Sakura!!! Answer me!!!{/size}"
    hero ""

# game/dialogs.rpy:3694
translate tokipona scene8_43e0fc44:

    # hero "{size=+20}SAKURAAAAAAAAAAAAAAA!!!!!!{/size}"
    hero ""

# game/dialogs.rpy:3698
translate tokipona scene8_7a909493:

    # "The police and the ambulance arrived shortly after, with Rika-chan."
    ""

# game/dialogs.rpy:3699
translate tokipona scene8_dd0ebba0:

    # "There was still a pulse inside Sakura's body, but she was badly hurt."
    ""

# game/dialogs.rpy:3700
translate tokipona scene8_913d018c:

    # "The police arrested her father for attempted homicide and domestic violence."
    ""

# game/dialogs.rpy:3701
translate tokipona scene8_53b8960a:

    # "Sakura's mom went with her in the ambulance."
    ""

# game/dialogs.rpy:3703
translate tokipona scene8_8f2b17c9:

    # "I stayed there with Rika-chan for a moment... when everyone was gone."
    ""

# game/dialogs.rpy:3705
translate tokipona scene8_e09c696e:

    # "I cried in Rika-chan's arms, uncontrollably."
    ""

# game/dialogs.rpy:3706
translate tokipona scene8_c44c2f72:

    # hero "Sakura... No, damnit, not Sakura..."
    hero ""

# game/dialogs.rpy:3707
translate tokipona scene8_68b58967:

    # "Rika was sobbing as well."
    ""

# game/dialogs.rpy:3708
translate tokipona scene8_c6f7844b:

    # "When we calmed down a little, she spoke."
    ""

# game/dialogs.rpy:3709
translate tokipona scene8_d30a40a6:

    # r "You know... {p}I don't know how to say it..."
    r ""

# game/dialogs.rpy:3710
translate tokipona scene8_906477c5:

    # r "But I think it's the right time to tell you..."
    r ""

# game/dialogs.rpy:3711
translate tokipona scene8_ea880f58:

    # r "Before you came to our club... I was in love with Sakura..."
    r ""

# game/dialogs.rpy:3712
translate tokipona scene8_6c7d6f4e:

    # hero "You were? With Sakura-chan?"
    hero ""

# game/dialogs.rpy:3713
translate tokipona scene8_73d36031:

    # r "Yes..."
    r ""

# game/dialogs.rpy:3714
translate tokipona scene8_1a4eb0ff:

    # r "I was attracted by this boy who is so girly..."
    r ""

# game/dialogs.rpy:3715
translate tokipona scene8_e94b0b53:

    # r "After that, you came,... {size=-15}and it changed...{/size}"
    r ""

# game/dialogs.rpy:3716
translate tokipona scene8_402f030d:

    # r "By the way, you know why I consider Sakura as my best friend, now..."
    r ""

# game/dialogs.rpy:3717
translate tokipona scene8_e668a3b6:

    # hero "I see..."
    hero ""

# game/dialogs.rpy:3718
translate tokipona scene8_7030c013:

    # r "But don't worry... I was shocked when I heard about you two, but..."
    r ""

# game/dialogs.rpy:3719
translate tokipona scene8_09ad0db3:

    # r "I was genuinely happy for the both of you."
    r ""

# game/dialogs.rpy:3720
translate tokipona scene8_39b70c91:

    # r "I never seen Sakura so happy before. You changed her life so much..."
    r ""

# game/dialogs.rpy:3721
translate tokipona scene8_c0eddcd4:

    # r "It's why I'm not mad at you... Sakura's happiness is all that matters..."
    r ""

# game/dialogs.rpy:3722
translate tokipona scene8_190df487:

    # r "And now..."
    r ""

# game/dialogs.rpy:3723
translate tokipona scene8_9a33713f:

    # r "I just hope...{w}That she will be alright!"
    r ""

# game/dialogs.rpy:3724
translate tokipona scene8_0bd0684e:

    # "She started to weep too. I had never seen her like that before."
    ""

# game/dialogs.rpy:3725
translate tokipona scene8_0fb4909e:

    # "I held her in my arms gently."
    ""

# game/dialogs.rpy:3729
translate tokipona scene8_f4f6d6da:

    # r "Are you okay, hun?"
    r ""

# game/dialogs.rpy:3731
translate tokipona scene8_422d11ba:

    # r "Are you okay?"
    r ""

# game/dialogs.rpy:3732
translate tokipona scene8_e8331494:

    # hero "I'm fine... Horribly anxious but fine..."
    hero ""

# game/dialogs.rpy:3733
translate tokipona scene8_f8b52a34:

    # "Rika stayed silent for a moment. But finally, she cried in my arms."
    ""

# game/dialogs.rpy:3734
translate tokipona scene8_a4d9e75c:

    # r "I'm so scared... I don't want to loose Sakura...!"
    r ""

# game/dialogs.rpy:3736
translate tokipona scene8_103c3486:

    # hero "I know, babe... Me neither..."
    hero ""

# game/dialogs.rpy:3738
translate tokipona scene8_bfb9febc:

    # hero "I know... Me neither..."
    hero ""

# game/dialogs.rpy:3739
translate tokipona scene8_5546a02e:

    # "She cried for a long time in my arms."
    ""

# game/dialogs.rpy:3740
translate tokipona scene8_46acf1af:

    # "When she stopped, she spoke."
    ""

# game/dialogs.rpy:3741
translate tokipona scene8_72e004e1:

    # r "You know... She told me that..."
    r ""

# game/dialogs.rpy:3742
translate tokipona scene8_72694f8f:

    # r "Sakura told me that she loved you..."
    r ""

# game/dialogs.rpy:3743
translate tokipona scene8_1ae45319:

    # hero "Ah?"
    hero ""

# game/dialogs.rpy:3744
translate tokipona scene8_6ea028e4:

    # "I had guessed it since the car incident, some days ago."
    ""

# game/dialogs.rpy:3745
translate tokipona scene8_4e396aba:

    # "I had the feeling that she was in love with me, sort-of..."
    ""

# game/dialogs.rpy:3746
translate tokipona scene8_77b8faa3:

    # r "She knew that it was an impossible love since you're straight...{p}But she had always loved you with all her heart..."
    r ""

# game/dialogs.rpy:3748
translate tokipona scene8_5f050a71:

    # r "And to be honest... It's thanks to her that we are going out together now."
    r ""

# game/dialogs.rpy:3749
translate tokipona scene8_9fcae4dd:

    # hero "What?"
    hero ""

# game/dialogs.rpy:3750
translate tokipona scene8_601c81c7:

    # hero "But...{p}How? {w}Wh-{nw}"
    hero ""

# game/dialogs.rpy:3751
translate tokipona scene8_b37d1c72:

    # r "She was the one who organized everything in our first date, at the festival, with the help of Nanami."
    r ""

# game/dialogs.rpy:3752
translate tokipona scene8_4a5bf62a:

    # r "It didn't happen as she expected it, but the result was the same, in the end."
    r ""

# game/dialogs.rpy:3753
translate tokipona scene8_1900c1b3:

    # hero "That's why they weren't there at the festival?"
    hero ""

# game/dialogs.rpy:3754
translate tokipona scene8_bec3b4af:

    # r "Yes."
    r ""

# game/dialogs.rpy:3755
translate tokipona scene8_6220eee7:

    # r "I wasn't very okay about the idea first, but she knew that..."
    r ""

# game/dialogs.rpy:3756
translate tokipona scene8_a0391de0:

    # "She blushed."
    ""

# game/dialogs.rpy:3757
translate tokipona scene8_4232035f:

    # r "That I had... feelings for you too..."
    r ""

# game/dialogs.rpy:3758
translate tokipona scene8_65088067:

    # "I smiled. Tears started to come again from my eyes."
    ""

# game/dialogs.rpy:3759
translate tokipona scene8_33789624:

    # hero "She wanted us to be all together."
    hero ""

# game/dialogs.rpy:3760
translate tokipona scene8_b16b0eb8:

    # hero "That she will be in our relationship like a best unique friend... Like a sister to each of us..."
    hero ""

# game/dialogs.rpy:3761
translate tokipona scene8_e66f523f:

    # hero "The sister who is ready to abandon everything...just for the happiness of her best friends..."
    hero ""

# game/dialogs.rpy:3762
translate tokipona scene8_f1b0dae1:

    # "She was in love with me... Being lovers was impossible but her friendship and love for me never stopped..."
    ""

# game/dialogs.rpy:3763
translate tokipona scene8_cfdac421:

    # "And she even went to create happiness between me and Rika just because she loved us..."
    ""

# game/dialogs.rpy:3764
translate tokipona scene8_0bd26214:

    # "It was my turn to cry, thinking about that adorable angel Sakura."
    ""

# game/dialogs.rpy:3766
translate tokipona scene8_f1b0dae1_1:

    # "She was in love with me... Being lovers was impossible but her friendship and love for me never stopped..."
    ""

# game/dialogs.rpy:3767
translate tokipona scene8_86a7a846:

    # "It was my turn to cry, thinking about Sakura's kind heart."
    ""

# game/dialogs.rpy:3768
translate tokipona scene8_01146bd8:

    # "At the same time, I felt like a jerk..."
    ""

# game/dialogs.rpy:3769
translate tokipona scene8_23b3af63:

    # "Rika-chan held me tight in her arms, rubbing my hair."
    ""

# game/dialogs.rpy:3770
translate tokipona scene8_f0b3fd2d:

    # "I was confused. It's not something she would do normally..."
    ""

# game/dialogs.rpy:3771
translate tokipona scene8_2e4a7bba:

    # "But I felt better in her arms..."
    ""

# game/dialogs.rpy:3772
translate tokipona scene8_d1802fa3:

    # "The night was silent..."
    ""

# game/dialogs.rpy:3779
translate tokipona scene9_e9b7540d:

    # "The next day at school was gloomy for me, Nanami and Rika."
    ""

# game/dialogs.rpy:3781
translate tokipona scene9_43a31284:

    # "At the club, we were just sitting... doing nothing, saying nothing."
    ""

# game/dialogs.rpy:3782
translate tokipona scene9_d9d4a59c:

    # "The only noise we could hear was Nanami's crying."
    ""

# game/dialogs.rpy:3783
translate tokipona scene9_de531cea:

    # "She couldn't stop crying when she heard the news..."
    ""

# game/dialogs.rpy:3784
translate tokipona scene9_748065c0:

    # "The club was silent without Sakura..."
    ""

# game/dialogs.rpy:3786
translate tokipona scene9_be3ea4ed:

    # "Finally I spoke as I stood up, smashing my fist on the table."
    ""

# game/dialogs.rpy:3788
translate tokipona scene9_e9e02a55:

    # hero "I can't stay here like this! I have to go the hospital and get news about Sakura!"
    hero ""

# game/dialogs.rpy:3789
translate tokipona scene9_cf4c0510:

    # "Rika-chan smiled, determined, and stood up as well."
    ""

# game/dialogs.rpy:3790
translate tokipona scene9_00b28479:

    # r "You're right, kid! Let's go there right now!"
    r ""

# game/dialogs.rpy:3791
translate tokipona scene9_611208b8:

    # r "Nobody said that the manga club would leave one of its members alone!"
    r ""

# game/dialogs.rpy:3792
translate tokipona scene9_3966d3ed:

    # "I smiled. I at last found the Rika-chan I knew."
    ""

# game/dialogs.rpy:3794
translate tokipona scene9_551012d0:

    # "Finally, Rika-chan stood up, determined."
    ""

# game/dialogs.rpy:3796
translate tokipona scene9_3cf11638:

    # r "I can't stay here like this! I must go to the hospital and get news about Sakura-chan!"
    r ""

# game/dialogs.rpy:3797
translate tokipona scene9_567a77f3:

    # "I nodded and stood up as well."
    ""

# game/dialogs.rpy:3798
translate tokipona scene9_00c27679:

    # hero "You're right! Let's go right now!"
    hero ""

# game/dialogs.rpy:3799
translate tokipona scene9_2fe99060:

    # "Nanami didn't say a word, but she stopped crying, dried her tears and followed us with a cute determined face."
    ""

# game/dialogs.rpy:3801
translate tokipona scene9_243a70b8:

    # hero "So, Doctor, how is Sakura?"
    hero ""

# game/dialogs.rpy:3803
translate tokipona scene9_9eefb10b:

    # "Doctor" "Oh, don't worry, kids. Your friend is fine, now!"
    "Doctor" ""

# game/dialogs.rpy:3804
translate tokipona scene9_063c6c0a:

    # "Doctor" "We managed to save him right on time, thanks to his mother's call!"
    "Doctor" ""

# game/dialogs.rpy:3805
translate tokipona scene9_24b1777f:

    # "We couldn't believe it! We heaved a sigh of relief."
    ""

# game/dialogs.rpy:3806
translate tokipona scene9_f6dd7e91:

    # "Doctor" "He should be ready in a few days."
    "Doctor" ""

# game/dialogs.rpy:3807
translate tokipona scene9_e7741f1a:

    # "Doctor" "He can have visitors, if you want to see him."
    "Doctor" ""

# game/dialogs.rpy:3808
translate tokipona scene9_61585f71:

    # "We nodded and followed the doctor to Sakura's hospital room."
    ""

# game/dialogs.rpy:3810
translate tokipona scene9_81c4b68b:

    # "Her face shone as she saw us."
    ""

# game/dialogs.rpy:3811
translate tokipona scene9_9b4114a6:

    # s "%(stringhero)s-kun!! Rika-chan!! Nana-chan!!"
    s ""

# game/dialogs.rpy:3812
translate tokipona scene9_417ded14:

    # n "Sakura-nee!!!"
    n ""

# game/dialogs.rpy:3813
translate tokipona scene9_c0cdba56:

    # "Nanami jumped on the bed to hug Sakura."
    ""

# game/dialogs.rpy:3814
translate tokipona scene9_5fcae85e:

    # "She flinched a little bit by the pain but hugged Nanami back, rubbing her head like child."
    ""

# game/dialogs.rpy:3815
translate tokipona scene9_06d60896:

    # s "Easy, little one... I need time for the wound to heal."
    s ""

# game/dialogs.rpy:3816
translate tokipona scene9_52c6a554:

    # n "I'm not little!"
    n ""

# game/dialogs.rpy:3817
translate tokipona scene9_00bc78d2:

    # "She said that while going back to crying. But of joy this time."
    ""

# game/dialogs.rpy:3818
translate tokipona scene9_f46ea2ef:

    # hero "Oh my gosh... Sakura-chan...{p}I'm so happy you survived!"
    hero ""

# game/dialogs.rpy:3819
translate tokipona scene9_b9968b56:

    # hero "I was so scared that you...!"
    hero ""

# game/dialogs.rpy:3820
translate tokipona scene9_1508b046:

    # "She seemed to be really okay, as if nothing ever happened."
    ""

# game/dialogs.rpy:3822
translate tokipona scene9_b8aeb0be:

    # s "I will not leave this planet where I can have the best boyfriend ever!"
    s ""

# game/dialogs.rpy:3823
translate tokipona scene9_0d3e5a44:

    # s "As well as the best friends!"
    s ""

# game/dialogs.rpy:3825
translate tokipona scene9_0ac19270:

    # s "I will not leave this planet where I can have my best friends!"
    s ""

# game/dialogs.rpy:3826
translate tokipona scene9_fadc1602:

    # r "We all love you, Sakura! {image=heart.png}"
    r ""

# game/dialogs.rpy:3827
translate tokipona scene9_ab6bc56f:

    # "Sakura smiled to us."
    ""

# game/dialogs.rpy:3828
translate tokipona scene9_c7659595:

    # s "I love you all too... So much..."
    s ""

# game/dialogs.rpy:3830
translate tokipona scene9_dad43788:

    # hero "You know, Sakura-chan..."
    hero ""

# game/dialogs.rpy:3831
translate tokipona scene9_1acb2356:

    # hero "I feel I was a terrible friend to you..."
    hero ""

# game/dialogs.rpy:3832
translate tokipona scene9_8d593ce9:

    # hero "Please forgive me..."
    hero ""

# game/dialogs.rpy:3833
translate tokipona scene9_79003247:

    # "Sakura put a finger on my lips."
    ""

# game/dialogs.rpy:3834
translate tokipona scene9_69c6fa27:

    # s "Hush! Stop talking nonsense."
    s ""

# game/dialogs.rpy:3835
translate tokipona scene9_e9594450:

    # s "You faced my father to defend me and you were almost ready to die for that!"
    s ""

# game/dialogs.rpy:3836
translate tokipona scene9_7b24500a:

    # s "How can you possibly think you're an awful friend?"
    s ""

# game/dialogs.rpy:3837
translate tokipona scene9_0cde7c21:

    # s "You're my best friend!"
    s ""

# game/dialogs.rpy:3838
translate tokipona scene9_993b25b5:

    # "Tears rolled down my eyes."
    ""

# game/dialogs.rpy:3839
translate tokipona scene9_1cce67e1:

    # hero "You're an angel, Sakura-chan."
    hero ""

# game/dialogs.rpy:3841
translate tokipona scene9_57b35236:

    # r "Did your mom visit you?"
    r ""

# game/dialogs.rpy:3842
translate tokipona scene9_85c4e509:

    # s "Yes she did."
    s ""

# game/dialogs.rpy:3843
translate tokipona scene9_a3b8f1b9:

    # s "She's a bit sad that father was taken to jail,... {w}But I think she can't love him anymore after what he did."
    s ""

# game/dialogs.rpy:3844
translate tokipona scene9_f1990ad2:

    # s "I don't know what to think either. {w}It's a relief that he won't bother me and mom anymore...{w}but he's still my father..."
    s ""

# game/dialogs.rpy:3845
translate tokipona scene9_05ffe314:

    # hero "Don't worry, Sakura-chan. We'll all be here for you!"
    hero ""

# game/dialogs.rpy:3846
translate tokipona scene9_0c126df5:

    # hero "I don't know how yet, but we'll find out!"
    hero ""

# game/dialogs.rpy:3847
translate tokipona scene9_363e4438:

    # r "Yes, we will!"
    r ""

# game/dialogs.rpy:3848
translate tokipona scene9_6a9ed22c:

    # n "Yeah!"
    n ""

# game/dialogs.rpy:3849
translate tokipona scene9_eecde369:

    # "A tear of joy rolled down Sakura's cheek as she smiled."
    ""

# game/dialogs.rpy:3850
translate tokipona scene9_cb78f15e:

    # s "I know you all do."
    s ""

# game/dialogs.rpy:3851
translate tokipona scene9_e926c122:

    # s "Thank you..."
    s ""

# game/dialogs.rpy:3852
translate tokipona scene9_81a1d957:

    # "Since Rika-chan brought some manga with us, we did our club activities with Sakura in her room."
    ""

# game/dialogs.rpy:3853
translate tokipona scene9_87845d82:

    # "That was fun."
    ""

# game/dialogs.rpy:3857
translate tokipona scene9_b859f96a:

    # "I was so happy..."
    ""

# game/dialogs.rpy:3858
translate tokipona scene9_4055f551:

    # "I think my luck helped me again..."
    ""

# game/dialogs.rpy:3859
translate tokipona scene9_9095e36e:

    # "But this time, it helped Sakura too."
    ""

# game/dialogs.rpy:3862
translate tokipona scene9_8dbb56a9:

    # n "Why the doctor said \"he\" to Sakura?"
    n ""

# game/dialogs.rpy:3863
translate tokipona scene9_ad0764fc:

    # "Uh-oh..."
    ""

# game/dialogs.rpy:3864
translate tokipona scene9_ff11d5e2:

    # "Well, better late than never."
    ""

# game/dialogs.rpy:3865
translate tokipona scene9_984c292d:

    # "Since there probably wouldn't be any other better moment for that, We told Nanami about Sakura's secret."
    ""

# game/dialogs.rpy:3866
translate tokipona scene9_fcc6280e:

    # "She took it well and understood. Her feelings for Sakura didn't changed at all. Just as I expected of her."
    ""

# game/dialogs.rpy:3867
translate tokipona scene9_a20cefa7:

    # "..."
    ""

# game/dialogs.rpy:3868
translate tokipona scene9_19a38d50:

    # "Though, for a whole week, she randomly said \"Seriously, guys!!\" multiple times!"
    ""

# game/dialogs.rpy:3869
translate tokipona scene9_7c3cef17:

    # "I think she was a bit upset that we kept the secret to us until now."
    ""

# game/dialogs.rpy:3870
translate tokipona scene9_0bfbc1f4:

    # "We promised it was the last time we kept a secret from Nanami."
    ""

# game/dialogs.rpy:3874
translate tokipona end_d9a3a0bf:

    # "Summer vacation came."
    ""

# game/dialogs.rpy:3875
translate tokipona end_d7bb0ba9:

    # "To all of us, school was over until next September."
    ""

# game/dialogs.rpy:3880
translate tokipona end_d95c02cb:

    # "Love can be really strange..."
    ""

# game/dialogs.rpy:3881
translate tokipona end_c409fabd:

    # "You think you know what kind of person you'll end up with but..."
    ""

# game/dialogs.rpy:3882
translate tokipona end_5eba593d:

    # "Love at first sight can hit anybody..."
    ""

# game/dialogs.rpy:3883
translate tokipona end_617e0a41:

    # "I was pretty sure I would only love real girls..."
    ""

# game/dialogs.rpy:3884
translate tokipona end_e3e4774b:

    # "And my first girlfriend is actually a very girlish boy!"
    ""

# game/dialogs.rpy:3885
translate tokipona end_6292a02f:

    # "Hehe, who could have predicted this?{p}Not me, for sure..."
    ""

# game/dialogs.rpy:3886
translate tokipona end_e4e1f13a:

    # "I don't know how much time we will stay together, Sakura and I."
    ""

# game/dialogs.rpy:3887
translate tokipona end_32aa38be:

    # "Months? Years? Maybe for the rest of our lives...?"
    ""

# game/dialogs.rpy:3888
translate tokipona end_478778fd:

    # "I'm not sure of what the future holds for us..."
    ""

# game/dialogs.rpy:3889
translate tokipona end_d076867a:

    # "But if my story with Sakura must end someday,{w} I'll never forget the wonderful times I passed with her."
    ""

# game/dialogs.rpy:3890
translate tokipona end_7dbb760e:

    # "Or him..."
    ""

# game/dialogs.rpy:3891
translate tokipona end_54a608c6:

    # "Who cares..."
    ""

# game/dialogs.rpy:3892
translate tokipona end_85655125:

    # s "Honey! What are you doing? Come see the view!"
    s ""

# game/dialogs.rpy:3893
translate tokipona end_7c43af43:

    # hero "I'm coming, sweetie!"
    hero ""

# game/dialogs.rpy:3900
translate tokipona end_59a65d95:

    # "We're waiting at our plane at the airport."
    ""

# game/dialogs.rpy:3901
translate tokipona end_ae3dd256:

    # "Nanami and I decided to take some vacations at Nanami's homeland, Okinawa."
    ""

# game/dialogs.rpy:3902
translate tokipona end_d12ce3e4:

    # "My head was full of ondo and traditional music, like at the festival."
    ""

# game/dialogs.rpy:3903
translate tokipona end_4452fe89:

    # "Toshio escorted us to the airport.{p}Rika and Sakura were here too."
    ""

# game/dialogs.rpy:3904
translate tokipona end_8b58bec9:

    # "It's been a while that Rika and Sakura started to going out together, now."
    ""

# game/dialogs.rpy:3905
translate tokipona end_e30bea45:

    # "They look like a cute couple of girls."
    ""

# game/dialogs.rpy:3906
translate tokipona end_ca017532:

    # toshio "Tell our grand-parents that I love them."
    toshio ""

# game/dialogs.rpy:3907
translate tokipona end_a7442fed:

    # n "I will, don't worry."
    n ""

# game/dialogs.rpy:3908
translate tokipona end_d3330439:

    # toshio "%(stringhero)s-san... Take good care of my sister."
    toshio ""

# game/dialogs.rpy:3909
translate tokipona end_a831d3fe:

    # hero "I will."
    hero ""

# game/dialogs.rpy:3910
translate tokipona end_17ebd900:

    # hero "And you, good luck with your new job."
    hero ""

# game/dialogs.rpy:3911
translate tokipona end_5b348dc8:

    # "My parents hired him at the grocery shop."
    ""

# game/dialogs.rpy:3912
translate tokipona end_16f4a6f4:

    # "That way, he can renew with his past while keeping going straight forward to his new adult life."
    ""

# game/dialogs.rpy:3913
translate tokipona end_95d64dcd:

    # s "Take a lot of photos, %(stringhero)s-kun! The whole place is beautiful, it deserve photographs!"
    s ""

# game/dialogs.rpy:3914
translate tokipona end_8f710e49:

    # r "And bring back some souvenirs!{p}Like, an okinawa manga or something!"
    r ""

# game/dialogs.rpy:3915
translate tokipona end_9cf1c80f:

    # "I chuckled at Rika's request."
    ""

# game/dialogs.rpy:3916
translate tokipona end_a725dde6:

    # "Then, I heard the call for our plane."
    ""

# game/dialogs.rpy:3917
translate tokipona end_bb0d139e:

    # "Nanami and I went for our departure."
    ""

# game/dialogs.rpy:3918
translate tokipona end_8ea8e17b:

    # "We waved at our friends and they waved back."
    ""

# game/dialogs.rpy:3919
translate tokipona end_ff8fb8bb:

    # "One thing is certain,..."
    ""

# game/dialogs.rpy:3920
translate tokipona end_06a038b7:

    # "It's in moments like this that having friends like that is a true luck."
    ""

# game/dialogs.rpy:3921
translate tokipona end_099873e9:

    # "And with a girlfriend like Nanami,...{w}and friends like I have,..."
    ""

# game/dialogs.rpy:3922
translate tokipona end_3f02d717:

    # "I can tell I'm the luckiest dude ever."
    ""

# game/dialogs.rpy:3923
translate tokipona end_bc578e9b:

    # s "Don't forget to come back!"
    s ""

# game/dialogs.rpy:3924
translate tokipona end_867c4c03:

    # n "We will, Sakura-nee!"
    n ""

# game/dialogs.rpy:3931
translate tokipona end_2d2fc7dd:

    # "I finally asked Rika-chan to marry me."
    ""

# game/dialogs.rpy:3932
translate tokipona end_7d486c68:

    # "And I promised Sakura-chan that she will be the godparent of our children."
    ""

# game/dialogs.rpy:3933
translate tokipona end_16d52d33:

    # "I hope Sakura will find love someday. She really deserves it."
    ""

# game/dialogs.rpy:3934
translate tokipona end_8052eee9:

    # "I was actually ready for the ceremony."
    ""

# game/dialogs.rpy:3935
translate tokipona end_d8e35e56:

    # "In the village, we usually do traditional weddings, but Rika wanted an occidental wedding."
    ""

# game/dialogs.rpy:3936
translate tokipona end_e5ffcd4a:

    # "I guess it's for the dress..."
    ""

# game/dialogs.rpy:3937
translate tokipona end_c371d9bb:

    # "It's the best cosplay she ever did. Except, this time, it wasn't a cosplay. It was real."
    ""

# game/dialogs.rpy:3938
translate tokipona end_ecc6ece6:

    # "Priest" "Dear %(stringhero)s-san, do you take Rika-san to be your wedded wife, to live together in marriage?"
    "Priest" ""

# game/dialogs.rpy:3939
translate tokipona end_02471825:

    # hero "You bet I do!"
    hero ""

# game/dialogs.rpy:3940
translate tokipona end_34741d03:

    # r "Can't you just say \"I do.\", you city rat?!"
    r ""

# game/dialogs.rpy:3941
translate tokipona end_96f20b66:

    # "*{i}laughs{/i}*"
    ""

# game/dialogs.rpy:3942
translate tokipona end_c2e91305:

    # hero "Hey do you plan to call me city rat even after being married!"
    hero ""

# game/dialogs.rpy:3943
translate tokipona end_5848829c:

    # r "Yes I will!"
    r ""

# game/dialogs.rpy:3944
translate tokipona end_96f20b66_1:

    # "*{i}laughs{/i}*"
    ""

# game/dialogs.rpy:3945
translate tokipona end_6ddd1312:

    # "Priest" "Hem hem..."
    "Priest" ""

# game/dialogs.rpy:3946
translate tokipona end_aea646e4:

    # "Priest" "And you, dear Rika-san,...do you take %(stringhero)s-san to be your wedded husband, to live together in marriage?"
    "Priest" ""

# game/dialogs.rpy:3947
translate tokipona end_742d691c:

    # "Then, Rika made a smile.{p}A smile I never seen on her before."
    ""

# game/dialogs.rpy:3948
translate tokipona end_2665aacc:

    # "Not that naughty smile of when she prepares a joke or anything."
    ""

# game/dialogs.rpy:3949
translate tokipona end_dfe9a447:

    # "A shining, bright smile. The cutest smile she ever made."
    ""

# game/dialogs.rpy:3950
translate tokipona end_b8af1a6a:

    # r "... I do!"
    r ""

# game/dialogs.rpy:3996
translate tokipona trueend_906ebb63:

    # centered "You finished the game for the first time!\nGo check the Bonus menu to see what you have unlocked!"
    centered ""

# game/dialogs.rpy:4018
translate tokipona trueend_66eedbda:

    # centered "You finished the game with Sakura's route for the first time!"
    centered ""

# game/dialogs.rpy:4040
translate tokipona trueend_1561cc61:

    # centered "You finished the game with Rika's route for the first time!"
    centered ""

# game/dialogs.rpy:4062
translate tokipona trueend_bb63381f:

    # centered "You finished the game with Nanami's route for the first time!"
    centered ""

# game/dialogs.rpy:4079
translate tokipona bonus1_25742137:

    # hero "Hey girls, do you remember the first anime you ever watched?"
    hero ""

# game/dialogs.rpy:4081
translate tokipona bonus1_6e60ac8f:

    # s "Hmm... That's a hard one..."
    s ""

# game/dialogs.rpy:4082
translate tokipona bonus1_25a3fcbc:

    # r "Hmm, I think my first one was {i}Sanae-san{/i}."
    r ""

# game/dialogs.rpy:4083
translate tokipona bonus1_1ac8f05b:

    # hero "{i}Sanae-san{/i}?{p}That anime that about politics and stuff?"
    hero ""

# game/dialogs.rpy:4084
translate tokipona bonus1_1e55e98f:

    # r "Yeah! That one was nice!"
    r ""

# game/dialogs.rpy:4085
translate tokipona bonus1_89c89c91:

    # hero "But it was aired when we were around four or five years old! You could understand what they were talking about?"
    hero ""

# game/dialogs.rpy:4087
translate tokipona bonus1_b9b8111c:

    # "Rika gave me a disgusted look."
    ""

# game/dialogs.rpy:4088
translate tokipona bonus1_17789b93:

    # r "Whatever! Well what about you? What was your first anime?!"
    r ""

# game/dialogs.rpy:4089
translate tokipona bonus1_517679f1:

    # hero "{i}Panpanman{/i}. You know, the one with the donut-headed guy..."
    hero ""

# game/dialogs.rpy:4090
translate tokipona bonus1_c5d8e4d4:

    # r "And could you understand everything in that show?"
    r ""

# game/dialogs.rpy:4091
translate tokipona bonus1_8b365fc3:

    # "Ah damn, she's got a point there."
    ""

# game/dialogs.rpy:4092
translate tokipona bonus1_85a0d1bb:

    # "{i}Panpanman{/i} wasn't as complicated as {i}Sanae-san{/i}, but it's just as old. I was young so...I just followed along the animation."
    ""

# game/dialogs.rpy:4093
translate tokipona bonus1_61a5b6c2:

    # hero "And you, Nanami-san?"
    hero ""

# game/dialogs.rpy:4097
translate tokipona bonus1_b075ad7f:

    # n "Hmm..."
    n ""

# game/dialogs.rpy:4098
translate tokipona bonus1_2d685261:

    # n "I think my first one was {i}Galaxyboy{/i}..."
    n ""

# game/dialogs.rpy:4099
translate tokipona bonus1_88ba5484:

    # r "The one with that robot kid?"
    r ""

# game/dialogs.rpy:4100
translate tokipona bonus1_31293345:

    # n "Yeah! I remember it being really colorful and fun to watch."
    n ""

# game/dialogs.rpy:4101
translate tokipona bonus1_cd1e46e5:

    # n "There was a game called {i}Punkman{/i} with a similar looking robot."
    n ""

# game/dialogs.rpy:4102
translate tokipona bonus1_f81a7f2c:

    # n "Maybe that's why I remember {i}Galaxyboy{/i} a little."
    n ""

# game/dialogs.rpy:4104
translate tokipona bonus1_e11f5261:

    # n "Although, I don't remember the story at all!"
    n ""

# game/dialogs.rpy:4106
translate tokipona bonus1_d7bf3979:

    # hero "What about you, Sakura-san?{p}What was your first anime?"
    hero ""

# game/dialogs.rpy:4108
translate tokipona bonus1_85feadfd:

    # s "Hmm... Well... I think my first one was {i}La fleur de Paris{/i}..."
    s ""

# game/dialogs.rpy:4109
translate tokipona bonus1_7076984c:

    # hero "The one based on a book with that girl from France?"
    hero ""

# game/dialogs.rpy:4110
translate tokipona bonus1_e929395f:

    # hero "That one is more recent... It only aired around six years ago."
    hero ""

# game/dialogs.rpy:4111
translate tokipona bonus1_4343cc28:

    # s "Yeah. My love for anime started a little later. I was 12 when I started watching it."
    s ""

# game/dialogs.rpy:4112
translate tokipona bonus1_5a7baf3a:

    # s "I loved the story. It was about a girl who disguises herself as a man just to get closer to her king."
    s ""

# game/dialogs.rpy:4113
translate tokipona bonus1_64c82b58:

    # hero "{i}La fleur de Paris{/i} sounds like a shoujo... I think my sister tried to read it once..."
    hero ""

# game/dialogs.rpy:4114
translate tokipona bonus1_9c8d2196:

    # s "I didn't know you had a sister!"
    s ""

# game/dialogs.rpy:4115
translate tokipona bonus1_376fc77d:

    # hero "Well, yeah."
    hero ""

# game/dialogs.rpy:4116
translate tokipona bonus1_a4020f07:

    # hero "But she's way older than me. She's already married."
    hero ""

# game/dialogs.rpy:4117
translate tokipona bonus1_5d47aa0b:

    # hero "She's in Tokyo with her husband."
    hero ""

# game/dialogs.rpy:4118
translate tokipona bonus1_3e2e6e7f:

    # r "That's sounds nice!"
    r ""

# game/dialogs.rpy:4120
translate tokipona bonus1_5a889d44:

    # s "Don't you miss having your sister around?"
    s ""

# game/dialogs.rpy:4121
translate tokipona bonus1_38aa09d8:

    # hero "Oh well, she's always been independent. I got used to it after awhile, so it's okay! I still write to her from time to time."
    hero ""

# game/dialogs.rpy:4122
translate tokipona bonus1_2e2dc283:

    # hero "By the way, do you guys have siblings?"
    hero ""

# game/dialogs.rpy:4123
translate tokipona bonus1_ceca0652:

    # n "I have an older brother."
    n ""

# game/dialogs.rpy:4124
translate tokipona bonus1_1e3491ce:

    # s "I don't."
    s ""

# game/dialogs.rpy:4125
translate tokipona bonus1_7c225574:

    # r "Me neither."
    r ""

# game/dialogs.rpy:4126
translate tokipona bonus1_bc19b4bf:

    # hero "Ever wish you had one?"
    hero ""

# game/dialogs.rpy:4127
translate tokipona bonus1_bdb5b07a:

    # r "Nah, I never did. I'm happy with my current life."
    r ""

# game/dialogs.rpy:4128
translate tokipona bonus1_58cc7ba4:

    # s "Well for me... I sometimes dream of having a big brother."
    s ""

# game/dialogs.rpy:4129
translate tokipona bonus1_dc1a8fcd:

    # n "I can understand that! My big brother is awesome!"
    n ""

# game/dialogs.rpy:4130
translate tokipona bonus1_f18686d4:

    # s "But I'll never have any siblings. My parents don't want to..."
    s ""

# game/dialogs.rpy:4131
translate tokipona bonus1_7568d34b:

    # hero "It's sad...{p}But maybe it's better like this."
    hero ""

# game/dialogs.rpy:4132
translate tokipona bonus1_453d2365:

    # s "Why is that?"
    s ""

# game/dialogs.rpy:4133
translate tokipona bonus1_d027b066:

    # hero "Usually big brothers are mean to their little sisters in the first years."
    hero ""

# game/dialogs.rpy:4134
translate tokipona bonus1_3fae45d0:

    # n "Hmm... My big brother was a jerk to me when I was younger... {p}But not anymore..."
    n ""

# game/dialogs.rpy:4135
translate tokipona bonus1_6d59e0e0:

    # hero "What do you mean?"
    hero ""

# game/dialogs.rpy:4136
translate tokipona bonus1_e148f620:

    # n "Well... {w}Nevermind... {p}I guess it's normal for siblings after some time..."
    n ""

# game/dialogs.rpy:4137
translate tokipona bonus1_207b93d2:

    # hero "You must be right...{p}Anyway,..."
    hero ""

# game/dialogs.rpy:4138
translate tokipona bonus1_0ee56e3f:

    # hero "What I mean is, at least nobody can harm you, Sakura-san."
    hero ""

# game/dialogs.rpy:4139
translate tokipona bonus1_557b9c7c:

    # s ". . ."
    s ""

# game/dialogs.rpy:4140
translate tokipona bonus1_b3b9c0e8:

    # s "Hmm..."
    s ""

# game/dialogs.rpy:4142
translate tokipona bonus1_743a5cb3:

    # s "Yes, you're probably right, %(stringhero)s-san."
    s ""

# game/dialogs.rpy:4143
translate tokipona bonus1_d501d7aa:

    # "That hesitation again...{p}I don't find it very natural..."
    ""

# game/dialogs.rpy:4144
translate tokipona bonus1_b9ad9038:

    # "Is she hiding something?..."
    ""

# game/dialogs.rpy:4145
translate tokipona bonus1_7d45cddc:

    # "And Nanami's acting weird too. Maybe I brought up a sore subject..."
    ""

# game/dialogs.rpy:4146
translate tokipona bonus1_8534ca42:

    # ". . ."
    ""

# game/dialogs.rpy:4147
translate tokipona bonus1_a191a0bc:

    # "Nah, I'm just being paranoid..."
    ""

# game/dialogs.rpy:4168
translate tokipona bonus2_8d40556d:

    # "I was watching Sakura as she was looking for her yukata."
    ""

# game/dialogs.rpy:4169
translate tokipona bonus2_2eb7bc22:

    # "She noticed that I was staring and she smiled and stopped."
    ""

# game/dialogs.rpy:4170
translate tokipona bonus2_1ab585f3:

    # s "Why the smile, honey?"
    s ""

# game/dialogs.rpy:4171
translate tokipona bonus2_f27ba686:

    # hero "Well, now that I've seen you nude... I realized something..."
    hero ""

# game/dialogs.rpy:4172
translate tokipona bonus2_28c06110:

    # hero "You look so girly with that hair and thin body..."
    hero ""

# game/dialogs.rpy:4173
translate tokipona bonus2_1740af69:

    # hero "But you have the chest of a thin guy... and a penis too..."
    hero ""

# game/dialogs.rpy:4174
translate tokipona bonus2_cf68db7f:

    # hero "I have the evidence that you're born as a boy... And I was pretty sure I would only like girls."
    hero ""

# game/dialogs.rpy:4175
translate tokipona bonus2_a2c879ff:

    # hero "But in my eyes, you're way prettier than any woman or girl I've ever seen. And I can't stop loving you anyway..."
    hero ""

# game/dialogs.rpy:4176
translate tokipona bonus2_bc3f3461:

    # "She smiled. Then she sat beside me on the bed."
    ""

# game/dialogs.rpy:4177
translate tokipona bonus2_6e930048:

    # s "Do you love me because I look like a girl?"
    s ""

# game/dialogs.rpy:4178
translate tokipona bonus2_32aff81a:

    # s "You think it would be different if I was a real boy?"
    s ""

# game/dialogs.rpy:4179
translate tokipona bonus2_b543397f:

    # hero "Well..."
    hero ""

# game/dialogs.rpy:4180
translate tokipona bonus2_aa6128c8:

    # hero "To be honest, I..."
    hero ""

# game/dialogs.rpy:4181
translate tokipona bonus2_2983804d:

    # "I felt a headache."
    ""

# game/dialogs.rpy:4182
translate tokipona bonus2_f7a250f4:

    # hero "Darn, actually, I have no idea. I don't know at all..."
    hero ""

# game/dialogs.rpy:4183
translate tokipona bonus2_4c90784c:

    # hero "The first thing that attracted me to you was your feminine features and personality...{w} I didn't realize you were male at the time."
    hero ""

# game/dialogs.rpy:4184
translate tokipona bonus2_7a20d415:

    # hero "I fell in love with you at first sight...But surprisingly, your secret didn't stop me from loving you."
    hero ""

# game/dialogs.rpy:4185
translate tokipona bonus2_7fb40657:

    # hero "I'm wondering about my own sexuality... Does this mean I'm \"biologically gay\" or something?"
    hero ""

# game/dialogs.rpy:4186
translate tokipona bonus2_8b8a7483:

    # hero "Maybe it's not gay since you are a girl inside and that I love you..."
    hero ""

# game/dialogs.rpy:4187
translate tokipona bonus2_7ae5be64:

    # hero "I have so many questions about myself and I don't have any answers..."
    hero ""

# game/dialogs.rpy:4188
translate tokipona bonus2_0a5fc0a9:

    # "Sakura gently leaned her head against mine and looked into my eyes."
    ""

# game/dialogs.rpy:4189
translate tokipona bonus2_4951c5b2:

    # s "The important thing isn't what your mind wants."
    s ""

# game/dialogs.rpy:4190
translate tokipona bonus2_91e5ab91:

    # s "But what your heart wants."
    s ""

# game/dialogs.rpy:4191
translate tokipona bonus2_c269d052:

    # s "You know... sometimes I ask that about myself too."
    s ""

# game/dialogs.rpy:4192
translate tokipona bonus2_77498e04:

    # s "My father was always telling me that I should like girls. And I actually do, to be honest..."
    s ""

# game/dialogs.rpy:4193
translate tokipona bonus2_891bc3a6:

    # s "But as I tried to become more comfortable with who I was..."
    s ""

# game/dialogs.rpy:4194
translate tokipona bonus2_dbb3d044:

    # s "I started to be attracted to boys as well."
    s ""

# game/dialogs.rpy:4195
translate tokipona bonus2_97ff28cd:

    # s "I was completely undecided during this time. I was feeling the same exact way you are right now. But I finally figured out..."
    s ""

# game/dialogs.rpy:4196
translate tokipona bonus2_f9789510:

    # s "Girls? Boys? It doesn't matter."
    s ""

# game/dialogs.rpy:4197
translate tokipona bonus2_6041b77d:

    # s "The most important is: {w}Does the person you love, return as much love as you do?"
    s ""

# game/dialogs.rpy:4198
translate tokipona bonus2_7d1800b4:

    # "Looking at her cute blue eyes made my heart beat louder."
    ""

# game/dialogs.rpy:4199
translate tokipona bonus2_49959dea:

    # "Yeah, she's right. Why does gender matter...?"
    ""

# game/dialogs.rpy:4200
translate tokipona bonus2_473681a8:

    # hero "Sakura-chan... My love... My heart wants to stay with you..."
    hero ""

# game/dialogs.rpy:4201
translate tokipona bonus2_8482b65d:

    # hero "I don't care about about your body and what others may think."
    hero ""

# game/dialogs.rpy:4202
translate tokipona bonus2_e9d4d741:

    # hero "No matter what happens, my feelings for you won't change. You're the one I want to be with."
    hero ""

# game/dialogs.rpy:4203
translate tokipona bonus2_b8d074ce:

    # hero "You're right... What's gender, anyway? It's just a box to check on a paper."
    hero ""

# game/dialogs.rpy:4204
translate tokipona bonus2_9df6f08e:

    # s "Exactly... Genders are just a label..."
    s ""

# game/dialogs.rpy:4205
translate tokipona bonus2_d9b204fb:

    # hero "I love you, Sakura-chan... I love you..."
    hero ""

# game/dialogs.rpy:4206
translate tokipona bonus2_96516aa7:

    # hero "And my only hope is that you love me too..."
    hero ""

# game/dialogs.rpy:4207
translate tokipona bonus2_05a7a895:

    # "She cuddled me tighter."
    ""

# game/dialogs.rpy:4208
translate tokipona bonus2_66ae317a:

    # s "I do, %(stringhero)s-kun. I love you, more than anything..."
    s ""

# game/dialogs.rpy:4209
translate tokipona bonus2_25c4ff40:

    # s "I fell in love for you the first time we met, as well."
    s ""

# game/dialogs.rpy:4210
translate tokipona bonus2_a7a9aeaa:

    # s "You are the man of my life."
    s ""

# game/dialogs.rpy:4211
translate tokipona bonus2_20830efe:

    # hero "And you are the girl of mine, Sakura-chan. Forever."
    hero ""

# game/dialogs.rpy:4212
translate tokipona bonus2_6b6e7e46:

    # hero "And that secret of yours... Actually, it makes you pretty unique..."
    hero ""

# game/dialogs.rpy:4213
translate tokipona bonus2_27fcf534:

    # hero "And I feel so proud and happy to have a so unique girlfriend."
    hero ""

# game/dialogs.rpy:4214
translate tokipona bonus2_57845a53:

    # "I kissed her tenderly."
    ""

# game/dialogs.rpy:4215
translate tokipona bonus2_3db00aad:

    # "I leaned on her naked body as I carried on."
    ""

# game/dialogs.rpy:4216
translate tokipona bonus2_0949b3d0:

    # "I stopped a moment to look at her beautiful face."
    ""

# game/dialogs.rpy:4217
translate tokipona bonus2_ad103b2f:

    # "Some tears were falling."
    ""

# game/dialogs.rpy:4218
translate tokipona bonus2_2b9b486c:

    # hero "Why are you crying, hun?"
    hero ""

# game/dialogs.rpy:4219
translate tokipona bonus2_193b5b96:

    # s "I'm just... so happy... %(stringhero)s-kun..."
    s ""

# game/dialogs.rpy:4220
translate tokipona bonus2_a9227760:

    # s "I'm so happy that you love me, no matter what I am."
    s ""

# game/dialogs.rpy:4221
translate tokipona bonus2_09611ce1:

    # s "I love you so much, %(stringhero)s-kun... So much..."
    s ""

# game/dialogs.rpy:4222
translate tokipona bonus2_c1ae5d6f:

    # "I cuddled her lovingly. Her body was warming mine."
    ""

# game/dialogs.rpy:4223
translate tokipona bonus2_8d1c6006:

    # "Then I suddenly remembered... My parents! They must not find Sakura here and naked!"
    ""

# game/dialogs.rpy:4224
translate tokipona bonus2_0f61a346:

    # "After a moment, we stood up and I helped Sakura putting her yukata again..."
    ""

# game/dialogs.rpy:4228
translate tokipona bonus2_38df4441:

    # "In front of her house, I felt my stomach twisting. I don't want to be without her..."
    ""

# game/dialogs.rpy:4229
translate tokipona bonus2_82ba49f2:

    # hero "I don't want to leave you, I want to stay with you!"
    hero ""

# game/dialogs.rpy:4230
translate tokipona bonus2_1a74f79c:

    # s "Me too... I miss your arms already... {image=heart.png}"
    s ""

# game/dialogs.rpy:4231
translate tokipona bonus2_4a161b65:

    # s "It's okay, love, we'll meet again tomorrow at school, remember?"
    s ""

# game/dialogs.rpy:4232
translate tokipona bonus2_2b99de4b:

    # s "It will be the last week until the summer vacation."
    s ""

# game/dialogs.rpy:4233
translate tokipona bonus2_f405c4f8:

    # s "And I will be free during this period."
    s ""

# game/dialogs.rpy:4234
translate tokipona bonus2_c5fd93a1:

    # hero "So will I..."
    hero ""

# game/dialogs.rpy:4235
translate tokipona bonus2_4ff85ef0:

    # hero "And you'll be sure that I'll see you everyday during the vacations."
    hero ""

# game/dialogs.rpy:4236
translate tokipona bonus2_903c8c1d:

    # "She smiled and pecks my cheek."
    ""

# game/dialogs.rpy:4237
translate tokipona bonus2_f3fb3329:

    # s "I can't wait for it, %(stringhero)s-honey-bun! {image=heart.png}"
    s ""

# game/dialogs.rpy:4256
translate tokipona bonus3_5f502601:

    # write "{b}Headline news : A father tried to murder his daughter.{/b}"
    write ""

# game/dialogs.rpy:4257
translate tokipona bonus3_eae14b39:

    # write "Last night, in the little village of N. in the Osaka prefecture at 1am, a drunken father, 44 years old, attempted to murder the young %(stringhero)s, 19 years old.{w} The father's daughter protected him and got stabbed in the abdomen."
    write ""

# game/dialogs.rpy:4258
translate tokipona bonus3_1fc20115:

    # write "The father was arrested and the girl, 18 years old, has been taken to the hospital.{w} The reasons of the fight are unknown, but the Police suspect it was due to the effects of the alcohol and the offender's criminal record."
    write ""

# game/dialogs.rpy:4259
translate tokipona bonus3_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:4294
translate tokipona bonus4_244a1f9c:

    # "It was Sunday. Our first Sunday of summer vacation."
    ""

# game/dialogs.rpy:4295
translate tokipona bonus4_d04b10e5:

    # "We decided to hold a picnic near the rice field."
    ""

# game/dialogs.rpy:4296
translate tokipona bonus4_41f4816b:

    # "I remembered it was here where Sakura revealed her secret."
    ""

# game/dialogs.rpy:4298
translate tokipona bonus4_c69f2e23:

    # "It's also here that we kissed for the first time."
    ""

# game/dialogs.rpy:4299
translate tokipona bonus4_edd1ee56:

    # "Rika organized a little game for us and we started playing after lunch."
    ""

# game/dialogs.rpy:4304
translate tokipona bonus4_c4dbd1b7:

    # r "Okay, this is a game based on manga, anime and videogames."
    r ""

# game/dialogs.rpy:4305
translate tokipona bonus4_e067d3ab:

    # r "It's a simple quiz where the losers will have to throw dice and the punishment is decided by the who rolls the highest."
    r ""

# game/dialogs.rpy:4306
translate tokipona bonus4_3d239b9b:

    # hero "Uhhh... What kind of punishment...?"
    hero ""

# game/dialogs.rpy:4308
translate tokipona bonus4_5b7ca773:

    # r "Ohohoho. Scared to lose, city rat?"
    r ""

# game/dialogs.rpy:4310
translate tokipona bonus4_53fc8dd1:

    # hero "I'm not a city rat! I'm your boyfriend!{p}Bring on the questions, I'm not afraid!"
    hero ""

# game/dialogs.rpy:4312
translate tokipona bonus4_785461af:

    # hero "I'm not a city rat! Bring on the questions, I'm not afraid!"
    hero ""

# game/dialogs.rpy:4313
translate tokipona bonus4_0a90e627:

    # s "Please be gentle with %(stringhero)s-kun, it's his first time."
    s ""

# game/dialogs.rpy:4314
translate tokipona bonus4_5e33fd5b:

    # r "Don't worry, Sakura-chan... I'll be just fine..."
    r ""

# game/dialogs.rpy:4315
translate tokipona bonus4_57287ff9:

    # "I don't like that smirk on Rika's face..."
    ""

# game/dialogs.rpy:4316
translate tokipona bonus4_1f4b7948:

    # "Nanami noticed the smirk as well."
    ""

# game/dialogs.rpy:4318
translate tokipona bonus4_bb701bb3:

    # n "Ooooh, you will not like this, %(stringhero)s-nii!"
    n ""

# game/dialogs.rpy:4320
translate tokipona bonus4_03d1d4c1:

    # n "Ooooh, you will not like this, %(stringhero)s-senpai!"
    n ""

# game/dialogs.rpy:4321
translate tokipona bonus4_ae65064e:

    # "Rika took some cards in her bag."
    ""

# game/dialogs.rpy:4324
translate tokipona bonus4_0158abfa:

    # r "Okay guys, Here's the first question..."
    r ""

# game/dialogs.rpy:4325
translate tokipona bonus4_c9e531e1:

    # r "\"What is the name of the tsundere girl in the game {i}Real Love '95{/i}?\""
    r ""

# game/dialogs.rpy:4326
translate tokipona bonus4_9f44cf6d:

    # "Ouch, this first question is hard..."
    ""

# game/dialogs.rpy:4327
translate tokipona bonus4_db066cbc:

    # "Knowing Rika, I'm surprised about this question, since {i}Real Love '95{/i} is actually an eroge for PC."
    ""

# game/dialogs.rpy:4328
translate tokipona bonus4_0e0a402b:

    # "And I remember playing it a few months ago, when I still was in Tokyo."
    ""

# game/dialogs.rpy:4329
translate tokipona bonus4_d39d805f:

    # "So... The tsundere girl... Hmmmm...{p}Yes! I remember!"
    ""

# game/dialogs.rpy:4330
translate tokipona bonus4_e4df0c97:

    # hero "It was Mayumi!"
    hero ""

# game/dialogs.rpy:4333
translate tokipona bonus4_0a0fb39e:

    # r "Good answer, honey!"
    r ""

# game/dialogs.rpy:4335
translate tokipona bonus4_74267ea4:

    # r "Good answer, city rat!"
    r ""

# game/dialogs.rpy:4336
translate tokipona bonus4_eab1e51f:

    # r "Sakura, Nanami, roll the dice!"
    r ""

# game/dialogs.rpy:4337
translate tokipona bonus4_b1e1b336:

    # "Sakura got a 2.{w} Nanami got a 3."
    ""

# game/dialogs.rpy:4338
translate tokipona bonus4_50bfd4d4:

    # r "Sakura-chan, you lost this round, so Nanami-chan must choose a punishment for you!"
    r ""

# game/dialogs.rpy:4340
translate tokipona bonus4_86b16bed:

    # "Sakura started to feel shy."
    ""

# game/dialogs.rpy:4341
translate tokipona bonus4_4a9a8b0c:

    # "Nanami made a funny grin."
    ""

# game/dialogs.rpy:4343
translate tokipona bonus4_724f715d:

    # n "Sakura-nee, you must call us by a cute anime-style name for the rest of the day!"
    n ""

# game/dialogs.rpy:4344
translate tokipona bonus4_a42514e8:

    # n "I want to be called \"Senpai\",... Senpai! *{i}giggles{/i}*"
    n ""

# game/dialogs.rpy:4345
translate tokipona bonus4_997b66a7:

    # r "I want to be called \"Big sister\"!"
    r ""

# game/dialogs.rpy:4347
translate tokipona bonus4_f47c567a:

    # s "Okay!"
    s ""

# game/dialogs.rpy:4348
translate tokipona bonus4_4b1e12da:

    # s "And you, %(stringhero)s-kun? What should I call you?" nointeract
    s "" nointeract

# game/dialogs.rpy:4356
translate tokipona bonus4_aab8ef6a:

    # hero "Sakura-chan...{w}I want you to call me \"[hnick!t]\"!"
    hero ""

# game/dialogs.rpy:4358
translate tokipona bonus4_7d57ea39:

    # s "E... Eeeeeeeh!!!"
    s ""

# game/dialogs.rpy:4359
translate tokipona bonus4_5541ddc2:

    # "She became embarrassed, hiding her little mouth behind her fist... So cute..."
    ""

# game/dialogs.rpy:4360
translate tokipona bonus4_cebdf164:

    # "I blushed myself, waiting for her answer, knowing what she was about to say already..."
    ""

# game/dialogs.rpy:4362
translate tokipona bonus4_c5c21450:

    # s "A-Alright... I'll d-do it... [hnick!t]..."
    s ""

# game/dialogs.rpy:4364
translate tokipona bonus4_70397aff:

    # hero "Gah! The cuteness!"
    hero ""

# game/dialogs.rpy:4365
translate tokipona bonus4_b312e964:

    # "If this was an anime, my nose would be bleeding."
    ""

# game/dialogs.rpy:4367
translate tokipona bonus4_92f46d2c:

    # r "Hey, wake up, city rat! Quit daydreaming and get ready for the next question. It's Sakura's turn to ask the question!"
    r ""

# game/dialogs.rpy:4368
translate tokipona bonus4_24294182:

    # "Yikes! It means that it will be me against Rika and Nanami! I must not fail!"
    ""

# game/dialogs.rpy:4372
translate tokipona bonus4_731520a2:

    # s "O... Okay... So..."
    s ""

# game/dialogs.rpy:4373
translate tokipona bonus4_1011202f:

    # s "The question is:{p}\"How many episodes are in the anime {i}Dragon Sphere{/i}?\""
    s ""

# game/dialogs.rpy:4374
translate tokipona bonus4_37f34882:

    # "I knew this anime well. I was about to answer but Rika was faster."
    ""

# game/dialogs.rpy:4376
translate tokipona bonus4_60b1dd5c:

    # r "26! {w}And 3 OVAs!"
    r ""

# game/dialogs.rpy:4378
translate tokipona bonus4_ed2c49db:

    # s "Correct, Big sister!"
    s ""

# game/dialogs.rpy:4379
translate tokipona bonus4_c9aee644:

    # s "[hnick!t], Nanami-senpai, roll the dice."
    s ""

# game/dialogs.rpy:4380
translate tokipona bonus4_906b5890:

    # "I got a five! Yes!"
    ""

# game/dialogs.rpy:4381
translate tokipona bonus4_1581741a:

    # "Nanami rolls...{w}and got a 6! Oh come on!"
    ""

# game/dialogs.rpy:4382
translate tokipona bonus4_db50cb13:

    # "Nanami directed an evil smile at me. Now I'm pretty scared..."
    ""

# game/dialogs.rpy:4383
translate tokipona bonus4_021acfb6:

    # "She took something in Rika's bag and gave it to me."
    ""

# game/dialogs.rpy:4384
translate tokipona bonus4_d1658509:

    # "Cat-ears!"
    ""

# game/dialogs.rpy:4385
translate tokipona bonus4_3d592873:

    # n "Here, put this on your head and say 'Nyan nyan~'!"
    n ""

# game/dialogs.rpy:4386
translate tokipona bonus4_e4a0fb20:

    # hero "Heeeeey!{p}I'm pretty sure this was Rika-chan's idea, huh!"
    hero ""

# game/dialogs.rpy:4387
translate tokipona bonus4_eb7fa7f2:

    # r "Who knows?... Get to work, city rat! Or I should say 'City cat'!"
    r ""

# game/dialogs.rpy:4388
translate tokipona bonus4_8bc9d837:

    # n "Get to work, you slacker! *{i}with a fake strong male voice from some video game{/i}*"
    n ""

# game/dialogs.rpy:4389
translate tokipona bonus4_ff3a6290:

    # "I sighed and I put the ears on my head."
    ""

# game/dialogs.rpy:4390
translate tokipona bonus4_cf248989:

    # "We didn't have any mirrors around so I couldn't see how I looked. But I'm sure it was ridiculous."
    ""

# game/dialogs.rpy:4391
translate tokipona bonus4_7ae9ab60:

    # "I put my fists in front of me, kitty-like."
    ""

# game/dialogs.rpy:4392
translate tokipona bonus4_ad791bf6:

    # hero "N... Ny... Nyan~ nyan~!"
    hero ""

# game/dialogs.rpy:4398
translate tokipona bonus4_3686e5b0:

    # "Sakura made a big charming smile that instantly made me feel better."
    ""

# game/dialogs.rpy:4400
translate tokipona bonus4_af9b813d:

    # "Sakura made a big charming smile."
    ""

# game/dialogs.rpy:4401
translate tokipona bonus4_17625773:

    # s "Awwww it's so cuuuuute!!!"
    s ""

# game/dialogs.rpy:4402
translate tokipona bonus4_6328f30b:

    # "Rika smiled too, but her smile was way more sinister."
    ""

# game/dialogs.rpy:4403
translate tokipona bonus4_bda42d96:

    # "Nanami was just laughing out loud. I definitely should look ridiculous."
    ""

# game/dialogs.rpy:4404
translate tokipona bonus4_8d7688c3:

    # r "Hehe, doesn't fit you at all!"
    r ""

# game/dialogs.rpy:4405
translate tokipona bonus4_12c6774c:

    # r "Since you lost, it's your turn to ask the question!"
    r ""

# game/dialogs.rpy:4410
translate tokipona bonus4_ede3e123:

    # "Rika gave me another card and I read the question."
    ""

# game/dialogs.rpy:4411
translate tokipona bonus4_0e18d25a:

    # "I was also thinking about the pledge they could imagine."
    ""

# game/dialogs.rpy:4412
translate tokipona bonus4_9fe745b2:

    # hero "Okay so...{p}\"What is the name of the seiyuu who plays Haruki in {i}Panty, Bra und Strumpfgürtel{/i}?\""
    hero ""

# game/dialogs.rpy:4413
translate tokipona bonus4_9c8a3481:

    # "Nanami answered immediately, just before Rika could answer."
    ""

# game/dialogs.rpy:4415
translate tokipona bonus4_5cc59595:

    # n "Hidaka Megumi!"
    n ""

# game/dialogs.rpy:4416
translate tokipona bonus4_a7215825:

    # hero "You got it, Nanami-chan!"
    hero ""

# game/dialogs.rpy:4418
translate tokipona bonus4_fc07c2f9:

    # hero "Rika-chan, Sakura-chan, roll the dice!"
    hero ""

# game/dialogs.rpy:4419
translate tokipona bonus4_1a6d10bf:

    # "Sakura rolled a 3."
    ""

# game/dialogs.rpy:4421
translate tokipona bonus4_d821c96c:

    # "Rika made a naughty grin and rolled the dice..."
    ""

# game/dialogs.rpy:4423
translate tokipona bonus4_619191be:

    # "She pouted when she got a one..."
    ""

# game/dialogs.rpy:4425
translate tokipona bonus4_667bfdcf:

    # s "Hmmm... Let's see..."
    s ""

# game/dialogs.rpy:4428
translate tokipona bonus4_ddb63045:

    # "She thought for a moment but finally made a smile... One worse than Rika-chan's!"
    ""

# game/dialogs.rpy:4429
translate tokipona bonus4_b721b3d2:

    # "It's so unusual to see such a smile on Sakura's face."
    ""

# game/dialogs.rpy:4431
translate tokipona bonus4_226ccb5a:

    # s "Big sister, I want you to kiss [hnick!t] on the mouth, in front of us!"
    s ""

# game/dialogs.rpy:4433
translate tokipona bonus4_288e9d30:

    # s "Big sister, I want you to kiss [hnick!t] on the cheek!"
    s ""

# game/dialogs.rpy:4435
translate tokipona bonus4_54428ce0:

    # r "Whaaaaaaat???"
    r ""

# game/dialogs.rpy:4436
translate tokipona bonus4_3efa7eb4:

    # hero "Eeeeeeeeh!!!"
    hero ""

# game/dialogs.rpy:4439
translate tokipona bonus4_329e5a99:

    # n "Eeeeh but it's my boyfriend!!"
    n ""

# game/dialogs.rpy:4441
translate tokipona bonus4_7529a0c6:

    # r "In front of you all?!"
    r ""

# game/dialogs.rpy:4443
translate tokipona bonus4_2f9c2ac6:

    # r "I don't want to, Sakura-chan!{p}It's mean!"
    r ""

# game/dialogs.rpy:4444
translate tokipona bonus4_d7114010:

    # s "I'm sorry but it's in the rules. You told us earlier, Rika-chan!{p}And it wouldn't be a punishment if it was too easy!"
    s ""

# game/dialogs.rpy:4447
translate tokipona bonus4_eb063792:

    # n "I feel like punished too, even if I won..."
    n ""

# game/dialogs.rpy:4448
translate tokipona bonus4_a2b2dbfe:

    # "Sakura can be scary sometimes..."
    ""

# game/dialogs.rpy:4450
translate tokipona bonus4_b9f6e14e:

    # r "Hmpf!... {w}A... Alright... I'll do it!"
    r ""

# game/dialogs.rpy:4451
translate tokipona bonus4_6213e31d:

    # "My eyes wide open, I saw Rika-chan crawling slowly to me with her pissed expression."
    ""

# game/dialogs.rpy:4454
translate tokipona bonus4_7062c20b:

    # "Then we closed our eyes and we kissed, after some hesitation."
    ""

# game/dialogs.rpy:4455
translate tokipona bonus4_23c6d1be:

    # "At the moment our lips touched, I think we both forgot the girls were here because the kiss was long and lovey."
    ""

# game/dialogs.rpy:4457
translate tokipona bonus4_6e084a46:

    # "My heart was beating loudly..."
    ""

# game/dialogs.rpy:4459
translate tokipona bonus4_0defb206:

    # "Then she closed her eyes and neared her face to my cheek."
    ""

# game/dialogs.rpy:4461
translate tokipona bonus4_f50c2971:

    # "My heart was beating loudly as she draws near..."
    ""

# game/dialogs.rpy:4462
translate tokipona bonus4_fcdd662b:

    # ".........{i}*chu~*{/i}......"
    ""

# game/dialogs.rpy:4463
translate tokipona bonus4_e916cd9d:

    # s "Awww, so cute!!!"
    s ""

# game/dialogs.rpy:4465
translate tokipona bonus4_4ef9b2c5:

    # n "You better wash your cheek and kiss me at once after this!"
    n ""

# game/dialogs.rpy:4467
translate tokipona bonus4_79a8814d:

    # n "Hey, get a room, you two!"
    n ""

# game/dialogs.rpy:4469
translate tokipona bonus4_d218ebbe:

    # "Then Rika got back to where she was sitting."
    ""

# game/dialogs.rpy:4471
translate tokipona bonus4_e986db9f:

    # "She was embarrassed but her face was still looking pissed."
    ""

# game/dialogs.rpy:4473
translate tokipona bonus4_98a2e64c:

    # r "T-t-that's private stuff! You're not supposed to watch that!..."
    r ""

# game/dialogs.rpy:4474
translate tokipona bonus4_20c841e6:

    # "I silently agreed, blushing red."
    ""

# game/dialogs.rpy:4475
translate tokipona bonus4_479b77b6:

    # "But well...as Sakura said, it was a punishment..."
    ""

# game/dialogs.rpy:4477
translate tokipona bonus4_8dbc83a8:

    # r "D... Don't expect anything, you pervert, you city rat!... It's just a punishment!"
    r ""

# game/dialogs.rpy:4479
translate tokipona bonus4_e77cd0a8:

    # r "It should be Sakura's job, anyway!"
    r ""

# game/dialogs.rpy:4481
translate tokipona bonus4_b9726a19:

    # r "It should be Nanami's job, anyway!"
    r ""

# game/dialogs.rpy:4482
translate tokipona bonus4_929d41bb:

    # n "Right!"
    n ""

# game/dialogs.rpy:4483
translate tokipona bonus4_d0abd9fc:

    # hero "Hmph! Fine with me!"
    hero ""

# game/dialogs.rpy:4484
translate tokipona bonus4_9b79ecac:

    # "I felt that I'm turning red too..."
    ""

# game/dialogs.rpy:4486
translate tokipona bonus4_8fc82e11:

    # "Sakura woke me up in my confusion."
    ""

# game/dialogs.rpy:4487
translate tokipona bonus4_ad51f715:

    # s "Hey, I'm your girlfriend, don't you forget!"
    s ""

# game/dialogs.rpy:4488
translate tokipona bonus4_2aa2b9a6:

    # "I smiled and kissed Sakura fondly on the cheek."
    ""

# game/dialogs.rpy:4489
translate tokipona bonus4_a2e4f8f1:

    # n "Saku and %(stringhero)s, sitting in a tree, K. I. S. S. I. N. G.!..."
    n ""

# game/dialogs.rpy:4491
translate tokipona bonus4_4420fe81:

    # "Nanami woke me up in my confusion."
    ""

# game/dialogs.rpy:4492
translate tokipona bonus4_c197d768:

    # n "Hey, I'm your girlfriend, don't you forget!"
    n ""

# game/dialogs.rpy:4493
translate tokipona bonus4_c6260d04:

    # "I smiled and kissed Nanami fondly on the cheek."
    ""

# game/dialogs.rpy:4494
translate tokipona bonus4_a5c16330:

    # hero "By the way... It's your turn for the next question, Rika-chan..."
    hero ""

# game/dialogs.rpy:4500
translate tokipona bonus4_fc3d4898:

    # "The game was long and the punishments was fun... Sometimes evil but fun..."
    ""

# game/dialogs.rpy:4501
translate tokipona bonus4_4c6939cf:

    # "When evening came, we started to go back to our homes."
    ""

# game/dialogs.rpy:4507
translate tokipona bonus4_f83000c7:

    # "I was escorting Sakura-chan to her house as usual and we were chatting about the picnic."
    ""

# game/dialogs.rpy:4508
translate tokipona bonus4_ca69579e:

    # s "That was fun... [hnick!t]!"
    s ""

# game/dialogs.rpy:4509
translate tokipona bonus4_313e8d85:

    # hero "You know, you don't need to keep calling me [hnick!t], now. The girls aren't with us anymore."
    hero ""

# game/dialogs.rpy:4510
translate tokipona bonus4_c63b8929:

    # s "It's okay, I like taking the punishment seriously...{p}Unless you don't like me calling you that?"
    s ""

# game/dialogs.rpy:4511
translate tokipona bonus4_e5dc6483:

    # hero "No, it's okay, I enjoy it!"
    hero ""

# game/dialogs.rpy:4514
translate tokipona bonus4_90714309:

    # hero "Even if it might sound weird between lovers..."
    hero ""

# game/dialogs.rpy:4515
translate tokipona bonus4_b9b5a607:

    # "We smiled together."
    ""

# game/dialogs.rpy:4518
translate tokipona bonus4_756dbf3c:

    # s "[hnick!t], do you...{p}Do you think I would look pretty in a maid outfit?"
    s ""

# game/dialogs.rpy:4520
translate tokipona bonus4_6dc8daef:

    # "I was close to nose bleeding as I was imagining Sakura in a maid outfit."
    ""

# game/dialogs.rpy:4521
translate tokipona bonus4_6049d7bb:

    # hero "I... I'm pretty sure of it, Sakura-chan!"
    hero ""

# game/dialogs.rpy:4522
translate tokipona bonus4_e234cd17:

    # hero "In fact... No matter what you'll wear, you always will be cute to me."
    hero ""

# game/dialogs.rpy:4523
translate tokipona bonus4_138db92c:

    # "We blushed together."
    ""

# game/dialogs.rpy:4525
translate tokipona bonus4_554402fd:

    # hero "Well... I guess you would..."
    hero ""

# game/dialogs.rpy:4526
translate tokipona bonus4_03905b91:

    # hero "You know how cute you look already."
    hero ""

# game/dialogs.rpy:4527
translate tokipona bonus4_a0391de0:

    # "She blushed."
    ""

# game/dialogs.rpy:4530
translate tokipona bonus4_4722aed7:

    # s "You know... I like to call you [hnick!t]..."
    s ""

# game/dialogs.rpy:4531
translate tokipona bonus4_1ea38385:

    # s "I always wanted to have a big brother but I didn't got this chance."
    s ""

# game/dialogs.rpy:4532
translate tokipona bonus4_dcd60152:

    # s "So, calling you like this... It's almost as if... If I have one..."
    s ""

# game/dialogs.rpy:4533
translate tokipona bonus4_acc2309b:

    # "We blushed together, deeply."
    ""

# game/dialogs.rpy:4535
translate tokipona bonus4_119e7f5e:

    # "Well, I guess it would be nice for her to have a brother. As long as he doesn't turn crazy easily like Nanami's brother..."
    ""

# game/dialogs.rpy:4536
translate tokipona bonus4_1f716602:

    # hero "Well... If you still want to call me like this all the time... I won't mind at all, Sakura-chan."
    hero ""

# game/dialogs.rpy:4537
translate tokipona bonus4_a465601d:

    # hero "To be honest, I always wanted to have a little sister."
    hero ""

# game/dialogs.rpy:4538
translate tokipona bonus4_b5b85398:

    # s "But you have a big sister, don't you?"
    s ""

# game/dialogs.rpy:4539
translate tokipona bonus4_3f93cb80:

    # hero "Yeah, but it's not the same thing. Plus, she doesn't live with me anymore..."
    hero ""

# game/dialogs.rpy:4540
translate tokipona bonus4_6fc9312b:

    # "Sakura nodded..."
    ""

# game/dialogs.rpy:4544
translate tokipona bonus4_bd6e96bc:

    # s "So, how's your couple with Rika-chan?"
    s ""

# game/dialogs.rpy:4545
translate tokipona bonus4_a9623e40:

    # hero "Pretty good! I think I'm going to marry her!"
    hero ""

# game/dialogs.rpy:4547
translate tokipona bonus4_dd27c7e7:

    # s "Really?! That's great!!"
    s ""

# game/dialogs.rpy:4548
translate tokipona bonus4_699e47c9:

    # hero "You think she will say yes?"
    hero ""

# game/dialogs.rpy:4550
translate tokipona bonus4_df94e128:

    # s "I'm sure she will."
    s ""

# game/dialogs.rpy:4551
translate tokipona bonus4_c15523d1:

    # s "You know, she really loves you more than it looks."
    s ""

# game/dialogs.rpy:4552
translate tokipona bonus4_f0c8c519:

    # s "It's just her way of being herself."
    s ""

# game/dialogs.rpy:4553
translate tokipona bonus4_10b77e4e:

    # hero "Yeah, I know..."
    hero ""

# game/dialogs.rpy:4554
translate tokipona bonus4_7f081c6b:

    # "Then I stopped and I asked Sakura."
    ""

# game/dialogs.rpy:4555
translate tokipona bonus4_f25fd7bb:

    # hero "Sakura-chan..."
    hero ""

# game/dialogs.rpy:4556
translate tokipona bonus4_7347e2f5:

    # hero "I would be so honored that you become our future children's godparent!"
    hero ""

# game/dialogs.rpy:4557
translate tokipona bonus4_2662060c:

    # hero "Would you like to?"
    hero ""

# game/dialogs.rpy:4559
translate tokipona bonus4_197853e0:

    # "Sakura got surprised and didn't said a word for a while."
    ""

# game/dialogs.rpy:4560
translate tokipona bonus4_08e5cd3b:

    # "But her face made a shiny smile, finally."
    ""

# game/dialogs.rpy:4562
translate tokipona bonus4_a89ea5da:

    # s "Oh yes! Yes, I would love to!"
    s ""

# game/dialogs.rpy:4563
translate tokipona bonus4_95d8a37f:

    # hero "Yes! Thank you, Sakura-chan!"
    hero ""

# game/dialogs.rpy:4564
translate tokipona bonus4_189cb5d4:

    # s "I'm so happy you asked me that!"
    s ""

# game/dialogs.rpy:4565
translate tokipona bonus4_1170dcb4:

    # hero "You really are a close friend, Sakura-chan..."
    hero ""

# game/dialogs.rpy:4567
translate tokipona bonus4_2fff80d3:

    # hero "I know we can't technically be brothers and sisters... But then I want you to be a family friend!"
    hero ""

# game/dialogs.rpy:4569
translate tokipona bonus4_c58e083a:

    # hero "I would like so much to have you as a family friend!"
    hero ""

# game/dialogs.rpy:4570
translate tokipona bonus4_02601609:

    # "Sakura suddenly hugged me."
    ""

# game/dialogs.rpy:4571
translate tokipona bonus4_8b5ff4e0:

    # s "Thank you so much, %(stringhero)s!... I mean, [hnick!t]!"
    s ""

# game/dialogs.rpy:4572
translate tokipona bonus4_bdc5b511:

    # "I held her a moment with a smile."
    ""

# game/dialogs.rpy:4575
translate tokipona bonus4_7b0de0eb:

    # s "So, how's your couple with Nana-chan?"
    s ""

# game/dialogs.rpy:4576
translate tokipona bonus4_22478f8e:

    # hero "It's great! She's an amazing girl."
    hero ""

# game/dialogs.rpy:4577
translate tokipona bonus4_5ba932be:

    # hero "We plan to go to her grand-parents at Okinawa for the vacations, for a couple of weeks."
    hero ""

# game/dialogs.rpy:4578
translate tokipona bonus4_a54e7c2a:

    # s "That's great!"
    s ""

# game/dialogs.rpy:4579
translate tokipona bonus4_d33cd195:

    # s "What you will do, there?"
    s ""

# game/dialogs.rpy:4580
translate tokipona bonus4_15d9f409:

    # hero "I don't know yet."
    hero ""

# game/dialogs.rpy:4581
translate tokipona bonus4_b396e8be:

    # hero "Since it's Nanami-chan's birth land, she probably knows what's the best."
    hero ""

# game/dialogs.rpy:4582
translate tokipona bonus4_fb2732b4:

    # hero "So far, she said there's a lot of festivals at this period of the year."
    hero ""

# game/dialogs.rpy:4583
translate tokipona bonus4_aff5d3d2:

    # s "You're so lucky!"
    s ""

# game/dialogs.rpy:4584
translate tokipona bonus4_a3265a2a:

    # s "I wish I could go there."
    s ""

# game/dialogs.rpy:4585
translate tokipona bonus4_9597c256:

    # "I nodded, undestanding her feeling."
    ""

# game/dialogs.rpy:4586
translate tokipona bonus4_d8972d72:

    # "Then I got an idea."
    ""

# game/dialogs.rpy:4587
translate tokipona bonus4_3d45bfe0:

    # hero "Hey... Why not trying to organize a trip for all of us when we will come back?"
    hero ""

# game/dialogs.rpy:4588
translate tokipona bonus4_7e322d73:

    # hero "Like, I'll go there with Nanami in recognition, then when we will come back, we will know what are the great spots we can go."
    hero ""

# game/dialogs.rpy:4589
translate tokipona bonus4_6796bcb2:

    # hero "And while that, we'll save money all together and Rika will organize the trip for all of us!"
    hero ""

# game/dialogs.rpy:4591
translate tokipona bonus4_0d18a236:

    # s "That sounds so great!"
    s ""

# game/dialogs.rpy:4592
translate tokipona bonus4_52addb72:

    # hero "I'll call Rika tomorrow to propose it!"
    hero ""

# game/dialogs.rpy:4593
translate tokipona bonus4_8cd12022:

    # hero "I'm sure it will be alot of fun!"
    hero ""

# game/dialogs.rpy:4598
translate tokipona bonus4_e1a7c00a:

    # "We finally reached Sakura's house."
    ""

# game/dialogs.rpy:4599
translate tokipona bonus4_94a9ed42:

    # s "See you soon and take care... [hnick!t]."
    s ""

# game/dialogs.rpy:4601
translate tokipona bonus4_50eaa374:

    # "Sakura does a maid-like reverence with her little summer dress."
    ""

# game/dialogs.rpy:4602
translate tokipona bonus4_0f54fbae:

    # hero "Take care, Sakura-chan!"
    hero ""

# game/dialogs.rpy:4604
translate tokipona bonus4_68a39d42:

    # hero "Take care, little sister!"
    hero ""

# game/dialogs.rpy:4606
translate tokipona bonus4_8dcab874:

    # "She giggled and disappeared in her house."
    ""

# game/dialogs.rpy:4607
translate tokipona bonus4_6b242521:

    # "That was a fine sunday..."
    ""

# game/dialogs.rpy:4608
translate tokipona bonus4_5206326a:

    # "A good start for the vacations..."
    ""

# game/dialogs.rpy:4621
translate tokipona previewmsg_5de028fa:

    # centered "{size=+12}The story starts but the preview ends here...{/size}"
    centered ""

# game/dialogs.rpy:4622
translate tokipona previewmsg_f6d47b23:

    # centered "{size=+12}Stay tuned and keep the app in your device to get the free complete update of Kare wa Kanojo in early 2018!{/size}"
    centered ""

translate tokipona strings:

    # game/dialogs.rpy:35
    old "What should I do?"
    new ""

    # game/dialogs.rpy:35
    old "Let's play some online game."
    new ""

    # game/dialogs.rpy:35
    old "Let's rewatch some anime VHS."
    new ""

    # game/dialogs.rpy:35
    old "Let's study a bit."
    new ""

    # game/dialogs.rpy:448
    old "What should I say?"
    new ""

    # game/dialogs.rpy:448
    old "The truth: {i}High School Samurai{/i}, an ecchi shonen manga"
    new ""

    # game/dialogs.rpy:448
    old "Not completely the truth: {i}Rosario Maiden{/i}, a seinen manga about vampire dolls"
    new ""

    # game/dialogs.rpy:962
    old "What should I do?..."
    new ""

    # game/dialogs.rpy:962
    old "Tell the truth to Sakura"
    new ""

    # game/dialogs.rpy:962
    old "Keep the secret and buy it another day"
    new ""

    # game/dialogs.rpy:1685
    old "Please help me! What should I do??"
    new ""

    # game/dialogs.rpy:1685
    old "No!... It's a boy! I can't love him, I'm not gay!"
    new ""

    # game/dialogs.rpy:1685
    old "Whatever! Love doesn't have gender!"
    new ""

    # game/dialogs.rpy:4348
    old "Call me \"Master\"!"
    new ""

    # game/dialogs.rpy:4348
    old "Call me \"Big brother\"!"
    new ""

