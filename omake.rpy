# TODO: Translation updated at 2022-06-01 14:34

translate tokipona strings:

    # game/omake.rpy:95
    old "Opening song \"TAKE MY HEART\""
    new "kalama musi open \"TAKE MY HEART\""

    # game/omake.rpy:96
    old "Performed by Mew Nekohime\nLyrics by Max le Fou and Masaki Deguchi\nComposed and sequenced by Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"
    new "jan toki li jan Mew Nekohime\nsitelen toki li tan jan Max le Fou li tan jan Masaki Deguchi\nsitelen kalama tan Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} sike nanpa 2018"

    # game/omake.rpy:102
    old "{b}Japanese lyrics:{/b}\n"
    new "{b}toki musi lon toki Nijon:{/b}\n"

    # game/omake.rpy:118
    old "{b}Romaji:{/b}\n"
    new "{b}kepeken sitelen Lasin:{/b}\n"

    # game/omake.rpy:134
    old "{b}Translation:{/b}\n"
    new "{b}ante toki:{/b}\n"

    # game/omake.rpy:135
    old "When you take my hand, I feel I could fly"
    new "sina lanpan e luka mi la, mi pinin e ni: mi ken tawa kon"

    # game/omake.rpy:136
    old "When I dive into your eyes, I feel I could drown in happiness\n"
    new "mi tawa lon oko sina la, mi pilin e ni: mi ken lon telo pi pilin pona\n"

    # game/omake.rpy:137
    old "We sure look different"
    new "mi tu li lukin ante"

    # game/omake.rpy:138
    old "But despite that, my heart beats loud\n"
    new "ni la, taso kalama pi pilin mi li suli\n"

    # game/omake.rpy:139
    old "A boy and a girl"
    new "mije en meli"

    # game/omake.rpy:140
    old "I'm just a human"
    new "mi jan taso"

    # game/omake.rpy:141
    old "I hope you don't mind"
    new "mi wile e ni: ni li ike ala tawa sina"

    # game/omake.rpy:142
    old "I can't control my feelings"
    new "mi ken ala anu e pilin mi"

    # game/omake.rpy:143
    old "Come to me, take my heart"
    new "o kama tawa mi, o lanpan e pilin mi"

    # game/omake.rpy:144
    old "I will devote myself to you"
    new "mi pana e ale mi tawa sina"

    # game/omake.rpy:145
    old "No matter what, I love you"
    new "ale la, mi olin e sina"

    # game/omake.rpy:166
    old "{b}D.O.B.:{/b} 1978/09/29\n"
    new "{b}t.k.l.:{/b} sike #1978 mun #9 suno #29\n"

    # game/omake.rpy:167
    old "{b}P.O.B.:{/b} Shinjuku, Tokyo\n"
    new "{b}m.k.l.:{/b} ma Sintuku lon ma tomo Tokijo\n"

    # game/omake.rpy:168
    old "{b}Height:{/b} 5.4ft\n"
    new "{b}nanpa pi suli sewi:{/b} 5.4ft\n"

    # game/omake.rpy:169
    old "{b}Weight:{/b} 136 pounds\n"
    new "{b}nanpa wawa:{/b} 136lb\n"

    # game/omake.rpy:170
    old "{b}Measurements:{/b} 74-64-83\n"
    new "{b}nanpa suli:{/b} 74-64-83\n"

    # game/omake.rpy:171
    old "{b}Blood type:{/b} A\n"
    new "{b}kulupu pi telo insa:{/b} A\n"

    # game/omake.rpy:172
    old "{b}Favourite manga:{/b} High School Samurai\n"
    new "{b}sitelen musi pona:{/b} High School Samurai\n"

    # game/omake.rpy:173
    old "{b}Favourite videogame:{/b} Lead of Fighters ‘96\n"
    new "{b}musi pona pi ilo sona:{/b} Lead of Fighters '96\n"

    # game/omake.rpy:174
    old "{b}Favourite food:{/b} American hamburgers\n"
    new "{b}moku pona:{/b} pan soweli pi ma Mewika\n"

    # game/omake.rpy:175
    old "A young boy from Tokyo who has just moved to the village. At first, he thinks he's going to miss the urban life he knew before. But meeting Sakura will quickly change his mind...\nHe is a nice guy, determined, and sometimes a little bit crazy. He likes computers, mangas and loves to have fun with his friends. He is not afraid to face problems, especially when the sake of his friends is involved. He is quite uncertain on big decisions so he usually lets his instinct (or the player!) leading his decisions most of the time...\nHe got an older sister that is married and still lives in Tokyo."
    new "mije lili tan ma tomo Tokijo li kama sin lon ma lili ni. tenpo wan la, ona li pilin e ni: ona li pilin weka tan ma tomo suli. taso toki tawa jan Sakula li ante e pilin ona kepeken tenpo lili...\nona li jan pona, li weka ala e utala, li nasa lili. ilo sona, en sitelen musi, en tenpo pona lon poka pi jan pona ona, li pona tawa ona. ona li pilin ala monsuta tawa ike. jan pona ona li lon la, ona li utala e ike. ona li sona ala e anu suli. ni la ona li pana e anu tawa pilin ona (anu jan musi)! tenpo mute la ni li lawa e anu ona...\nona li jo e jan sama meli. jan sama ona li wan lon poka pi jan ante li awen lon ma tomo Tokijo."

    # game/omake.rpy:193
    old "{b}D.O.B.:{/b} 1979/02/28\n"
    new "{b}t.k.l.:{/b} sike #1979 mun #2 suno #28\n"

    # game/omake.rpy:194
    old "{b}P.O.B.:{/b} Kameoka, Kyoto\n"
    new "{b}m.k.l.:{/b} ma Kamejoka lon ma tomo Tokijo\n"

    # game/omake.rpy:195
    old "{b}Height:{/b} 5.1ft\n"
    new "{b}nanpa pi suli sewi:{/b} 5.1ft\n"

    # game/omake.rpy:196
    old "{b}Weight:{/b} 121 pounds\n"
    new "{b}nanpa wawa:{/b} 121lb\n"

    # game/omake.rpy:197
    old "{b}Measurements:{/b} Unknown\n"
    new "{b}nanpa suli:{/b} sona ala\n"

    # game/omake.rpy:198
    old "{b}Blood type:{/b} AB\n"
    new "{b}kulupu pi telo insa:{/b} AB\n"

    # game/omake.rpy:199
    old "{b}Favourite manga:{/b} Uchuu Tenshi Moechan\n"
    new "{b}sitelen musi pona:{/b} Uchuu Tenshi Moechan\n"

    # game/omake.rpy:200
    old "{b}Favourite videogame:{/b} Taiko no Masuta EX 4’\n"
    new "{b}musi pona pi ilo sona:{/b} Taiko no Masuta EX 4'\n"

    # game/omake.rpy:201
    old "{b}Favourite food:{/b} Beef yakitori\n"
    new "{b}moku pona:{/b} waso seli\n"

    # game/omake.rpy:202
    old "Sakura is a member of the school's manga club and she has a very deep secret that makes of her a mysterious girl...\nShe is very shy but incredibly pretty. She was the idol of the school until a strange rumor about her started to spread. She likes classical music and plays violin sometimes in the night at her window..."
    new "jan Sakula li jan kulupu pi sitelen musi lon tomo sona, li jo e ijo pi len mute. ona li meli pi sona lili...\nona li weka tan kulupu, taso pona mute lukin. ona li jan pi pona lukin pi tomo sona, taso toki nasa lon ona li tawa. kalama musi majuna li pona tawa ona. ken la ona li musi kepeken ilo kalama ona lon tenpo pimeja..."

    # game/omake.rpy:225
    old "{b}D.O.B.:{/b} 1979/08/05\n"
    new "{b}t.k.l.:{/b} sike #1978 mun #8 suno #5\n"

    # game/omake.rpy:226
    old "{b}P.O.B.:{/b} The Village, Osaka\n"
    new "{b}m.k.l.:{/b} ma tomo lili pi ma tomo Osaka\n"

    # game/omake.rpy:227
    old "{b}Height:{/b} 5ft\n"
    new "{b}nanpa pi suli sewi:{/b} 5ft\n"

    # game/omake.rpy:228
    old "{b}Weight:{/b} 110 pounds\n"
    new "{b}nanpa wawa:{/b} 110lb\n"

    # game/omake.rpy:229
    old "{b}Measurements:{/b} 92-64-87\n"
    new "{b}nanpa suli:{/b} 92-64-87\n"

    # game/omake.rpy:230
    old "{b}Blood type:{/b} O\n"
    new "{b}kulupu pi telo insa:{/b} O\n"

    # game/omake.rpy:231
    old "{b}Favourite manga:{/b} Rosario Maiden\n"
    new "{b}sitelen musi pona:{/b} Rosario Maiden\n"

    # game/omake.rpy:232
    old "{b}Favourite videogame:{/b} Super Musashi Galaxy Fight\n"
    new "{b}musi pona pi ilo sona:{/b} Super Musashi Galaxy Fight\n"

    # game/omake.rpy:233
    old "{b}Favourite food:{/b} Takoyaki\n"
    new "{b}moku pona:{/b} kala seli\n"

    # game/omake.rpy:234
    old "Rika is the founder of the manga club.\nShe got very bad experiences with boys and she sees them as perverts since then. Rika cosplays as a hobby and her best and favourite cosplay is the heroine of the Domoco-chan anime. She have strange eyes minnows that makes every boys dreamy. She speaks in the Kansai dialect like most of the people originating from the Village.\nShe secretly have a little crush on Sakura..."
    new "jan Lika li jan pali pi kulupu pi sitelen musi.\nmije li pali e ike tawa ona la, ona li pilin ike tawa mije. jan Lika li len musi. len musi pona ona li jan pona pi sitelen tawa \"Domoco-chan\". oko tu ona li jo e kule ante tawa ante. ni li nasa e lawa mije. ona li toki kepeken toki lili Kansai sama jan ante pi ma tomo lili.\nlen la ona li olin lili e jan Sakula..."

    # game/omake.rpy:257
    old "{b}D.O.B.:{/b} 1980/10/11\n"
    new "{b}t.k.l.:{/b} sike #1980 mun #10 suno #11"

    # game/omake.rpy:258
    old "{b}P.O.B.:{/b} Ginoza, Okinawa\n"
    new "{b}m.k.l.:{/b} ma Kinosa pi ma tomo Okinawa\n"

    # game/omake.rpy:259
    old "{b}Height:{/b} 4.5ft\n"
    new "{b}nanpa pi suli sewi:{/b} 4.5ft\n"

    # game/omake.rpy:260
    old "{b}Weight:{/b} 99 pounds\n"
    new "{b}nanpa wawa:{/b} 99lb\n"

    # game/omake.rpy:261
    old "{b}Measurements:{/b} 71-51-74\n"
    new "{b}nanpa suli:{/b} 71-51-74\n"

    # game/omake.rpy:262
    old "{b}Blood type:{/b} B\n"
    new "{b}kulupu pi telo insa:{/b} B\n"

    # game/omake.rpy:263
    old "{b}Favourite manga:{/b} Nanda no Ryu\n"
    new "{b}sitelen musi pona:{/b} Nanda no Ryu\n"

    # game/omake.rpy:264
    old "{b}Favourite videogame:{/b} Pika Pika Rocket\n"
    new "{b}musi pona pi ilo sona:{/b} Pika Pika Rocket\n"

    # game/omake.rpy:265
    old "{b}Favourite food:{/b} Chanpuruu\n"
    new "{b}moku pona:{/b} moku Tanpulu\n"

    # game/omake.rpy:266
    old "Nanami lives alone with her big brother Toshio after their parents disappeared.\nShe's a quiet introvert girl at first glance, but once she's with her friends, she's a cute energy bomb. She has a natural talent for videogames, which made of her the champion of the Osaka prefecture in numerous videogames."
    new "jan Nanami li awen lon poka pi jan sama suli ona Tosijo. mama tu ona li weka.\nlukin nanpa wan la ona li toki ala li weka tan kulupu, taso ona li lon poka pi jan pona ona, ona li wawa mute suwi. ona li wawa mute lon musi pi ilo sona. ni la ona li kama jan lawa pi ma tomo Osaka lon musi pi ilo sona."

    # game/omake.rpy:276
    old "Chapter 1"
    new "lipu nanpa wan"

    # game/omake.rpy:276
    old "Complete the first chapter"
    new "o pini e lipu nanpa wan"

    # game/omake.rpy:276
    old "Chapter 2"
    new "lipu nanpa tu"

    # game/omake.rpy:276
    old "Complete the second chapter"
    new "o pini e lipu nanpa tu"

    # game/omake.rpy:276
    old "Chapter 3"
    new "lipu nanpa tu wan"

    # game/omake.rpy:276
    old "Complete the third chapter"
    new "o pini e lipu nanpa tu wan"

    # game/omake.rpy:276
    old "Chapter 4"
    new "lipu nanpa tu tu"

    # game/omake.rpy:276
    old "Complete the fourth chapter"
    new "o pini pi lipu nanpa tu tu"

    # game/omake.rpy:276
    old "Chapter 5"
    new "lipu nanpa luka"

    # game/omake.rpy:276
    old "Complete the fifth chapter"
    new "o pini e lipu nanpa luka"

    # game/omake.rpy:276
    old "Finally together"
    new "pini la lon poka"

    # game/omake.rpy:276
    old "Finish the game for the first time"
    new "tenpo wan la o pini e musi"

    # game/omake.rpy:276
    old "The perfume of rice fields"
    new "kon suwi pi ma pan"

    # game/omake.rpy:276
    old "Get a kiss from Sakura"
    new "o uta e jan Sakula"

    # game/omake.rpy:276
    old "It's not that I like you, baka!"
    new "mi olin ala e sina, jan nasa o!"

    # game/omake.rpy:276
    old "Get a kiss from Rika"
    new "o uta e jan Lika"

    # game/omake.rpy:276
    old "A new kind of game"
    new "musi sin"

    # game/omake.rpy:276
    old "Get a kiss from Nanami"
    new "o uta e jan Nanami"

    # game/omake.rpy:276
    old "It's a trap!"
    new "It's a trap!"

    # game/omake.rpy:276
    old "Find the truth about Sakura"
    new "o kama sona e lon pi jan Sakula"

    # game/omake.rpy:276
    old "Good guy"
    new "jan pona"

    # game/omake.rpy:276
    old "Tell Sakura the truth about the yukata"
    new "o toki e lon pi len Jukata tawa jan Sakula"

    # game/omake.rpy:276
    old "It's all about the Pentiums, baby"
    new "It's all about the Pentiums, baby"

    # game/omake.rpy:276
    old "Choose to game at the beginning of the game."
    new "o musi lon open musi"

    # game/omake.rpy:276
    old "She got me!"
    new "ona li powe e mi!"

    # game/omake.rpy:276
    old "Help Rika with the festival"
    new "o pana e pona tawa jan Lika lon musi suli"

    # game/omake.rpy:276
    old "Grope!"
    new "pilin!"

    # game/omake.rpy:276
    old "Grope Rika (accidentally)"
    new "o pilin (pakala) e jan Lika"

    # game/omake.rpy:276
    old "A good prank"
    new "powe pona"

    # game/omake.rpy:276
    old "Prank your friends with Nanami"
    new "o powe e jan pona sina lon poka pi jan Nanami"

    # game/omake.rpy:276
    old "I'm not little!"
    new "mi lili ala!"

    # game/omake.rpy:276
    old "Help Sakura tell the truth to Nanami"
    new "o pana e pona tawa ni: jan Sakula li toki e lon tawa jan Nanami"

    # game/omake.rpy:276
    old "Big change of life"
    new "ante suli ale"

    # game/omake.rpy:276
    old "Complete Sakura's route"
    new "o pini e nasin pi jan Sakula"

    # game/omake.rpy:276
    old "City Rat"
    new "soweli lili pi ma tomo"

    # game/omake.rpy:276
    old "Complete Rika's route"
    new "o pini e nasin pi jan Lika"

    # game/omake.rpy:276
    old "That new girl"
    new "meli sin"

    # game/omake.rpy:276
    old "Complete Nanami's route"
    new "o pini e nasin pi jan Nanami"

    # game/omake.rpy:276
    old "Knee-Deep into the 34D"
    new "Knee-Deep into the 34D"

    # game/omake.rpy:276
    old "Find the secret code in the game"
    new "o alasa e len lon musi"

