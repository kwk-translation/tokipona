﻿# TODO: Translation updated at 2022-06-01 12:28

# game/script.rpy:283
translate tokipona konami_code_e5b259c9:

    # "Max le Fou" "What the heck am I doing here?..."
    "Max le Fou" ""

# game/script.rpy:291
translate tokipona update_menu_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    ""

# game/script.rpy:305
translate tokipona gjconnect_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    ""

# game/script.rpy:329
translate tokipona gjconnect_4ae60ea0:

    # "Disconnected."
    ""

# game/script.rpy:358
translate tokipona gjconnect_e254a406:

    # "Connection to Game Jolt succesful.\nWARNING: The connection doesn't count for previously saved games."
    ""

translate tokipona strings:

    # game/script.rpy:13
    old "{size=80}Thanks for playing!"
    new ""

    # game/script.rpy:193
    old "Sakura"
    new ""

    # game/script.rpy:194
    old "Rika"
    new ""

    # game/script.rpy:195
    old "Nanami"
    new ""

    # game/script.rpy:196
    old "Sakura's mom"
    new ""

    # game/script.rpy:197
    old "Sakura's dad"
    new ""

    # game/script.rpy:198
    old "Toshio"
    new ""

    # game/script.rpy:203
    old "Taichi"
    new ""

    # game/script.rpy:215
    old "Master"
    new ""

    # game/script.rpy:216
    old "Big brother"
    new ""

    # game/script.rpy:227
    old "Achievement obtained!"
    new ""

    # game/script.rpy:256
    old "Please enter your name and press Enter:"
    new ""

    # game/script.rpy:314
    old "Disconnect from Game Jolt?"
    new ""

    # game/script.rpy:314
    old "Yes, disconnect."
    new ""

    # game/script.rpy:314
    old "No, return to menu."
    new ""

    # game/script.rpy:376
    old "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping...\n\nTry again?"
    new ""

    # game/script.rpy:376
    old "Yes, try again."
    new ""

    # game/script.rpy:394
    old "It seems the authentication failed. Maybe you didn't write correctly the username and token...\n\nTry again?"
    new ""

